//
//  Constants.h
//  app
//
//  Created by BERLINK on 1/31/17.
//  Copyright © 2017 sifo. All rights reserved.
//
#define GOOGLE_CLIENT_KEY @"46285661220-o3bqur2k34qtck09ut31n0r87eui4rdc.apps.googleusercontent.com"
#define GOOGLE_MAP_API_KEY @"AIzaSyA3Tw4v5cRmOKNEUHuUGhWf3n791MJJqjg"//@"AIzaSyB917D0IVN9Kkx8ypOrz-SRi-jDUetwCU4"
#define GOOGLE_PLACE_API_KEY @"AIzaSyCbAhhphC9JIlm82zrVsIow9BI9j6cXzwo"
#define GOOGLE_PLACE_WS_API_KEY @"AIzaSyDJy1B7HoBmLsKB72ARBAt6hxBiC_TfdkY"
#define GOOGLE_MAP_DIRECTION_API_KEY @"AIzaSyDJy1B7HoBmLsKB72ARBAt6hxBiC_TfdkY"

//#define SERVER_URL @"http://vps364448.ovh.net:7171/"

#define SERVER_URL @"http://berlinkold.ajitem.com/"

#define SERVER_BASE_URL @"http://berlink.ajitem.com/"

#define SERVER_IMAGE_URL @"http://berlink.ajitem.com/public/images/"

#define GET_DATA_URL @"auth/splashscreen"
//replace

#define GET_CARS_URL @"api/cars" //api/car

#define GET_CITY_URL @"api/all/cities" //api/city

#define REGISTRATION_URL @"auth/registration" //api/users/register
#define AUTH_URL @"auth/chekmobile"   // api/users/login
#define VALIDATE_EMAIL_URL @"auth/validemail" //api/users/login
#define UPDATE_USER_URL @"api/customer/profil?access_token=" // update user
#define UPDATE_DRIVER_URL @"api/driver?access_token=" // update user
#define UPLOAD_PHOTO_URL @"api/customer/photo?access_token="// update user
#define LOGOUT_URL @"api/user/logout?access_token=" //api/users/logout

#define VALIDATE_REQUEST_URL @"api/customer/request?access_token=" // NOt required

#define CREATE_REQUEST_URL @"api/customer/request?access_token=" //api/request

#define ACCEPTE_COURSE_URL @"api/request/response-driver?access_token=" // api/request

#define MY_COURSE_URL @"api/course/mes-course?access_token=" //
#define UPDATE_COURSE_URL @"api/courses/%@?access_token=%@"

#define DRIVER_CURRENT_COURSE_URL @"api/course/driver/actuelle?access_token="
#define CUSTOMER_CURRENT_COURSE_URL @"api/course/customer/actuelle?access_token="

#define USER_POINTS_UPDATE @"api/customer/points?access_token="

#define UPDATE_NOTE_URL @"api/courses/notes?access_token="

#define BUY_POINT_URL @"api/customer/point?access_token="

#define DRIVER_DISPO_URL @"api/driver/available?access_token=%@&categoryId=%@&city=%@"

#define DELETE_REQUEST_URL @"api/customer/request?access_token=%@&idrequest=%@"

#define POST_EMAIL @"api/emails/send?access_token=%@&object=%@&message=%@"


#define APP_COLOR [UIColor colorWithRed:0.94 green:0.81 blue:0.68 alpha:1.0]
#define APP_COLOR_DARK [UIColor colorWithRed:0.72 green:0.59 blue:0.45 alpha:1.0];
#define UIColorFromRGB(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

#define kTabBarHeight 44

#define REQUEST_DENY_DELAY 5


#pragma mark- API Request Keys

#define API_KEY_NAME                   @"name"
#define API_KEY_EMAIL                  @"email"
#define API_KEY_ROLE                   @"role"
#define API_KEY_DOB                    @"date_of_birth"
#define API_KEY_ADDRESS_LINE_1         @"line_1"
#define API_KEY_ADDRESS_LINE_2         @"line_2"
#define API_KEY_CITY                   @"city"
#define API_KEY_STATE                  @"state"
#define API_KEY_COUNTRY                @"country"
#define API_KEY_POSTEL_CODE            @"postal_code"
#define API_KEY_COUNTRY_CODE           @"country_code"
#define API_KEY_PHONE_NUMBER           @"phone_number"
#define API_KEY_IMAGE                  @"image"
#define API_KEY_TYPE                   @"type"
#define API_KEY_IBN                    @"ibn"
#define API_KEY_STRIPE_TOKEN           @"stripe_token"
#define API_KEY_PASSWORD               @"password"

#define API_KEY_TOKEN                  @"token"
#define API_KEY_NEW_PASSORD            @"new_password"

#define API_KEY_CATEGORY_ID            @"category_id"
#define API_KEY_CITY_ID                @"city_id"

#define API_KEY_RANGE                  @"range"
#define API_KEY_PRICE_OF_PACK          @"price_of_pack"
#define API_KEY_STATUS                 @"status"
#define API_KEY_OUT_OF_SUPPORT_VALUE   @"out_of_support_value"
#define API_KEY_OUT_OF_SUPPORT_UNIT    @"out_of_support_unit"
#define API_KEY_OUT_OF_SUPPORT_POINTS  @"out_of_support_points"
#define API_KEY_BIT_CARS               @"bid_cars"

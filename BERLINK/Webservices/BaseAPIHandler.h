//
//  SharedVar.h
//  BERLINK
//
//  Created by BERLINK on 2/8/17.
//  Copyright © 2017 berlink. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface BaseAPIHandler : AFHTTPSessionManager {
    
    NSInteger runningOperations;
}

+ (id)sharedInstance;


- (void) callPOST:(NSString *)URLString
       parameters:(nullable id)parameters
         progress:(nullable void (^)(NSProgress *uploadProgress))uploadProgress
          success:(nullable void (^)(id _Nullable responseObject))success
          failure:(nullable void (^)(NSError *error))failure;

- (void) callGET:(NSString *)URLString
       parameters:(nullable id)parameters
         progress:(nullable void (^)(NSProgress *uploadProgress))uploadProgress
          success:(nullable void (^)(id _Nullable responseObject))success
          failure:(nullable void (^)(NSError *error))failure;

- (void) callPUT:(NSString *)URLString
       parameters:(nullable id)parameters
          success:(nullable void (^)(id _Nullable responseObject))success
          failure:(nullable void (^)(NSError *error))failure;

- (void) callDELETE:(NSString *)URLString
       parameters:(nullable id)parameters
          success:(nullable void (^)(id _Nullable responseObject))success
          failure:(nullable void (^)(NSError *error))failure;


- (NSInteger)runningOperations;
- (void)addToRunningOperations;
- (void)removeFromRunningOperations;

@end

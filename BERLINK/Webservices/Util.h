//
//  Util.h
//  app
//
//  Created by BERLINK on 1/31/17.
//  Copyright © 2017 sifo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "User.h"
#import "SCLAlertView.h"
#import "MessageAlertViewController.h"


@interface Util : NSObject
{
    SCLAlertView *shared_alert;
    UIView *gifContenerView;
    MessageAlertViewController *messageAlertView;
}
+(Util*)shareManager;
- (NSString *)appVersion;
- (NSString *)tokenFcm;
- (NSString *)deviceId;
- (NSString *)typeDevice;
-(NSString*) currentDate;
-(void)loadGifAnimationToView:(UIView*)view;
-(void)loadSplashGifAnimationToView:(UIView*)view;
-(void)saveCustomObject:(User *)object;
-(User *)loadSavedUser;
-(BOOL)isValidEmail:(NSString*)str;
-(BOOL)containSpecailCharacter:(NSString*)string;
-(BOOL)validatePhone:(NSString *)phoneNumber;
-(BOOL)containSpecailCharacterAndNumbers:(NSString*)string;

-(void)showSuccess:(NSString*)message withTitle:(NSString *)title andViewController:(UIViewController*) viewc;
-(void)showError:(NSString*)message withTitle:(NSString *)title andViewController:(UIViewController*) viewc;
-(void)showNotice:(NSString*)message withTitle:(NSString *)title andViewController:(UIViewController*) viewc;
-(void)showWarning:(NSString*)message withTitle:(NSString *)title andViewController:(UIViewController*) viewc;
-(void)showInfo:(NSString*)message withTitle:(NSString *)title andViewController:(UIViewController*) viewc;
-(void)showWaitingViewController:(UIViewController*) viewc;
-(void)dismissWating;

- (void) showAlertView:(NSString *)title message:(NSString *)message yesTitle:(NSString *)yesTitle nolTitle:(NSString *)noTitle delegate:(id)delegate;
- (void) showAlertView:(NSString *)title message:(NSString *)message okTitle:(NSString *)okTitle cancelTitle:(NSString *)cancelTitle delegate:(id)delegate;
- (void) showAlertView:(NSString *)title message:(NSString *)message okTitle:(NSString *)okTitle delegate:(id)delegate;
@end

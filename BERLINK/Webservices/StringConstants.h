//
//  StringConstants.h
//  BERLINK
//
//  Created by BERLINK on 2/17/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#define APP_NAME @"BERLINK"
#define ROLE_USER @"ROLE_USER"
#define ROLE_DRIVER @"ROLE_DRIVER"
#define PERSONAL_VIEW 0
#define PRO_VIEW 1
#define GATEGORY_DATA @"*SELON DISPONIBILITE, (_P_)€ DE PRISE EN CHARGE\nPUIS (_X_)€ / (_Y_)"
#define HORS_SERVICE @"Hors service"
#define EN_COURSE @"En course"
#define ACTIF @"Actif"
#define YOU_WIN @"VOUS AVEZ GAGNÉ !"

#define ERROR_INSCRIPTION_UNKNOWM @"Erreur d'inscription inconnue !"
#define ERROR_EMAIL_ALREADY_EXIST @"E-mail déjà utilisé !"
#define ERROR_EMAIL_USER_EXIST @"Utilisateur avec le nom et prénom déjà utilisé !"
#define ERROR_PHONE_EXIST @"Téléphone déjà utilisé !"
#define ERROR_PASSWORD_CONFIRMATION @"Votre mot de passe n'est pas conforme !"
#define ERROR_REQUIRED_INSCRIPTION_FIELDS @"Merci de saisir tous les champs !"
#define ERR0R_CONNECTION @"Erreur de connexion !"
#define ERROR_LOGIN_UNKNOWM @"Erreur d'authentification inconnue !"
#define ERROR_FACEBOOK_ACCOUNT_LINK @"Votre compte n’est pas lié a Facebook !"
#define ERROR_GOOGLE_ACCOUNT_LINK @"Votre compte n’est pas lié a Google !"

#define ERROR_LOGIN_INCORRECT @"E-mail ou mot de passe incorrect !"
#define ERR0R_LOGIN_REQUIRED_FILED @"Merci de saisir votre email et mot de passe !"
#define ERR0R_FORGET_PASSWORD_TITLE @"Mot de passe oubliée !"
#define ERR0R_FORGET_PASSWORD_SUBTITLE @"Saisir votre e-mail"
#define ERROR_SESSION_EXPIRED @"La session a expirée !"
#define ERROR_UNKNOWM @"Erreur inconnue !"
#define ERROR_DRIVER @"cette voiture est déjà utilisée"
#define ERROR_MAIL_EXIST @"E-mail déjà utilisé !"
#define ERROR_INCORRECT_CODE @"Ce code est incorrecte !"
#define ERROR_EXPIRE_CODE @"Ce code est expiré !"
#define ERROR_INVALID_TOKEN @"Token invalide !"
#define ERROR_EMAIL_INVALID @"Format E-mail invalide !"
#define ERROR_PHONE_INVALID @"Format Téléphone invalide !"
#define ERROR_PASSWORD_LENGTH @"Le mot de passe doit comprendre au moins 6 caractères !"
#define ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED @"Les caractères spéciaux ne sont pas autorisés. !"
#define ERROR_PAIEMENT_INFO @"Carte bancaire non valide !"
#define ERROR_PAIEMENT_INFO_REQUIRED @"Merci de saisir votre mode de paiement !"
#define ERROR_REQUIRED_EMAIL @"Merci de saisir votre E-MAIL !"
#define ENTER_EMAIL @"Votre E-MAIL !"
#define LOADING @"Chargement..."
#define FORGET_PASSWORD_MESSAGE @"Vous avez demandé à récupérer vos codes d'accès, vous recevrez un email contenant un code pour générer votre nouveau mot de passe !"

#define MESSAGE_PROFIL_UPDATED @"Votre profil est mis à jour !"
#define MESSAGE_PASSWORD_CHANGE @"Votre mot de passe a été modifie avec succès !"
#define MESSAGE_REQUEST_SENT @"Nous cherchons un véhicule disponible"
#define CHANGE_PROFIL_PHOTO_MESSAGE @"Voulez-vous changer votre image de profile ?"
#define DENY_REQUEST_MESSAGE @"Voulez-vous annuler votre demande ?"

#define INSUFFISANT_POINT_TITLE @"Insuffissance des points"
#define INSUFFISANT_POINT_MESSAGE @"Vous avez besoin d'acheter des points ?"
#define UPDATED_PARAM_MESSAGE @"Paramètres mis à jour !"
#define PHOTO_SAVED_MESSAGE @"Votre photo a été enregistrée dans la Galerie de photos "
#define ERROR_NAME_CARACTERS @"Le champ nom ne doit pas avoir d’autres caractères à part les lettres !"
#define NO_DRIVER_MESSAGE @"Aucun chauffeur n’est disponible dans cette ville !"
#define UNABLE_TO_GET_CITY_MESSAGE @"Nous nous pouvons pas déterminer votre localisation !"

#define POINT_PAYEMENT_SUCCESS_MESSAGE @"Payement effectuée avec succès, Vous avez %d points maintenant."
#define POINT_PAYEMENT_FAILURE_MESSAGE @"Payement non effectuée"
#define PAYEMENT_METHOD_SELECTION_MESSAGE @"Quelle méthode de payement voulez-vous utiliser pour cet achat?"


#define ERROR_OTP_LENGTH @"Please enter current otp"

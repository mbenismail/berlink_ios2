//
//  SharedVar.m
//  BERLINK
//
//  Created by BERLINK on 2/8/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "SharedVar.h"
#import "Util.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "StringConstants.h"
#import "RequestWaitingViewController.h"
#import "CourseRequestViewController.h"
#import "WCity.h"
#import "Brand.h"
#import "Car.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@implementation SharedVar

+(SharedVar*)shareManager{
    
    static SharedVar *sharedInstance=nil;
    static dispatch_once_t  oncePredecate;
    
    dispatch_once(&oncePredecate,^{
        sharedInstance=[[SharedVar alloc] init];
        
    });
    return sharedInstance;
}

-(UIStoryboard*)mystoryboard{
    UIStoryboard* storyBoard;

    storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    return storyBoard;
}
 
-(void)userLogout{
    [[self revealController] dismissViewControllerAnimated:NO completion:^{
        //[[self mainViewController].navigationController popToRootViewControllerAnimated:NO];
    }];
    [self setMainUser:nil];
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [[GIDSignIn sharedInstance] signOut];

    //UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    //[vc.navigationController popToRootViewControllerAnimated:NO];
    [[Util shareManager] saveCustomObject:nil];
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    [appd stopUpdateCourseLocation];
    [appd enterApp];
    
    
    
}

-(void)goToAccount:(UIViewController*) viewController{
    
    AccountLeftViewController* accountLeftViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"AccountLeftViewController"];
    [[SharedVar shareManager] setAccountLeftVC:accountLeftViewC];
    
    AccountCenterViewController* accountCenterViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"AccountCenterViewController"];
    [[SharedVar shareManager] setLatestViewController:accountCenterViewC];

    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:accountCenterViewC];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:accountLeftViewC];
    
    frontNavigationController.navigationBar.translucent = NO;
    rearNavigationController.navigationBar.translucent = NO;
    
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    [viewController presentViewController:revealController animated:YES completion:nil];
    [[SharedVar shareManager] setRevealController:revealController];
  // Delay execution of my block for 2 seconds.
  /*dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
   [[[SharedVar shareManager] accountLeftVC] goToCategoriesViewController:nil];
   });*/
  [[[SharedVar shareManager] accountLeftVC] goToCategoriesViewController:nil];

}

-(void)goToDriverAccount:(UIViewController*) viewController{
    AccountDriverLeftViewController* accountLeftViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"AccountDriverLeftViewController"];
    [[SharedVar shareManager] setAccountDriverLeftVC:accountLeftViewC];
    UpdateDriveInfoViewController* accountCenterViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"UpdateDriveInfoViewController"];
    [[SharedVar shareManager] setLatestViewController:accountCenterViewC];

    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:accountCenterViewC];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:accountLeftViewC];
    
    frontNavigationController.navigationBar.translucent = NO;
    rearNavigationController.navigationBar.translucent = NO;
    
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    [viewController presentViewController:revealController animated:YES completion:nil];
    [[SharedVar shareManager] setRevealController:revealController];
   [[[SharedVar shareManager] accountLeftVC] goToCategoriesViewController:nil];
}

-(SWRevealViewController*)goToAccount{
    AccountLeftViewController* accountLeftViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"AccountLeftViewController"];
    [[SharedVar shareManager] setAccountLeftVC:accountLeftViewC];

    AccountCenterViewController* accountCenterViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"AccountCenterViewController"];
    [[SharedVar shareManager] setLatestViewController:accountCenterViewC];

 UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:accountCenterViewC];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:accountLeftViewC];
    
    frontNavigationController.navigationBar.translucent = NO;
    rearNavigationController.navigationBar.translucent = NO;
    
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    [[SharedVar shareManager] setRevealController:revealController];
  [[[SharedVar shareManager] accountLeftVC] goToCategoriesViewController:nil];
  [[SharedVar shareManager] setRevealController:revealController];
  // Delay execution of my block for 2 seconds.
  /*dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
   [[[SharedVar shareManager] accountLeftVC] goToCategoriesViewController:nil];
   });*/
 // [[[SharedVar shareManager] accountLeftVC] goToCategoriesViewController:nil];
    return revealController;
}

-(SWRevealViewController*)goToDriverAccount{
    AccountDriverLeftViewController* accountLeftViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"AccountDriverLeftViewController"];
    [[SharedVar shareManager] setAccountDriverLeftVC:accountLeftViewC];

    UpdateDriveInfoViewController* accountCenterViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"UpdateDriveInfoViewController"];
    [[SharedVar shareManager] setLatestViewController:accountCenterViewC];

    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:accountCenterViewC];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:accountLeftViewC];
    
    frontNavigationController.navigationBar.translucent = NO;
    rearNavigationController.navigationBar.translucent = NO;
    
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    [[SharedVar shareManager] setRevealController:revealController];
  // Delay execution of my block for 2 seconds.
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
   [[[SharedVar shareManager] accountLeftVC] goToCategoriesViewController:nil];
   });

    return revealController;

}

-(SWRevealViewController*)goToWaitingView{
    if([_mainUser.role isEqualToString:ROLE_USER])
    {
        AccountLeftViewController*  accountLeftViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"AccountLeftViewController"];
        [[SharedVar shareManager] setAccountLeftVC:accountLeftViewC];

        WaitingViewController* accountCenterViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"WaitingViewController"];
        [[SharedVar shareManager] setLatestViewController:accountCenterViewC];
        
        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:accountCenterViewC];
        UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:accountLeftViewC];
        
        frontNavigationController.navigationBar.translucent = NO;
        rearNavigationController.navigationBar.translucent = NO;
        
        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
        [[SharedVar shareManager] setRevealController:revealController];
        return revealController;
    }
    else
    {
        AccountDriverLeftViewController*  accountLeftViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"AccountDriverLeftViewController"];
        [[SharedVar shareManager] setAccountDriverLeftVC:accountLeftViewC];

        WaitingViewController* accountCenterViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"WaitingViewController"];
        [[SharedVar shareManager] setLatestViewController:accountCenterViewC];

        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:accountCenterViewC];
        UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:accountLeftViewC];
        
        frontNavigationController.navigationBar.translucent = NO;
        rearNavigationController.navigationBar.translucent = NO;
        
        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
        [[SharedVar shareManager] setRevealController:revealController];
        return revealController;
    }
}

-(SWRevealViewController*)goToCourseView{
    NSLog(@"houni 6");

    if([_mainUser.role isEqualToString:ROLE_USER])
    {
        AccountLeftViewController*  accountLeftViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"AccountLeftViewController"];
        [[SharedVar shareManager] setAccountLeftVC:accountLeftViewC];

        CourseViewController* accountCenterViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"CourseViewController"];
        [[SharedVar shareManager] setLatestViewController:accountCenterViewC];

        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:accountCenterViewC];
        UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:accountLeftViewC];
        
        frontNavigationController.navigationBar.translucent = NO;
        rearNavigationController.navigationBar.translucent = NO;
        
        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
        [[SharedVar shareManager] setRevealController:revealController];
        return revealController;
    }
    else
    {
        AccountDriverLeftViewController*  accountLeftViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"AccountDriverLeftViewController"];
        [[SharedVar shareManager] setAccountDriverLeftVC:accountLeftViewC];

        CourseViewController* accountCenterViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"CourseViewController"];
        [[SharedVar shareManager] setLatestViewController:accountCenterViewC];

        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:accountCenterViewC];
        UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:accountLeftViewC];
        
        frontNavigationController.navigationBar.translucent = NO;
        rearNavigationController.navigationBar.translucent = NO;
        
        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
        [[SharedVar shareManager] setRevealController:revealController];
        return revealController;
    }
}

-(SWRevealViewController*)goToFinCourseView{
    
    if([_mainUser.role isEqualToString:ROLE_USER])
    {
        AccountLeftViewController*  accountLeftViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"AccountLeftViewController"];
        [[SharedVar shareManager] setAccountLeftVC:accountLeftViewC];

        FinCourseViewController* accountCenterViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"FinCourseViewController"];
        [[SharedVar shareManager] setLatestViewController:accountCenterViewC];

        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:accountCenterViewC];
        UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:accountLeftViewC];
        
        frontNavigationController.navigationBar.translucent = NO;
        rearNavigationController.navigationBar.translucent = NO;
        
        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
        [[SharedVar shareManager] setRevealController:revealController];
        return revealController;
    }
    else
    {
        AccountDriverLeftViewController*  accountLeftViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"AccountDriverLeftViewController"];
        [[SharedVar shareManager] setAccountDriverLeftVC:accountLeftViewC];

        FinCourseViewController* accountCenterViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"FinCourseViewController"];
        [[SharedVar shareManager] setLatestViewController:accountCenterViewC];

        UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:accountCenterViewC];
        UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:accountLeftViewC];
        
        frontNavigationController.navigationBar.translucent = NO;
        rearNavigationController.navigationBar.translucent = NO;
        
        SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
        [[SharedVar shareManager] setRevealController:revealController];
        return revealController;
    }
    
}

-(SWRevealViewController*)gotoCourseRequestView:(NSMutableArray*) list_request_waiting{
    AccountDriverLeftViewController*  accountLeftViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"AccountDriverLeftViewController"];
    [[SharedVar shareManager] setAccountDriverLeftVC:accountLeftViewC];

    CourseRequestViewController* accountCenterViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"CourseRequestViewController"];
    accountCenterViewC.list_request_waiting = list_request_waiting;
    [[SharedVar shareManager] setLatestViewController:accountCenterViewC];
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:accountCenterViewC];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:accountLeftViewC];
    
    frontNavigationController.navigationBar.translucent = NO;
    rearNavigationController.navigationBar.translucent = NO;
    
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    [[SharedVar shareManager] setRevealController:revealController];
    return revealController;
    
}

-(SWRevealViewController*)gotoRequestWaitingView:(Request*) request{
    AccountLeftViewController*  accountLeftViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"AccountLeftViewController"];
    [[SharedVar shareManager] setAccountLeftVC:accountLeftViewC];

    RequestWaitingViewController* accountCenterViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"RequestWaitingViewController"];
    accountCenterViewC.request = request;
    [[SharedVar shareManager] setLatestViewController:accountCenterViewC];
    
    UINavigationController *frontNavigationController = [[UINavigationController alloc] initWithRootViewController:accountCenterViewC];
    UINavigationController *rearNavigationController = [[UINavigationController alloc] initWithRootViewController:accountLeftViewC];
    
    frontNavigationController.navigationBar.translucent = NO;
    rearNavigationController.navigationBar.translucent = NO;
    
    SWRevealViewController *revealController = [[SWRevealViewController alloc] initWithRearViewController:rearNavigationController frontViewController:frontNavigationController];
    [[SharedVar shareManager] setRevealController:revealController];
    return revealController;
    
}

-(NSString*) getCatNameByID:(NSNumber*) id_cat {
    
    for (int i=0; i < _mainCategories.count; i++)
    {
        Categorie* cat = [_mainCategories objectAtIndex:i];
        if(cat.idcategory.intValue == id_cat.intValue)
            return cat.range;
    }
    return @"";
}
-(NSNumber*) getCatNoteByID:(NSNumber*) id_cat;
{
    for (int i=0; i < _mainCategories.count; i++)
    {
        Categorie* cat = [_mainCategories objectAtIndex:i];
        if(cat.idcategory.intValue == id_cat.intValue)
            return cat.note;
    }
    return nil;
}

-(NSString*) getCatNameByIdBrand:(NSNumber*) id_brand {
    
    for (int i=0; i < _mainCategories.count; i++)
    {
        Categorie* cat = [_mainCategories objectAtIndex:i];

        for (int i=0; i < cat.brands.count; i++)
        {
            Brand* b = [cat.brands objectAtIndex:i];
            if(b.idbrand.intValue == id_brand.intValue)
                return cat.range;
        }
    }
    return @"";
}

-(NSNumber*) getCatIdByIdBrand:(NSNumber*) id_brand {
    
    for (int i=0; i < _mainCategories.count; i++)
    {
        Categorie* cat = [_mainCategories objectAtIndex:i];
        
        for (int i=0; i < cat.brands.count; i++)
        {
            Brand* b = [cat.brands objectAtIndex:i];
            if(b.idbrand.intValue == id_brand.intValue)
                return cat.idcategory;
        }
    }
    return nil;
}

-(NSString*) getCityNameByID:(NSNumber*) id_city{
    for (int i=0; i < _mainCities.count; i++)
    {
        WCity* city = [_mainCities objectAtIndex:i];
        if(city.idcity.intValue == id_city.intValue)
            return city.range;
    }
    return @"";
}


-(Categorie*) getCategoryByID:(NSNumber*) id_cat{
    for (int i=0; i < _mainCategories.count; i++)
    {
        Categorie* cat = [_mainCategories objectAtIndex:i];
        
        if(cat.idcategory.intValue == id_cat.intValue)
            return cat;
    }
    return nil;
}

-(NSString*) getCarModelByID:(NSNumber*) id_car {
    
    for (int i=0; i < _mainCars.count; i++)
    {
        Car* car = [_mainCars objectAtIndex:i];
        if(car.idcar.intValue == id_car.intValue)
            return car.model;
    }
    return @"";
}
@end

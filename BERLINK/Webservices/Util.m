//
//  Util.m
//  app
//
//  Created by BERLINK on 1/31/17.
//  Copyright © 2017 sifo. All rights reserved.
//

#import "Util.h"
#import "AppDelegate.h"
#import "UIImage+animatedGIF.h"
#import "SharedVar.h"
#import "StringConstants.h"
#import "Constants.h"

@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;

@interface NSString (emailValidation)
- (BOOL)isValidEmail;
@end

@implementation NSString (emailValidation)
-(BOOL)isValidEmail
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}
@end

@implementation Util

+(Util*)shareManager{
    
    static Util *sharedInstance=nil;
    static dispatch_once_t  oncePredecate;
    
    dispatch_once(&oncePredecate,^{
        sharedInstance=[[Util alloc] init];
        
    });
    return sharedInstance;
}

- (NSString *)appVersion {
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *appDisplayName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    return [NSString stringWithFormat:@"%@, Version %@ (%@)",
            appDisplayName, majorVersion, minorVersion];
}

- (NSString *)tokenFcm {
    return [[FIRInstanceID instanceID] token];
}

- (NSString *)deviceId {
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
}

- (NSString *)typeDevice {
    return @"iphone";
    
}

-(BOOL)isValidEmail:(NSString*)str{
    return [str isValidEmail];
}

-(BOOL)containSpecailCharacter:(NSString*)string {
    
    NSString *specialCharacterString = @"!~`@#$%^&*-+();:={}[],.<>?\\/\"\'";
    NSCharacterSet *specialCharacterSet = [NSCharacterSet
                                           characterSetWithCharactersInString:specialCharacterString];
    
    if (string.length > 0 && [string.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length) {
        NSLog(@"contains special characters");
        return YES;
    }
    return NO;
}

-(BOOL)containSpecailCharacterAndNumbers:(NSString*)string {
    
    NSString *specialCharacterString = @"1234567890!~`@#$%^&*-+();:={}[],.<>?\\/\"\'";
    NSCharacterSet *specialCharacterSet = [NSCharacterSet
                                           characterSetWithCharactersInString:specialCharacterString];
    
    if (string.length > 0 && [string.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length) {
        NSLog(@"contains special characters");
        return YES;
    }
    return NO;
}




- (BOOL)validatePhone:(NSString *)phoneNumber
{
    if (nil == phoneNumber || ([phoneNumber length] < 2 ) )
        return NO;
    
    NSError *error;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];
    NSArray *matches = [detector matchesInString:phoneNumber options:0 range:NSMakeRange(0, [phoneNumber length])];
    for (NSTextCheckingResult *match in matches) {
        if ([match resultType] == NSTextCheckingTypePhoneNumber) {
            NSString *phoneNumber = [match phoneNumber];
            if ([phoneNumber isEqualToString:phoneNumber]) {
                return YES;
            }
        }
    }
    
    return NO;
}

-(NSString*) currentDate{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    return [dateFormatter stringFromDate:[NSDate date]];
}

-(void)loadGifAnimationToView:(UIView*)view{
    
    gifContenerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    UIImageView *dataImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(skipTheGifAnimation)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"Glisser pour Continuer" forState:UIControlStateNormal];
    button.frame = CGRectMake(20,view.frame.size.height - 80, view.frame.size.width - 40, 40.0);
    UIFont *font = [UIFont fontWithName:@"PlayfairDisplay-Regular" size:17];
    button.titleLabel.font = font;
    UIColor *color = UIColorFromRGB(220, 195, 170, 1);
    
    [button setTitleColor:color forState:UIControlStateNormal];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(20,view.frame.size.height - 90, view.frame.size.width - 40, 10.0)];
    imageView.image = [UIImage imageNamed:@"icon_up_arrow.png"];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    [gifContenerView addSubview:dataImageView];
    [gifContenerView addSubview:imageView];
    [gifContenerView addSubview:button];
    [view addSubview:gifContenerView];
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"berlink" withExtension:@"gif"];
    dataImageView.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    // Delay execution of my block for 10 seconds.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 8.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [gifContenerView removeFromSuperview];
    });
}

-(void)loadSplashGifAnimationToView:(UIView*)view{
    UIImageView *backImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    [backImage setImage:[UIImage imageNamed:@"bg.png"]];
    UIImageView *dataImageView = [[UIImageView alloc] initWithFrame:CGRectMake(view.frame.size.width/2 - 77, view.frame.size.height/2 - 77,154,154)];
    [view addSubview:backImage];
    [view addSubview:dataImageView];
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"logo" withExtension:@"gif"];
    dataImageView.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    // Delay execution of my block for 10 seconds.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [backImage removeFromSuperview];
        [dataImageView removeFromSuperview];
        [self loadGifAnimationToView:view];
    });
}


-(void)skipTheGifAnimation
{
     [gifContenerView removeFromSuperview];
}

-(void)saveCustomObject:(User *)object
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    [prefs setObject:object.address forKey:@"address"];
    [prefs setObject:object.workCity forKey:@"workCity"];
    [prefs setObject:object.password_app forKey:@"password_app"];
    [prefs setObject:object.birth forKey:@"birth"];
    [prefs setObject:object.city forKey:@"city"];
    [prefs setObject:object.codepostal forKey:@"codepostal"];
    [prefs setObject:object.company forKey:@"company"];
    [prefs setObject:object.created forKey:@"created"];
    [prefs setObject:object.email forKey:@"email"];
    [prefs setObject:object.firstname forKey:@"firstname"];
    [prefs setObject:object.idcustomer forKey:@"idcustomer"];
    [prefs setObject:object.isarchived forKey:@"isarchived"];
    [prefs setObject:[NSNumber numberWithBool:object.ispersonnel] forKey:@"ispersonnel"];
    [prefs setObject:object.lastname forKey:@"lastname"];
    [prefs setObject:object.name forKey:@"name"];
    [prefs setObject:object.phone forKey:@"phone"];
    [prefs setObject:object.photo forKey:@"photo"];
    [prefs setObject:object.point forKey:@"point"];
    [prefs setObject:object.role forKey:@"role"];
    [prefs setObject:object.siret forKey:@"siret"];
    [prefs setObject:object.token forKey:@"token"];
    [prefs setObject:object.tva forKey:@"tva"];
    [prefs setObject:object.updated forKey:@"updated"];
    
    [prefs setObject:object.statut forKey:@"statut"];
    [prefs setObject:object.lat forKey:@"lat"];
    [prefs setObject:object.lng forKey:@"lng"];
    [prefs setObject:object.ibn forKey:@"ibn"];
    [prefs setObject:object.idcategorie forKey:@"idcategorie"];
    [prefs setObject:object.immatriculation forKey:@"immatriculation"];
    [prefs setObject:object.idUser forKey:@"idUser"];
    [prefs setObject:object.stripeToken forKey:@"stripeToken"];

    //[prefs setObject:object.idfacebook forKey:@"idfacebook"];
    [prefs setObject:object.countrycode forKey:@"countrycode"];
    [prefs setObject:object.creditCard forKey:@"creditCard"];
    [prefs setObject:object.socialId forKey:@"socialId"];
    [prefs setObject:object.hashid forKey:@"hashid"];

    [prefs synchronize];
    [[SharedVar shareManager] setMainUser:object];
}

-(User *)loadSavedUser
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* name = [prefs objectForKey:@"name" ];
    User* user;
    if(name && name.length > 0)
    {
        user = [User new];
        user.address = [prefs objectForKey:@"address" ];
        user.workCity = [prefs objectForKey:@"workCity" ];
        user.password_app = [prefs objectForKey:@"password_app" ];
        user.birth = [prefs objectForKey:@"birth" ];
        user.city = [prefs objectForKey:@"city" ];
        user.codepostal = [prefs objectForKey:@"codepostal" ];
        user.company = [prefs objectForKey:@"company" ];
        user.created = [prefs objectForKey:@"created" ];
        user.email = [prefs objectForKey:@"email" ];
        user.firstname = [prefs objectForKey:@"firstname" ];
        user.idcustomer = [prefs objectForKey:@"idcustomer" ];
        user.isarchived = [prefs objectForKey:@"isarchived" ];
        user.ispersonnel = [[prefs objectForKey:@"ispersonnel" ] boolValue];
        user.lastname = [prefs objectForKey:@"lastname" ];
        user.name = [prefs objectForKey:@"name" ];
        user.phone = [prefs objectForKey:@"phone" ];
        user.photo = [prefs objectForKey:@"photo" ];
        user.point = [prefs objectForKey:@"point" ];
        user.role = [prefs objectForKey:@"role" ];
        user.siret = [prefs objectForKey:@"siret" ];
        user.token = [prefs objectForKey:@"token" ];
        user.tva = [prefs objectForKey:@"tva" ];
        user.updated = [prefs objectForKey:@"updated" ];
        
        user.statut = [prefs objectForKey:@"statut" ];
        user.lat = [prefs objectForKey:@"lat" ];
        user.lng = [prefs objectForKey:@"lng" ];
        user.ibn = [prefs objectForKey:@"ibn" ];
        user.idcategorie = [prefs objectForKey:@"idcategorie"];
        user.immatriculation = [prefs objectForKey:@"immatriculation" ];
        user.idUser = [prefs objectForKey:@"idUser" ];
        user.stripeToken = [prefs objectForKey:@"stripeToken" ];

        //user.idfacebook = [prefs objectForKey:@"idfacebook" ];
        user.countrycode = [prefs objectForKey:@"countrycode" ];
        user.creditCard = [prefs objectForKey:@"creditCard" ];
        user.socialId = [prefs objectForKey:@"socialId" ];
        user.hashid   = [prefs objectForKey:@"hashid"];
    }
    [[SharedVar shareManager] setMainUser:user];
    return user;
}



//messages alerts
-(void)showSuccess:(NSString*)message withTitle:(NSString *)title andViewController:(UIViewController*) viewc
{
    [self showAlertView:title message:message];
}
-(void)showError:(NSString*)message withTitle:(NSString *)title andViewController:(UIViewController*) viewc
{
    [self showAlertView:title message:message];
}
-(void)showNotice:(NSString*)message withTitle:(NSString *)title andViewController:(UIViewController*) viewc
{
    [self showAlertView:title message:message];
}
-(void)showWarning:(NSString*)message withTitle:(NSString *)title andViewController:(UIViewController*) viewc
{
    [self showAlertView:title message:message];
}
-(void)showInfo:(NSString*)message withTitle:(NSString *)title andViewController:(UIViewController*) viewc
{
    [self showAlertView:title message:message];
}

- (void)showAlertView:(NSString *)title message:(NSString *)message
{
    [messageAlertView hidePopup];
     messageAlertView = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"MessageAlertViewController"];
    [messageAlertView showWithTitle:title message:message];
}

- (void) showAlertView:(NSString *)title message:(NSString *)message okTitle:(NSString *)okTitle delegate:(id)delegate
{
    [messageAlertView hidePopup];
    messageAlertView = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"MessageAlertViewController"];
    [messageAlertView setDelegate:delegate];
    [messageAlertView.btnOk setTitle:okTitle forState:UIControlStateNormal];
    [messageAlertView showWithTitle:title message:message];
}

- (void) showAlertView:(NSString *)title message:(NSString *)message okTitle:(NSString *)okTitle cancelTitle:(NSString *)cancelTitle delegate:(id)delegate
{
    [messageAlertView hidePopup];
    messageAlertView = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"MessageAlertViewController"];
    [messageAlertView setDelegate:delegate];
    [messageAlertView showWithTitle:title message:message okButtonTitle:okTitle cancelTitle:cancelTitle];
}

- (void) showAlertView:(NSString *)title message:(NSString *)message yesTitle:(NSString *)yesTitle nolTitle:(NSString *)noTitle delegate:(id)delegate
{
    [messageAlertView hidePopup];
    messageAlertView = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"MessageAlertViewController"];
    [messageAlertView setDelegate:delegate];
    [messageAlertView showWithTitle:title message:message yesButtonTitle:yesTitle noButtonTitle:noTitle];
}

-(void)showWaitingViewController:(UIViewController*) viewc
{
    shared_alert = [[SCLAlertView alloc] init];
    shared_alert.iconTintColor = APP_COLOR_DARK;
    shared_alert.customViewColor = APP_COLOR_DARK;
    [shared_alert showWaiting:viewc title:LOADING subTitle:@"" closeButtonTitle:nil duration:0.0f];
}

-(void)dismissWating {
    if(shared_alert)
    {
        [shared_alert hideView];
        shared_alert = nil;
    }
}


@end

//
//  WebServiceApi.m
//  app
//
//  Created by BERLINK on 1/31/17.
//  Copyright © 2017 sifo. All rights reserved.
//

#import "WebServiceApi.h"
#import "Constants.h"
#import "Util.h"
#import "StringConstants.h"
#import "SharedVar.h"
#import "BaseAPIHandler.h"

@implementation WebServiceApi

+(WebServiceApi*)shareManager{
    
    static WebServiceApi *sharedInstance=nil;
    static dispatch_once_t  oncePredecate;
    
    dispatch_once(&oncePredecate,^{
        sharedInstance=[[WebServiceApi alloc] init];
        
    });
    return sharedInstance;
}

-(void)invokeGetDataWebService:(int)idws completionHandler:(void (^) (NSDictionary* json, NSError *error,NSMutableArray* points, NSMutableArray* conciergeries, NSMutableArray* categories))handler{
    
    NSLog(@"\n APi name %@",GET_DATA_URL);
    
    [[BaseAPIHandler sharedInstance] callPOST:GET_DATA_URL parameters:nil progress:nil success:^( id  _Nullable responseObject) {
        
        NSDictionary *json = responseObject;
        NSMutableArray* points = [NSMutableArray new];
        NSMutableArray* conciergeries = [NSMutableArray new];
        NSMutableArray* categories = [NSMutableArray new];
        
        if ([[json valueForKey:@"points"] isKindOfClass:[NSArray class]]) {
            NSArray* j_points = [json valueForKey:@"points"];
            for(NSDictionary* j_point in j_points)
            {
                WPoint *p = [WPoint new];
                if ([[j_point valueForKey:@"idpoints"] isKindOfClass:[NSNumber class]]) {
                    p.idpoints = [j_point valueForKey:@"idpoints"];
                }
                if ([[j_point valueForKey:@"title"] isKindOfClass:[NSString class]] && ![[j_point valueForKey:@"title"] isEqualToString:@"<null>"]) {
                    p.title = [j_point valueForKey:@"title"];
                }
                if ([[j_point valueForKey:@"number"] isKindOfClass:[NSNumber class]]) {
                    p.number = [j_point valueForKey:@"number"];
                }
                if ([[j_point valueForKey:@"price"] isKindOfClass:[NSString class]] && ![[j_point valueForKey:@"price"] isEqualToString:@"<null>"]) {
                    p.price = [j_point valueForKey:@"price"];
                }
                [points addObject:p];
            }
        }
        
        if ([[json valueForKey:@"conciergeries"] isKindOfClass:[NSArray class]]) {
            NSArray* j_conciergeries = [json valueForKey:@"conciergeries"];
            for(NSDictionary* j_conciergerie in j_conciergeries)
            {
                Conciergerie *c = [Conciergerie new];
                if ([[j_conciergerie valueForKey:@"idconcierge"] isKindOfClass:[NSNumber class]]) {
                    c.idconcierge = [j_conciergerie valueForKey:@"idconcierge"];
                }
                if ([[j_conciergerie valueForKey:@"title"] isKindOfClass:[NSString class]] && ![[j_conciergerie valueForKey:@"title"] isEqualToString:@"<null>"]) {
                    c.title = [j_conciergerie valueForKey:@"title"];
                }
                if ([[j_conciergerie valueForKey:@"image"] isKindOfClass:[NSString class]] && ![[j_conciergerie valueForKey:@"image"] isEqualToString:@"<null>"]) {
                    c.image = [j_conciergerie valueForKey:@"image"];
                }
                if ([[j_conciergerie valueForKey:@"products"] isKindOfClass:[NSArray class]]) {
                    c.products = [NSMutableArray new];
                    NSArray* j_products = [j_conciergerie valueForKey:@"products"];
                    for(NSDictionary* j_product in j_products)
                    {
                        Product *p = [Product new];
                        if ([[j_product valueForKey:@"idconsproduit"] isKindOfClass:[NSNumber class]]) {
                            p.idconsproduit = [j_product valueForKey:@"idconsproduit"];
                        }
                        if ([[j_product valueForKey:@"title"] isKindOfClass:[NSString class]] && ![[j_product valueForKey:@"title"] isEqualToString:@"<null>"]) {
                            p.title = [j_product valueForKey:@"title"];
                        }
                        if ([[j_product valueForKey:@"description"] isKindOfClass:[NSString class]] && ![[j_product valueForKey:@"description"] isEqualToString:@"<null>"]) {
                            p.wdescription = [j_product valueForKey:@"description"];
                        }
                        if ([[j_product valueForKey:@"point"] isKindOfClass:[NSNumber class]]) {
                            p.point = [j_product valueForKey:@"point"];
                        }
                        [c.products addObject:p];
                    }
                }
                [conciergeries addObject:c];
            }
        }
        
        if ([[json valueForKey:@"categories"] isKindOfClass:[NSArray class]]) {
            NSArray* j_categories = [json valueForKey:@"categories"];
            for(NSDictionary* j_category in j_categories)
            {
                Categorie *c = [Categorie new];
                if ([[j_category valueForKey:@"idcategory"] isKindOfClass:[NSNumber class]]) {
                    c.idcategory = [j_category valueForKey:@"idcategory"];
                }
                if ([[j_category valueForKey:@"range"] isKindOfClass:[NSString class]] && ![[j_category valueForKey:@"range"] isEqualToString:@"<null>"]) {
                    c.range = [j_category valueForKey:@"range"];
                }
                if ([[j_category valueForKey:@"pricepack"] isKindOfClass:[NSString class]] && ![[j_category valueForKey:@"pricepack"] isEqualToString:@"<null>"]) {
                    c.pricepack = [j_category valueForKey:@"pricepack"];
                }
                if ([[j_category valueForKey:@"pricesup"] isKindOfClass:[NSString class]] && ![[j_category valueForKey:@"pricesup"] isEqualToString:@"<null>"]) {
                    c.pricesup = [j_category valueForKey:@"pricesup"];
                }
                if ([[j_category valueForKey:@"maxbid"] isKindOfClass:[NSNumber class]]) {
                    c.maxbid = [j_category valueForKey:@"maxbid"];
                }
                if ([[j_category valueForKey:@"unitinclu"] isKindOfClass:[NSNumber class]]) {
                    c.unitinclu = [j_category valueForKey:@"unitinclu"];
                }
                if ([[j_category valueForKey:@"measure"] isKindOfClass:[NSString class]] && ![[j_category valueForKey:@"measure"] isEqualToString:@"<null>"]) {
                    c.measure = [j_category valueForKey:@"measure"];
                }
                if ([[j_category valueForKey:@"titrevoitures"] isKindOfClass:[NSString class]] && ![[j_category valueForKey:@"titrevoitures"] isEqualToString:@"<null>"]) {
                    c.titrevoitures = [j_category valueForKey:@"titrevoitures"];
                }
                if ([[j_category valueForKey:@"note"] isKindOfClass:[NSNumber class]]) {
                    c.note = [j_category valueForKey:@"note"];
                }
                if ([[j_category valueForKey:@"photo"] isKindOfClass:[NSString class]] && ![[j_category valueForKey:@"photo"] isEqualToString:@"<null>"]) {
                    c.photo = [j_category valueForKey:@"photo"];
                }
                if ([[j_category valueForKey:@"brands"] isKindOfClass:[NSArray class]]) {
                    c.brands = [NSMutableArray new];
                    NSArray* j_brands = [j_category valueForKey:@"brands"];
                    for(NSDictionary* j_brand in j_brands)
                    {
                        Brand *p = [Brand new];
                        if ([[j_brand valueForKey:@"idbrand"] isKindOfClass:[NSNumber class]]) {
                            p.idbrand = [j_brand valueForKey:@"idbrand"];
                        }
                        if ([[j_brand valueForKey:@"title"] isKindOfClass:[NSString class]] && ![[j_brand valueForKey:@"title"] isEqualToString:@"<null>"]) {
                            p.title = [j_brand valueForKey:@"title"];
                        }
                        if ([[j_brand valueForKey:@"statu"] isKindOfClass:[NSNumber class]]) {
                            p.statu = [j_brand valueForKey:@"statu"];
                        }
                        if ([[j_brand valueForKey:@"photo"] isKindOfClass:[NSString class]] && ![[j_brand valueForKey:@"photo"] isEqualToString:@"<null>"]) {
                            p.photo = [j_brand valueForKey:@"photo"];
                        }
                        [c.brands addObject:p];
                    }
                }
                [categories addObject:c];
            }
        }
        
        handler(json, nil, points, conciergeries, categories);
        
    } failure:^(NSError * _Nonnull error) {
        
        handler(nil, error, nil, nil, nil);
    }];
}

-(void)invokeGetListCarsWebService:(int)idws completionHandler:(void (^) (NSDictionary* json, NSError *error,NSMutableArray* cars))handler{
    
    
    [[BaseAPIHandler sharedInstance] callPOST:GET_CARS_URL parameters:nil progress:nil success:^( id  _Nullable responseObject) {
        
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:GET_CARS_URL andWithJson:NO andData:nil];
    [request setHTTPMethod:@"GET"];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(nil, error, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if(error!=nil)
        {
            NSLog(@"error = %@",error);
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(nil, error, nil);
            });
        }
        else {
            
            NSMutableArray* cars = [NSMutableArray new];
            
            
            if ([json isKindOfClass:[NSArray class]]) {
                for(NSDictionary* j_car in json)
                {
                    Car *c = [Car new];
                    [c parseFromJson:j_car];
                    Brand *p = [Brand new];
                    NSDictionary* j_brand = [j_car objectForKey:@"idbrand"];
                    if ([[j_brand valueForKey:@"idbrand"] isKindOfClass:[NSNumber class]]) {
                        p.idbrand = [j_brand valueForKey:@"idbrand"];
                    }
                    if ([[j_brand valueForKey:@"title"] isKindOfClass:[NSString class]] && ![[j_brand valueForKey:@"title"] isEqualToString:@"<null>"]) {
                        p.title = [j_brand valueForKey:@"title"];
                    }
                    if ([[j_brand valueForKey:@"statu"] isKindOfClass:[NSNumber class]]) {
                        p.statu = [j_brand valueForKey:@"statu"];
                    }
                    if ([[j_brand valueForKey:@"photo"] isKindOfClass:[NSString class]] && ![[j_brand valueForKey:@"photo"] isEqualToString:@"<null>"]) {
                        p.photo = [j_brand valueForKey:@"photo"];
                    }
                    c.brand = p;
                    [cars addObject:c];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"json = %@",json);
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(json, error, cars);
                });
            });
        }
        
    }];
    [postDataTask resume];
}

-(void)invokeGetListCityWebService:(int)idws completionHandler:(void (^) (NSDictionary* json, NSError *error,NSMutableArray* cities))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:GET_CITY_URL andWithJson:NO andData:nil];
    [request setHTTPMethod:@"GET"];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(nil, error, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if(error!=nil)
        {
            NSLog(@"error = %@",error);
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(nil, error, nil);
            });
        }
        else {
            
            NSMutableArray* cities = [NSMutableArray new];
            
            
            if ([json isKindOfClass:[NSArray class]]) {
                for(NSDictionary* j_city in json)
                {
                    WCity *c = [WCity new];
                    [c parseFromJson:j_city];
                    [cities addObject:c];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"json city = %@",json);
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(json, error, cities);
                });
            });
        }
        
    }];
    [postDataTask resume];
}

-(NSMutableURLRequest*)initialiseWebServiceWithUrl:(NSString*)urlStr andWithJson:(BOOL) withJson andData:(NSData*) requestData{
    
    NSLog(@"\n APi name %@",urlStr);
   // urlStr = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SERVER_URL, urlStr ]];
    NSLog(@"😀😀%@",url.absoluteString);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    if(!withJson)
    {
        [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    }
    else{
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    }
    [request setHTTPMethod:@"POST"];
    [request setValue:[[Util shareManager] appVersion] forHTTPHeaderField:@"appVersion"];
    [request setValue:[[Util shareManager] tokenFcm] forHTTPHeaderField:@"tokenFcm"];
    [request setValue:[[Util shareManager] deviceId] forHTTPHeaderField:@"deviceId"];
    [request setValue:[[Util shareManager] typeDevice] forHTTPHeaderField:@"typeDevice"];
    
    return request;
    
}

#pragma mark-  Login flow

-(void)invokePersonalRegistrationWebServiceWithParams_name:(NSString*)name _birth:(NSString*)birth _codepostal:(NSString*)codepostal _email:(NSString*)email _password:(NSString*)password _countrycode:(NSNumber*)countrycode _phone:(NSString*)phone _code_parrainage:(NSString*)code_parrainage _idfacebook:(NSNumber*)idfacebook _stripeToken:(NSString*)stripeToken _cardNumber:(NSString*)cardNumber completionHandler:(void (^) (NSString *errorMsg, User* user))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:REGISTRATION_URL andWithJson:NO andData:nil];
    
    NSString *postString = [NSString stringWithFormat:@"name=%@&ispersonnel=true&birth=%@&codepostal=%@&email=%@&password=%@&countrycode=%@&phone=%@&parrainage=%@&stripeToken=%@&creditCard=%@", name, birth, codepostal, email, password, countrycode, phone, code_parrainage, stripeToken, cardNumber];
    if(idfacebook)
        postString = [NSString stringWithFormat:@"name=%@&ispersonnel=true&birth=%@&codepostal=%@&email=%@&password=%@&countrycode=%@&phone=%@&parrainage=%@&idfacebook=%@&stripeToken=%@&creditCard=%@", name, birth, codepostal, email, password, countrycode, phone, code_parrainage, idfacebook, stripeToken, cardNumber];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if([httpResponse statusCode] == 301)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_EMAIL_ALREADY_EXIST, nil);
            });
        }
        else if([httpResponse statusCode] == 302){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_EMAIL_USER_EXIST, nil);
            });
        }
        else if([httpResponse statusCode] == 309){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_PHONE_EXIST, nil);
            });
        }
        else if([httpResponse statusCode] == 300){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_INSCRIPTION_UNKNOWM, nil);
            });
        }
        else if([httpResponse statusCode] == 200){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            User* user;
            if(error!=nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(ERROR_INSCRIPTION_UNKNOWM, nil);
                });
            }
            else {
                
                user = [User new];
                [user parseFromJson:json];
                user.password_app = password;
                if(idfacebook){
                    user.socialId = [NSString stringWithFormat:@"%@", idfacebook];
                    
                }
                [[Util shareManager] saveCustomObject:user];
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(@"", user);
                });
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
        }
        
    }];
    [postDataTask resume];
}

-(void)invokeProRegistrationWebServiceWithParams_name:(NSString*)name _codepostal:(NSString*)codepostal _email:(NSString*)email _password:(NSString*)password _countrycode:(NSNumber*)countrycode _phone:(NSString*)phone _company:(NSString*)company _siret:(NSString*)siret _tva:(NSString*)tva _code_parrainage:(NSString*)code_parrainage _idfacebook:(NSNumber*)idfacebook _stripeToken:(NSString*)stripeToken _cardNumber:(NSString*)cardNumber completionHandler:(void (^) (NSString *errorMsg, User* user))handler{
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:REGISTRATION_URL andWithJson:NO andData:nil];
    
    NSString *postString = [NSString stringWithFormat:@"name=%@&ispersonnel=false&codepostal=%@&email=%@&password=%@&countrycode=%@&phone=%@&company=%@&siret=%@&tva=%@&parrainage=%@&stripeToken=%@&creditCard=%@", name, codepostal, email, password, countrycode, phone, company, siret, tva, code_parrainage, stripeToken, cardNumber];
    if(idfacebook)
        postString = [NSString stringWithFormat:@"name=%@&ispersonnel=false&codepostal=%@&email=%@&password=%@&countrycode=%@&phone=%@&company=%@&siret=%@&tva=%@&parrainage=%@&idfacebook=%@&stripeToken=%@&creditCard=%@", name, codepostal, email, password, countrycode, phone, company, siret, tva, code_parrainage, idfacebook, stripeToken, cardNumber];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if([httpResponse statusCode] == 301)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_EMAIL_ALREADY_EXIST, nil);
            });
        }
        else if([httpResponse statusCode] == 302){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_EMAIL_USER_EXIST, nil);
            });
        }
        else if([httpResponse statusCode] == 309){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_PHONE_EXIST, nil);
            });
        }
        else if([httpResponse statusCode] == 300){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_INSCRIPTION_UNKNOWM, nil);
            });
        }
        else if([httpResponse statusCode] == 200){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            User* user;
            if(error!=nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(ERROR_INSCRIPTION_UNKNOWM, nil);
                });
            }
            else {
                
                user = [User new];
                [user parseFromJson:json];
                user.password_app = password;
                if(idfacebook){
                    user.socialId = [NSString stringWithFormat:@"%@", idfacebook];
                    
                }
                [[Util shareManager] saveCustomObject:user];
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(@"", user);
                });
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
        }
        
    }];
    [postDataTask resume];
}

-(void)invokeAuthentificationWebServiceWithParams_email:(NSString*)email _password:(NSString*)password completionHandler:(void (^) (NSString *errorMsg, User* user))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:AUTH_URL andWithJson:NO andData:nil];
    
    NSString *postString = [NSString stringWithFormat:@"email=%@&password=%@", email, password];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"HTTP request = %@", [request allHTTPHeaderFields]);
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        
        NSLog(@"ici 0");
        if([httpResponse statusCode] == 300)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"ici 1");
                handler(ERROR_LOGIN_INCORRECT, nil);
            });
        }
        else if([httpResponse statusCode] == 200){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            User* user;
            if(error!=nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(ERROR_LOGIN_UNKNOWM, nil);
                });
            }
            else {
                
                user = [User new];
                [user parseFromJson:json];
                user.password_app = password;
                [[Util shareManager] saveCustomObject:user];
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(@"", user);
                });
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
        }
        
        
    }];
    [postDataTask resume];
}

-(void)invokeAuthentificationWithFacebookWebServiceWithParams_idfacebook:(NSNumber*)idfacebook completionHandler:(void (^) (NSString *errorMsg, User* user))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:AUTH_URL andWithJson:NO andData:nil];
    
    NSString *postString = [NSString stringWithFormat:@"idfacebook=%@", idfacebook];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"HTTP request = %@", [request allHTTPHeaderFields]);
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        
        if([httpResponse statusCode] == 300)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_LOGIN_INCORRECT, nil);
            });
        }
        else if([httpResponse statusCode] == 305){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_FACEBOOK_ACCOUNT_LINK, nil);
            });
        }
        else if([httpResponse statusCode] == 200){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            User* user;
            if(error!=nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(ERROR_LOGIN_UNKNOWM, nil);
                });
            }
            else {
                
                user = [User new];
                [user parseFromJson:json];
                if(idfacebook){
                    user.socialId = [NSString stringWithFormat:@"%@", idfacebook];
                }
                [[Util shareManager] saveCustomObject:user];
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(@"", user);
                });
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
        }
        
        
    }];
    [postDataTask resume];
}


-(void)invokeUpdateRequestWebServiceWithParams_user:(User*)user andRequest:(Request*)req completionHandler:(void (^) (NSString *errorMsg, Request* request))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    
    NSDictionary *entry = [req dictionaryWithPropertiesOfObject:req];
    NSLog(@"entry %@", entry);
    
    
    NSString* addressstart = [entry objectForKey:@"addressstart"];
    NSString* adresseend = [entry objectForKey:@"adresseend"];
    
    NSString*  city= [entry objectForKey:@"city"];
    NSString* dateCreated = [entry objectForKey:@"dateCreated"];
    NSString* dateValidate = [entry objectForKey:@"dateValidate"];
    NSString* distanceEstimate= [entry objectForKey:@"distanceEstimate"];
    NSString* durationEstimate = [entry objectForKey:@"durationEstimate"];
    
    NSNumber* estimatePrice = [entry objectForKey:@"estimatePrice"];//number
    NSNumber* idcategory = [entry objectForKey:@"idcategory"];//number
    NSNumber* idcustomer = [entry objectForKey:@"idcustomer"];//number
    NSNumber* idrequest = [entry objectForKey:@"idrequest"];//number
    
    
    float latend = [[entry objectForKey:@"latend"] floatValue] ;//string
    float latstart = [[entry objectForKey:@"latstart"]floatValue];//string
    float lngend = [[entry objectForKey:@"lngend"]floatValue];//string
    float lngstart = [[entry objectForKey:@"lngstart"]floatValue];//string
    
    
    
    NSString *postString = [NSString stringWithFormat:@"adresseend=%@&addressstart=%@&city=%@&dateCreated=%@&dateValidate=%@&distanceEstimate=%@&durationEstimate=%@&estimatePrice=%@&idcategory=%@&idcustomer=%@&idrequest=%@&latend=%f&latstart=%f&lngend=%f&lngstart=%f",adresseend, addressstart, city, dateCreated, dateValidate, distanceEstimate, durationEstimate, estimatePrice, idcategory, idcustomer, idrequest, latend, latstart, lngend,lngstart];
    
    NSLog(@"\n request name %@",postString);

    NSString *strURL = [NSString stringWithFormat:@"%@%@%@", SERVER_URL, CREATE_REQUEST_URL,user.token];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",strURL ]];
    
    
    NSLog(@"\n APi name %@",url);
    
    
    NSLog(@"😀😀%@",url.absoluteString);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPMethod:@"POST"];
    
  //  postString = [postString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        
        if([httpResponse statusCode] == 310)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_SESSION_EXPIRED, nil);
            });
        }
        else if([httpResponse statusCode] == 500){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM, nil);
            });
        }
        else if([httpResponse statusCode] == 200){
            
            NSLog(@"ICI 2");
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"", nil);
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
        }
        
    }];
    [postDataTask resume];
}

-(void)invokeValidateEmailWebServiceWithParams_email:(NSString*)email _password:(NSString*)password _code:(NSString*)code _generated:(BOOL)generated completionHandler:(void (^) (NSString *errorMsg, BOOL success))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    NSString *postString = [NSString stringWithFormat:@"email=%@&generated=1", email];
    
    
    if(!generated)
        postString = [NSString stringWithFormat:@"email=%@&generated=0&password=%@&code=%@", email, password, code];
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:VALIDATE_EMAIL_URL andWithJson:NO andData:nil];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPMethod:@"POST"];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, NO);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        
        
        if([httpResponse statusCode] == 303)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_MAIL_EXIST, NO);
            });
        }
        else if([httpResponse statusCode] == 304){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_INCORRECT_CODE, NO);
            });
        }
        else if([httpResponse statusCode] == 305){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_EXPIRE_CODE, NO);
            });
        }
        else if([httpResponse statusCode] == 500){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM, NO);
            });
        }
        else if([httpResponse statusCode] == 200){
            /*
             NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             if(error!=nil)
             {
             dispatch_async(dispatch_get_main_queue(), ^{
             handler(ERROR_UNKNOWM, NO);
             });
             }
             else {
             //NSString* email = [json objectForKey:@"email"];
             //NSLog(@"email %@", email);
             dispatch_async(dispatch_get_main_queue(), ^{
             handler(SUCCESS_PASSWORD_CHANGE, YES);
             });
             }*/
            
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"", YES);
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM, NO);
            });
        }
        
    }];
    [postDataTask resume];
}

-(void)invokeContactWebServiceWithParams:(User*)user _email:(NSString*)email _password:(NSString*)password completionHandler:(void (^) (NSString *errorMsg, User* user))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    
    NSString* url = [NSString stringWithFormat:POST_EMAIL, user.token, email, password];
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:url andWithJson:NO andData:nil];
    
    [request setHTTPMethod:@"GET"];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        
        if([httpResponse statusCode] == 310)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_SESSION_EXPIRED, nil);
            });
        }
        else if([httpResponse statusCode] == 500){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM, nil);
            });
        }
        else if([httpResponse statusCode] == 200){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"", nil);
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
        }
    }];
    [postDataTask resume];
}

-(void)invokeUpdateUserWebServiceWithParams_user:(User*)user _name:(NSString*)name _birth:(NSString*)birth _codepostal:(NSString*)codepostal _countrycode:(NSNumber*)countrycode _phone:(NSString*)phone _image:(NSString*)image  _stripeToken:(NSString*)stripeToken _cardNumber:(NSString*)cardNumber completionHandler:(void (^) (NSString *errorMsg, User* user))handler{
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:[NSString stringWithFormat:@"%@%@", UPDATE_USER_URL, user.token] andWithJson:NO andData:nil];
    NSLog(@"idcustomer=%@&email=%@&name=%@&birth=%@&codepostal=%@&countrycode=%@&phone=%@&stripeToken=%@&creditCard=%@", user.idcustomer, user.email,name, birth, codepostal, countrycode, phone, stripeToken, cardNumber);
    NSString *postString = [NSString stringWithFormat:@"idcustomer=%@&email=%@&name=%@&birth=%@&codepostal=%@&countrycode=%@&phone=%@&stripeToken=%@&creditCard=%@", user.idcustomer, user.email,name, birth, codepostal, countrycode, phone, stripeToken, cardNumber];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"PUT"];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        
        if([httpResponse statusCode] == 310)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_SESSION_EXPIRED, nil);
            });
        }
        else if([httpResponse statusCode] == 500){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM, nil);
            });
        }
        else if([httpResponse statusCode] == 200){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"", nil);
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
        }
        
    }];
    [postDataTask resume];
    
}

-(void)invokeUpdateUserWebServiceWithParams_user:(User*)user _name:(NSString*)name _company:(NSString*)companyname _siret:(NSString*)siret _tva:(NSString*)tva _codepostal:(NSString*)codepostal _countrycode:(NSNumber*)countrycode _phone:(NSString*)phone _image:(NSString*)image  _stripeToken:(NSString*)stripeToken _cardNumber:(NSString*)cardNumber completionHandler:(void (^) (NSString *errorMsg, User* user))handler {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:[NSString stringWithFormat:@"%@%@", UPDATE_USER_URL, user.token] andWithJson:NO andData:nil];
    
    NSString *postString = [NSString stringWithFormat:@"idcustomer=%@&email=%@&name=%@&codepostal=%@&countrycode=%@&phone=%@&company=%@&siret=%@&tva=%@&phone=%@&stripeToken=%@&creditCard=%@", user.idcustomer, user.email, name, codepostal, countrycode, phone, companyname, siret, tva, phone, stripeToken, cardNumber];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"PUT"];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        
        if([httpResponse statusCode] == 310)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_SESSION_EXPIRED, nil);
            });
        }
        else if([httpResponse statusCode] == 500){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM, nil);
            });
        }
        else if([httpResponse statusCode] == 200){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"", nil);
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
        }
        
    }];
    [postDataTask resume];
}


-(void)invokeUpdateUserPointServiceWithParams_user:(User*)user _point:(NSNumber*)points completionHandler:(void (^) (NSString *errorMsg, User* user))handler {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:[NSString stringWithFormat:@"%@%@", USER_POINTS_UPDATE, user.token] andWithJson:NO andData:nil];
    
    NSString *postString = [NSString stringWithFormat:@"points=%@", points];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        
        if([httpResponse statusCode] == 310)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_SESSION_EXPIRED, nil);
            });
        }
        else if([httpResponse statusCode] == 500){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM, nil);
            });
        }
        else if([httpResponse statusCode] == 200){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"", nil);
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
        }
        
    }];
    [postDataTask resume];
}

-(void)invokeUpdateDriverWebServiceWithParams_user:(User*)user completionHandler:(void (^) (NSString *errorMsg, User* user))handler{
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    NSString *entry = [user toJSONString];
    NSLog(@"entry %@", entry);
    NSData* requestData = [entry dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:[NSString stringWithFormat:@"%@%@", UPDATE_DRIVER_URL, user.token] andWithJson:YES andData:requestData];
    [request setHTTPBody: requestData];
    [request setHTTPMethod:@"PUT"];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        
        if([httpResponse statusCode] == 310)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_SESSION_EXPIRED, nil);
            });
        }
        else if([httpResponse statusCode] == 500){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_DRIVER, nil);
            });
        }
        else if([httpResponse statusCode] == 200){
            /*
             NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
             User* user;
             if(error!=nil)
             {
             dispatch_async(dispatch_get_main_queue(), ^{
             handler(ERROR_UNKNOWM, nil);
             });
             }
             else {
             
             user = [User new];
             [user parseFromJson:json];
             dispatch_async(dispatch_get_main_queue(), ^{
             handler(@"", user);
             NSLog(@"user idUser:%@", user.idUser);
             });
             }*/
            NSLog(@"it's done");
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"", nil);
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
        }
        
    }];
    [postDataTask resume];
    
}


-(void)invokeLogoutWebServiceWithParams_User:(User*)user completionHandler:(void (^) (NSDictionary* json, NSError *error, NSString* iduser))handler{
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:[NSString stringWithFormat:@"%@%@", LOGOUT_URL, user.token] andWithJson:NO andData:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(nil, error, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        NSString* iduser;
        if(error!=nil)
        {
            NSLog(@"error = %@",error);
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(nil, error, iduser);
            });
        }
        else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"json = %@",json);
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(json, error, iduser);
                });
            });
        }
        
    }];
    [postDataTask resume];
}

-(void)invokeUploadPhotoWebServiceWithParams_user:(User*)user _image:(UIImage*)image completionHandler:(void (^) (NSString *errorMsg, NSString* path))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSString *urlString = [NSString stringWithFormat:@"%@%@",SERVER_URL, [NSString stringWithFormat:@"%@%@&idcustomer=%@", UPLOAD_PHOTO_URL, user.token, user.idcustomer] ];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"\n APi name %@",urlString);
    
    NSLog(@"😀😀%@",url.absoluteString);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    
    NSData *dataImage = UIImageJPEGRepresentation(image, 0.4f);
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"picture\"; filename=\"%@.jpg\"\r\n", user.idcustomer] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postbody appendData:[NSData dataWithData:dataImage]];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:postbody];
    
    /*
     NSData *dataImage = UIImageJPEGRepresentation(image, 1.0f);
     NSString *postString = [NSString stringWithFormat:@"picture=%@",dataImage];
     [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];*/
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        NSString* path;
        if([httpResponse statusCode] == 200){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if ([[json valueForKey:@"path"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"path"] isEqualToString:@"<null>"]) {
                path = [json valueForKey:@"path"];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"", path);
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM, @"");
            });
        }
        
        
    }];
    [postDataTask resume];
}

-(void)invokeCreateRequestWebServiceWithParams_user:(User*)user andRequest:(Request*)req completionHandler:(void (^) (NSString *errorMsg, Request* request))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    
    NSDictionary *entry = [req dictionaryWithPropertiesOfObject:req];
    NSLog(@"entry %@", entry);
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:entry options:0 error:nil];
    
    //TODO handle error
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:[NSString stringWithFormat:@"%@%@", CREATE_REQUEST_URL, user.token] andWithJson:YES andData:requestData];
    [request setHTTPBody: requestData];
    
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        
        if([httpResponse statusCode] == 310)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_SESSION_EXPIRED, nil);
            });
        }
        else if([httpResponse statusCode] == 500){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM, nil);
            });
        }
        else if([httpResponse statusCode] == 200){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            Request* request;
            if(error!=nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(ERROR_UNKNOWM, nil);
                });
            }
            else {
                
                request = [Request new];
                [request parseFromJson:json];
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(@"", request);
                    NSLog(@"request idcusomer:%@", request.idcustomer);
                });
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
        }
        
    }];
    [postDataTask resume];
    
}

-(void)invokeAccepteCourse_Driver:(User*)user andIdRequest:(NSNumber*)idrequest andResponse:(BOOL)reponse completionHandler:(void (^) (NSString *errorMsg, Course* course))handler{
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    NSData *webData = [@"{\"accept\": false}" dataUsingEncoding:NSUTF8StringEncoding];
    if(reponse)
        webData = [@"{\"accept\": true}" dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSError *error;
    NSDictionary* entry = [NSJSONSerialization JSONObjectWithData:webData options:0 error:&error];
    NSLog(@"JSON DIct: %@", entry);
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:entry options:0 error:nil];
    NSString* url = [NSString stringWithFormat:@"%@%@&requestId=%@", ACCEPTE_COURSE_URL, user.token, idrequest];
    NSLog(@"url %@", url);
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:url andWithJson:YES andData:requestData];
    
    [request setHTTPBody: requestData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        if([httpResponse statusCode] == 200){
            if(error!=nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"json = %@",json);
                    handler(@"Vous ne pouvez pas créer cette course !", nil);
                });
            }
            else {
                
                Course* c_course = [Course new];
                [c_course parseFromJson:json];
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(@"", c_course);
                });
                
                
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"Vous ne pouvez pas créer cette course !", nil);
            });
        }
        
        
        
    }];
    [postDataTask resume];
}

-(void)invokeMyCoureWebServiceWithParams_User:(User*)user completionHandler:(void (^) (NSString *errorMsg, Course* course, Request* request,NSArray *courseArray))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    NSString* url = [NSString stringWithFormat:@"%@%@", MY_COURSE_URL, user.token];
    NSLog(@"urlt = %@", url);
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:url andWithJson:NO andData:nil];
    [request setHTTPMethod:@"GET"];
    
    //NSString *postString = [NSString stringWithFormat:@"email=%@&password=%@", email, password];
    //[request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"HTTP request = %@", [request allHTTPHeaderFields]);
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil, nil,nil);
            });
            
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        Course* course;
        Request* request;
        if(error!=nil || !jsonArray || jsonArray.count == 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM, nil, nil,nil);
            });
        }
        else {
            NSDictionary* json = [jsonArray objectAtIndex:0];
            course = [Course new];
            request = [Request new];
            [course parseFromJson:json];
            [request parseFromJson:json];
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"", course, request,jsonArray);
            });
        }
        
    }];
    [postDataTask resume];
}

-(void)invokeDriverCourseWebServiceWithParams_User:(User*)user completionHandler:(void (^) (NSString *errorMsg, Course* course, NSMutableArray* list_request_waiting))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    NSString* url = [NSString stringWithFormat:@"%@%@", DRIVER_CURRENT_COURSE_URL, user.token];
    NSLog(@"urlt = %@", url);
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:url andWithJson:NO andData:nil];
    [request setHTTPMethod:@"GET"];
    
    NSLog(@"HTTP request = %@", [request allHTTPHeaderFields]);
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        Course* course;
        NSMutableArray* list_request_waiting;
        if(error!=nil)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM, nil, nil);
            });
        }
        else {
            NSDictionary* json_course = [json objectForKey:@"course_actuelle"];
            if([NSNull null] != [json objectForKey:@"course_actuelle"])
            {
                course = [Course new];
                [course parseFromJson:json_course];
            }
            if ([[json valueForKey:@"requests_waiting"] isKindOfClass:[NSArray class]]) {
                NSArray* j_requests_waiting = [json valueForKey:@"requests_waiting"];
                list_request_waiting = [NSMutableArray new];
                for(NSDictionary* j_request in j_requests_waiting)
                {
                    Request* request = [Request new];
                    [request parseFromJson:j_request];
                    [list_request_waiting addObject:request];
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"", course, list_request_waiting);
            });
        }
        
    }];
    [postDataTask resume];
}

-(void)invokeCustomerCourseWebServiceWithParams_User:(User*)user completionHandler:(void (^) (NSString *errorMsg, Course* course, Request* request))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    NSString* url = [NSString stringWithFormat:@"%@%@", CUSTOMER_CURRENT_COURSE_URL, user.token];
    NSLog(@"urlt = %@", url);
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:url andWithJson:NO andData:nil];
    [request setHTTPMethod:@"GET"];
    
    //NSString *postString = [NSString stringWithFormat:@"email=%@&password=%@", email, password];
    //[request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"HTTP request = %@", [request allHTTPHeaderFields]);
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        Course* course;
        Request* request;
        if(error!=nil)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM, nil, nil);
            });
        }
        else {
            NSDictionary* json_course = [json objectForKey:@"course"];
            NSDictionary* json_request = [json objectForKey:@"demands"];
            if([NSNull null] != [json objectForKey:@"course"])
            {
                course = [Course new];
                [course parseFromJson:json_course];
            }
            if([NSNull null] != [json objectForKey:@"demands"])
            {
                request = [Request new];
                [request parseFromJson:json_request];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"", course, request);
            });
        }
        
    }];
    [postDataTask resume];
}


-(void)invokeUpdateCourseWebServiceWithParams_user:(User*)user andCourse:(Course*)course completionHandler:(void (^) (NSString *errorMsg, Course* request))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    //NSString *entry = [course toJSONString];
    //NSLog(@"entry %@", entry);
    //NSData* requestData = [entry dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"course id %@", course.idcourse);
    
    NSDictionary *entry = [course dictionaryWithPropertiesOfObject:course];
    NSLog(@"entry %@", entry);
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:entry options:0 error:nil];
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:[NSString stringWithFormat:UPDATE_COURSE_URL, course.idcourse, user.token] andWithJson:YES andData:requestData];
    [request setHTTPBody: requestData];
    [request setHTTPMethod:@"PUT"];
    NSLog(@"HTTP request = %@", [request allHTTPHeaderFields]);
    NSLog(@"urlt = %@", [request URL]);
    
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        
        if([httpResponse statusCode] == 310)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_SESSION_EXPIRED, nil);
            });
        }
        else if([httpResponse statusCode] == 500){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM, nil);
            });
        }
        else if([httpResponse statusCode] == 200){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            Course* course;
            if(error!=nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(ERROR_UNKNOWM, nil);
                });
            }
            else {
                
                course = [Course new];
                [course parseFromJson:json];
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(@"", course);
                });
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
        }
        
    }];
    [postDataTask resume];
    
}

-(void)invokeBuyPointWebServiceWithParams_user:(User*)user andIdPoint:(NSNumber*)idpoint completionHandler:(void (^) (NSString *errorMsg))handler {
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSString* url = [NSString stringWithFormat:@"%@%@", BUY_POINT_URL, user.token];
    
    NSData *webData = [[NSString stringWithFormat:@"{\"idpoint\": \"%@\"}", idpoint] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSError *error;
    NSDictionary* entry = [NSJSONSerialization JSONObjectWithData:webData options:0 error:&error];
    NSLog(@"JSON DIct: %@", entry);
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:entry options:0 error:nil];
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:url andWithJson:YES andData:requestData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody: requestData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if([httpResponse statusCode] == 200){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"");
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION);
            });
        }
        
    }];
    [postDataTask resume];
}

-(void)invokeDriverDispo_user:(User*)user andIdCat:(NSNumber*)idcat andCity:(NSString*)city completionHandler:(void (^) (NSString *errorMsg))handler{
    
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSString* url = [NSString stringWithFormat:DRIVER_DISPO_URL, user.token, idcat, city];
    
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:url andWithJson:NO andData:nil];
    [request setHTTPMethod:@"GET"];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE  invokeDriverDispo_user= %@", resp_text);
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        
        if([httpResponse statusCode] == 200){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"");
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(NO_DRIVER_MESSAGE);
            });
        }
    }];
    [postDataTask resume];
}

-(void)invokeDeleteRequest_user:(User*)user andIdRequest:(NSNumber*)idrequest completionHandler:(void (^) (NSString *errorMsg))handler; {
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSString* url = [NSString stringWithFormat:DELETE_REQUEST_URL, user.token, idrequest];
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:url andWithJson:NO andData:nil];
    [request setHTTPMethod:@"DELETE"];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if([httpResponse statusCode] == 204){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"");
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM);
            });
        }
        
    }];
    [postDataTask resume];
}

-(void)invokeNoteCourseWebServiceWithParams_user:(User*)user andCourse:(Course*)course andidUserChangNote:(NSNumber*)idUserChangNote andNote:(NSNumber*)note completionHandler:(void (^) (NSString *errorMsg))handler{
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:[NSString stringWithFormat:@"%@%@", UPDATE_NOTE_URL, user.token] andWithJson:NO andData:nil];
    
    NSString *postString = [NSString stringWithFormat:@"idUser=%@&idCourse=%@&idUserChangNote=%@&note=%@",user.idUser,course.idcourse, idUserChangNote, note];
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if([httpResponse statusCode] == 200){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"");
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION);
            });
        }
        
    }];
    [postDataTask resume];
}



//synchronus get data
-(void)invokeGetDataWebService:(int)idws{
    
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:GET_DATA_URL andWithJson:NO andData:nil];
    NSURLResponse *response;
    NSError *error;
    NSData* data = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
    
    
    if(!data)
    {
        return;
    }
    
    NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if(error!=nil)
    {
        NSLog(@"error = %@",error);
    }
    else {
        
        NSMutableArray* points = [NSMutableArray new];
        NSMutableArray* conciergeries = [NSMutableArray new];
        NSMutableArray* categories = [NSMutableArray new];
        
        if ([[json valueForKey:@"points"] isKindOfClass:[NSArray class]]) {
            NSArray* j_points = [json valueForKey:@"points"];
            for(NSDictionary* j_point in j_points)
            {
                WPoint *p = [WPoint new];
                if ([[j_point valueForKey:@"idpoints"] isKindOfClass:[NSNumber class]]) {
                    p.idpoints = [j_point valueForKey:@"idpoints"];
                }
                if ([[j_point valueForKey:@"title"] isKindOfClass:[NSString class]] && ![[j_point valueForKey:@"title"] isEqualToString:@"<null>"]) {
                    p.title = [j_point valueForKey:@"title"];
                }
                if ([[j_point valueForKey:@"number"] isKindOfClass:[NSNumber class]]) {
                    p.number = [j_point valueForKey:@"number"];
                }
                if ([[j_point valueForKey:@"price"] isKindOfClass:[NSString class]] && ![[j_point valueForKey:@"price"] isEqualToString:@"<null>"]) {
                    p.price = [j_point valueForKey:@"price"];
                }
                [points addObject:p];
            }
        }
        
        if ([[json valueForKey:@"conciergeries"] isKindOfClass:[NSArray class]]) {
            NSArray* j_conciergeries = [json valueForKey:@"conciergeries"];
            for(NSDictionary* j_conciergerie in j_conciergeries)
            {
                Conciergerie *c = [Conciergerie new];
                if ([[j_conciergerie valueForKey:@"idconcierge"] isKindOfClass:[NSNumber class]]) {
                    c.idconcierge = [j_conciergerie valueForKey:@"idconcierge"];
                }
                if ([[j_conciergerie valueForKey:@"title"] isKindOfClass:[NSString class]] && ![[j_conciergerie valueForKey:@"title"] isEqualToString:@"<null>"]) {
                    c.title = [j_conciergerie valueForKey:@"title"];
                }
                if ([[j_conciergerie valueForKey:@"image"] isKindOfClass:[NSString class]] && ![[j_conciergerie valueForKey:@"image"] isEqualToString:@"<null>"]) {
                    c.image = [j_conciergerie valueForKey:@"image"];
                }
                if ([[j_conciergerie valueForKey:@"products"] isKindOfClass:[NSArray class]]) {
                    c.products = [NSMutableArray new];
                    NSArray* j_products = [j_conciergerie valueForKey:@"products"];
                    for(NSDictionary* j_product in j_products)
                    {
                        Product *p = [Product new];
                        if ([[j_product valueForKey:@"idconsproduit"] isKindOfClass:[NSNumber class]]) {
                            p.idconsproduit = [j_product valueForKey:@"idconsproduit"];
                        }
                        if ([[j_product valueForKey:@"title"] isKindOfClass:[NSString class]] && ![[j_product valueForKey:@"title"] isEqualToString:@"<null>"]) {
                            p.title = [j_product valueForKey:@"title"];
                        }
                        if ([[j_product valueForKey:@"description"] isKindOfClass:[NSString class]] && ![[j_product valueForKey:@"description"] isEqualToString:@"<null>"]) {
                            p.wdescription = [j_product valueForKey:@"description"];
                        }
                        if ([[j_product valueForKey:@"point"] isKindOfClass:[NSNumber class]]) {
                            p.point = [j_product valueForKey:@"point"];
                        }
                        [c.products addObject:p];
                    }
                }
                [conciergeries addObject:c];
            }
        }
        
        if ([[json valueForKey:@"categories"] isKindOfClass:[NSArray class]]) {
            NSArray* j_categories = [json valueForKey:@"categories"];
            for(NSDictionary* j_category in j_categories)
            {
                Categorie *c = [Categorie new];
                if ([[j_category valueForKey:@"idcategory"] isKindOfClass:[NSNumber class]]) {
                    c.idcategory = [j_category valueForKey:@"idcategory"];
                }
                if ([[j_category valueForKey:@"range"] isKindOfClass:[NSString class]] && ![[j_category valueForKey:@"range"] isEqualToString:@"<null>"]) {
                    c.range = [j_category valueForKey:@"range"];
                }
                if ([[j_category valueForKey:@"pricepack"] isKindOfClass:[NSString class]] && ![[j_category valueForKey:@"pricepack"] isEqualToString:@"<null>"]) {
                    c.pricepack = [j_category valueForKey:@"pricepack"];
                }
                if ([[j_category valueForKey:@"pricesup"] isKindOfClass:[NSString class]] && ![[j_category valueForKey:@"pricesup"] isEqualToString:@"<null>"]) {
                    c.pricesup = [j_category valueForKey:@"pricesup"];
                }
                if ([[j_category valueForKey:@"maxbid"] isKindOfClass:[NSNumber class]]) {
                    c.maxbid = [j_category valueForKey:@"maxbid"];
                }
                if ([[j_category valueForKey:@"unitinclu"] isKindOfClass:[NSNumber class]]) {
                    c.unitinclu = [j_category valueForKey:@"unitinclu"];
                }
                if ([[j_category valueForKey:@"measure"] isKindOfClass:[NSString class]] && ![[j_category valueForKey:@"measure"] isEqualToString:@"<null>"]) {
                    c.measure = [j_category valueForKey:@"measure"];
                }
                if ([[j_category valueForKey:@"titrevoitures"] isKindOfClass:[NSString class]] && ![[j_category valueForKey:@"titrevoitures"] isEqualToString:@"<null>"]) {
                    c.titrevoitures = [j_category valueForKey:@"titrevoitures"];
                }
                if ([[j_category valueForKey:@"note"] isKindOfClass:[NSNumber class]]) {
                    c.note = [j_category valueForKey:@"note"];
                }
                if ([[j_category valueForKey:@"photo"] isKindOfClass:[NSString class]] && ![[j_category valueForKey:@"photo"] isEqualToString:@"<null>"]) {
                    c.photo = [j_category valueForKey:@"photo"];
                }
                if ([[j_category valueForKey:@"brands"] isKindOfClass:[NSArray class]]) {
                    c.brands = [NSMutableArray new];
                    NSArray* j_brands = [j_category valueForKey:@"brands"];
                    for(NSDictionary* j_brand in j_brands)
                    {
                        Brand *p = [Brand new];
                        if ([[j_brand valueForKey:@"idbrand"] isKindOfClass:[NSNumber class]]) {
                            p.idbrand = [j_brand valueForKey:@"idbrand"];
                        }
                        if ([[j_brand valueForKey:@"title"] isKindOfClass:[NSString class]] && ![[j_brand valueForKey:@"title"] isEqualToString:@"<null>"]) {
                            p.title = [j_brand valueForKey:@"title"];
                        }
                        if ([[j_brand valueForKey:@"statu"] isKindOfClass:[NSNumber class]]) {
                            p.statu = [j_brand valueForKey:@"statu"];
                        }
                        if ([[j_brand valueForKey:@"photo"] isKindOfClass:[NSString class]] && ![[j_brand valueForKey:@"photo"] isEqualToString:@"<null>"]) {
                            p.photo = [j_brand valueForKey:@"photo"];
                        }
                        [c.brands addObject:p];
                    }
                }
                [categories addObject:c];
            }
        }
        
        
        [[SharedVar shareManager] setMainPoints:points];
        [[SharedVar shareManager] setMainConciergeries:conciergeries];
        [[SharedVar shareManager] setMainCategories:categories];
        
    }
    
}


-(void)invokeGetListCarsWebService:(int)idws {
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:GET_CARS_URL andWithJson:NO andData:nil];
    [request setHTTPMethod:@"GET"];
    NSURLResponse *response;
    NSError *error;
    NSData* data = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
    
    
    if(!data)
    {
        return;
    }
    
    NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if(error!=nil)
    {
        NSLog(@"error = %@",error);
    }
    else {
        
        NSMutableArray* cars = [NSMutableArray new];
        
        
        if ([json isKindOfClass:[NSArray class]]) {
            for(NSDictionary* j_car in json)
            {
                Car *c = [Car new];
                [c parseFromJson:j_car];
                Brand *p = [Brand new];
                NSDictionary* j_brand = [j_car objectForKey:@"idbrand"];
                if ([[j_brand valueForKey:@"idbrand"] isKindOfClass:[NSNumber class]]) {
                    p.idbrand = [j_brand valueForKey:@"idbrand"];
                }
                if ([[j_brand valueForKey:@"title"] isKindOfClass:[NSString class]] && ![[j_brand valueForKey:@"title"] isEqualToString:@"<null>"]) {
                    p.title = [j_brand valueForKey:@"title"];
                }
                if ([[j_brand valueForKey:@"statu"] isKindOfClass:[NSNumber class]]) {
                    p.statu = [j_brand valueForKey:@"statu"];
                }
                if ([[j_brand valueForKey:@"photo"] isKindOfClass:[NSString class]] && ![[j_brand valueForKey:@"photo"] isEqualToString:@"<null>"]) {
                    p.photo = [j_brand valueForKey:@"photo"];
                }
                c.brand = p;
                [cars addObject:c];
            }
        }
        
        [[SharedVar shareManager] setMainCars:cars];
        
        
    }
    
    
}

-(void)invokeGetListCityWebService:(int)idws{
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:GET_CITY_URL andWithJson:NO andData:nil];
    [request setHTTPMethod:@"GET"];
    NSURLResponse *response;
    NSError *error;
    NSData* data = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
    
    
    if(!data)
    {
        return;
    }
    
    NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if(error!=nil)
    {
        NSLog(@"error = %@",error);
    }
    else {
        
        NSMutableArray* cities = [NSMutableArray new];
        
        
        if ([json isKindOfClass:[NSArray class]]) {
            for(NSDictionary* j_city in json)
            {
                WCity *c = [WCity new];
                [c parseFromJson:j_city];
                [cities addObject:c];
            }
        }
        
        [[SharedVar shareManager] setMainCities:cities];
        
    }
    
}


#pragma mark - Error handling

-(void)handleError:(NSError*)error
{
    
}
@end


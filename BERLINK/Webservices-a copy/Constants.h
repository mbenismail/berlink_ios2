//
//  Constants.h
//  app
//
//  Created by BERLINK on 1/31/17.
//  Copyright © 2017 sifo. All rights reserved.
//

#define GOOGLE_MAP_API_KEY @"AIzaSyB917D0IVN9Kkx8ypOrz-SRi-jDUetwCU4"
#define GOOGLE_PLACE_API_KEY @"AIzaSyCbAhhphC9JIlm82zrVsIow9BI9j6cXzwo"
#define GOOGLE_PLACE_WS_API_KEY @"AIzaSyDJy1B7HoBmLsKB72ARBAt6hxBiC_TfdkY"
#define GOOGLE_MAP_DIRECTION_API_KEY @"AIzaSyDJy1B7HoBmLsKB72ARBAt6hxBiC_TfdkY"

//#define SERVER_URL @"http://vps364448.ovh.net:7171/"

#define SERVER_URL @"http://berlinkold.ajitem.com/"

#define SERVER_BASE_URL @"http://berlink.ajitem.com/"

#define SERVER_IMAGE_URL @"http://berlink.ajitem.com/public/images/"


#define REGISTRATION_URL @"api/users/register"
#define AUTH_URL @"api/users/login"
#define SOCIAL_LOGIN_URL @"api/users/login/social"
#define FORGOT_PASSWORD @"api/users/forgot-password"
#define RESET_PASSWORD @"api/users/reset-password"

#define LOGOUT_URL @"api/users/logout"
#define UPDATE_USER_URL @"api/customers" 
#define USER_URL @"api/users/me"

#define GET_DATA_URL @"api/car"

#define GET_CARS_URL @"api/car"
#define GET_CITY_URL @"api/city"

#define GET_CATEGORY_URL @"api/category"
#define GET_Point_URL @"api/point"
#define GET_CONCIERGE_URL @"api/concierge"

#define USER_POINTS_UPDATE @"api/customer/points?access_token="

#define BUY_POINT_URL @"api/customer/point?access_token="


#define UPDATE_DRIVER_URL @"api/drivers" 
#define UPLOAD_PHOTO_URL @"api/customer/photo?access_token="

#define VALIDATE_REQUEST_URL @"api/customer/request?access_token="

#define CREATE_REQUEST_URL @"api/customer/request?access_token="

#define ACCEPTE_COURSE_URL @"api/request/response-driver?access_token="

#define MY_COURSE_URL @"api/course/mes-course?access_token="
#define UPDATE_COURSE_URL @"api/courses/%@?access_token=%@"

#define DRIVER_CURRENT_COURSE_URL @"api/course/driver/actuelle?access_token="
#define CUSTOMER_CURRENT_COURSE_URL @"api/course/customer/actuelle?access_token="



#define DRIVER_DISPO_URL @"api/driver/available?access_token=%@&categoryId=%@&city=%@"

#define DELETE_REQUEST_URL @"api/customer/request?access_token=%@&idrequest=%@"

#define POST_EMAIL @"api/emails/send?access_token=%@&object=%@&message=%@"

#define UPDATE_NOTE_URL @"api/courses/notes?access_token="


#define APP_COLOR [UIColor colorWithRed:0.94 green:0.81 blue:0.68 alpha:1.0]
#define APP_COLOR_DARK [UIColor colorWithRed:0.72 green:0.59 blue:0.45 alpha:1.0];

#define UIColorFromRGB(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

#define kTabBarHeight 44

#define REQUEST_DENY_DELAY 20


#pragma mark- API Request Keys

#define API_KEY_NAME                   @"name"
#define API_KEY_EMAIL                  @"email"
#define API_KEY_ROLE                   @"role"
#define API_KEY_DOB                    @"date_of_birth"
#define API_KEY_ADDRESS_LINE_1         @"line_1"
#define API_KEY_ADDRESS_LINE_2         @"line_2"
#define API_KEY_CITY                   @"city"
#define API_KEY_STATE                  @"state"
#define API_KEY_COUNTRY                @"country"
#define API_KEY_POSTEL_CODE            @"postal_code"
#define API_KEY_COUNTRY_CODE           @"country_code"
#define API_KEY_PHONE_NUMBER           @"phone_number"
#define API_KEY_IMAGE                  @"image"
#define API_KEY_TYPE                   @"type"
#define API_KEY_IBN                    @"ibn"
#define API_KEY_STRIPE_TOKEN           @"stripe_token"
#define API_KEY_PASSWORD               @"password"
#define API_KEY_NEW_PASSWORD           @"new_password"
#define API_KEY_OTP                    @"otp"

#define API_KEY_SOCIAL_ID              @"facebook_id"
#define API_KEY_CODE                   @"code"
#define API_KEY_IS_PERSONAL            @"is_personal"

#define API_KEY_SPONSOR_CODE           @"sponsor_code"
#define API_KEY_COMPANY                @"company_name"
#define API_KEY_TVA                    @"vat_number"
#define API_KEY_SIRET                  @"siret_number"
#define API_KEY_MESSAGE                @"message"


#define API_KEY_TOKEN                  @"token"
#define API_KEY_NEW_PASSORD            @"new_password"

#define API_KEY_CAR_ID                @"car_id"
#define API_KEY_CATEGORY_ID            @"category_id"
#define API_KEY_CITY_ID                @"city_id"
#define API_KEY_CAR_REGI_NUMBER        @"registration_number"
#define API_KEY_WORK_CITY_ID           @"work_city_id"

#define API_KEY_POINTS                  @"points"


#define API_KEY_RANGE                  @"range"
#define API_KEY_PRICE_OF_PACK          @"price_of_pack"
#define API_KEY_STATUS                 @"status"
#define API_KEY_OUT_OF_SUPPORT_VALUE   @"out_of_support_value"
#define API_KEY_OUT_OF_SUPPORT_UNIT    @"out_of_support_unit"
#define API_KEY_OUT_OF_SUPPORT_POINTS  @"out_of_support_points"
#define API_KEY_BIT_CARS               @"bid_cars"


#define API_KEY_USER_ID @"user_id"
#define API_KEY_COURSE_ID @"course_id"
#define API_KEY_CHANGE_NOTE_USER_ID @"change_note_user_id"
#define API_KEY_NOTE    @"note"

#define API_KEY_REQUEST_ID   @"request_id"
#define API_KEY_CATEGORY_ID  @"category_id"
#define API_KEY_POINT_ID     @"point_id"

#define API_KEY_ACCEPT     @"accept"

#define API_KEY_USER_INFO_ME      @"user_info_me"
#define API_KEY_DRIVE_INFO_ME     @"drive_info_me"









//
//  SharedVar.h
//  BERLINK
//
//  Created by BERLINK on 2/8/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "BaseAPIHandler.h"
#import "Constants.h"
#import "User.h"
#import "Util.h"
static BaseAPIHandler *baseAPIHandler = nil;

@implementation BaseAPIHandler

+ (id)sharedInstance {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURL *baseUrl = [NSURL URLWithString:SERVER_BASE_URL];

        baseAPIHandler = [[BaseAPIHandler alloc] initWithBaseURL:baseUrl sessionConfiguration:configuration];
        
        AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-type"];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        baseAPIHandler.requestSerializer = requestSerializer;
    });
  
    User *user = [[Util shareManager] loadSavedUser];
    if(user)
    {
        NSString *token = [NSString stringWithFormat:@"Bearer %@", user.token];
        [baseAPIHandler.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    }
    return baseAPIHandler;
}

#pragma mark - API calls

- (void) callPOST:(NSString *)URLString
                             parameters:(nullable id)parameters
                               progress:(nullable void (^)(NSProgress *uploadProgress))uploadProgress
                                success:(nullable void (^)(id _Nullable responseObject))success
                                failure:(nullable void (^)(NSError *error))failure
{
    NSLog(@"\n\n request : - %@",parameters);
    [self POST:URLString parameters:parameters progress:uploadProgress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"\n\n Response : - %@",responseObject);
        dispatch_async(dispatch_get_main_queue(), ^{
            success(responseObject);
        });
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(error);
        });
    }];
}

- (void) callGET:(NSString *)URLString
       parameters:(nullable id)parameters
        progress:(nullable void (^)(NSProgress *uploadProgress))uploadProgress
          success:(nullable void (^)(id _Nullable responseObject))success
          failure:(nullable void (^)(NSError *error))failure
{
    NSLog(@"\n\n request : - %@",parameters);
    [self GET:URLString parameters:parameters progress:uploadProgress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"\n\n Response : - %@",responseObject);
        dispatch_async(dispatch_get_main_queue(), ^{
            success(responseObject);
        });
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(error);
        });
    }];
}

- (void) callPUT:(NSString *)URLString
      parameters:(nullable id)parameters
         success:(nullable void (^)(id _Nullable responseObject))success
         failure:(nullable void (^)(NSError *error))failure
{
    NSLog(@"\n\n request : - %@",parameters);

    [self PUT:URLString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"\n\n Response : - %@",responseObject);
        dispatch_async(dispatch_get_main_queue(), ^{
            success(responseObject);
        });
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(error);
        });
    }];
}

- (void) callDELETE:(NSString *)URLString
         parameters:(nullable id)parameters
         success:(nullable void (^)(id _Nullable responseObject))success
         failure:(nullable void (^)(NSError *error))failure
{
    NSLog(@"\n\n request : - %@",parameters);
    [self DELETE:URLString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"\n\n Response : - %@",responseObject);

        dispatch_async(dispatch_get_main_queue(), ^{
            success(responseObject);
        });
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            failure(error);
        });
    }];
}

#pragma mark - Running operation

- (NSInteger)runningOperations {
    
    return runningOperations;
}

- (void)addToRunningOperations {

    runningOperations += 1;
}

- (void)removeFromRunningOperations {
    
    runningOperations -= 1;
}



@end

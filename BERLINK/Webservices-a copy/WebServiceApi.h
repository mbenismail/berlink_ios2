//
//  WebServiceApi.h
//  app
//
//  Created by BERLINK on 1/31/17.
//  Copyright © 2017 sifo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "JSONModelLib.h"
#import "User.h"
#import "WPoint.h"
#import "Conciergerie.h"
#import "Categorie.h"
#import "Product.h"
#import "Brand.h"
#import "Request.h"
#import "Course.h"
#import "Car.h"
#import "WCity.h"

@interface WebServiceApi : NSObject
+(WebServiceApi*)shareManager;

-(void)invokeGetCategortWebService:(int)idws;
-(void)invokeGetConciergeriesWebService:(int)idws;
-(void)invokeGetPointsWebService:(int)idws;
-(void)invokeGetListCarsWebService:(int)idws;
-(void)invokeGetListCityWebService:(int)idws;

-(void)invokeGetConciergeriesWebService:(int)idws completionHandler:(void (^) (NSDictionary* json, NSError *error, NSMutableArray* conciergeries))handler;
-(void)invokeGetListCategoryWebService:(int)idws completionHandler:(void (^) (NSDictionary* json, NSError *error,NSMutableArray* categories))handler;
-(void)invokeGetPointsWebService:(int)idws completionHandler:(void (^) (NSDictionary* json, NSError *error,NSMutableArray* points))handler;

-(void)invokeGetListCarsWebService:(int)idws completionHandler:(void (^) (NSDictionary* json, NSError *error,NSMutableArray* cars))handler;

-(void)invokeGetListCityWebService:(int)idws completionHandler:(void (^) (NSDictionary* json, NSError *error,NSMutableArray* cities))handler;

-(void)invokePersonalRegistrationWebServiceWithParams_name:(NSString*)name _birth:(NSString*)birth _codepostal:(NSString*)codepostal _email:(NSString*)email _password:(NSString*)password _countrycode:(NSNumber*)countrycode _phone:(NSString*)phone _code_parrainage:(NSString*)code_parrainage _idfacebook:(NSNumber*)idfacebook _stripeToken:(NSString*)stripeToken _cardNumber:(NSString*)cardNumber completionHandler:(void (^) (NSString *errorMsg, User* user))handler;

-(void)invokeProRegistrationWebServiceWithParams_name:(NSString*)name _codepostal:(NSString*)codepostal _email:(NSString*)email _password:(NSString*)password _countrycode:(NSNumber*)countrycode _phone:(NSString*)phone _company:(NSString*)company _siret:(NSString*)siret _tva:(NSString*)tva _code_parrainage:(NSString*)code_parrainage _idfacebook:(NSNumber*)idfacebook _stripeToken:(NSString*)stripeToken _cardNumber:(NSString*)cardNumber completionHandler:(void (^) (NSString *errorMsg, User* user))handler;

-(void)invokeAuthentificationWebServiceWithParams_email:(NSString*)email _password:(NSString*)password completionHandler:(void (^) (NSString *errorMsg, User* user))handler;

-(void)invokeContactWebServiceWithParams:(User*)user _email:(NSString*)email _password:(NSString*)password completionHandler:(void (^) (NSString *errorMsg, User* user))handler;

-(void)invokeAuthentificationWithFacebookWebServiceWithParams_idfacebook:(NSString*)idfacebook completionHandler:(void (^) (NSString *errorMsg, User* user))handler;

-(void)invokeAuthentificationWithFMobileWithParams_countryCode:(NSString*)countryCode MobileNumber:(NSString *)mobile completionHandler:(void (^) (NSString *errorMsg))handler;

-(void)invokeAuthentificationWithFMobileWithParams_countryCode:(NSString*)countryCode MobileNumber:(NSString *)mobile otp:(NSString *)otp completionHandler:(void (^) (NSString *errorMsg,User* user))handler;

-(void)invokeUpdateRequestWebServiceWithParams_user:(User*)user andRequest:(Request*)req completionHandler:(void (^) (NSString *errorMsg, Request* request))handler;

-(void)invokeValidateEmailWebServiceWithParams_email:(NSString*)email _password:(NSString*)password _code:(NSString*)code _generated:(BOOL)generated completionHandler:(void (^) (NSString *errorMsg, BOOL success))handler;

-(void)invokeUpdateUserWebServiceWithParams_user:(User*)user _name:(NSString*)name _birth:(NSString*)birth _codepostal:(NSString*)codepostal _countrycode:(NSNumber*)countrycode _phone:(NSString*)phone _image:(NSString*)imgage _stripeToken:(NSString*)stripeToken _cardNumber:(NSString*)cardNumber completionHandler:(void (^) (NSString *errorMsg, User* user))handler;

-(void)invokeUpdateUserWebServiceWithParams_user:(User*)user _name:(NSString*)name _company:(NSString*)companyname _siret:(NSString*)siret _tva:(NSString*)tva _codepostal:(NSString*)codepostal _countrycode:(NSNumber*)countrycode _phone:(NSString*)phone _image:(NSString*)imgage _stripeToken:(NSString*)stripeToken _cardNumber:(NSString*)cardNumber completionHandler:(void (^) (NSString *errorMsg, User* user))handler;

-(void)invokeUpdateUserPointServiceWithParams_user:(User*)user _point:(NSNumber*)points completionHandler:(void (^) (NSString *errorMsg, User* user))handler;

-(void)invokeUpdateDriverWebServiceWithParams_user:(User*)user completionHandler:(void (^) (NSString *errorMsg, User* user))handler;

-(void)invokeLogoutWebServiceWithParams_User:(User*)user completionHandler:(void (^) (NSDictionary* json, NSError *error, NSNumber* iduser))handler;

-(void)invokeUploadPhotoWebServiceWithParams_user:(User*)user _image:(UIImage*)image completionHandler:(void (^) (NSString *errorMsg, NSString* path))handler;

-(void)invokeAccepteCourse_Driver:(User*)user andIdRequest:(NSNumber*)idrequest andResponse:(BOOL)reponse completionHandler:(void (^) (NSString *errorMsg, Course* course))handler;

-(void)invokeCreateRequestWebServiceWithParams_user:(User*)user andRequest:(Request*)req completionHandler:(void (^) (NSString *errorMsg, Request* request))handler;

-(void)invokeMyCoureWebServiceWithParams_User:(User*)user completionHandler:(void (^) (NSString *errorMsg, Course* course, Request* request,NSArray *courseArray))handler;

-(void)invokeUpdateCourseWebServiceWithParams_user:(User*)user andCourse:(Course*)course completionHandler:(void (^) (NSString *errorMsg, Course* request))handler;


-(void)invokeDriverCourseWebServiceWithParams_User:(User*)user completionHandler:(void (^) (NSString *errorMsg, Course* course, NSMutableArray* list_request_waiting))handler;

-(void)invokeCustomerCourseWebServiceWithParams_User:(User*)user completionHandler:(void (^) (NSString *errorMsg, Course* course, Request* request))handler;

-(void)invokeNoteCourseWebServiceWithParams_user:(User*)user andCourse:(Course*)course andidUserChangNote:(NSNumber*)idUserChangNote andNote:(NSNumber*)note completionHandler:(void (^) (NSString *errorMsg))handler;

-(void)invokeBuyPointWebServiceWithParams_user:(User*)user andIdPoint:(NSNumber*)idpoint completionHandler:(void (^) (NSString *errorMsg))handler;

-(void)invokeDriverDispo_user:(User*)user andIdCat:(NSNumber*)idcat andCity:(NSString*)city completionHandler:(void (^) (NSString *errorMsg))handler;

-(void)invokeDeleteRequest_user:(User*)user andIdRequest:(NSNumber*)idrequest completionHandler:(void (^) (NSString *errorMsg))handler;
@end

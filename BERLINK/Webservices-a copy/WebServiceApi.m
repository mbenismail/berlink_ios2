//
//  WebServiceApi.m
//  app
//
//  Created by BERLINK on 1/31/17.
//  Copyright © 2017 sifo. All rights reserved.
//

#import "WebServiceApi.h"
#import "Constants.h"
#import "Util.h"
#import "StringConstants.h"
#import "SharedVar.h"
#import "BaseAPIHandler.h"

@implementation WebServiceApi

+(WebServiceApi*)shareManager{
    
    static WebServiceApi *sharedInstance=nil;
    static dispatch_once_t  oncePredecate;
    
    dispatch_once(&oncePredecate,^{
        sharedInstance=[[WebServiceApi alloc] init];
    });
    return sharedInstance;
}


#pragma mark-  Login flow

-(void)invokeGetUserWithAccessToken:(NSString *)accessToken completionHandler:(void (^) (NSString *errorMsg, User* user))handler
{
    BaseAPIHandler *apiHandler = [BaseAPIHandler sharedInstance];
    NSString *token = [NSString stringWithFormat:@"Bearer %@", accessToken];
    [apiHandler.requestSerializer setValue:token forHTTPHeaderField:@"Authorization"];
    [apiHandler callGET:USER_URL parameters:nil progress:nil success:^( id  _Nullable responseObject) {
        
        User *user = [User new];
        [user parseFromJson:responseObject[@"data"]];
        user.token = accessToken;
        [[Util shareManager] saveCustomObject:user];
        handler(@"", user);
        
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error], nil);
    }];
}

-(void)invokePersonalRegistrationWebServiceWithParams_name:(NSString*)name _birth:(NSString*)birth _codepostal:(NSString*)codepostal _email:(NSString*)email _password:(NSString*)password _countrycode:(NSNumber*)countrycode _phone:(NSString*)phone _code_parrainage:(NSString*)code_parrainage _idfacebook:(NSNumber*)idfacebook _stripeToken:(NSString*)stripeToken _cardNumber:(NSString*)cardNumber completionHandler:(void (^) (NSString *errorMsg, User* user))handler{
    
    NSString *role = @"ROLE_USER";
    
    NSDictionary *param = @{
                            API_KEY_IS_PERSONAL    : @YES,
                            API_KEY_NAME           : name,
                            API_KEY_EMAIL          : email,
                            API_KEY_ROLE           : role,
                            API_KEY_DOB            : birth,
                            API_KEY_POSTEL_CODE    : codepostal,
                            API_KEY_COUNTRY_CODE   :@([countrycode intValue]),
                            API_KEY_STRIPE_TOKEN   : stripeToken,
                            API_KEY_PHONE_NUMBER   :@([phone intValue]) ,
                            API_KEY_ADDRESS_LINE_1 : @"a",
                            API_KEY_ADDRESS_LINE_2 : @"a",
                            API_KEY_CITY           : @"a",
                            API_KEY_STATE          : @"a",
                            API_KEY_COUNTRY        : @"a",
                            API_KEY_SPONSOR_CODE   : code_parrainage
                            };
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] initWithDictionary:param];
    if(idfacebook)
    {
        NSString *fbId = [NSString  stringWithFormat:@"%@",idfacebook];
        parameter[API_KEY_SOCIAL_ID] = fbId;
    }else{
        parameter[API_KEY_PASSWORD] = password;
    }
    
    [[BaseAPIHandler sharedInstance] callPOST:REGISTRATION_URL parameters:parameter progress:nil success:^( id  _Nullable responseObject) {

         if(idfacebook)
         {
             NSString *fbId = [NSString  stringWithFormat:@"%@",idfacebook];
             [self invokeAuthentificationWithFacebookWebServiceWithParams_idfacebook:fbId completionHandler:handler];
         }else{
         [self invokeAuthentificationWebServiceWithParams_email:email _password:password completionHandler:handler];
         }
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error],nil);
    }];
}

-(void)invokeProRegistrationWebServiceWithParams_name:(NSString*)name _codepostal:(NSString*)codepostal _email:(NSString*)email _password:(NSString*)password _countrycode:(NSNumber*)countrycode _phone:(NSString*)phone _company:(NSString*)company _siret:(NSString*)siret _tva:(NSString*)tva _code_parrainage:(NSString*)code_parrainage _idfacebook:(NSNumber*)idfacebook _stripeToken:(NSString*)stripeToken _cardNumber:(NSString*)cardNumber completionHandler:(void (^) (NSString *errorMsg, User* user))handler{
    

    NSString *role = @"ROLE_USER";
    NSString *birth = @"1989-09-30";
    NSDictionary *param = @{
                            API_KEY_IS_PERSONAL    : @NO,
                            API_KEY_NAME           : name,
                            API_KEY_EMAIL          : email,
                            API_KEY_ROLE           : role,
                            API_KEY_POSTEL_CODE    : codepostal,
                            API_KEY_COUNTRY_CODE   : @([countrycode intValue]),
                            API_KEY_STRIPE_TOKEN   : stripeToken,
                            API_KEY_PHONE_NUMBER   : @([phone intValue]) ,
                            API_KEY_ADDRESS_LINE_1 : @"a",
                            API_KEY_ADDRESS_LINE_2 : @"a",
                            API_KEY_CITY           : @"a",
                            API_KEY_STATE          : @"a",
                            API_KEY_COUNTRY        : @"a",
                            API_KEY_COMPANY        : company,
                            API_KEY_SIRET          : siret,
                            API_KEY_TVA            : tva,
                            API_KEY_SPONSOR_CODE   : code_parrainage,
                            API_KEY_DOB            : birth,
                            };
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] initWithDictionary:param];
    if(idfacebook)
    {
        NSString *fbId = [NSString  stringWithFormat:@"%@",idfacebook];
        parameter[API_KEY_SOCIAL_ID] = fbId;
    }else{
        parameter[API_KEY_PASSWORD] = password;
    }
    
    [[BaseAPIHandler sharedInstance] callPOST:REGISTRATION_URL parameters:parameter progress:nil success:^( id  _Nullable responseObject) {
        if(idfacebook)
        {
            NSString *fbId = [NSString  stringWithFormat:@"%@",idfacebook];

            [self invokeAuthentificationWithFacebookWebServiceWithParams_idfacebook:fbId completionHandler:handler];
        }else{
            [self invokeAuthentificationWebServiceWithParams_email:email _password:password completionHandler:handler];
        }
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error],nil);
    }];
}

-(void)invokeAuthentificationWebServiceWithParams_email:(NSString*)email _password:(NSString*)password completionHandler:(void (^) (NSString *errorMsg, User* user))handler{
    
    NSDictionary *param = @{
                            API_KEY_EMAIL : email,
                            API_KEY_PASSWORD:password,
                            };
    
    [[BaseAPIHandler sharedInstance] callPOST:AUTH_URL parameters:param progress:nil success:^( id  _Nullable responseObject) {
        
        NSString *token = responseObject[@"token"];
        [self invokeGetUserWithAccessToken:token completionHandler:^(NSString *errorMsg, User *user) {
            if(user)
            {
                user.password_app = password;
                [[Util shareManager] saveCustomObject:user];
            }
            handler(errorMsg,user);
        }];
        
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error], nil);
    }];
}

-(void)invokeAuthentificationWithFMobileWithParams_countryCode:(NSString*)countryCode MobileNumber:(NSString *)mobile completionHandler:(void (^) (NSString *errorMsg))handler
{
    NSDictionary *param = @{
                            API_KEY_PHONE_NUMBER : mobile
                            };
    
    [[BaseAPIHandler sharedInstance] callPOST:SOCIAL_LOGIN_URL parameters:param progress:nil success:^( id  _Nullable responseObject) {
        handler(@"");
    } failure:^(NSError * _Nonnull error) {
        
        NSString *errorInfo = [self getErrorString:error];
        handler(errorInfo);
    }];
}

-(void)invokeAuthentificationWithFMobileWithParams_countryCode:(NSString*)countryCode MobileNumber:(NSString *)mobile otp:(NSString *)otp completionHandler:(void (^) (NSString *errorMsg,User* user))handler
{
    NSDictionary *param = @{
                            API_KEY_PHONE_NUMBER : mobile,
                            API_KEY_OTP          : otp
                            };
    
    [[BaseAPIHandler sharedInstance] callPOST:SOCIAL_LOGIN_URL parameters:param progress:nil success:^( id  _Nullable responseObject) {
        NSString *token = responseObject[@"token"];

        [self invokeGetUserWithAccessToken:token completionHandler:^(NSString *errorMsg, User *user) {
            if(user)
            {
                [[Util shareManager] saveCustomObject:user];
            }
            handler(errorMsg,user);
        }];
    } failure:^(NSError * _Nonnull error) {
        NSString *errorInfo = [self getErrorString:error];
        handler(errorInfo,nil);
    }];
}

-(void)invokeAuthentificationWithFacebookWebServiceWithParams_idfacebook:(NSString*)idfacebook completionHandler:(void (^) (NSString *errorMsg, User* user))handler{
    
    NSDictionary *param = @{
                            API_KEY_SOCIAL_ID: [NSString stringWithFormat:@"%@", idfacebook]
                            };
    
    [[BaseAPIHandler sharedInstance] callPOST:SOCIAL_LOGIN_URL parameters:param progress:nil success:^( id  _Nullable responseObject) {
        
        NSString *token = responseObject[@"token"];
        [self invokeGetUserWithAccessToken:token completionHandler:^(NSString *errorMsg, User *user) {
            if(user)
            {
                user.socialId = [NSString stringWithFormat:@"%@", idfacebook];
                [[Util shareManager] saveCustomObject:user];
            }
            handler(errorMsg,user);
        }];
        
    } failure:^(NSError * _Nonnull error) {
        
        NSString *errorInfo = [self getErrorString:error];
        NSLog(@"\n error %@",errorInfo);
        if([errorInfo isEqualToString:@"social token not found"])
        {
            handler(ERROR_FACEBOOK_ACCOUNT_LINK, nil);
        }else{
            handler(errorInfo, nil);
        }
    }];
}

-(void)invokeValidateEmailWebServiceWithParams_email:(NSString*)email _password:(NSString*)password _code:(NSString*)code _generated:(BOOL)generated completionHandler:(void (^) (NSString *errorMsg, BOOL success))handler{
    
    NSDictionary *param = @{
                            API_KEY_EMAIL:email,
                            };
    if(generated == NO)
    {
        param = @{
                  API_KEY_EMAIL        :email,
                  API_KEY_NEW_PASSWORD :password,
                  API_KEY_TOKEN        :code
                  };
    }
    NSString *url = generated ? FORGOT_PASSWORD : RESET_PASSWORD;
    [[BaseAPIHandler sharedInstance] callPOST:url parameters:param progress:nil success:^( id  _Nullable responseObject) {
        handler(@"", YES);
    
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error], nil);
    }];
}

-(void)invokeUpdateUserWebServiceWithParams_user:(User*)user _name:(NSString*)name _birth:(NSString*)birth _codepostal:(NSString*)codepostal _countrycode:(NSNumber*)countrycode _phone:(NSString*)phone _image:(NSString*)imgage _stripeToken:(NSString*)stripeToken _cardNumber:(NSString*)cardNumber completionHandler:(void (^) (NSString *errorMsg, User* user))handler{
    
    NSDictionary *param = @{
                            API_KEY_NAME : name,
                            API_KEY_DOB : birth,
                            API_KEY_POSTEL_CODE : codepostal,
                            API_KEY_COUNTRY_CODE :@([countrycode intValue]),
                            API_KEY_PHONE_NUMBER :@([phone intValue]),
                            API_KEY_IMAGE : imgage
                            };
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] initWithDictionary:param];
    if(stripeToken && ![stripeToken isEqualToString:@""])
    {
        parameter[API_KEY_STRIPE_TOKEN] = stripeToken;
    }
    NSString *token = user.token;
    NSString *socialId = user.socialId;
    NSString *password = user.password_app;
    
    NSString *url = [NSString stringWithFormat:@"%@/%@",UPDATE_USER_URL,user.idUser];
    [[BaseAPIHandler sharedInstance] callPUT:url parameters:parameter success:^( id  _Nullable responseObject) {
        
        
        [self invokeGetUserWithAccessToken:token completionHandler:^(NSString *errorMsg, User *user) {
            if(user)
            {
                user.socialId = [NSString stringWithFormat:@"%@", socialId];
                user.password_app = password;
                user.token = token;
                [[Util shareManager] saveCustomObject:user];
            }
            handler(@"", user);
        }];
        
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error], nil);
    }];
}

-(void)invokeUpdateUserWebServiceWithParams_user:(User*)user _name:(NSString*)name _company:(NSString*)companyname _siret:(NSString*)siret _tva:(NSString*)tva _codepostal:(NSString*)codepostal _countrycode:(NSNumber*)countrycode _phone:(NSString*)phone _image:(NSString*)imgage _stripeToken:(NSString*)stripeToken _cardNumber:(NSString*)cardNumber completionHandler:(void (^) (NSString *errorMsg, User* user))handler {
    
    NSDictionary *param = @{
                            API_KEY_NAME         : name,
                            API_KEY_POSTEL_CODE  : codepostal,
                            API_KEY_COUNTRY_CODE : @([countrycode intValue]),
                            API_KEY_PHONE_NUMBER : @([phone intValue]),
                            API_KEY_COMPANY      : companyname,
                            API_KEY_SIRET        : siret,
                            API_KEY_TVA          : tva,
                            API_KEY_IMAGE        : imgage,
                            API_KEY_DOB          : @"1993-02-21"
                            };
    
    NSString *token = user.token;
    NSString *socialId = user.socialId;
    NSString *password = user.password_app;
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] initWithDictionary:param];
    if(stripeToken && ![stripeToken isEqualToString:@""])
    {
        parameter[API_KEY_STRIPE_TOKEN] = stripeToken;
    }
    NSString *url = [NSString stringWithFormat:@"%@/%@",UPDATE_USER_URL,user.idUser];
    [[BaseAPIHandler sharedInstance] callPUT: url parameters:param success:^( id  _Nullable responseObject) {
        
        [self invokeGetUserWithAccessToken:token completionHandler:^(NSString *errorMsg, User *user) {
            if(user)
            {
                user.socialId = [NSString stringWithFormat:@"%@", socialId];
                user.password_app = password;
                user.token = token;
                [[Util shareManager] saveCustomObject:user];
            }
            handler(@"", user);
        }];
        
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error], nil);
    }];
}

-(void)invokeUploadPhotoWebServiceWithParams_user:(User*)user _image:(UIImage*)image completionHandler:(void (^) (NSString *errorMsg, NSString* path))handler{
    
    NSData *imageData = UIImagePNGRepresentation(image);
    NSString * base64String = @"data:image/png;base64,";
  base64String = [NSString stringWithFormat:@"%@%@", base64String ,[imageData base64EncodedStringWithOptions:0]];
    if(user.ispersonnel)
    {
    [self invokeUpdateUserWebServiceWithParams_user:user _name:user.name _birth:user.birth _codepostal:user.codepostal _countrycode:user.countrycode _phone:user.phone _image:base64String _stripeToken:user.stripeToken _cardNumber:user.creditCard completionHandler:^(NSString *errorMsg, User *user) {
        handler(errorMsg,user != nil ? user.photo : @"" );
    }];
    }else{
        [self invokeUpdateUserWebServiceWithParams_user:user _name:user.name _company:user.company _siret:user.siret _tva:user.tva _codepostal:user.codepostal _countrycode:user.countrycode _phone:user.phone _image:base64String _stripeToken:user.stripeToken _cardNumber:user.creditCard completionHandler:^(NSString *errorMsg, User *user) {
            handler(errorMsg,user != nil ? user.photo : @"" );
        }];
    }
}

-(void)invokeUpdateDriverWebServiceWithParams_user:(User*)user completionHandler:(void (^) (NSString *errorMsg, User* user))handler{
    
    NSNumber *carId = user.carIdForDriver;
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithDictionary:@{
                            API_KEY_STATUS       : user.statut,
                            API_KEY_WORK_CITY_ID : user.workCity,
                            API_KEY_DOB          : user.birth,
                            API_KEY_POSTEL_CODE  : user.codepostal,
                            API_KEY_COUNTRY_CODE : user.countrycode,
                            API_KEY_PHONE_NUMBER : @([user.phone longLongValue]),
                            API_KEY_NAME         : user.name,
                            }];
    
    if(carId != nil)
    {
        param[API_KEY_CAR_ID] = carId;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@/%@",UPDATE_DRIVER_URL,user.idcustomer];
    [[BaseAPIHandler sharedInstance] callPUT:url parameters:param  success:^( id  _Nullable responseObject) {
        
        NSString *token = user.token;
        NSString *password = user.password_app;
        [self invokeGetUserWithAccessToken:token completionHandler:^(NSString *errorMsg, User *user) {
            if(user)
            {
                user.password_app = password;
                user.token = token;
                [[Util shareManager] saveCustomObject:user];
            }
            handler(@"", nil);
        }];
        
    } failure:^(NSError * _Nonnull error) {
       //handler(@"", nil);

        handler([self getErrorString:error], nil);
    }];
}

-(void)invokeLogoutWebServiceWithParams_User:(User*)user completionHandler:(void (^) (NSDictionary* json, NSError *error, NSNumber* iduser))handler{
    
    NSNumber *userId = user.idUser;
    [[BaseAPIHandler sharedInstance] callDELETE:LOGOUT_URL  parameters:nil success:^( id  _Nullable responseObject) {
        
        handler(nil, nil, userId);
        
    } failure:^(NSError * _Nonnull error) {
       // handler([self getErrorString:error], nil);
        handler(nil, error, userId);
    }];
}

-(void)invokeUpdateUserPointServiceWithParams_user:(User*)user _point:(NSNumber*)points completionHandler:(void (^) (NSString *errorMsg, User* user))handler {
    
    NSDictionary *param = @{
                            API_KEY_POINTS         : points,
                            };
    
    [[BaseAPIHandler sharedInstance] callPOST:USER_POINTS_UPDATE parameters:param progress:nil success:^( id  _Nullable responseObject) {
        
        handler(@"", nil);
        
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error], nil);
    }];
}

-(void)invokeBuyPointWebServiceWithParams_user:(User*)user andIdPoint:(NSNumber*)idpoint completionHandler:(void (^) (NSString *errorMsg))handler {
    
    NSDictionary *param = @{
                            API_KEY_POINT_ID:idpoint
                            };
    [[BaseAPIHandler sharedInstance] callPOST:BUY_POINT_URL parameters:param progress:nil success:^( id  _Nullable responseObject) {
        
        handler(@"");
        
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error]);
    }];
}

-(void)invokeContactWebServiceWithParams:(User*)user _email:(NSString*)email _password:(NSString*)message completionHandler:(void (^) (NSString *errorMsg, User* user))handler{
    
    NSDictionary *param = @{
                            API_KEY_EMAIL      :email,
                            API_KEY_MESSAGE     :message,
                            };
    [[BaseAPIHandler sharedInstance] callPOST:POST_EMAIL parameters:param progress:nil success:^( id  _Nullable responseObject) {
        
        handler(@"", nil);
        
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error], nil);
    }];
}

-(void)invokeNoteCourseWebServiceWithParams_user:(User*)user andCourse:(Course*)course andidUserChangNote:(NSNumber*)idUserChangNote andNote:(NSNumber*)note completionHandler:(void (^) (NSString *errorMsg))handler{
    
    NSDictionary *param = @{
                            API_KEY_USER_ID   :user.idUser,
                            API_KEY_COURSE_ID  :course.idcourse,
                            API_KEY_CHANGE_NOTE_USER_ID:idUserChangNote,
                            API_KEY_NOTE:note
                            };
    [[BaseAPIHandler sharedInstance] callPOST:UPDATE_NOTE_URL parameters:param progress:nil success:^( id  _Nullable responseObject) {
        
        handler(@"");
        
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error]);
    }];
}



-(void)invokeUpdateRequestWebServiceWithParams_user:(User*)user andRequest:(Request*)req completionHandler:(void (^) (NSString *errorMsg, Request* request))handler{
    
    [[BaseAPIHandler sharedInstance] callPOST:CREATE_REQUEST_URL parameters:nil progress:nil success:^( id  _Nullable responseObject) {
        
        
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error], nil);
    }];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    
    
    NSDictionary *entry = [req dictionaryWithPropertiesOfObject:req];
    NSLog(@"entry %@", entry);
    
    
    NSString* addressstart = [entry objectForKey:@"addressstart"];
    NSString* adresseend = [entry objectForKey:@"adresseend"];
    
    NSString*  city= [entry objectForKey:@"city"];
    NSString* dateCreated = [entry objectForKey:@"dateCreated"];
    NSString* dateValidate = [entry objectForKey:@"dateValidate"];
    NSString* distanceEstimate= [entry objectForKey:@"distanceEstimate"];
    NSString* durationEstimate = [entry objectForKey:@"durationEstimate"];
    
    NSNumber* estimatePrice = [entry objectForKey:@"estimatePrice"];//number
    NSNumber* idcategory = [entry objectForKey:@"idcategory"];//number
    NSNumber* idcustomer = [entry objectForKey:@"idcustomer"];//number
    NSNumber* idrequest = [entry objectForKey:@"idrequest"];//number
    
    
    float latend = [[entry objectForKey:@"latend"] floatValue] ;//string
    float latstart = [[entry objectForKey:@"latstart"]floatValue];//string
    float lngend = [[entry objectForKey:@"lngend"]floatValue];//string
    float lngstart = [[entry objectForKey:@"lngstart"]floatValue];//string
    
    
    
    NSString *postString = [NSString stringWithFormat:@"adresseend=%@&addressstart=%@&city=%@&dateCreated=%@&dateValidate=%@&distanceEstimate=%@&durationEstimate=%@&estimatePrice=%@&idcategory=%@&idcustomer=%@&idrequest=%@&latend=%f&latstart=%f&lngend=%f&lngstart=%f",adresseend, addressstart, city, dateCreated, dateValidate, distanceEstimate, durationEstimate, estimatePrice, idcategory, idcustomer, idrequest, latend, latstart, lngend,lngstart];
    
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@%@", SERVER_URL, CREATE_REQUEST_URL,user.token];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",strURL ]];
    
    NSLog(@"😀😀%@",url.absoluteString);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPMethod:@"POST"];
    
    
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(!data)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
            return;
        }
        
        NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"🏁🏁HTTP RESPONSE CODE = %ld", (long)[httpResponse statusCode]);
        
        if([httpResponse statusCode] == 310)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_SESSION_EXPIRED, nil);
            });
        }
        else if([httpResponse statusCode] == 500){
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERROR_UNKNOWM, nil);
            });
        }
        else if([httpResponse statusCode] == 200){
            
            NSLog(@"ICI 2");
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(@"", nil);
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(ERR0R_CONNECTION, nil);
            });
        }
        
    }];
    [postDataTask resume];
}

-(void)invokeCreateRequestWebServiceWithParams_user:(User*)user andRequest:(Request*)req completionHandler:(void (^) (NSString *errorMsg, Request* request))handler{
    
    NSDictionary *param = [req dictionaryWithPropertiesOfObject:req];
    
    [[BaseAPIHandler sharedInstance] callPOST: CREATE_REQUEST_URL parameters:param progress:nil success:^( id  _Nullable responseObject) {
        
        Request *request = [Request new];
        [request parseFromJson:responseObject];
        handler(@"", request);
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error], nil);
    }];
}

-(void)invokeAccepteCourse_Driver:(User*)user andIdRequest:(NSNumber*)idrequest andResponse:(BOOL)reponse completionHandler:(void (^) (NSString *errorMsg, Course* course))handler{
    
    NSString* isAccept = reponse ? @"true" : @"false";
    NSDictionary *param = @{
                            API_KEY_REQUEST_ID :idrequest,
                            API_KEY_ACCEPT : isAccept
                            };
    [[BaseAPIHandler sharedInstance] callPOST:ACCEPTE_COURSE_URL parameters:param progress:nil success:^( id  _Nullable responseObject) {
        
        Course* c_course = [Course new];
        [c_course parseFromJson:responseObject];
        handler(@"", c_course);
        
    } failure:^(NSError * _Nonnull error) {
        handler(@"Vous ne pouvez pas créer cette course !", nil);
       // handler([self getErrorString:error], nil);
    }];
    
}

-(void)invokeMyCoureWebServiceWithParams_User:(User*)user completionHandler:(void (^) (NSString *errorMsg, Course* course, Request* request,NSArray *courseArray))handler{
    
    [[BaseAPIHandler sharedInstance] callGET:MY_COURSE_URL parameters:nil progress:nil success:^( id  _Nullable responseObject) {
        NSArray *jsonArray = responseObject;
        Course* course;
        Request* request;
        NSDictionary* json = [jsonArray objectAtIndex:0];
        course = [Course new];
        request = [Request new];
        [course parseFromJson:json];
        [request parseFromJson:json];
        dispatch_async(dispatch_get_main_queue(), ^{
            handler(@"", course, request,jsonArray);
        });
        
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error], nil,nil,nil);
    }];
}

-(void)invokeDriverCourseWebServiceWithParams_User:(User*)user completionHandler:(void (^) (NSString *errorMsg, Course* course, NSMutableArray* list_request_waiting))handler{
    
    [[BaseAPIHandler sharedInstance] callGET: DRIVER_CURRENT_COURSE_URL parameters:nil progress:nil success:^( id  _Nullable responseObject) {
        Course* course;
        NSMutableArray* list_request_waiting;
        NSDictionary* json_course = responseObject[@"course_actuelle"];
        if([NSNull null] != responseObject[@"course_actuelle"])
        {
            course = [Course new];
            [course parseFromJson:json_course];
        }
        if ([responseObject[@"requests_waiting"] isKindOfClass:[NSArray class]]) {
            NSArray* j_requests_waiting = responseObject[@"requests_waiting"];
            list_request_waiting = [NSMutableArray new];
            for(NSDictionary* j_request in j_requests_waiting)
            {
                Request* request = [Request new];
                [request parseFromJson:j_request];
                [list_request_waiting addObject:request];
            }
        }
        handler(@"", course, list_request_waiting);
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error], nil,nil);
    }];
}

-(void)invokeCustomerCourseWebServiceWithParams_User:(User*)user completionHandler:(void (^) (NSString *errorMsg, Course* course, Request* request))handler{
    
    [[BaseAPIHandler sharedInstance] callGET: CUSTOMER_CURRENT_COURSE_URL parameters:nil progress:nil success:^( id  _Nullable responseObject) {
        
        NSDictionary* json_course = responseObject[@"course"];
        NSDictionary* json_request = responseObject[@"demands"];
        Course* course;
        Request* request;
        if([NSNull null] != responseObject[@"course"])
        {
            course = [Course new];
            [course parseFromJson:json_course];
        }
        if([NSNull null] != responseObject[@"demands"])
        {
            request = [Request new];
            [request parseFromJson:json_request];
        }
        handler(@"", course, request);
        
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error], nil,nil);
    }];
}

-(void)invokeUpdateCourseWebServiceWithParams_user:(User*)user andCourse:(Course*)course completionHandler:(void (^) (NSString *errorMsg, Course* request))handler{
    
    NSDictionary *param = @{
                            API_KEY_STATUS:course.statut,
                            API_KEY_USER_INFO_ME:course.userInforme,
                            API_KEY_DRIVE_INFO_ME:course.driverInforme,
                            };
    
    [[BaseAPIHandler sharedInstance] callPOST:UPDATE_COURSE_URL parameters:param progress:nil success:^( id  _Nullable responseObject) {
        Course* course = [Course new];
        [course parseFromJson:responseObject[@"data"]];
        handler(@"", course);
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error], nil);
    }];
    //PUT
}


-(void)invokeDriverDispo_user:(User*)user andIdCat:(NSNumber*)idcat andCity:(NSString*)city completionHandler:(void (^) (NSString *errorMsg))handler{
    
    NSDictionary *param = @{
                            API_KEY_CATEGORY_ID:idcat,
                            API_KEY_CITY:city
                            };
    
    [[BaseAPIHandler sharedInstance] callPOST:DRIVER_DISPO_URL parameters:param progress:nil success:^( id  _Nullable responseObject) {
        
        handler(@"");
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error]);
    }];
}

-(void)invokeDeleteRequest_user:(User*)user andIdRequest:(NSNumber*)idrequest completionHandler:(void (^) (NSString *errorMsg))handler; {
    
    NSDictionary *param = @{
                            API_KEY_REQUEST_ID:idrequest
                            };
    [[BaseAPIHandler sharedInstance] callPOST:DELETE_REQUEST_URL parameters:param progress:nil success:^( id  _Nullable responseObject) {
        
        handler(@"");
    } failure:^(NSError * _Nonnull error) {
        handler([self getErrorString:error]);
    }];
}

#pragma mark :- Initial Data
-(void)invokeGetPointsWebService:(int)idws completionHandler:(void (^) (NSDictionary* json, NSError *error,NSMutableArray* points))handler{
    
    [[BaseAPIHandler sharedInstance] callGET:GET_Point_URL parameters:nil progress:nil success:^( id  _Nullable responseObject) {
        
        NSDictionary *json = responseObject;
        NSMutableArray* points = [NSMutableArray new];
        
        if ([[json valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
            NSArray* j_points = [json valueForKey:@"data"];
            for(NSDictionary* j_point in j_points)
            {
                WPoint *p = [WPoint new];
                [p parseFromJson:j_point];
                [points addObject:p];
            }
        }
        handler(json, nil, points);
        
    } failure:^(NSError * _Nonnull error) {
        NSString *errorInfo = [self getErrorString:error];
        NSLog(@"\n error %@",errorInfo);
        handler(nil, error, nil);
    }];
}
-(void)invokeGetConciergeriesWebService:(int)idws completionHandler:(void (^) (NSDictionary* json, NSError *error, NSMutableArray* conciergeries))handler{
    
    [[BaseAPIHandler sharedInstance] callGET:GET_CONCIERGE_URL parameters:nil progress:nil success:^( id  _Nullable responseObject) {
        
        NSDictionary *json = responseObject;
        NSMutableArray* conciergeries = [NSMutableArray new];
        
        if ([[json valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
            NSArray* j_conciergeries = [json valueForKey:@"data"];
            for(NSDictionary* j_conciergerie in j_conciergeries)
            {
                Conciergerie *c = [Conciergerie new];
                [c parseFromJson:j_conciergerie];
                [conciergeries addObject:c];
            }
        }
        handler(json, nil, conciergeries);
        
    } failure:^(NSError * _Nonnull error) {
        NSString *errorInfo = [self getErrorString:error];
        NSLog(@"\n error %@",errorInfo);
        handler(nil, error, nil);
    }];
}

-(void)invokeGetListCategoryWebService:(int)idws completionHandler:(void (^) (NSDictionary* json, NSError *error,NSMutableArray* categories))handler{
    
    [[BaseAPIHandler sharedInstance] callGET:GET_CATEGORY_URL parameters:nil progress:nil success:^( id  _Nullable responseObject) {
        
        NSDictionary *json = responseObject;
        NSMutableArray* categories = [NSMutableArray new];
        
        
        if ([[json valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
            NSArray* j_categories = [json valueForKey:@"data"];
            for(NSDictionary* j_category in j_categories)
            {
                Categorie *c = [Categorie new];
                [c parseFromJson:j_category];
                [categories addObject:c];
            }
        }
        handler(json, nil, categories);
    } failure:^(NSError * _Nonnull error) {
        NSString *errorInfo = [self getErrorString:error];
        NSLog(@"\n error %@",errorInfo);
        handler(nil, error, nil);
    }];
}

-(void)invokeGetListCarsWebService:(int)idws completionHandler:(void (^) (NSDictionary* json, NSError *error,NSMutableArray* cars))handler{
    
    [[BaseAPIHandler sharedInstance] callPOST:GET_CARS_URL parameters:nil progress:nil success:^( id  _Nullable responseObject) {
        NSMutableArray* cars = [NSMutableArray new];
        if ([responseObject[@"data"] isKindOfClass:[NSArray class]]) {
            for(NSDictionary* j_car in responseObject)
            {
                Car *c = [Car new];
                [c parseFromJson:j_car];
                [cars addObject:c];
            }
        }
        handler(responseObject, nil, cars);
    } failure:^(NSError * _Nonnull error) {
        NSString *errorInfo = [self getErrorString:error];
        NSLog(@"\n error %@",errorInfo);
        handler(nil, error, nil);
    }];
}

-(void)invokeGetListCityWebService:(int)idws completionHandler:(void (^) (NSDictionary* json, NSError *error,NSMutableArray* cities))handler{
    [[BaseAPIHandler sharedInstance] callGET:GET_CITY_URL parameters:nil progress:nil success:^( id  _Nullable responseObject) {
        
        NSMutableArray* cities = [NSMutableArray new];
        
        if ([responseObject isKindOfClass:[NSArray class]]) {
            for(NSDictionary* j_city in responseObject)
            {
                WCity *c = [WCity new];
                [c parseFromJson:j_city];
                [cities addObject:c];
            }
        }
        handler(responseObject, nil, cities);
    } failure:^(NSError * _Nonnull error) {
        NSString *errorInfo = [self getErrorString:error];
        NSLog(@"\n error %@",errorInfo);
        handler(nil, error, nil);
    }];
}

#pragma mark:- Synchronus Data
//synchronus get data

-(NSMutableURLRequest*)initialiseWebServiceWithUrl:(NSString*)urlStr andWithJson:(BOOL) withJson andData:(NSData*) requestData{
    
    NSLog(@"\n APi name %@",urlStr);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SERVER_BASE_URL, urlStr ]];
    NSLog(@"😀😀%@",url.absoluteString);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];

        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    [request setHTTPMethod:@"GET"];
    
    [request setValue:[[Util shareManager] appVersion] forHTTPHeaderField:@"appVersion"];
    [request setValue:[[Util shareManager] tokenFcm] forHTTPHeaderField:@"tokenFcm"];
    [request setValue:[[Util shareManager] deviceId] forHTTPHeaderField:@"deviceId"];
    [request setValue:[[Util shareManager] typeDevice] forHTTPHeaderField:@"typeDevice"];
    User *user = [[Util shareManager] loadSavedUser];
    if(user)
    {
        NSString *token = [NSString stringWithFormat:@"Bearer %@", user.token];
        [request setValue:token forHTTPHeaderField:@"Authorization"];
    }
    return request;
}

-(void)invokeGetPointsWebService:(int)idws{
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:GET_Point_URL andWithJson:NO andData:nil];
    NSURLResponse *response;
    NSError *error;
    NSData* data = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
    if(!data)
    {
        return;
    }
    NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if(error!=nil)
    {
        NSLog(@"error = %@",error);
    }
    else {
        NSMutableArray* points = [NSMutableArray new];
        
        if ([[json valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
            NSArray* j_points = [json valueForKey:@"data"];
            for(NSDictionary* j_point in j_points)
            {
                WPoint *p = [WPoint new];
                [p parseFromJson:j_point];
                [points addObject:p];
            }
        }
        [[SharedVar shareManager] setMainPoints:points];
    }
}

-(void)invokeGetConciergeriesWebService:(int)idws{
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:GET_CONCIERGE_URL andWithJson:NO andData:nil];
    NSURLResponse *response;
    NSError *error;
    NSData* data = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
    if(!data)
    {
        return;
    }
    NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if(error!=nil)
    {
        NSLog(@"error = %@",error);
    }
    else {
        NSMutableArray* conciergeries = [NSMutableArray new];
        
        if ([[json valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
            NSArray* j_conciergeries = [json valueForKey:@"data"];
            for(NSDictionary* j_conciergerie in j_conciergeries)
            {
                Conciergerie *c = [Conciergerie new];
                [c parseFromJson:j_conciergerie];
                [conciergeries addObject:c];
            }
        }
        [[SharedVar shareManager] setMainConciergeries:conciergeries];
    }
}

-(void)invokeGetCategortWebService:(int)idws{
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:GET_CATEGORY_URL andWithJson:NO andData:nil];
    NSURLResponse *response;
    NSError *error;
    NSData* data = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
    if(!data)
    {
        return;
    }
    NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if(error!=nil)
    {
        NSLog(@"error = %@",error);
    }
    else {
        NSMutableArray* categories = [NSMutableArray new];
        
        if ([[json valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
            NSArray* j_categories = [json valueForKey:@"data"];
            for(NSDictionary* j_category in j_categories)
            {
                Categorie *c = [Categorie new];
                [c parseFromJson:j_category];
                [categories addObject:c];
            }
        }
        [[SharedVar shareManager] setMainCategories:categories];
    }
}
-(void)invokeGetListCarsWebService:(int)idws {
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:GET_CARS_URL andWithJson:NO andData:nil];
    [request setHTTPMethod:@"GET"];
    NSURLResponse *response;
    NSError *error;
    NSData* data = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
    
    if(!data)
    {
        return;
    }
    
    NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if(error!=nil)
    {
        NSLog(@"error = %@",error);
    }
    else {
        NSMutableArray* cars = [NSMutableArray new];
        if ([json[@"data"] isKindOfClass:[NSArray class]]) {
            for(NSDictionary* j_car in json[@"data"])
            {
                Car *c = [Car new];
                [c parseFromJson:j_car];
                [cars addObject:c];
            }
        }
        [[SharedVar shareManager] setMainCars:cars];
    }
}

-(void)invokeGetListCityWebService:(int)idws{
    
    NSMutableURLRequest *request = [self initialiseWebServiceWithUrl:GET_CITY_URL andWithJson:NO andData:nil];
    [request setHTTPMethod:@"GET"];
    NSURLResponse *response;
    NSError *error;
    NSData* data = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
    if(!data)
    {
        return;
    }
    NSString * resp_text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    NSLog(@"🏁🏁HTTP RESPONSE = %@", resp_text);
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if(error!=nil)
    {
        NSLog(@"error = %@",error);
    }
    else {
        
        NSMutableArray* cities = [NSMutableArray new];
        if ([json[@"data"] isKindOfClass:[NSArray class]]) {
            for(NSDictionary* j_city in json[@"data"])
            {
                WCity *c = [WCity new];
                [c parseFromJson:j_city];
                [cities addObject:c];
            }
        }
        [[SharedVar shareManager] setMainCities:cities];
    }
}

#pragma mark - Error handling

-(NSString*)getErrorString:(NSError*)error
{
    NSString *errorInfo = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
    if(errorInfo.length != 0)
    {
        NSData *jsonData = [errorInfo dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error;
        id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
        
        if (error) {
            NSLog(@"\nError in json parsing unknown");
            return ERROR_UNKNOWM;
        }
        else
        {
            NSLog(@"\nError %@",jsonObject[@"message"]);
            return jsonObject[@"message"];
        }
    }
    NSLog(@"\nError unknown");
    return ERROR_UNKNOWM;
}

@end


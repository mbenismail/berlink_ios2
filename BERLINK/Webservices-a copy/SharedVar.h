//
//  SharedVar.h
//  BERLINK
//
//  Created by BERLINK on 2/8/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"
#import "SWRevealViewController.h"
#import "User.h"
#import "AccountLeftViewController.h"
#import "AccountCenterViewController.h"
#import "SWRevealViewController.h"
#import "AccountDriverLeftViewController.h"
#import "UpdateDriveInfoViewController.h"
#import "WaitingViewController.h"
#import "CourseViewController.h"
#import "FinCourseViewController.h"
#import "Request.h"
#import "Categorie.h"

@interface SharedVar : NSObject

+(SharedVar*)shareManager;
@property (nonatomic, strong) User* mainUser;
@property (nonatomic, strong) ViewController* mainViewController;
@property (nonatomic, strong) UIViewController* latestViewController;
@property (nonatomic, strong) SWRevealViewController* revealController;
@property (nonatomic, strong) NSMutableArray* mainPoints;
@property (nonatomic, strong) NSMutableArray* mainConciergeries;
@property (nonatomic, strong) NSMutableArray* mainCategories;
@property (nonatomic, strong) NSMutableArray* mainCars;
@property (nonatomic, strong) NSMutableArray* mainCities;

@property (nonatomic, strong) AccountLeftViewController *accountLeftVC;
@property (nonatomic, strong) AccountDriverLeftViewController *accountDriverLeftVC;

-(void)userLogout;
-(void)goToAccount:(UIViewController*) viewController;
-(void)goToDriverAccount:(UIViewController*) viewController;
-(SWRevealViewController*)goToAccount;
-(SWRevealViewController*)goToDriverAccount;
-(SWRevealViewController*)goToWaitingView;
-(SWRevealViewController*)goToCourseView;
-(SWRevealViewController*)goToFinCourseView;
-(UIStoryboard*)mystoryboard;
-(NSString*) getCatNameByID:(NSNumber*) id_cat;
-(NSNumber*) getCatNoteByID:(NSNumber*) id_cat;
-(NSNumber*) getCatIdByIdBrand:(NSNumber*) id_brand;
-(Categorie*) getCategoryByID:(NSNumber*) id_cat;
-(NSString*) getCityNameByID:(NSNumber*) id_city;
-(NSString*) getCatNameByIdBrand:(NSNumber*) id_brand;

-(SWRevealViewController*)gotoCourseRequestView:(NSMutableArray*) list_request_waiting;
-(SWRevealViewController*)gotoRequestWaitingView:(Request*) request;
-(NSString*) getCarModelByID:(NSNumber*) id_car;
@end

//
//  AppDelegate.m
//  BERLINK
//
//  Created by Akram on 1/31/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import "Util.h"
#import "WebServiceApi.h"
#import "SharedVar.h"
#import "ViewController.h"
#import "SWRevealViewController.h"
#import "StringConstants.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "PayPalMobile.h"

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;
@import Stripe;
@import GoogleSignIn;


#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate, FIRMessagingDelegate,GIDSignInDelegate>
@end
#endif

// Copied from Apple's header in case it is missing in some cases (e.g. pre-Xcode 8 builds).
#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif

@implementation AppDelegate

NSString *const kGCMMessageIDKey = @"gcm.message_id";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.

    [GIDSignIn sharedInstance].clientID = GOOGLE_CLIENT_KEY;
    [GIDSignIn sharedInstance].delegate = self;
    
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"YOUR_CLIENT_ID_FOR_PRODUCTION",
                                                           PayPalEnvironmentSandbox : @"YOUR_CLIENT_ID_FOR_SANDBOX"}];
    
//    [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:@"pk_test_Q8hKfL6sgSt5uexXzV4GQted"];
    
    [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:@"pk_test_YddzHE5pbYHh9D6l2j3IfRjP"];
    
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    [[UINavigationBar appearance]setBackgroundColor:[UIColor blackColor] ];

    [GMSPlacesClient provideAPIKey:GOOGLE_PLACE_API_KEY];
    [GMSServices provideAPIKey:GOOGLE_MAP_API_KEY];
    
    // Register for remote notifications. This shows a permission dialog on first run, to
    // show the dialog at a more appropriate time move this registration accordingly.

            [UNUserNotificationCenter currentNotificationCenter].delegate = self;
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
            }];
            
            // For iOS 10 data message (sent via FCM)
            [FIRMessaging messaging].delegate = self;

        [[UIApplication sharedApplication] registerForRemoteNotifications];
        // [END register_for_notifications]
    
    // [START configure_firebase]
    [FIRApp configure];
    // [END configure_firebase]
    // [START add_token_refresh_observer]
    // Add observer for InstanceID token refresh callback.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor=[UIColor blackColor];
    
/*
    [[WebServiceApi shareManager] invokeGetCategortWebService:1];
    [[WebServiceApi shareManager] invokeGetConciergeriesWebService:1];
    [[WebServiceApi shareManager] invokeGetPointsWebService:1];
    [[WebServiceApi shareManager] invokeGetListCarsWebService:1];
    [[WebServiceApi shareManager] invokeGetListCityWebService:1];*/
    [[WebServiceApi shareManager] invokeGetDataWebService:1];
    [[WebServiceApi shareManager] invokeGetListCarsWebService:1];
    [[WebServiceApi shareManager] invokeGetListCityWebService:1];
    [self enterApp];
    [[Util shareManager] loadSplashGifAnimationToView:self.window];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    
    return YES;
}

-(void)enterApp{
 
    
    /*
    if(![[SharedVar shareManager] mainPoints])
    {
        [[WebServiceApi shareManager] invokeGetPointsWebService:1 completionHandler:^(NSDictionary *json, NSError *error, NSMutableArray *points) {
            
            if(!error && points && points.count > 0 )
            {
                [[SharedVar shareManager] setMainPoints:points];
            }
        }];
    }
    
    if(![[SharedVar shareManager] mainCategories])
    {
        [[WebServiceApi shareManager] invokeGetListCategoryWebService:1 completionHandler:^(NSDictionary *json, NSError *error, NSMutableArray *categories) {
            
            if(!error  && categories && categories.count)
            {
                [[SharedVar shareManager] setMainCategories:categories];
            }
        }];
    }
    
    if(![[SharedVar shareManager] mainConciergeries])
    {
        [[WebServiceApi shareManager] invokeGetConciergeriesWebService:1 completionHandler:^(NSDictionary *json, NSError *error, NSMutableArray *conciergeries) {
         
            if(!error  && conciergeries && conciergeries.count )
            {
                [[SharedVar shareManager] setMainConciergeries:conciergeries];
            }
        }];
    }
    */
    
    if(![[SharedVar shareManager] mainPoints])
    {
        [[WebServiceApi shareManager] invokeGetDataWebService:1 completionHandler:^(NSDictionary *json, NSError *error, NSMutableArray *points, NSMutableArray *conciergeries, NSMutableArray *categories) {
            
            
            if(!error && points && points.count > 0 && conciergeries && conciergeries.count && categories && categories.count)
            {
                [[SharedVar shareManager] setMainPoints:points];
                [[SharedVar shareManager] setMainConciergeries:conciergeries];
                [[SharedVar shareManager] setMainCategories:categories];
            }
        }];
    }
    
    if(![[SharedVar shareManager] mainCars])
    {
        [[WebServiceApi shareManager] invokeGetListCarsWebService:1 completionHandler:^(NSDictionary *json, NSError *error, NSMutableArray *cars) {
            
            
            if(!error && cars && cars.count > 0)
            {
                [[SharedVar shareManager] setMainCars:cars];
            }
        }];
    }
    
    if(![[SharedVar shareManager] mainCities])
    {
        [[WebServiceApi shareManager] invokeGetListCityWebService:1 completionHandler:^(NSDictionary *json, NSError *error, NSMutableArray *cities) {
            
            
            if(!error && cities && cities.count > 0)
            {
                [[SharedVar shareManager] setMainCities:cities];
            }
        }];
    }
    
    UINavigationController *navController;
    User* user = [[Util shareManager] loadSavedUser];
    if(!user)
    {
        ViewController *loginController = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"ViewController"]; //or the homeController
        navController = [[UINavigationController alloc] initWithRootViewController:loginController];
        self.window.rootViewController = navController;
    }
    else{
        [self removeAllViewController];
        SWRevealViewController *vcontroller;
        if([user.role isEqualToString:ROLE_USER])
            vcontroller = [[SharedVar shareManager] goToAccount];
        else
            vcontroller = [[SharedVar shareManager] goToDriverAccount];
        self.window.rootViewController = vcontroller;
    }
    
    
    [self.window setTintColor:[UIColor whiteColor]];
    
    [self.window makeKeyAndVisible];
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
   /* BOOL googleHandel = [[GIDSignIn sharedInstance] handleURL:url
                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    return handled || googleHandel;*/
    return handled;
} 

-(void)gotoWaitingView{
    [self removeAllViewController];
    SWRevealViewController *vcontroller;
    vcontroller = [[SharedVar shareManager] goToWaitingView];

    self.window.rootViewController = vcontroller;
    [self.window setTintColor:[UIColor whiteColor]];
    
    [self.window makeKeyAndVisible];
    
}

-(void)gotoCourseView{
    [self removeAllViewController];
    SWRevealViewController *vcontroller;
    vcontroller = [[SharedVar shareManager] goToCourseView];
    
    self.window.rootViewController = vcontroller;
    [self.window setTintColor:[UIColor whiteColor]];
    
    [self.window makeKeyAndVisible];
    
}

-(void)gotoFinCourseView{
    [self removeAllViewController];
    SWRevealViewController *vcontroller;
    vcontroller = [[SharedVar shareManager] goToFinCourseView];
    
   
    self.window.rootViewController = vcontroller;
    [self.window setTintColor:[UIColor whiteColor]];
    
    [self.window makeKeyAndVisible];
    
}

-(void)gotoCourseRequestView:(NSMutableArray*) list_request_waiting{
    [self removeAllViewController];
    SWRevealViewController *vcontroller;
    vcontroller = [[SharedVar shareManager] gotoCourseRequestView:list_request_waiting];
    
    
    self.window.rootViewController = vcontroller;
    [self.window setTintColor:[UIColor whiteColor]];
    
    [self.window makeKeyAndVisible];
    
}

-(void)gotoRequestWaitingView:(Request*) request{
    [self removeAllViewController];
    SWRevealViewController *vcontroller;
    vcontroller = [[SharedVar shareManager] gotoRequestWaitingView:request];
    
    
    self.window.rootViewController = vcontroller;
    [self.window setTintColor:[UIColor whiteColor]];
    
    [self.window makeKeyAndVisible];
    
}

-(void)removeAllViewController{

    if([[SharedVar shareManager] revealController])
    {
        NSArray* tempVCA = [(UINavigationController*)([[SharedVar shareManager] revealController].rearViewController) viewControllers];
        
        for(UIViewController *tempVC in tempVCA)
        {
            if([tempVC isKindOfClass:[UIViewController class]])
            {
                [tempVC removeFromParentViewController];
            }
        }
        
        tempVCA = [(UINavigationController*)([[SharedVar shareManager] revealController].frontViewController) viewControllers];
        
        for(UIViewController *tempVC in tempVCA)
        {
            if([tempVC isKindOfClass:[UIViewController class]])
            {
                [tempVC removeFromParentViewController];
            }
        }
        
        [[[SharedVar shareManager] revealController] removeFromParentViewController];
    }
    
    NSArray* tempVCA = [self.window.rootViewController.navigationController viewControllers];
    
    for(UIViewController *tempVC in tempVCA)
    {
        if([tempVC isKindOfClass:[UIViewController class]])
        {
            [tempVC removeFromParentViewController];
        }
    }
    [self.window.rootViewController removeFromParentViewController];
    NSArray *subViewArray = [self.window subviews];
    for (id obj in subViewArray)
    {
        [obj removeFromSuperview];
    }
}

// [START receive_message]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    [self receivePush:userInfo];

}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    [self receivePush:userInfo];
    
    completionHandler(UIBackgroundFetchResultNewData);
}
// [END receive_message]


// [START ios_10_message_handling]
// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    // Print message ID.
    NSDictionary *userInfo = notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    [self receivePush:userInfo];

    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionNone);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    
    [self receivePush:userInfo];
    
    completionHandler();
}
#endif
// [END ios_10_message_handling]

// [START ios_10_data_message_handling]
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Receive data message on iOS 10 devices while app is in the foreground.
- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    // Print full message
    
    [self receivePush:remoteMessage.appData];
}

-(void) receivePush:(NSDictionary *)userInfo {
    NSLog(@"receivePush");
    NSString* role = [[[SharedVar shareManager] mainUser] role];
    if([role isEqualToString:ROLE_DRIVER])
    {
        [[WebServiceApi shareManager] invokeDriverCourseWebServiceWithParams_User:[[SharedVar shareManager] mainUser] completionHandler:^(NSString *errorMsg, Course *course, NSMutableArray* list_request_waiting) {
            if(course && course.idcourse)
            {
                //self.currentCourse = course;
                //[self gotoCourseView];
            }
            else {
                [self gotoCourseRequestView:list_request_waiting];
            }
            
        }];
    }
    else if([role isEqualToString:ROLE_USER]){
        
        [[WebServiceApi shareManager] invokeCustomerCourseWebServiceWithParams_User:[[SharedVar shareManager] mainUser] completionHandler:^(NSString *errorMsg, Course *course, Request* request) {
            if(course && course.idcourse)
            {
                self.currentCourse = course;
                [self gotoWaitingView];
            }
            else if(request && request.idrequest){
                AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                [appd gotoRequestWaitingView:request];
            }
        }];
        
    }
}

-(void)startUpdateCourseLocation{
    [locationManager startUpdatingLocation];
}

-(void)stopUpdateCourseLocation{
   if(locationManager)
   {
       [locationManager stopUpdatingLocation];
       locationManager = nil;
   }
    _currentCourse = nil;
    if(db)
       [db removeAllObservers];
}
#endif
// [END ios_10_data_message_handling]

// [START refresh_token]
- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    self.strDeviceToken = refreshedToken;

    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}
// [END refresh_token]

// [START connect_to_fcm]
- (void)connectToFcm {
    // Won't connect since there is no token
    if (![[FIRInstanceID instanceID] token]) {
        return;
    }
    
    // Disconnect previous FCM connection if it exists.
    [[FIRMessaging messaging] disconnect];
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    
    if (currentLocation != nil) {
        [self updateChildren:currentLocation.coordinate.latitude andLng:currentLocation.coordinate.longitude];
    }
}

-(void)updateChildren:(double)lat andLng:(double)lng{
    
    FIRDatabaseReference *rootRef= [[FIRDatabase database] referenceWithPath:@"course"];
   
    NSString* userStr = [NSString stringWithFormat:@"/course_%d",_currentCourse.idcourse.intValue];
    NSLog(@"firebase course %@", userStr);
    
    NSMutableDictionary* userValue = [NSMutableDictionary new];
    [userValue setValue:[NSNumber numberWithDouble:lat] forKey:@"lat_driver"];
    [userValue setValue:[NSNumber numberWithDouble:lng] forKey:@"lng_driver"];
    NSLog(@"firebase statut %d", _current_statut);

    [userValue setValue:[NSNumber numberWithDouble:_current_statut] forKey:@"statut"];
    
    NSMutableDictionary* cValue = [NSMutableDictionary new];
    [cValue setValue:userValue forKey:userStr];
    
    [rootRef updateChildValues:cValue];
    
    
}
// [END connect_to_fcm]

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Unable to register for remote notifications: %@", error);
}

// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
// If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
// the InstanceID token.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"APNs token retrieved: %@", deviceToken);
    
    // With swizzling disabled you must set the APNs token here.
    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeUnknown];
}

// [START connect_on_active]
- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self connectToFcm];
}
// [END connect_on_active]

// [START disconnect_from_fcm]
- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[FIRMessaging messaging] disconnect];
    NSLog(@"Disconnected from FCM");
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self stopUpdateCourseLocation];
}


- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    
    
}

@end

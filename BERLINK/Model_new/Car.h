//
//  Car.h
//  BERLINK
//
//  Created by BERLINK on 4/4/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyObject.h"
#import "Brand.h"

@interface Car : MyObject

@property (nonatomic, strong) NSNumber* isreserved;
@property (nonatomic, strong) NSString* matriculation;
@property (nonatomic, strong) NSString* model;
@property (nonatomic, strong) NSNumber* isarchived;
@property (nonatomic, strong) NSNumber* statu;
@property (nonatomic, strong) NSNumber* idcar;
@property (nonatomic, strong) NSNumber* image;

@property (nonatomic, strong) Brand* brand;

-(void)parseFromJson:(NSDictionary*) json;

@end



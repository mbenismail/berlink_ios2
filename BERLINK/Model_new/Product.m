//
//  Product.m
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "Product.h"

@implementation Product

-(void)parseFromJson:(NSDictionary*) json{
    
    if (![json isKindOfClass:[NSDictionary class]])
        return;
    if ([[json valueForKey:@"id"] isKindOfClass:[NSNumber class]]) {
        self.idconsproduit = [json valueForKey:@"id"];
    }
    if ([[json valueForKey:@"name"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"name"] isEqualToString:@"<null>"]) {
        self.title = [json valueForKey:@"name"];
    }
    if ([[json valueForKey:@"description"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"description"] isEqualToString:@"<null>"]) {
        self.wdescription = [json valueForKey:@"description"];
    }
    if ([[json valueForKey:@"price"] isKindOfClass:[NSNumber class]]) {
        self.point = [json valueForKey:@"price"];
    }
}
@end

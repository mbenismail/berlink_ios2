//
//  User.m
//  BERLINK
//
//  Created by Akram on 2/3/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "User.h"
#import "Car.h"
#import "Brand.h"
#import "WCity.h"
#import "SharedVar.h"
@implementation User
-(void) initialise {
    self.address = @"";
    self.birth = @"";
    self.city = @"";
    //self.codepostal = [NSNumber numberWithInt:0];
    self.company = @"";
    self.created = @"";
    self.email = @"";
    self.firstname = @"";
    self.idcustomer = [NSNumber numberWithInt:0];
    self.isarchived = @"";
    //self.ispersonnel = @"";
    self.lastname = @"";
    self.name = @"";
    //self.phone = [NSNumber numberWithInt:0];
    self.photo = @"";
    self.point = [NSNumber numberWithInt:0];
    self.role = @"";
    self.siret = @"";
    self.token = @"";
    self.tva = @"";
    self.updated = @"";
    
    self.statut = [NSNumber numberWithInt:0];
    self.lat = [NSNumber numberWithInt:0];
    self.lng = [NSNumber numberWithInt:0];
    self.ibn = [NSNumber numberWithInt:0];
    self.idcategorie = [NSMutableArray new];
    self.immatriculation = @"";
    self.idUser = [NSNumber numberWithInt:0];
}

-(void)parseFromJson:(NSDictionary*) json{
    
    if ([[json valueForKey:@"id"] isKindOfClass:[NSNumber class]]) {
        self.idcustomer = [json valueForKey:@"id"];
    }
    
    if ([[json valueForKey:@"role"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"role"] isEqualToString:@"<null>"]) {
        self.role = [json valueForKey:@"role"];
    }
    if ([[json valueForKey:@"email"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"email"] isEqualToString:@"<null>"]) {
        self.email = [json valueForKey:@"email"];
    }
    
    if ([[json valueForKey:@"status"] isKindOfClass:[NSNumber class]]) {
        self.statut = [json valueForKey:@"status"];
    }
    
    NSDictionary *custDetails = json[@"customer_details"];
    if([self.role isEqualToString:@"ROLE_DRIVER"])
    {
         custDetails = json[@"driver_details"];
    }
    NSDictionary *address = custDetails[@"address"];
    
    if ([[json valueForKey:@"address"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"address"] isEqualToString:@"<null>"]) {
        self.address = [json valueForKey:@"address"];
    }
    
    if ([[custDetails valueForKey:@"date_of_birth"] isKindOfClass:[NSString class]] && ![[custDetails valueForKey:@"date_of_birth"] isEqualToString:@"<null>"]) {
        self.birth = [custDetails valueForKey:@"date_of_birth"];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        NSDate* date = [dateFormatter dateFromString:self.birth];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        self.birth = [dateFormatter stringFromDate:date];
    }
    
    if ([[address valueForKey:@"city"] isKindOfClass:[NSString class]] && ![[address valueForKey:@"city"] isEqualToString:@"<null>"]) {
        self.city = [address valueForKey:@"city"];
    }
    if ([[address valueForKey:@"postal_code"] isKindOfClass:[NSString class]] && ![[address valueForKey:@"postal_code"] isEqualToString:@"<null>"]) {
        self.codepostal = [address valueForKey:@"postal_code"];
    }
    
    if ([[custDetails valueForKey:@"company_name"] isKindOfClass:[NSString class]] && ![[custDetails valueForKey:@"company_name"] isEqualToString:@"<null>"]) {
        self.company = [custDetails valueForKey:@"company_name"];
    }
    if ([[json valueForKey:@"created_on"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"created_on"] isEqualToString:@"<null>"]) {
        self.created = [json valueForKey:@"created_on"];
    }
    
    if ([[json valueForKey:@"firstname"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"firstname"] isEqualToString:@"<null>"]) {
        self.firstname = [json valueForKey:@"firstname"];
    }
   

    if ([[json valueForKey:@"isarchived"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"isarchived"] isEqualToString:@"<null>"]) {
        self.isarchived = [json valueForKey:@"isarchived"];
    }
    self.ispersonnel = [[custDetails valueForKey:@"is_personal"] boolValue];
    
    if ([[json valueForKey:@"lastname"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"lastname"] isEqualToString:@"<null>"]) {
        self.lastname = [json valueForKey:@"lastname"];
    }
    
    if ([[json valueForKey:@"name"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"name"] isEqualToString:@"<null>"]) {
        self.name = [json valueForKey:@"name"];
    }
    if ([[custDetails valueForKey:@"phone_number"] isKindOfClass:[NSNumber class]]) {
        self.phone = [NSString stringWithFormat:@"%@",[custDetails valueForKey:@"phone_number"]];
    }
    if ([[custDetails valueForKey:@"image"] isKindOfClass:[NSString class]] && ![[custDetails valueForKey:@"image"] isEqualToString:@"<null>"]) {
        self.photo = [custDetails valueForKey:@"image"];
    }
    if ([[custDetails valueForKey:@"points"] isKindOfClass:[NSNumber class]]) {
        self.point = [custDetails valueForKey:@"points"];
    }
   
    if ([[custDetails valueForKey:@"siret_number"] isKindOfClass:[NSString class]] && ![[custDetails valueForKey:@"siret_number"] isEqualToString:@"<null>"]) {
        self.siret = [custDetails valueForKey:@"siret_number"];
    }
    if ([[json valueForKey:@"token"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"token"] isEqualToString:@"<null>"]) {
        self.token = [json valueForKey:@"token"];
    }
    if ([[custDetails valueForKey:@"vat_number"] isKindOfClass:[NSString class]] && ![[custDetails valueForKey:@"vat_number"] isEqualToString:@"<null>"]) {
        self.tva = [custDetails valueForKey:@"vat_number"];
    }
    if ([[json valueForKey:@"updated_on"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"updated_on"] isEqualToString:@"<null>"]) {
        self.updated = [json valueForKey:@"updated_on"];
    }
    
    if ([[json valueForKey:@"stripe_token"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"stripe_token"] isEqualToString:@"<null>"]) {
        self.stripeToken = [json valueForKey:@"stripe_token"];
    }
    if ([[custDetails valueForKey:@"country_code"] isKindOfClass:[NSNumber class]]) {
        self.countrycode = [custDetails valueForKey:@"country_code"];
    }
    
    if ([[custDetails valueForKey:@"stripe_invoice_prefix"] isKindOfClass:[NSString class]] && ![[custDetails valueForKey:@"stripe_invoice_prefix"] isEqualToString:@"<null>"]) {
        self.hashid = [custDetails valueForKey:@"stripe_invoice_prefix"];
    }
    
    if ([[custDetails valueForKey:@"stripe_card_last_4"] isKindOfClass:[NSString class]] && ![[custDetails valueForKey:@"stripe_card_last_4"] isEqualToString:@"<null>"]) {
        self.creditCard = [custDetails valueForKey:@"stripe_card_last_4"];
    }
    
    
    if ([[json valueForKey:@"categorie_id"] isKindOfClass:[NSArray class]]) {
        self.idcategorie = [NSMutableArray new];
        for(NSNumber* cat in [json valueForKey:@"categorie_id"])
        {
            [self.idcategorie addObject:cat];
        }
    }
    
    //driver
    if ([[json valueForKey:@"id"] isKindOfClass:[NSNumber class]]) {
        self.idUser = [json valueForKey:@"id"];
    }
    
    if ([[custDetails valueForKey:@"latitude"] isKindOfClass:[NSNumber class]]) {
        self.lat = [custDetails valueForKey:@"latitude"];
    }
    if ([[custDetails valueForKey:@"longitude"] isKindOfClass:[NSNumber class]]) {
        self.lng = [custDetails valueForKey:@"longitude"];
    }
    if ([[custDetails valueForKey:@"ibn"] isKindOfClass:[NSNumber class]]) {
        self.ibn = [custDetails valueForKey:@"ibn"];
    }
    
    Car *car = [Car new];
    [car parseFromJson:custDetails[@"car"]];
    
    WCity *city = [WCity new];
    [city parseFromJson:custDetails[@"city"]];
    
    self.idcategorie = [[NSMutableArray alloc] initWithObjects:car.brand.categoryId, nil];
    self.immatriculation = car.matriculation;
    self.workCity = city.idcity;
}

-(NSNumber *)carIdForDriver
{
    NSArray *cars = [[SharedVar shareManager] mainCars];
    for(Car * car in cars)
    {
        if([car.matriculation isEqualToString:self.immatriculation]){
            return car.idcar;
        }
    }
    return 0;
}

@end

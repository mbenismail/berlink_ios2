//
//  Course.m
//  BERLINK
//
//  Created by BERLINK on 3/16/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "Course.h"
#import "Product.h"

@implementation Course

-(void)parseFromJson:(NSDictionary*) json{
    
    if ([[json valueForKey:@"idcourse"] isKindOfClass:[NSNumber class]]) {
        self.idcourse = [json valueForKey:@"idcourse"];
    }
    if ([[json valueForKey:@"statut"] isKindOfClass:[NSNumber class]]) {
        self.statut = [json valueForKey:@"statut"];
    }

    if ([[json valueForKey:@"idRequest"] isKindOfClass:[NSNumber class]]) {
        self.idRequest = [json valueForKey:@"idRequest"];
    }
    if ([[json valueForKey:@"idCustomer"] isKindOfClass:[NSNumber class]]) {
        self.idCustomer = [json valueForKey:@"idCustomer"];
    }
    if ([[json valueForKey:@"idCategory"] isKindOfClass:[NSNumber class]]) {
        self.idCategory = [json valueForKey:@"idCategory"];
    }
    
    if ([[json valueForKey:@"phoneDriver"] isKindOfClass:[NSNumber class]]) {
        self.phoneDriver = [json valueForKey:@"phoneDriver"];
    }
    
    if ([[json valueForKey:@"duration"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"duration"] isEqualToString:@"<null>"]) {
        self.duration = [json valueForKey:@"duration"];
    }
    
    if ([[json valueForKey:@"lngend"] isKindOfClass:[NSNumber class]]) {
        self.lngend = [json valueForKey:@"lngend"];
    }
    if ([[json valueForKey:@"lngstart"] isKindOfClass:[NSNumber class]]) {
        self.lngstart = [json valueForKey:@"lngstart"];
    }
    if ([[json valueForKey:@"latend"] isKindOfClass:[NSNumber class]]) {
        self.latend = [json valueForKey:@"latend"];
    }
    if ([[json valueForKey:@"latstart"] isKindOfClass:[NSNumber class]]) {
        self.latstart = [json valueForKey:@"latstart"];
    }
    
    if ([[json valueForKey:@"userInforme"] isKindOfClass:[NSNumber class]]) {
        self.userInforme = [json valueForKey:@"userInforme"];
    }
    
    if ([[json valueForKey:@"driverInforme"] isKindOfClass:[NSNumber class]]) {
        self.driverInforme = [json valueForKey:@"driverInforme"];
    }
    
    if ([[json valueForKey:@"prixTotal"] isKindOfClass:[NSNumber class]]) {
        self.prixTotal = [json valueForKey:@"prixTotal"];
    }
    
    if ([[json valueForKey:@"prixTotal"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"prixTotal"] isEqualToString:@"<null>"]) {
        self.prixTotalStr = [json valueForKey:@"prixTotal"];
    }

    
    if ([[json valueForKey:@"nameDriver"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"nameDriver"] isEqualToString:@"<null>"]) {
        self.nameDriver = [json valueForKey:@"nameDriver"];
    }
   
    if ([[json valueForKey:@"adressStart"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"adressStart"] isEqualToString:@"<null>"]) {
        self.adressStart = [json valueForKey:@"adressStart"];
    }
    if ([[json valueForKey:@"adressEnd"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"adressEnd"] isEqualToString:@"<null>"]) {
        self.adressEnd = [json valueForKey:@"adressEnd"];
    }
    
    
    if ([[json valueForKey:@"dateCreated"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"dateCreated"] isEqualToString:@"<null>"]) {
        self.dateCreated = [json valueForKey:@"dateCreated"];
    }
    
    if ([[json valueForKey:@"datStartCourse"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"datStartCourse"] isEqualToString:@"<null>"]) {
        self.datStartCourse = [json valueForKey:@"datStartCourse"];
    }
    
    if ([[json valueForKey:@"dateEndCourse"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"dateEndCourse"] isEqualToString:@"<null>"]) {
        self.dateEndCourse = [json valueForKey:@"dateEndCourse"];
    }
    
    if ([[json valueForKey:@"distanceEstimate"] isKindOfClass:[NSNumber class]]) {
        self.distanceEstimate = [json valueForKey:@"distanceEstimate"];
    }
    if ([[json valueForKey:@"distance"] isKindOfClass:[NSNumber class]]) {
        self.distance = [json valueForKey:@"distance"];
    }
    
    if ([[json valueForKey:@"pricebid"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"pricebid"] isEqualToString:@"<null>"]) {
        self.pricebid = [json valueForKey:@"pricebid"];
    }
    if ([[json valueForKey:@"durationEstimate"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"durationEstimate"] isEqualToString:@"<null>"]) {
        self.durationEstimate = [json valueForKey:@"durationEstimate"];
    }
    if ([[json valueForKey:@"descriptionCar"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"descriptionCar"] isEqualToString:@"<null>"]) {
        self.descriptionCar = [json valueForKey:@"descriptionCar"];
    }
    if ([[json valueForKey:@"imageCar"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"imageCar"] isEqualToString:@"<null>"]) {
        self.imageCar = [json valueForKey:@"imageCar"];
    }
    if ([[json valueForKey:@"name"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"name"] isEqualToString:@"<null>"]) {
        self.name = [json valueForKey:@"name"];
    }
    if ([[json valueForKey:@"nameCustomer"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"nameCustomer"] isEqualToString:@"<null>"]) {
        self.nameCustomer = [json valueForKey:@"nameCustomer"];
    }
    
    if ([[json valueForKey:@"Products"] isKindOfClass:[NSArray class]]) {
        self.Products = [NSMutableArray new];
        for(NSDictionary* prod in [json valueForKey:@"Products"])
        {
            Product* p = [Product new];
            [p parseFromJson:prod];
            [self.Products addObject:p];
        }
    }
    
}

@end

//
//  Categorie.h
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyObject.h"
#import "Brand.h"

@interface Categorie : MyObject

@property (nonatomic, strong) NSNumber* idcategory;
@property (nonatomic, strong) NSString* range;
@property (nonatomic, strong) NSString* pricepack;
@property (nonatomic, strong) NSString* pricesup; //out_of_support.value
@property (nonatomic, strong) NSNumber* maxbid; //bid_cars
@property (nonatomic, strong) NSNumber* unitinclu; //point
@property (nonatomic, strong) NSString* measure;//unit

@property (nonatomic, strong) NSString* titrevoitures; //not use
@property (nonatomic, strong) NSNumber* note;

@property (nonatomic, strong) NSString* photo;
@property (nonatomic, strong) NSMutableArray* brands;

-(void)parseFromJson:(NSDictionary*) json;


@end



//
//  WPoint.m
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "WPoint.h"

@implementation WPoint
-(void)parseFromJson:(NSDictionary*) json
{
    if ([[json valueForKey:@"idpoints"] isKindOfClass:[NSNumber class]]) {
        self.idpoints = [json valueForKey:@"idpoints"];
    }
    if ([[json valueForKey:@"title"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"title"] isEqualToString:@"<null>"]) {
        self.title = [json valueForKey:@"title"];
    }
    if ([[json valueForKey:@"number"] isKindOfClass:[NSNumber class]]) {
        self.number = [json valueForKey:@"number"];
    }
    if ([[json valueForKey:@"price"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"price"] isEqualToString:@"<null>"]) {
        self.price = [json valueForKey:@"price"];
    }
}
@end

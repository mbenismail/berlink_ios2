//
//  Conciergerie.h
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyObject.h"

@interface Conciergerie : MyObject

@property (nonatomic, strong) NSNumber* idconcierge;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSMutableArray* products;

-(void)parseFromJson:(NSDictionary*) json;

@end



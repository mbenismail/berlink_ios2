//
//  Categorie.m
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "Categorie.h"

@implementation Categorie

//"id": 1,
//"range": "Premium",
//"price_of_pack": 123,
//
//"image": "1547811716_Premium.png",
//"status": true,

//"out_of_support": {
//    "value": 12,
//    "unit": "km",
//    "points": 2
//},
//"bid_cars": 1

-(void)parseFromJson:(NSDictionary*) json
{
    if ([[json valueForKey:@"id"] isKindOfClass:[NSNumber class]]) {
        self.idcategory = [json valueForKey:@"id"];
    }
    if ([[json valueForKey:@"name"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"name"] isEqualToString:@"<null>"]) {
        self.range = [json valueForKey:@"name"];
    }
    if ([[json valueForKey:@"price_of_pack"] isKindOfClass:[NSNumber class]] ) {
        self.pricepack = [NSString stringWithFormat:@"%@", [json valueForKey:@"price_of_pack"]];
    }
    if ([[json valueForKey:@"bid_cars"] isKindOfClass:[NSNumber class]]) {
        self.maxbid = [json valueForKey:@"bid_cars"];
    }
    NSDictionary *outOfSupport  = [json valueForKey:@"out_of_support"];
    
    if ([[outOfSupport valueForKey:@"value"] isKindOfClass:[NSNumber class]] ) {
        self.pricesup = [NSString stringWithFormat:@"%@", [outOfSupport valueForKey:@"value"]];
    }
    if ([[outOfSupport valueForKey:@"points"] isKindOfClass:[NSNumber class]]) {
        self.unitinclu = [outOfSupport valueForKey:@"points"];
    }
    if ([[outOfSupport valueForKey:@"unit"] isKindOfClass:[NSString class]] && ![[outOfSupport valueForKey:@"unit"] isEqualToString:@"<null>"]) {
        self.measure = [outOfSupport valueForKey:@"unit"];
    }
    
    if ([[json valueForKey:@"note"] isKindOfClass:[NSNumber class]]) {
        self.note = [json valueForKey:@"note"];
    }
    if ([[json valueForKey:@"image"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"image"] isEqualToString:@"<null>"]) {
        self.photo = [json valueForKey:@"image"];
    }
    if ([[json valueForKey:@"brands"] isKindOfClass:[NSArray class]]) {
        self.brands = [NSMutableArray new];
        NSArray* j_brands = [json valueForKey:@"brands"];
        for(NSDictionary* j_brand in j_brands)
        {
            Brand *p = [Brand new];
            [p parseFromJson:j_brand];
            [self.brands addObject:p];
        }
    }
    
    if ([[json valueForKey:@"titrevoitures"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"titrevoitures"] isEqualToString:@"<null>"]) {
        self.titrevoitures = [json valueForKey:@"titrevoitures"];
    }
}

@end

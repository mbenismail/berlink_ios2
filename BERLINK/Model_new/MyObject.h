//
//  MyObject.h
//  BERLINK
//
//  Created by BERLINK on 3/3/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import "JSONModelLib.h"

@interface MyObject : JSONModel
- (NSDictionary *) dictionaryWithPropertiesOfObject:(id)obj;
@end

//
//  Car.m
//  BERLINK
//
//  Created by BERLINK on 4/4/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "Car.h"

@implementation Car

-(void)parseFromJson:(NSDictionary*) json{
    
    
    if ([[json valueForKey:@"registration_number"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"registration_number"] isEqualToString:@"<null>"]) {
        self.matriculation = [json valueForKey:@"registration_number"];
    }
    if ([[json valueForKey:@"model"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"model"] isEqualToString:@"<null>"]) {
        self.model = [json valueForKey:@"model"];
    }
    
    if ([[json valueForKey:@"status"] isKindOfClass:[NSNumber class]]) {
        self.statu = [json valueForKey:@"status"];
    }
    if ([[json valueForKey:@"id"] isKindOfClass:[NSNumber class]]) {
        self.idcar = [json valueForKey:@"id"];
    }
    
    if ([[json valueForKey:@"image"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"image"] isEqualToString:@"<null>"]) {
        self.image = [json valueForKey:@"image"];
    }
    
    if ([[json valueForKey:@"brand"] isKindOfClass:[NSDictionary class]]) {
        NSDictionary* brand = [json valueForKey:@"brand"];
        self.brand  = [Brand new];
        [self.brand parseFromJson:brand];
    }

    //not in curent api
    if ([[json valueForKey:@"reserved"] isKindOfClass:[NSNumber class]]) {
        self.isreserved = [json valueForKey:@"reserved"];
    }
    // key not in json
    if ([[json valueForKey:@"delete_on"] isKindOfClass:[NSNumber class]]) {
        self.isarchived = [json valueForKey:@"delete_on"];
    }
}

@end

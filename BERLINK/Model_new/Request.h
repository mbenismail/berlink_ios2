//
//  Request.h
//  BERLINK
//
//  Created by BERLINK on 2/16/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyObject.h"
#import "Bid.h"

@interface Request : MyObject

@property (nonatomic, strong) NSNumber* idrequest;
@property (nonatomic, strong) NSNumber* idcustomer;
@property (nonatomic, strong) NSNumber* idcategory;
@property (nonatomic, strong) NSString* latstart;
@property (nonatomic, strong) NSString* lngstart;
@property (nonatomic, strong) NSString* addressstart;
@property (nonatomic, strong) NSString* latend;
@property (nonatomic, strong) NSString* lngend;
@property (nonatomic, strong) NSString* adresseend;
@property (nonatomic, strong) NSString* dateCreated;
@property (nonatomic, strong) Bid* bid;
@property (nonatomic, strong) NSNumber* pricebid;
@property (nonatomic, strong) NSNumber* estimatePrice;
@property (nonatomic, strong) NSString* city;

@property (nonatomic, strong) NSString* dateValidate;
@property (nonatomic, strong) NSNumber* distanceEstimate;
@property (nonatomic, strong) NSString* durationEstimate;
@property (nonatomic, strong) NSMutableArray* Products;


-(void)parseFromJson:(NSDictionary*) json;

@end

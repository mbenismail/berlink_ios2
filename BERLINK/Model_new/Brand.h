//
//  Brand.h
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyObject.h"

@interface Brand : MyObject
@property (nonatomic, strong) NSNumber* idbrand;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSNumber* statu;
@property (nonatomic, strong) NSString* photo;
@property (nonatomic, strong) NSString* categoryId;

-(void)parseFromJson:(NSDictionary*) json;

@end

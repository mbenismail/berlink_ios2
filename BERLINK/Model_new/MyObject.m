//
//  MyObject.m
//  BERLINK
//
//  Created by BERLINK on 3/3/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "MyObject.h"

@implementation MyObject

- (NSDictionary *) dictionaryWithPropertiesOfObject:(id)obj
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        Class classObject = NSClassFromString([key capitalizedString]);
        
        id object = [obj valueForKey:key];
        
        if (classObject && ![key isEqualToString:@"city"] && object) {
            id subObj = [self dictionaryWithPropertiesOfObject:object];
            [dict setObject:subObj forKey:key];
        }
        else if([object isKindOfClass:[NSArray class]])
        {
            NSLog(@"key1 %@", key);
            NSMutableArray *subObj = [NSMutableArray array];
            for (id o in object) {
                NSLog(@"key2 %@", key);
                    [subObj addObject:[self dictionaryWithPropertiesOfObject:o] ];
            }
            [dict setObject:subObj forKey:key];
        }
        else
        {
            if(object) [dict setObject:object forKey:key];
            else
            {
                //dict[key] = @"";
            }
            
        }
    }
    
    free(properties);
    NSLog(@"dict %@", dict);

    
    return [NSDictionary dictionaryWithDictionary:dict];
}


@end

//
//  Brand.m
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "Brand.h"

@implementation Brand

-(void)parseFromJson:(NSDictionary*) j_brand
{
    if ([[j_brand valueForKey:@"id"] isKindOfClass:[NSNumber class]]) {
        self.idbrand = [j_brand valueForKey:@"id"];
    }
    if ([[j_brand valueForKey:@"name"] isKindOfClass:[NSString class]] && ![[j_brand valueForKey:@"name"] isEqualToString:@"<null>"]) {
        self.title = [j_brand valueForKey:@"name"];
    }
    if ([[j_brand valueForKey:@"status"] isKindOfClass:[NSNumber class]]) {
        self.statu = [j_brand valueForKey:@"status"];
    }
    if ([[j_brand valueForKey:@"image"] isKindOfClass:[NSString class]] && ![[j_brand valueForKey:@"image"] isEqualToString:@"<null>"]) {
        self.photo = [j_brand valueForKey:@"image"];
    }
    if ([[j_brand valueForKey:@"category_id"] isKindOfClass:[NSNumber class]]) {
        self.categoryId = [NSString stringWithFormat:@"%@", [j_brand valueForKey:@"category_id"]];
    }
}
@end


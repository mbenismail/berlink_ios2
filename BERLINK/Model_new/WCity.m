//
//  City.m
//  BERLINK
//
//  Created by BERLINK on 4/4/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "WCity.h"

@implementation WCity

-(void)parseFromJson:(NSDictionary*) json{
    
    if ([[json valueForKey:@"id"] isKindOfClass:[NSNumber class]]) {
        self.idcity = [json valueForKey:@"id"];
    }
    if ([[json valueForKey:@"name"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"name"] isEqualToString:@"<null>"]) {
        self.range = [json valueForKey:@"name"];
    }
    self.status = [json valueForKey:@"status"];
}
@end

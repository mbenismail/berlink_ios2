//
//  Conciergerie.m
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "Conciergerie.h"
#import "Product.h"
@implementation Conciergerie

-(void)parseFromJson:(NSDictionary*) json
{
    
    if ([[json valueForKey:@"id"] isKindOfClass:[NSNumber class]]) {
        self.idconcierge = [json valueForKey:@"id"];
    }
    if ([[json valueForKey:@"name"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"name"] isEqualToString:@"<null>"]) {
        self.title = [json valueForKey:@"name"];
    }
    if ([[json valueForKey:@"image"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"image"] isEqualToString:@"<null>"]) {
        self.image = [json valueForKey:@"image"];
    }
    if ([[json valueForKey:@"products"] isKindOfClass:[NSArray class]]) {
        self.products = [NSMutableArray new];
        NSArray* j_products = [json valueForKey:@"products"];
        for(NSDictionary* j_product in j_products)
        {
            Product *p = [Product new];
            [p parseFromJson:j_product];
            [self.products addObject:p];
        }
    }
}
@end

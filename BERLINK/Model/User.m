//
//  User.m
//  BERLINK
//
//  Created by Akram on 2/3/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "User.h"

@implementation User
-(void) initialise {
    self.address = @"";
    self.birth = @"";
    self.city = @"";
    //self.codepostal = [NSNumber numberWithInt:0];
    self.company = @"";
    self.created = @"";
    self.email = @"";
    self.firstname = @"";
    self.idcustomer = [NSNumber numberWithInt:0];
    self.isarchived = @"";
    //self.ispersonnel = @"";
    self.lastname = @"";
    self.name = @"";
    //self.phone = [NSNumber numberWithInt:0];
    self.photo = @"";
    self.point = [NSNumber numberWithInt:0];
    self.role = @"";
    self.siret = @"";
    self.token = [NSNumber numberWithInt:0];
    self.tva = @"";
    self.updated = @"";
    
    self.statut = [NSNumber numberWithInt:0];
    self.lat = [NSNumber numberWithInt:0];
    self.lng = [NSNumber numberWithInt:0];
    self.ibn = [NSNumber numberWithInt:0];
    self.idcategorie = [NSMutableArray new];
    self.immatriculation = @"";
    self.idUser = [NSNumber numberWithInt:0];
}

-(void)parseFromJson:(NSDictionary*) json{
    

    
    if ([[json valueForKey:@"address"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"address"] isEqualToString:@"<null>"]) {
        self.address = [json valueForKey:@"address"];
    }
    if ([[json valueForKey:@"birth"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"birth"] isEqualToString:@"<null>"]) {
        self.birth = [json valueForKey:@"birth"];
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate* date = [dateFormatter dateFromString:self.birth];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        self.birth = [dateFormatter stringFromDate:date];
    }
    
    if ([[json valueForKey:@"city"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"city"] isEqualToString:@"<null>"]) {
        self.city = [json valueForKey:@"city"];
    }
    
    if ([[json valueForKey:@"codepostal"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"codepostal"] isEqualToString:@"<null>"]) {
        self.codepostal = [json valueForKey:@"codepostal"];
    }
    if ([[json valueForKey:@"iddriver"] isKindOfClass:[NSNumber class]]) {
        self.iddriver = [json valueForKey:@"iddriver"];
    }
    if ([[json valueForKey:@"workCity"] isKindOfClass:[NSNumber class]]) {
        self.workCity = [json valueForKey:@"workCity"];
    }
    if ([[json valueForKey:@"hashid"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"hashid"] isEqualToString:@"<null>"]) {
        self.hashid = [json valueForKey:@"hashid"];
    }
    self.ispersonnel = [[json valueForKey:@"ispersonnel"] boolValue];

    /*
    if ([[json valueForKey:@"workCity"] isKindOfClass:[NSNumber class]]) {
        self.idcity = [json valueForKey:@"workCity"];
    }
    self.idcity = [NSNumber numberWithInt:0];*/
    
    if ([[json valueForKey:@"company"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"company"] isEqualToString:@"<null>"]) {
        self.company = [json valueForKey:@"company"];
        self.ispersonnel = false;
    }
    if ([[json valueForKey:@"created"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"created"] isEqualToString:@"<null>"]) {
        self.created = [json valueForKey:@"created"];
    }
    if ([[json valueForKey:@"email"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"email"] isEqualToString:@"<null>"]) {
        self.email = [json valueForKey:@"email"];
    }
    if ([[json valueForKey:@"firstname"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"firstname"] isEqualToString:@"<null>"]) {
        self.firstname = [json valueForKey:@"firstname"];
    }
    if ([[json valueForKey:@"idcustomer"] isKindOfClass:[NSNumber class]]) {
        self.idcustomer = [json valueForKey:@"idcustomer"];
    }
    if ([[json valueForKey:@"idUser"] isKindOfClass:[NSNumber class]]) {
        self.idUser = [json valueForKey:@"idUser"];
    }

    if ([[json valueForKey:@"isarchived"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"isarchived"] isEqualToString:@"<null>"]) {
        self.isarchived = [json valueForKey:@"isarchived"];
    }
    
    if ([[json valueForKey:@"lastname"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"lastname"] isEqualToString:@"<null>"]) {
        self.lastname = [json valueForKey:@"lastname"];
    }
    
    if ([[json valueForKey:@"name"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"name"] isEqualToString:@"<null>"]) {
        self.name = [json valueForKey:@"name"];
    }
    if ([[json valueForKey:@"phone"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"phone"] isEqualToString:@"<null>"]) {
        self.phone = [json valueForKey:@"phone"];
    }
    if ([[json valueForKey:@"photo"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"photo"] isEqualToString:@"<null>"]) {
        self.photo = [json valueForKey:@"photo"];
    }
    if ([[json valueForKey:@"point"] isKindOfClass:[NSNumber class]]) {
        self.point = [json valueForKey:@"point"];
    }
    if ([[json valueForKey:@"role"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"role"] isEqualToString:@"<null>"]) {
        self.role = [json valueForKey:@"role"];
    }
    if ([[json valueForKey:@"siret"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"siret"] isEqualToString:@"<null>"]) {
        self.siret = [json valueForKey:@"siret"];
    }
    if ([[json valueForKey:@"token"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"token"] isEqualToString:@"<null>"]) {
        self.token = [json valueForKey:@"token"];
    }
    if ([[json valueForKey:@"tva"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"tva"] isEqualToString:@"<null>"]) {
        self.tva = [json valueForKey:@"tva"];
    }
    if ([[json valueForKey:@"updated"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"updated"] isEqualToString:@"<null>"]) {
        self.updated = [json valueForKey:@"updated"];
    }
    
    if ([[json valueForKey:@"statut"] isKindOfClass:[NSNumber class]]) {
        self.statut = [json valueForKey:@"statut"];
    }
    if ([[json valueForKey:@"lat"] isKindOfClass:[NSNumber class]]) {
        self.lat = [json valueForKey:@"lat"];
    }
    if ([[json valueForKey:@"lng"] isKindOfClass:[NSNumber class]]) {
        self.lng = [json valueForKey:@"lng"];
    }
    if ([[json valueForKey:@"ibn"] isKindOfClass:[NSNumber class]]) {
        self.ibn = [json valueForKey:@"ibn"];
    }
    if ([[json valueForKey:@"idcategorie"] isKindOfClass:[NSArray class]]) {
        self.idcategorie = [NSMutableArray new];
        for(NSNumber* cat in [json valueForKey:@"idcategorie"])
        {
            [self.idcategorie addObject:cat];
        }
    }
    if ([[json valueForKey:@"immatriculation"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"immatriculation"] isEqualToString:@"<null>"]) {
        self.immatriculation = [json valueForKey:@"immatriculation"];
    }
    
    if ([[json valueForKey:@"stripeToken"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"stripeToken"] isEqualToString:@"<null>"]) {
        self.stripeToken = [json valueForKey:@"stripeToken"];
    }
    if ([[json valueForKey:@"countrycode"] isKindOfClass:[NSNumber class]]) {
        self.countrycode = [json valueForKey:@"countrycode"];
    }
    
    if ([[json valueForKey:@"creditCard"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"creditCard"] isEqualToString:@"<null>"]) {
        self.creditCard = [json valueForKey:@"creditCard"];
    }
}



@end

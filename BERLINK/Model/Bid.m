//
//  Bid.m
//  BERLINK
//
//  Created by BERLINK on 2/16/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "Bid.h"

@implementation Bid

-(void)parseFromJson:(NSDictionary*) json{
    
    if ([[json valueForKey:@"idBid"] isKindOfClass:[NSNumber class]]) {
        self.idBid = [json valueForKey:@"idBid"];
    }
    if ([[json valueForKey:@"dateCreated"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"dateCreated"] isEqualToString:@"<null>"]) {
        self.dateCreated = [json valueForKey:@"dateCreated"];
    }
    if ([[json valueForKey:@"firebaseDb"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"firebaseDb"] isEqualToString:@"<null>"]) {
        self.firebaseDb = [json valueForKey:@"firebaseDb"];
    }
    if ([[json valueForKey:@"price"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"price"] isEqualToString:@"<null>"]) {
        self.price = [json valueForKey:@"price"];
    }
}
-(void)dateFromString:(NSString *)dateString
{
    //"2019-02-10 02:49:17"
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-dd-MM hh:mm:ss"];
    self.date = [dateFormatter dateFromString:dateString];
}
@end

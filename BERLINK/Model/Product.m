//
//  Product.m
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "Product.h"

@implementation Product

-(void)parseFromJson:(NSDictionary*) json{
    
    if (![json isKindOfClass:[NSDictionary class]])
        return;
    if ([[json valueForKey:@"idconsproduit"] isKindOfClass:[NSNumber class]]) {
        self.idconsproduit = [json valueForKey:@"idconsproduit"];
    }
    if ([[json valueForKey:@"title"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"title"] isEqualToString:@"<null>"]) {
        self.title = [json valueForKey:@"title"];
    }
    if ([[json valueForKey:@"wdescription"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"wdescription"] isEqualToString:@"<null>"]) {
        self.wdescription = [json valueForKey:@"wdescription"];
    }
    if ([[json valueForKey:@"point"] isKindOfClass:[NSNumber class]]) {
        self.point = [json valueForKey:@"point"];
    }
}
@end

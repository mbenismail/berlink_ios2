//
//  Categorie.h
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyObject.h"

@interface Categorie : MyObject

@property (nonatomic, strong) NSNumber* idcategory;
@property (nonatomic, strong) NSString* range;
@property (nonatomic, strong) NSString* pricepack;
@property (nonatomic, strong) NSString* pricesup;
@property (nonatomic, strong) NSNumber* maxbid;
@property (nonatomic, strong) NSNumber* unitinclu;
@property (nonatomic, strong) NSString* measure;
@property (nonatomic, strong) NSString* titrevoitures;
@property (nonatomic, strong) NSNumber* note;
@property (nonatomic, strong) NSString* photo;
@property (nonatomic, strong) NSMutableArray* brands;

@end

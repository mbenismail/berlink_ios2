//
//  Course.h
//  BERLINK
//
//  Created by BERLINK on 3/16/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyObject.h"

@interface Course : MyObject

@property (nonatomic, strong) NSNumber* idcourse;
@property (nonatomic, strong) NSString* dateCreated;
@property (nonatomic, strong) NSString* datStartCourse;
@property (nonatomic, strong) NSString* dateEndCourse;
@property (nonatomic, strong) NSNumber* idDriver;
@property (nonatomic, strong) NSString* nameDriver;
@property (nonatomic, strong) NSString* phoneDriver;
@property (nonatomic, strong) NSNumber* noteDriver;
@property (nonatomic, strong) NSNumber* idRequest;
@property (nonatomic, strong) NSNumber* estimateDurationNear;
@property (nonatomic, strong) NSNumber* estimateDistanceNear;
@property (nonatomic, strong) NSNumber* idCustomer;
@property (nonatomic, strong) NSString* nameCustomer;
@property (nonatomic, strong) NSNumber* noteCustomer;
@property (nonatomic, strong) NSNumber* idCategory;
@property (nonatomic, strong) NSString* nameCategory;
@property (nonatomic, strong) NSNumber* idCar;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSNumber* descriptionUrl;
@property (nonatomic, strong) NSString* adressStart;
@property (nonatomic, strong) NSString* adressEnd;
@property (nonatomic, strong) NSNumber* distanceEstimate;
@property (nonatomic, strong) NSNumber* durationEstimate;
@property (nonatomic, strong) NSMutableArray* Products;
@property (nonatomic, strong) NSNumber* lngstart;
@property (nonatomic, strong) NSNumber* latend;
@property (nonatomic, strong) NSNumber* latstart;
@property (nonatomic, strong) NSNumber* lngend;
@property (nonatomic, strong) NSNumber* distance;
@property (nonatomic, strong) NSString* duration;
@property (nonatomic, strong) NSNumber* statut;
@property (nonatomic, strong) NSNumber* driverInforme;
@property (nonatomic, strong) NSNumber* userInforme;
@property (nonatomic, strong) NSString* descriptionCar;
@property (nonatomic, strong) NSString* imageCar;
@property (nonatomic, strong) NSNumber* prixTotal;
@property (nonatomic, strong) NSString* prixTotalStr;
@property (nonatomic, strong) NSString* pricebid;


-(void)parseFromJson:(NSDictionary*) json;

@end

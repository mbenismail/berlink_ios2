//
//  Bid.h
//  BERLINK
//
//  Created by BERLINK on 2/16/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyObject.h"

@interface Bid : MyObject

@property (nonatomic, strong) NSNumber* idBid;
@property (nonatomic, strong) NSString* dateCreated;
@property (nonatomic, strong) NSString* firebaseDb;
@property (nonatomic, strong) NSNumber* price;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSDate* date;

-(void)dateFromString:(NSString *)date;


-(void)parseFromJson:(NSDictionary*) json;
@end

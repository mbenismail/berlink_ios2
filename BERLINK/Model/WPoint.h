//
//  WPoint.h
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyObject.h"

@interface WPoint : MyObject

@property (nonatomic, strong) NSNumber* idpoints;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSNumber* number;
@property (nonatomic, strong) NSString* price;

@end

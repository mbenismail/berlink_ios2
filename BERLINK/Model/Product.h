//
//  Product.h
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyObject.h"

@interface Product : MyObject

@property (nonatomic, strong) NSNumber* idconsproduit;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* wdescription;
@property (nonatomic, strong) NSNumber* point;

-(void)parseFromJson:(NSDictionary*) json;

@end

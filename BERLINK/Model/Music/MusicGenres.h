//
//  MusicGenres.h
//  BERLINK
//
//  Created by Pravin Jadhao on 20/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MusicGenres : NSObject

@property (nonatomic, strong) NSNumber* genresId;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, assign) BOOL* isSelected;

@end

NS_ASSUME_NONNULL_END

//
//  Car.m
//  BERLINK
//
//  Created by BERLINK on 4/4/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "Car.h"

@implementation Car

-(void)parseFromJson:(NSDictionary*) json{
    
    if ([[json valueForKey:@"isreserved"] isKindOfClass:[NSNumber class]]) {
        self.isreserved = [json valueForKey:@"isreserved"];
    }
    if ([[json valueForKey:@"matriculation"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"matriculation"] isEqualToString:@"<null>"]) {
        self.matriculation = [json valueForKey:@"matriculation"];
    }
    if ([[json valueForKey:@"model"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"model"] isEqualToString:@"<null>"]) {
        self.model = [json valueForKey:@"model"];
    }
    
    if ([[json valueForKey:@"isarchived"] isKindOfClass:[NSNumber class]]) {
        self.isarchived = [json valueForKey:@"isarchived"];
    }
    if ([[json valueForKey:@"statu"] isKindOfClass:[NSNumber class]]) {
        self.statu = [json valueForKey:@"statu"];
    }
    if ([[json valueForKey:@"idcar"] isKindOfClass:[NSNumber class]]) {
        self.idcar = [json valueForKey:@"idcar"];
    }
    
}
@end

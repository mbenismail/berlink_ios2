//
//  City.m
//  BERLINK
//
//  Created by BERLINK on 4/4/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "WCity.h"

@implementation WCity
-(void)parseFromJson:(NSDictionary*) json{
    
    if ([[json valueForKey:@"idcity"] isKindOfClass:[NSNumber class]]) {
        self.idcity = [json valueForKey:@"idcity"];
    }
    if ([[json valueForKey:@"range"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"range"] isEqualToString:@"<null>"]) {
        self.range = [json valueForKey:@"range"];
    }
    
    
}
@end

//
//  City.h
//  BERLINK
//
//  Created by BERLINK on 4/4/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyObject.h"

@interface WCity : MyObject

@property (nonatomic, strong) NSNumber* idcity;
@property (nonatomic, strong) NSString* range;

-(void)parseFromJson:(NSDictionary*) json;
@end

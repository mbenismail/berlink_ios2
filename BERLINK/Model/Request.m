//
//  Request.m
//  BERLINK
//
//  Created by BERLINK on 2/16/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "Request.h"
#import "Bid.h"
#import "Product.h"

@implementation Request

-(void)parseFromJson:(NSDictionary*) json{

    if ([[json valueForKey:@"idrequest"] isKindOfClass:[NSNumber class]]) {
        self.idrequest = [json valueForKey:@"idrequest"];
    }
    if ([[json valueForKey:@"idcustomer"] isKindOfClass:[NSNumber class]]) {
        self.idcustomer = [json valueForKey:@"idcustomer"];
    }
    if ([[json valueForKey:@"idcategory"] isKindOfClass:[NSNumber class]]) {
        self.idcategory = [json valueForKey:@"idcategory"];
    }
    if ([[json valueForKey:@"pricebid"] isKindOfClass:[NSNumber class]]) {
        self.pricebid = [json valueForKey:@"pricebid"];
    }
    if ([[json valueForKey:@"estimatePrice"] isKindOfClass:[NSNumber class]]) {
        self.estimatePrice = [json valueForKey:@"estimatePrice"];
    }
    if ([[json valueForKey:@"latstart"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"latstart"] isEqualToString:@"<null>"]) {
        self.latstart = [json valueForKey:@"latstart"];
    }
    if ([[json valueForKey:@"lngstart"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"lngstart"] isEqualToString:@"<null>"]) {
        self.lngstart = [json valueForKey:@"lngstart"];
    }
    if ([[json valueForKey:@"addressstart"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"addressstart"] isEqualToString:@"<null>"]) {
        self.addressstart = [json valueForKey:@"addressstart"];
    }
    
    if ([[json valueForKey:@"city"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"city"] isEqualToString:@"<null>"]) {
        self.city = [json valueForKey:@"city"];
    }
    if ([[json valueForKey:@"latend"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"latend"] isEqualToString:@"<null>"]) {
        self.latend = [json valueForKey:@"latend"];
    }
    if ([[json valueForKey:@"lngend"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"lngend"] isEqualToString:@"<null>"]) {
        self.lngend = [json valueForKey:@"lngend"];
    }
    if ([[json valueForKey:@"adresseend"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"adresseend"] isEqualToString:@"<null>"]) {
        self.adresseend = [json valueForKey:@"adresseend"];
    }
    if ([[json valueForKey:@"dateCreated"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"dateCreated"] isEqualToString:@"<null>"]) {
        self.dateCreated = [json valueForKey:@"dateCreated"];
    }
    if ([[json valueForKey:@"bid"] isKindOfClass:[NSDictionary class]]) {
        NSDictionary* bidItem = [json valueForKey:@"bid"];
        if(bidItem && [bidItem valueForKey:@"idBid"])
        {
            self.bid = [Bid new];
            [self.bid parseFromJson:bidItem];
        }
    }
    if ([[json valueForKey:@"dateValidate"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"dateValidate"] isEqualToString:@"<null>"]) {
        self.dateValidate = [json valueForKey:@"dateValidate"];
    }
    if ([[json valueForKey:@"distanceEstimate"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"distanceEstimate"] isEqualToString:@"<null>"]) {
        self.distanceEstimate = [json valueForKey:@"distanceEstimate"];
    }
    if ([[json valueForKey:@"durationEstimate"] isKindOfClass:[NSString class]] && ![[json valueForKey:@"durationEstimate"] isEqualToString:@"<null>"]) {
        self.durationEstimate = [json valueForKey:@"durationEstimate"];
    }
    if ([[json valueForKey:@"Products"] isKindOfClass:[NSArray class]]) {
        self.Products = [NSMutableArray new];
        for(NSDictionary* prod in [json valueForKey:@"Products"])
        {
            Product* p = [Product new];
            [p parseFromJson:prod];
            [self.Products addObject:p];
        }
    }
   
    
}
@end

//
//  User.h
//  BERLINK
//
//  Created by Akram on 2/3/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyObject.h"

@interface User : MyObject
@property (nonatomic, strong) NSString* address;
@property (nonatomic, strong) NSString* password_app;
@property (nonatomic, strong) NSString* birth;
@property (nonatomic, strong) NSString* city;
@property (nonatomic, strong) NSString* codepostal;
@property (nonatomic, strong) NSString* company;
@property (nonatomic, strong) NSString* created;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* firstname;
@property (nonatomic, strong) NSNumber* idcustomer;
@property (nonatomic, strong) NSString* isarchived;
@property BOOL ispersonnel;
@property (nonatomic, strong) NSString* lastname;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* photo;
@property (nonatomic, strong) NSNumber* point;
@property (nonatomic, strong) NSString* role;
@property (nonatomic, strong) NSString* siret;
@property (nonatomic, strong) NSNumber* token;
@property (nonatomic, strong) NSString* tva;
@property (nonatomic, strong) NSString* updated;
@property (nonatomic, strong) NSNumber* iddriver;
@property (nonatomic, strong) NSNumber* workCity;
//@property (nonatomic, strong) NSNumber* idcity;
@property (nonatomic, strong) NSString* hashid;
@property (nonatomic, strong) NSNumber* statut;
@property (nonatomic, strong) NSNumber* lat;
@property (nonatomic, strong) NSNumber* lng;
@property (nonatomic, strong) NSNumber* ibn;
@property (nonatomic, strong) NSMutableArray* idcategorie;
@property (nonatomic, strong) NSString* immatriculation;
@property (nonatomic, strong) NSNumber* idUser;
@property (nonatomic, strong) NSString* stripeToken;
@property (nonatomic, strong) NSNumber* countrycode;
@property (nonatomic, strong) NSString* creditCard;
@property (nonatomic, strong) NSString* socialId;

//@property (nonatomic, strong) NSNumber* idfacebook;

-(void)parseFromJson:(NSDictionary*) json;

@end

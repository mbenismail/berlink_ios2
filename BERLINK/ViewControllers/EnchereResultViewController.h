//
//  EnchereResultViewController.h
//  BERLINK
//
//  Created by BERLINK on 2/10/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "ViewController.h"
#import "Request.h"
#import "User.h"
#import "Categorie.h"

@interface EnchereResultViewController : ViewController
{
    IBOutlet UILabel* winerNameLabel;
    IBOutlet UILabel* aremporteLabel;
    __weak IBOutlet UIImageView *catCarImage;
}

@property (strong, nonatomic) Request* currentRequest;
@property (strong, nonatomic) NSString* currentUserID;
@property (strong, nonatomic) NSString* currentUserName;
@property (nonatomic, strong) Categorie* currentCategory;

@end

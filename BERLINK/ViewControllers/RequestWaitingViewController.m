//
//  RequestWaitingViewController.m
//  BERLINK
//
//  Created by BERLINK on 3/21/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "RequestWaitingViewController.h"
#import "SWRevealViewController.h"
#import "Util.h"
#import "StringConstants.h"
#import "WebServiceApi.h"
#import "SharedVar.h"
#import "Constants.h"

@interface RequestWaitingViewController ()

@end

@implementation RequestWaitingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SWRevealViewController *revealController = [self revealViewController];
    
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    // Do any additional setup after loading the view.
    userAddressTextField.text = _request.addressstart;
    destinationTextField.text = _request.adresseend;
    
    [activityIndicator startAnimating];
    
    annulerBtn.hidden = YES;

    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;

    
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerCalled) userInfo:nil repeats:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)timerCalled
{
    //do smth
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* dateCreated = [dateFormatter dateFromString:_request.dateCreated];

    NSDateComponents *components= [[NSDateComponents alloc] init];
    [components setMinute:REQUEST_DENY_DELAY];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *myNewDate=[calendar dateByAddingComponents:components toDate:dateCreated options:0];

    if ([[NSDate date] compare:myNewDate] == NSOrderedDescending) {
        annulerBtn.hidden = NO;

    } else {
        annulerBtn.hidden = YES;
    }
}

-(IBAction)annulerDemande:(id)sender{
    
    [[Util shareManager] showAlertView:APP_NAME message:PAYEMENT_METHOD_SELECTION_MESSAGE okTitle:@"OUI" delegate:self];
}

-(void)alertOkButtonClicked
{
    User* user = [[SharedVar shareManager] mainUser];
    [[Util shareManager] showWaitingViewController:self];
    [[WebServiceApi shareManager] invokeDeleteRequest_user:user andIdRequest:_request.idrequest completionHandler:^(NSString *errorMsg) {
        
        [[Util shareManager] dismissWating];
        if(errorMsg.length > 0)
        {
            [[Util shareManager] showWarning:errorMsg withTitle:APP_NAME andViewController:self];
        }
        else{
            
            [[[SharedVar shareManager] accountLeftVC] goToCategoriesViewController:nil];
        }
    }];
}

@end

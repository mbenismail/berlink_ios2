//
//  CourseViewController.h
//  BERLINK
//
//  Created by BERLINK on 3/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@import GoogleMaps;
@import Firebase;
@import FirebaseDatabase;

@interface CourseViewController : UIViewController<CLLocationManagerDelegate, UITextFieldDelegate>
{
    CLLocationManager *locationManager;
    NSString* lngUser;
    NSString* latUser;
    IBOutlet UITextField* userAddressTextField;
    IBOutlet GMSMapView* umapView;
    CLLocation *currentLocation;
    int draw_user_annotation;
    FIRDatabaseReference* db;
    double lng_driver;
    double lat_driver;
    IBOutlet UIButton* finirCourseBtn;
    IBOutlet UIView* tabBarView;
    
    IBOutlet UIButton* carBtn;
    IBOutlet UILabel* carBtnLabel;
    IBOutlet UILabel* nameDriverLabel;
    
    int calculated_distance;
    NSString* calculated_duration;
}

@property (weak, nonatomic) IBOutlet UIView *driverView;

@property (weak, nonatomic) IBOutlet UILabel *lblDuratation;
@property (weak, nonatomic) IBOutlet UIView *imageView;

@end

//
//  CGUViewController.h
//  BERLINK
//
//  Created by TRIMECH on 09/03/2018.
//  Copyright © 2018 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CGUTableViewCell.h"
#import "DetailsCGUViewController.h"

@interface CGUViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray* cguList;


@end

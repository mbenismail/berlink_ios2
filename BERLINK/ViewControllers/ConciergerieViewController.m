//
//  ConciergerieViewController.m
//  BERLINK
//
//  Created by BERLINK on 3/3/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "ConciergerieViewController.h"
#import "ConciergerieTableViewCell.h"
#import "SharedVar.h"
#import "Conciergerie.h"
#import "Constants.h"
#import "Product.h"
#import "WebServiceApi.h"
#import "Util.h"
#import "WaitingViewController.h"
#import "AppDelegate.h"
#import "StringConstants.h"
#import "ProductViewController.h"
#import "Gradient.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PointsPopupViewController.h"
#import "MessageAlertViewController.h"
// Set the environment:
// - For live charges, use PayPalEnvironmentProduction (default).
// - To use the PayPal sandbox, use PayPalEnvironmentSandbox.
// - For testing, use PayPalEnvironmentNoNetwork.
#define kPayPalEnvironment PayPalEnvironmentNoNetwork

@interface ConciergerieViewController ()<PointsPopupViewControllerDelegate,MessageAlertViewControllerDelegate>
{
    PointsPopupViewController* pointPopupViewC;
    UITextField *pickerTextField;
    int alertType;//0-insuficient point , 2- payment mode, 3- request messagwe
}
@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;
@property (weak, nonatomic) IBOutlet UILabel *lblProductTotalPrice;

@end

@implementation ConciergerieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    if(appd.currentCourse.nameDriver && appd.currentCourse.nameDriver.length > 0)
        self.lblDriverName.text = appd.currentCourse.nameDriver;
    else
        self.lblDriverName.text = @"Chauffeur";
    
    self.lblTime.text = self.strTime;
    
    // Do any additional setup after loading the view.
    // Set up payPalConfig
    _payPalConfig = [[PayPalConfiguration alloc] init];
    _payPalConfig.acceptCreditCards = NO;
    _payPalConfig.merchantName = @"BERLINK";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    
    [Gradient setBackgroundGradientForView:self.viewDriverInfo color1Red:30.0 color1Green:30.0 color1Blue:30.0 color2Red:0.0 color2Green:0.0 color2Blue:0.0 alpha:1.0];
    
    
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    
    // Do any additional setup after loading the view, typically from a nib.
    
    picker.dataSource = self;
    picker.delegate = self;
    //picker.hidden = YES;
    [pickerTextField resignFirstResponder];
    productsList = [NSMutableArray new];
    
    self.conciergeriesLabel.text =  [NSString stringWithFormat:@"%@ Points Disponibles", [[[SharedVar shareManager] mainUser] point]];
    self.lblProductTotalPrice.text = @"0 Point";
    
    self.environment = kPayPalEnvironment;
    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
    
    old_point = [[[[SharedVar shareManager] mainUser] point] intValue];
    
    
    numberProductLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)productsList.count];
    [self setupPicker];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = @"Consierge";

    [self setPayPalEnvironment:self.environment];
    [self setupNavigationBar];
}

-(void)setupNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    UIImage *backBtnImage = [UIImage imageNamed:@"n_back_btn.png"]  ;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = [view bounds];
    UIImageView *iView = [[UIImageView alloc] initWithImage:backBtnImage];
    iView.frame =  CGRectMake(0, 5, 20, 20);
    [iView setContentMode:UIViewContentModeScaleAspectFit];
    [view addSubview:iView];
    [view addSubview:backBtn];
    [backBtn addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = backButton;
}

-(void)backButtonAction
{
    [self.navigationController popViewControllerAnimated:true];
}


- (void)setPayPalEnvironment:(NSString *)environment {
    self.environment = environment;
    [PayPalMobile preconnectWithEnvironment:environment];
}

-(void)cancelAchat:(id)sender{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if(toolBar && toolBar.superview != nil)
            [toolBar removeFromSuperview];
       // picker.hidden = YES;
        [pickerTextField resignFirstResponder];
    }];
}

-(void)validateAchat:(id)sender{
    NSLog(@"type %d", pickerType);
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
       // picker.hidden = YES;
        [pickerTextField resignFirstResponder];

        if(toolBar && toolBar.superview != nil)
            [toolBar removeFromSuperview];
        Product *prod = [[[[[SharedVar shareManager] mainConciergeries] objectAtIndex:congiergerieSelected] products] objectAtIndex:productSelected];
        
        if(old_point < prod.point.intValue)
        {
            alertType = 0;
              [[Util shareManager] showAlertView:INSUFFISANT_POINT_TITLE message:INSUFFISANT_POINT_MESSAGE okTitle:@"OUI" cancelTitle:@"NON" delegate:self];
        }
        else{
            [productsList addObject:prod];
            old_point = old_point - prod.point.intValue;
            numberProductLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)productsList.count];
            self.conciergeriesLabel.text =  [NSString stringWithFormat:@"%d Points Disponibles", old_point];
            int totalPrice = 0;
            for(Product *p in productsList)
            {
                totalPrice = totalPrice + p.point.intValue;
            }
            self.lblProductTotalPrice.text = [NSString stringWithFormat:@"%d Points", totalPrice];
        }
    }];
}

-(IBAction)validateRequest:(id)sender{
    
    User* user = [[SharedVar shareManager] mainUser];
    NSMutableArray* old_products = _currentRequest.Products;
    _currentRequest.Products = productsList;
    
    if(_currentRequest.bid)
    {
        NSLog(@"bidi");
        _currentRequest.bid.price = [NSNumber numberWithInt:_currentRequest.pricebid.intValue];
    }
    _currentRequest.dateValidate = [[Util shareManager] currentDate];
    [[Util shareManager] showWaitingViewController:self];
    [[WebServiceApi shareManager] invokeUpdateRequestWebServiceWithParams_user:user andRequest:_currentRequest completionHandler:^(NSString *errorMsg, Request *request) {
        [[Util shareManager] dismissWating];
        
        if([errorMsg isEqualToString:@""])
        {
        
            alertType = 1;
               [[Util shareManager] showAlertView:APP_NAME message:MESSAGE_REQUEST_SENT okTitle:@"OUI" delegate:self];
        }
        else
        {
            [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
            _currentRequest.Products = old_products;
        }
        
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[SharedVar shareManager] mainConciergeries] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 172;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ConciergerieTableViewCell";
    
    ConciergerieTableViewCell *cell = (ConciergerieTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Conciergerie* c = [[[SharedVar shareManager] mainConciergeries] objectAtIndex:indexPath.row];
    cell.nameLabel.text = c.title;
    cell.pointsLabel.text = @"200 Points";
    NSURL* aURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SERVER_URL,c.image]];
    [[cell photo] sd_setImageWithURL:aURL
                    placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(toolBar && toolBar.superview != nil)
        [toolBar removeFromSuperview];
    congiergerieSelected = indexPath.row;
    pickerType = PRODUCT;
    [picker reloadAllComponents];
    //picker.hidden = NO;
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,(self.view.frame.size.height - 216 - 44),self.view.frame.size.width,44)];
    toolbarLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    Conciergerie *c = [[[SharedVar shareManager] mainConciergeries] objectAtIndex:congiergerieSelected];
    toolbarLabel.text = [NSString stringWithFormat:@"Les produits du %@", c.title];
    [toolbarLabel sizeToFit];
    toolbarLabel.backgroundColor = [UIColor clearColor];
    toolbarLabel.textColor = [UIColor grayColor];
    toolbarLabel.textAlignment = NSTextAlignmentCenter;
    flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Valider"
                                                     style:UIBarButtonItemStylePlain target:self action:@selector(validateAchat:)];
    barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Annuler"
                                                       style:UIBarButtonItemStylePlain target:self action:@selector(cancelAchat:)];
    labelItem = [[UIBarButtonItem alloc] initWithCustomView:toolbarLabel];
    toolBar.items = [NSArray arrayWithObjects:barButtonCancel, flexible, labelItem, flexible, barButtonDone, nil];
    //[self.view addSubview:toolBar];
    pickerTextField.inputAccessoryView = toolBar;
    [pickerTextField becomeFirstResponder];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setupPicker
{
    pickerTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    
    picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 216)];
    [picker setDataSource: self];
    [picker setDelegate: self];
    picker.showsSelectionIndicator = YES;
    pickerTextField.inputView = picker;
    [self.view addSubview:pickerTextField];
}


// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(pickerType == PRODUCT)
        return [[[[[SharedVar shareManager] mainConciergeries] objectAtIndex:congiergerieSelected] products] count];
    else
        return [[[SharedVar shareManager] mainPoints] count];
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(pickerType == PRODUCT)
    {
        Product *prod = [[[[[SharedVar shareManager] mainConciergeries] objectAtIndex:congiergerieSelected] products] objectAtIndex:row];
        productSelected=0;
        return [NSString stringWithFormat:@"%@  (%d points)", prod.title, prod.point.intValue];
        
    }
    else
    {
        WPoint *p = [[[SharedVar shareManager] mainPoints] objectAtIndex:row];
        return [NSString stringWithFormat:@"%@ (%d €)", p.title, p.price.intValue];
    }
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
    if(pickerType == PRODUCT)
    {
        productSelected = row;
    }
    else
    {
        pointSelected = row;
    }
}

-(IBAction)returnToCategoriesViewController:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:NO];
}


-(IBAction)ProductList:(id)sender{
    
    ProductViewController* eViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"ProductViewController"];
    eViewC.request = nil;
    eViewC.productList = productsList;
    [self.navigationController pushViewController:eViewC animated:YES];
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)payPointWithCard:(WPoint*)p {
    User* user = [[SharedVar shareManager] mainUser];
    [[Util shareManager] showWaitingViewController:self];
    [[WebServiceApi shareManager] invokeBuyPointWebServiceWithParams_user:user andIdPoint:p.idpoints completionHandler:^(NSString *errorMsg) {
        [[Util shareManager] dismissWating];
        if(errorMsg && errorMsg.length > 0)
        {
            [self payementFail];
        }
        else
        {
            [self payementDoneWithSuccess:p];
        }
    }];
}

-(void)payementDoneWithSuccess:(WPoint*)p{
    User* user = [[SharedVar shareManager] mainUser];
    [[SharedVar shareManager] setMainUser:user];
    pickerType = PRODUCT;
    [picker reloadAllComponents];
    //picker.hidden = NO;
    [pickerTextField becomeFirstResponder];
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,(picker.frame.origin.y - 44),self.view.frame.size.width,44)];
    toolbarLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    Conciergerie *c = [[[SharedVar shareManager] mainConciergeries] objectAtIndex:congiergerieSelected];
    toolbarLabel.text = [NSString stringWithFormat:@"Les produits du %@", c.title];
    [toolbarLabel sizeToFit];
    toolbarLabel.backgroundColor = [UIColor clearColor];
    toolbarLabel.textColor = [UIColor grayColor];
    toolbarLabel.textAlignment = NSTextAlignmentCenter;
    flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Valider"
                                                     style:UIBarButtonItemStylePlain target:self action:@selector(validateAchat:)];
    barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Annuler"
                                                       style:UIBarButtonItemStylePlain target:self action:@selector(cancelAchat:)];
    labelItem = [[UIBarButtonItem alloc] initWithCustomView:toolbarLabel];
    toolBar.items = [NSArray arrayWithObjects:barButtonCancel, flexible, labelItem, flexible, barButtonDone, nil];
   // [self.view addSubview:toolBar];
    NSNumber* points = [NSNumber numberWithInt:(old_point + p.number.intValue) ];
    
    [[Util shareManager] showSuccess:[NSString stringWithFormat:POINT_PAYEMENT_SUCCESS_MESSAGE, points.intValue] withTitle:APP_NAME andViewController:self];
    old_point = old_point + p.number.intValue;
    self.conciergeriesLabel.text =  [NSString stringWithFormat:@"%d Points Disponibles", old_point];
}

-(void)payementFail {
    [[Util shareManager] showError:POINT_PAYEMENT_FAILURE_MESSAGE withTitle:APP_NAME andViewController:self];
}

#pragma mark - Receive Single Payment

- (void)payPointByPayPal:(WPoint*)point {
    // Remove our last completed payment, just for demo purposes.
    self.resultText = nil;
    
    // Note: For purposes of illustration, this example shows a payment that includes
    //       both payment details (subtotal, shipping, tax) and multiple items.
    //       You would only specify these if appropriate to your situation.
    //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
    //       and simply set payment.amount to your total charge.
    
    // Optional: include multiple items
    PayPalItem *item1 = [PayPalItem itemWithName:point.title
                                    withQuantity:1
                                       withPrice:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d", point.price.intValue]]
                                    withCurrency:@"EUR"
                                         withSku:[NSString stringWithFormat:@"BERLINK%@", point.idpoints]];
    NSArray *items = @[item1];
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
    
    // Optional: include payment details
    //NSDecimalNumber *shipping = [[NSDecimalNumber alloc] initWithString:@"5.99"];
    //NSDecimalNumber *tax = [[NSDecimalNumber alloc] initWithString:@"2.50"];
    PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                                                               withShipping:nil
                                                                                    withTax:nil];
    
    //NSDecimalNumber *total = [[subtotal decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
    NSDecimalNumber *total = subtotal;
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = total;
    payment.currencyCode = @"EUR";
    payment.shortDescription = point.title;
    payment.items = items;  // if not including multiple items, then leave payment.items as nil
    payment.paymentDetails = paymentDetails; // if not including payment details, then leave payment.paymentDetails as nil
    
    if (!payment.processable) {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
    }
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success!");
    self.resultText = [completedPayment description];
    
    [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
    [self dismissViewControllerAnimated:YES completion:nil];
    
    User* user = [[SharedVar shareManager] mainUser];
    [[Util shareManager] showWaitingViewController:self];
    NSNumber* points = [NSNumber numberWithInt:(user.point.intValue + current_point.number.intValue) ];
    [[WebServiceApi shareManager] invokeUpdateUserPointServiceWithParams_user:user _point:points completionHandler:^(NSString *errorMsg, User *user) {
        
        
        [[Util shareManager] dismissWating];
        
        if([errorMsg isEqualToString:@""])
        {
            
            [self payementDoneWithSuccess:current_point];
            
        }
        else
        {
            [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
            [self payementFail];
        }
        
    }];
    
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    self.resultText = nil;
    //
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    // TODO: Send completedPayment.confirmation to server
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
}


#pragma mark - Authorize Future Payments

- (IBAction)getUserAuthorizationForFuturePayments:(id)sender {
    
    PayPalFuturePaymentViewController *futurePaymentViewController = [[PayPalFuturePaymentViewController alloc] initWithConfiguration:self.payPalConfig delegate:self];
    [self presentViewController:futurePaymentViewController animated:YES completion:nil];
}


#pragma mark PayPalFuturePaymentDelegate methods

- (void)payPalFuturePaymentViewController:(PayPalFuturePaymentViewController *)futurePaymentViewController
                didAuthorizeFuturePayment:(NSDictionary *)futurePaymentAuthorization {
    NSLog(@"PayPal Future Payment Authorization Success!");
    self.resultText = [futurePaymentAuthorization description];
    [[Util shareManager] showSuccess:self.resultText withTitle:APP_NAME andViewController:self];
    
    [self sendFuturePaymentAuthorizationToServer:futurePaymentAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalFuturePaymentDidCancel:(PayPalFuturePaymentViewController *)futurePaymentViewController {
    NSLog(@"PayPal Future Payment Authorization Canceled");
    //[[Util shareManager] showWarning:self.resultText withTitle:APP_NAME andViewController:self];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendFuturePaymentAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete future payment setup.", authorization);
}


#pragma mark - Authorize Profile Sharing

- (IBAction)getUserAuthorizationForProfileSharing:(id)sender {
    
    NSSet *scopeValues = [NSSet setWithArray:@[kPayPalOAuth2ScopeOpenId, kPayPalOAuth2ScopeEmail, kPayPalOAuth2ScopeAddress, kPayPalOAuth2ScopePhone]];
    
    PayPalProfileSharingViewController *profileSharingPaymentViewController = [[PayPalProfileSharingViewController alloc] initWithScopeValues:scopeValues configuration:self.payPalConfig delegate:self];
    [self presentViewController:profileSharingPaymentViewController animated:YES completion:nil];
}

- (void)popupDidClose
{
    pointPopupViewC = nil;

}
- (void)popupDidCloseOnOk:(WPoint *)p
{
    pointPopupViewC = nil;
    NSLog(@"p %@", p.title);
    current_point = p;
    alertType = 2;
    [[Util shareManager] showAlertView:APP_NAME message:PAYEMENT_METHOD_SELECTION_MESSAGE okTitle:@"PAYPAL" cancelTitle:@"Carte VISA/MasterCard" delegate:self];
}

-(void)alertOkButtonClicked
{
    if(alertType == 0)
    {
        pointPopupViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"PointsPopupViewController"];
        [pointPopupViewC.view setFrame:self.view.bounds];
        [pointPopupViewC setDelegate:self];
        [self.view addSubview:pointPopupViewC.view];
        [pointPopupViewC show];
    }
    if(alertType == 1)
    {
        [[[SharedVar shareManager] accountLeftVC] goToCategoriesViewController:nil];
    }
    if(alertType == 2)
    {
        [self payPointByPayPal:current_point];
    }
}

- (void)alertCancelButtonClicked
{
    if(alertType == 2)
    {
        [self payPointWithCard:current_point];
    }
    if(alertType == 1)
    {
        [[[SharedVar shareManager] accountLeftVC] goToCategoriesViewController:nil];
    }
}

#pragma mark PayPalProfileSharingDelegate methods

- (void)payPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
             userDidLogInWithAuthorization:(NSDictionary *)profileSharingAuthorization {
    NSLog(@"PayPal Profile Sharing Authorization Success!");
    self.resultText = [profileSharingAuthorization description];
    [[Util shareManager] showSuccess:self.resultText withTitle:APP_NAME andViewController:self];
    
    [self sendProfileSharingAuthorizationToServer:profileSharingAuthorization];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidCancelPayPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController {
    NSLog(@"PayPal Profile Sharing Authorization Canceled");
    //
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendProfileSharingAuthorizationToServer:(NSDictionary *)authorization {
    // TODO: Send authorization to server
    NSLog(@"Here is your authorization:\n\n%@\n\nSend this to your server to complete profile sharing setup.", authorization);
}
@end

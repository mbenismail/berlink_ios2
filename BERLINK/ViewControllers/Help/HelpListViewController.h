//
//  HelpListViewController.h
//  BERLINK
//
//  Created by Pravin Jadhao on 08/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

NS_ASSUME_NONNULL_BEGIN

@interface HelpListViewController : UIViewController

@property (nonatomic, strong) NSArray* coursesList;
@property (nonatomic, strong) User* mainUser;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
NS_ASSUME_NONNULL_END

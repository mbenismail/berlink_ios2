//
//  HelpListViewController.m
//  BERLINK
//
//  Created by Pravin Jadhao on 08/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import "HelpListViewController.h"
#import "HistoriqueTableViewCell.h"
#import "HelpDetailViewController.h"
#import "SharedVar.h"
#import "AppDelegate.h"
#import "StringConstants.h"
#import "WebServiceApi.h"
#import "AppDelegate.h"
#import "Util.h"
#import "SharedVar.h"

@interface HelpListViewController ()

@end

@implementation HelpListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    

    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    self.navigationItem.title = @"Aide";
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    [self getListCoursesWS];
    
    
}

- (void)getListCoursesWS {
    
    [[Util shareManager] showWaitingViewController:self];
    [[WebServiceApi shareManager] invokeMyCoureWebServiceWithParams_User:[[SharedVar shareManager] mainUser] completionHandler:^(NSString *errorMsg, Course* course, Request* request,NSArray *courseArray) {
        [[Util shareManager] dismissWating];
        if([errorMsg isEqualToString:@""])
        {
            NSLog(@"%@",courseArray);
            _coursesList=courseArray;
            [self.tableView reloadData];
        }
        else
        {
            [[Util shareManager] showWarning:@"Aucune course enregistrée" withTitle:APP_NAME andViewController:self];
        }
    }];
}

-(NSString *)getDate:(NSString *)dateString{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* date = [dateFormatter dateFromString:dateString];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *mydate = [dateFormatter stringFromDate:date];
    return mydate;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _coursesList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"HistoriqueTableViewCell";
    HistoriqueTableViewCell *cell = (HistoriqueTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HistoriqueTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSString *mydate = _coursesList[indexPath.row][@"dateCreated"];
    
    cell.dateLabel.text = [self getDate:mydate];
    
    cell.totalLabel.text = [NSString stringWithFormat:@"%@ €",_coursesList[indexPath.row][@"prixTotal"]];
    cell.nameLabel.text = _coursesList[indexPath.row][@"nameDriver"];
    cell.sourceLabel.text = _coursesList[indexPath.row][@"adressStart"];
    cell.destinationLabel.text = _coursesList[indexPath.row][@"adressEnd"];
    int rating = (int)_coursesList[indexPath.row][@"noteDriver"];
    [cell setRating:rating];
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 230;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    HelpDetailViewController* eViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"HelpDetailViewController"];
    eViewC.course = _coursesList[indexPath.row];
    
    [self.navigationController pushViewController:eViewC animated:YES];
}
@end

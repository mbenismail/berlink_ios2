//
//  HelpDetailViewController.m
//  BERLINK
//
//  Created by Pravin Jadhao on 08/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import "HelpDetailViewController.h"
#import "HistoriqueTableViewCell.h"
#import "HelpViewController.h"
#import "SharedVar.h"
#import "CGUTableViewCell.h"
#import "Gradient.h"

@interface HelpDetailViewController ()

@end

@implementation HelpDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setBackBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.title = @"Votre Course";
    [self.navigationItem.backBarButtonItem setTitle:@""];
    [self setupNavigationBar];
}

-(void)setupNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    UIImage *backBtnImage = [UIImage imageNamed:@"n_back_btn.png"]  ;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = [view bounds];
    UIImageView *iView = [[UIImageView alloc] initWithImage:backBtnImage];
    iView.frame =  CGRectMake(0, 5, 20, 20);
    [iView setContentMode:UIViewContentModeScaleAspectFit];
    [view addSubview:iView];
    [view addSubview:backBtn];
    [backBtn addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = backButton;
}

-(void)backButtonAction
{
    [self.navigationController popViewControllerAnimated:true];
}
-(NSString *)getDate:(NSString *)dateString{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* date = [dateFormatter dateFromString:dateString];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *mydate = [dateFormatter stringFromDate:date];
    return mydate;
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == 0)
    {
        static NSString *simpleTableIdentifier = @"HistoriqueTableViewCell";
        HistoriqueTableViewCell *cell = (HistoriqueTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HistoriqueTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
         NSString *mydate = _course[@"dateCreated"];
         
         cell.dateLabel.text = [self getDate:mydate];
         
         cell.totalLabel.text = [NSString stringWithFormat:@"%@ €",_course[@"prixTotal"]];
         cell.nameLabel.text = _course[@"nameDriver"];
         cell.sourceLabel.text = _course[@"adressStart"];
         cell.destinationLabel.text = _course[@"adressEnd"];
         int rating = (int)_course[@"noteDriver"];
         [cell setRating:rating];
        return cell;
    }else if(indexPath.row == 1)
    {
        UITableViewCell *cell = [[UITableViewCell alloc] init];
        cell.backgroundColor = [UIColor  clearColor];
        return cell;
    }else{
        static NSString *simpleTableIdentifier = @"CGUTableViewCell";
        CGUTableViewCell *cell = (CGUTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CGUTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lblTitle.text= @"Signaler un problème avec cette course";
       
        [Gradient setBackgroundGradientForView:cell.imgBG color1Red:0.0 color1Green:00.0 color1Blue:00.0 color2Red:40.0 color2Green:40.0 color2Blue:40.0 alpha:1.0];
        
        cell.backgroundColor = [UIColor blackColor];
        
        cell.imgBG.layer.masksToBounds = YES;
        cell.imgBG.layer.borderColor = [UIColor colorWithRed:86.0/255 green:86.0/255 blue:86.0/255 alpha:1].CGColor;
        cell.imgBG.layer.borderWidth = 1;
        cell.imgBG.layer.cornerRadius = 5;
        return cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 1)
    {
        return 10;
    }
    return  indexPath.row == 0 ?  230 : 64;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if(indexPath.row == 2)
    {
        HelpViewController* eViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"HelpViewController"];
        [self.navigationController pushViewController:eViewC animated:YES];
    }
}
@end

//
//  HelpDetailViewController.h
//  BERLINK
//
//  Created by Pravin Jadhao on 08/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HelpDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSDictionary *course;
@end

NS_ASSUME_NONNULL_END

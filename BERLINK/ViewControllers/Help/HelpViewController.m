//
//  HelpViewController.m
//  BERLINK
//
//  Created by Dhaker Trimech on 16/03/2018.
//  Copyright © 2018 berlink. All rights reserved.
//

#import "HelpViewController.h"
#import "SWRevealViewController.h"
#import "WebServiceApi.h"
#import "SharedVar.h"
#import "Util.h"
#import "Constants.h"
#import "StringConstants.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([[[[SharedVar shareManager] mainUser] role] isEqualToString:ROLE_DRIVER])
    {
    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    if(![[[[SharedVar shareManager] mainUser] role] isEqualToString:ROLE_DRIVER])
    {
    self.navigationController.navigationBar.hidden = NO;
       [self setupNavigationBar];
    }
    self.navigationItem.title = @"Votre Course";

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)setupNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    UIImage *backBtnImage = [UIImage imageNamed:@"n_back_btn.png"]  ;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = [view bounds];
    UIImageView *iView = [[UIImageView alloc] initWithImage:backBtnImage];
    iView.frame =  CGRectMake(0, 5, 20, 20);
    [iView setContentMode:UIViewContentModeScaleAspectFit];
    [view addSubview:iView];
    [view addSubview:backBtn];
    [backBtn addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = backButton;
}

-(void)backButtonAction
{
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - Button action

- (IBAction)sendContact:(id)sender {
 NSString* object = _objectTextFiled.text;
    NSString* message = _contactMessage.text;
    
     if(object && object.length > 0 )
    {
        [[Util shareManager] showWaitingViewController:self];
         User* user = [[SharedVar shareManager] mainUser];
        [[WebServiceApi shareManager] invokeContactWebServiceWithParams:user _email:object _password:message completionHandler:^(NSString *errorMsg, User *user) {
            NSLog(@"ici 2");
            [[Util shareManager] dismissWating];
              if(errorMsg && errorMsg.length > 0)
            {
                NSLog(@"ici 3");

                [[Util shareManager] showError:errorMsg withTitle:@"Berlink" andViewController:self];
                return;
            }
            else{
                [[Util shareManager] showError:@"Message envoyé" withTitle:APP_NAME andViewController:self];
            }
        }];
    }
}
#pragma mark - TextView Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
@end

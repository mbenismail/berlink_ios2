//
//  HelpViewController.h
//  BERLINK
//
//  Created by Dhaker Trimech on 16/03/2018.
//  Copyright © 2018 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *objectTextFiled;
@property (weak, nonatomic) IBOutlet UITextView *contactMessage;
 
 
@end

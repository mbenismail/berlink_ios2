//
//  TotalPriceViewController.h
//  BERLINK
//
//  Created by BERLINK on 3/3/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Request.h"
#import "Bid.h"

@interface TotalPriceViewController : UIViewController{
    IBOutlet UITextField* departText;
    IBOutlet UITextField* destinationText;
    
    IBOutlet UILabel* priseEnChargePrice;
    IBOutlet UILabel* bidPrice;
    IBOutlet UILabel* totalPrice;
}
@property int estmatedPrice;
@property(nonatomic, retain)NSString* strTime;

@property (weak, nonatomic) IBOutlet UILabel *lblDriverName;

@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@property (nonatomic, strong) Request* currentRequest;

@property (weak, nonatomic) IBOutlet UIImageView *starIcon1;
@property (weak, nonatomic) IBOutlet UIImageView *starIcon2;
@property (weak, nonatomic) IBOutlet UIImageView *starIcon3;
@property (weak, nonatomic) IBOutlet UIImageView *starIcon4;
@property (weak, nonatomic) IBOutlet UIImageView *starIcon5;
@property (weak, nonatomic) IBOutlet UIView *driverInfoview;

@end

//
//  ProductViewController.m
//  BERLINK
//
//  Created by helmi on 4/13/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "ProductViewController.h"
#import "ProductTableViewCell.h"
#import "Product.h"
#import "Constants.h"

@interface ProductViewController ()

@end

@implementation ProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [mytableView reloadData];
    if(_request && _request.Products)
        NSLog(@"products count %lu", [_request.Products count]);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Table View datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(_request && _request.Products)
        return [_request.Products count];
    
    return [_productList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    Product* p;
    if(_request && _request.Products)
        p = [_request.Products objectAtIndex:indexPath.row];
    else
        p = [_productList objectAtIndex:indexPath.row];
    NSLog(@"p title %@", p);

    cell.textLabel.text = p.title;
    [cell.textLabel setTextColor:APP_COLOR];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


@end

//
//  CGUViewController.m
//  BERLINK
//
//  Created by TRIMECH on 09/03/2018.
//  Copyright © 2018 berlink. All rights reserved.
//

#import "CGUViewController.h"
#import "SWRevealViewController.h"
#import "SharedVar.h"
#import "StringConstants.h"
#import "WebServiceApi.h"
#import "AppDelegate.h"
#import "Util.h"
#import "Gradient.h"

@interface CGUViewController ()

@end

@implementation CGUViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    SWRevealViewController *revealController = [self revealViewController];
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    self.navigationItem.title = @"Conditions Genérales";
    
    _cguList = [[NSArray alloc] initWithObjects:@"PROTECTION DES DROITS",@"CONDITIONS D'UTILISATION",@"POLITIQUE DE CONFIDENTIALITÉ", nil];
    
    [self.tableView reloadData];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _cguList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"CGUTableViewCell";
    CGUTableViewCell *cell = (CGUTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CGUTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.lblTitle.text=_cguList[indexPath.row];
    

  [Gradient setBackgroundGradientForView:cell.imgBG color1Red:0.0 color1Green:00.0 color1Blue:00.0 color2Red:40.0 color2Green:40.0 color2Blue:40.0 alpha:1.0];

    cell.backgroundColor = [UIColor blackColor];
    
    cell.imgBG.layer.masksToBounds = YES;
    cell.imgBG.layer.borderColor = [UIColor colorWithRed:86.0/255 green:86.0/255 blue:86.0/255 alpha:1].CGColor;
    cell.imgBG.layer.borderWidth = 1;
    cell.imgBG.layer.cornerRadius = 5;
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //DetailsCGUViewController
    DetailsCGUViewController* detailsCGUViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"DetailsCGUViewController"];
    [self.navigationController pushViewController:detailsCGUViewC animated:YES];
}
@end

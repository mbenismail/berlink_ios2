//
//  CategoriesViewController.h
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface CategoriesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    NSString* lngUser;
    NSString* latUser;
    IBOutlet UITableView* myTableView;
    CLLocation *currentLocation;
    NSString* strCat;
}
@end

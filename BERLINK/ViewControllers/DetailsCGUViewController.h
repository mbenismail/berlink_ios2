//
//  DetailsCGUViewController.h
//  BERLINK
//
//  Created by TRIMECH on 10/03/2018.
//  Copyright © 2018 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsCGUViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *CGUtextView;
@end

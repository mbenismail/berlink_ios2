/*=====================
 -- Pixl --
 
 Created for CodeCanyon
 by FV iMAGINATION
 =====================*/

// Ad banners imports
#import <AudioToolbox/AudioToolbox.h>


#import <UIKit/UIKit.h>
#import "MYYCameraViewController.h"


UIImagePickerController *picker;

UIDocumentInteractionController *imageFile;
UIImage *combinedImage;



@interface PartageViewController : UIViewController
<
UIPopoverControllerDelegate,
UINavigationControllerDelegate,
UIImagePickerControllerDelegate,
UITabBarDelegate,
UIActionSheetDelegate,
UIScrollViewDelegate,
UIDocumentInteractionControllerDelegate,
YCameraViewControllerDelegate
>


// Views ==============
@property (weak, nonatomic) IBOutlet UIView *imageContainer;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *_imageView;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;



@end

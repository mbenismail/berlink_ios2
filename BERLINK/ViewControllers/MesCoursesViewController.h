//
//  MesCoursesViewController.h
//  BERLINK
//
//  Created by helmi on 4/19/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HistoriqueTableViewCell.h"
#import "User.h"

@interface MesCoursesViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
int note;

}

@property (nonatomic, strong) NSArray* coursesList;
@property (nonatomic, strong) User* mainUser;

@property (weak, nonatomic) IBOutlet UIButton *passeBtn;
@property (weak, nonatomic) IBOutlet UIButton *venirBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthViewBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


- (IBAction)passeBtnSelected:(id)sender;
- (IBAction)venirBtnSelected:(id)sender;


@end

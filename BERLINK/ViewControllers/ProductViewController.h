//
//  ProductViewController.h
//  BERLINK
//
//  Created by helmi on 4/13/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Request.h"
@interface ProductViewController : UIViewController
{
    IBOutlet UITableView* mytableView;
}
@property (nonatomic, strong) Request* request;
@property (nonatomic, strong) NSMutableArray* productList;
@property BOOL editable;
@end

//
//  CarDetailsViewController.h
//  BERLINK
//
//  Created by BERLINK on 4/6/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Course.h"
@interface CarDetailsViewController : UIViewController
{
    IBOutlet UIImageView* carImageView;
    IBOutlet UITextView* carDescriptionView;
}

@property (nonatomic, strong) Course* currentCourse;
@end

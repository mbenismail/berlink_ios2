//
//  CourseViewController.m
//  BERLINK
//
//  Created by BERLINK on 3/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "CourseViewController.h"
#import "SWRevealViewController.h"
#import "Constants.h"
#import "StringConstants.h"
#import "SharedVar.h"
#import "WebServiceApi.h"
#import "Util.h"
#import "AppDelegate.h"
#import "FinCourseViewController.h"
#import "PartageViewController.h"
#import "CarDetailsViewController.h"
#import "SpotifyLoginViewController.h"
#import "Gradient.h"

@interface CourseViewController ()

@end

@implementation CourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
 [Gradient setBackgroundGradientForView:self.driverView color1Red:30.0 color1Green:30.0 color1Blue:30.0 color2Red:0.0 color2Green:0.0 color2Blue:0.0 alpha:1.0];
    
    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    
    
    if([[[[SharedVar shareManager] mainUser] role] isEqualToString:ROLE_USER])
    {
        [tabBarView setHidden:NO];
        [finirCourseBtn setHidden:YES];
    }
    else{
        [tabBarView setHidden:YES];
        [finirCourseBtn setHidden:NO];
    }
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    userAddressTextField.text = appd.currentCourse.adressEnd;
    
    draw_user_annotation = 0;
    
    [self drawTheMap];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    
    FIRDatabase*  database = [FIRDatabase database];
    FIRDatabaseReference* myRef = [database referenceWithPath:@"course"];
    
    NSString* courseStr = [NSString stringWithFormat:@"course_%d", appd.currentCourse.idcourse.intValue];
    NSLog(@"course %@", courseStr);
    db = [myRef child:courseStr];
    
    [db observeEventType:FIRDataEventTypeValue
               withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                   NSLog(@"event fri 2");
                   NSLog(@"CC %@", [snapshot value]);
                   FIRDataSnapshot* lat_d =[snapshot childSnapshotForPath:@"lat_driver"];
                   if(lat_d && lat_d.value)
                   {
                       lat_driver = [[lat_d value] doubleValue];
                   }
                   FIRDataSnapshot* lng_d =[snapshot childSnapshotForPath:@"lng_driver"];
                   if(lng_d && lng_d.value)
                   {
                       lng_driver = [[lng_d value] doubleValue];
                   }
                   [self drawAnnotation:lat_driver andLng:lng_driver];
               }
         withCancelBlock:^(NSError * _Nonnull error) {
             
         }];
    
    [db observeEventType:FIRDataEventTypeValue
               withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                   NSLog(@"event fri 1");
                   NSLog(@"CC %@", [snapshot value]);
                   FIRDataSnapshot* statut =[snapshot childSnapshotForPath:@"statut"];
                   if([snapshot hasChild:@"statut"] &&  statut && statut.value && [statut.value intValue] == 2)
                   {
                       if([[[[SharedVar shareManager] mainUser] role] isEqualToString:ROLE_USER])
                       {
                           [[Util shareManager] showWaitingViewController:self.revealViewController];
                           [[WebServiceApi shareManager] invokeCustomerCourseWebServiceWithParams_User:[[SharedVar shareManager] mainUser] completionHandler:^(NSString *errorMsg, Course *course, Request* request) {
                               [[Util shareManager] dismissWating];
                               
                               if([errorMsg isEqualToString:@""])
                               {
                                   if(course && course.idcourse)
                                   {
                                       appd.currentCourse = course;
                                       [appd gotoFinCourseView];
                                   }
                               }
                               else
                               {
                                   [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
                               }
                               
                           }];
                       }
                       else{
                           [[Util shareManager] showWaitingViewController:self.revealViewController];
                           [[WebServiceApi shareManager] invokeDriverCourseWebServiceWithParams_User:[[SharedVar shareManager] mainUser] completionHandler:^(NSString *errorMsg, Course *course, NSMutableArray* list_request_waiting) {
                               [[Util shareManager] dismissWating];
                               
                               if([errorMsg isEqualToString:@""])
                               {
                                   if(course && course.idcourse)
                                   {
                                       appd.currentCourse = course;
                                       [appd gotoFinCourseView];
                                   }
                               }
                               else
                               {
                                   [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
                               }
                               
                           }];
                       }
                       
                       
                       
                       [db removeAllObservers];
                   }
                   
               }
         withCancelBlock:^(NSError * _Nonnull error) {
             
         }];
    
    NSString* modelCar = [[SharedVar shareManager] getCarModelByID:appd.currentCourse.idCar];
    
    if(modelCar && modelCar.length > 0)
        [carBtnLabel setText:[appd.currentCourse.nameDriver uppercaseString]];
    else
        [carBtnLabel setText:appd.currentCourse.name];
    
    if(appd.currentCourse.nameDriver && appd.currentCourse.nameDriver.length > 0)
        nameDriverLabel.text = appd.currentCourse.nameDriver;
    else
        nameDriverLabel.text = @"Chauffeur";
    [self setRating:appd.currentCourse.noteDriver];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [locationManager stopUpdatingHeading];
}

-(void)setRating:(NSNumber *)rating
{
    for(int i = 1;i<[rating intValue];i++)
    {
        UIImageView *ratingImage =  (UIImageView*)[self.imageView viewWithTag:i];
        [ratingImage setHighlighted:true];
    }
}


-(IBAction)carDetail:(id)sender  {
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    CarDetailsViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"CarDetailsViewController"];
    catViewC.currentCourse = appd.currentCourse;
    [self presentViewController:catViewC animated:YES completion:nil];
}


-(IBAction)finirLaCourse:(id)sender{
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSNumber* old_statut = appd.currentCourse.statut;
    appd.currentCourse.statut = [NSNumber numberWithInt:2];
    [[Util shareManager] showWaitingViewController:self];
    [[WebServiceApi shareManager] invokeUpdateCourseWebServiceWithParams_user:[[SharedVar shareManager] mainUser] andCourse:appd.currentCourse completionHandler:^(NSString *errorMsg, Course *request) {
        [[Util shareManager] dismissWating];
        if([errorMsg isEqualToString:@""])
        {
            FIRDatabaseReference *rootRef= [[FIRDatabase database] referenceWithPath:@"course"];
            
            NSString* userStr = [NSString stringWithFormat:@"/course_%d",appd.currentCourse.idcourse.intValue];
            NSLog(@"firebase course %@", userStr);
            
            NSMutableDictionary* userValue = [NSMutableDictionary new];
            [userValue setValue:[NSNumber numberWithDouble:currentLocation.coordinate.latitude] forKey:@"lat_driver"];
            [userValue setValue:[NSNumber numberWithDouble:currentLocation.coordinate.longitude] forKey:@"lng_driver"];
            appd.current_statut = 2;
            NSLog(@"firebase statut %d", 2);
            [userValue setValue:[NSNumber numberWithDouble:2] forKey:@"statut"];
            
            NSMutableDictionary* cValue = [NSMutableDictionary new];
            [cValue setValue:userValue forKey:userStr];
            
            [rootRef updateChildValues:cValue];
            
            if(request && request.idcourse)
            {
                //actif
                [[[SharedVar shareManager] mainUser] setStatut:[NSNumber numberWithInt:2]];
                appd.currentCourse = request;
                [appd gotoFinCourseView];
                [appd stopUpdateCourseLocation];
            }
        }
        else
        {
            [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
            appd.currentCourse.statut = old_statut;
        }
        
    }];
    
}


-(void)updateCourseInfo {
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSString* destination = [NSString stringWithFormat:@"%8f,%8f", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude];
    NSString* depart = [NSString stringWithFormat:@"%8f,%8f", appd.currentCourse.latstart.doubleValue, appd.currentCourse.lngstart.doubleValue];
    
    [self fetchFinalPrice:depart destination:destination completionHandler:^(NSError *error){
        if(!error)
        {
            
            appd.currentCourse.latend = [NSNumber numberWithDouble:currentLocation.coordinate.latitude];
            appd.currentCourse.lngend = [NSNumber numberWithDouble:currentLocation.coordinate.longitude];
            appd.currentCourse.duration = calculated_duration;
            appd.currentCourse.distance = [NSNumber numberWithInt:calculated_distance];
            
            int final_price = [self calculateFinalPrice];
            appd.currentCourse.prixTotal = [NSNumber numberWithInt:final_price];
        }
    }];
}

-(int) calculateFinalPrice {
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    Categorie* currentCategory = [[SharedVar shareManager] getCategoryByID:appd.currentCourse.idCategory];
    int price = currentCategory.pricepack.intValue;
    if(appd.currentCourse.distance.intValue <= currentCategory.unitinclu.intValue)
    {
        
    }
    else{
        
        price = currentCategory.pricepack.intValue + currentCategory.pricesup.intValue*(calculated_distance - currentCategory.unitinclu.intValue);
    }
    
    if(appd.currentCourse.pricebid)
        price = price + appd.currentCourse.pricebid.intValue;
    
    
    return price;
}
- (void)fetchFinalPrice:(NSString* )originStr destination:(NSString* )destinationStr completionHandler:(void (^)(NSError * error))completionHandler
{
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"origin=%@&destination=%@&mode=driving&key=%@", originStr, destinationStr, GOOGLE_MAP_DIRECTION_API_KEY];
    NSString *escapedString = [directionsUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSURL *directionsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", directionsAPI, escapedString]];
    
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                                 ^(NSData *data, NSURLResponse *response, NSError *error)
                                                 {
                                                     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                     if(error)
                                                     {
                                                         if(completionHandler)
                                                             completionHandler(nil);
                                                         return;
                                                     }
                                                     
                                                     NSArray *routesArray;
                                                     
                                                     if(json)
                                                     {
                                                         routesArray = [json objectForKey:@"routes"];
                                                     }
                                                     
                                                     if (routesArray && [routesArray count] > 0)
                                                     {
                                                         NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                         NSArray *legs = [routeDict objectForKey:@"legs"];
                                                         if ([legs count] > 0)
                                                         {
                                                             NSDictionary *legDict = [legs objectAtIndex:0];
                                                             
                                                             NSDictionary *duration = [legDict objectForKey:@"duration"];
                                                             calculated_duration = [duration objectForKey:@"text"];
                                                             
                                                             if(calculated_duration)
                                                                 self.lblDuratation.text = calculated_duration;
                                                             else
                                                                 self.lblDuratation.text = @"60 min";
                                                             
                                                             NSDictionary *dist_dict = [legDict objectForKey:@"distance"];
                                                             NSNumber *distance_t = [dist_dict objectForKey:@"value"];
                                                             
                                                             NSLog(@"distance_t %@", distance_t);
                                                             calculated_distance = [distance_t intValue]/1000;
                                                             NSLog(@"calculated_distance %d", calculated_distance);
                                                             
                                                        
                                                            
                                                         }
                                                         
                                                     }
                                                     
                                                     // run completionHandler on main thread
                                                     dispatch_sync(dispatch_get_main_queue(), ^{
                                                         completionHandler(error);
                                                     });
                                                 }];
    [fetchDirectionsTask resume];
}

-(void)drawTheMap{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
                                                            longitude:151.20
                                                                 zoom:14];
    [umapView setCamera:camera];
    //umapView.myLocationEnabled = YES;
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"mapstyle" withExtension:@"json"];
    NSError *error;
    
    // Set the map style by passing the URL for style.json.
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    
    umapView.mapStyle = style;
}

-(void)drawEndLocation{
    GMSMarker *marker = [[GMSMarker alloc] init];
    CLLocationCoordinate2D location;
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    location.latitude = appd.currentCourse.latend.doubleValue;
    location.longitude = appd.currentCourse.lngend.doubleValue;
    
    marker.position = location;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    //UIImage *markerIcon = [UIImage imageNamed:@"n_radar_icon.png"];
    //markerIcon = [markerIcon imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 0, (markerIcon.size.height/2), 0)];
    //marker.icon = markerIcon;
    marker.map = umapView;
    
    NSString* depart = [NSString stringWithFormat:@"%8f,%8f", lat_driver, lng_driver];
    //NSString* destination = [NSString stringWithFormat:@"%8f,%8f", appd.currentCourse.latend.doubleValue, appd.currentCourse.lngend.doubleValue];
    [self fetchPolylineWithOrigin:depart destination:appd.currentCourse.adressEnd completionHandler:^(GMSPolyline *polyline)
     {
         if(polyline)
             polyline.map = umapView;
     }];
}

- (void)fetchPolylineWithOrigin:(NSString* )originStr destination:(NSString* )destinationStr completionHandler:(void (^)(GMSPolyline *))completionHandler
{
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"origin=%@&destination=%@&mode=driving&key=%@", originStr, destinationStr, GOOGLE_MAP_DIRECTION_API_KEY];
    NSString *escapedString = [directionsUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSURL *directionsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", directionsAPI, escapedString]];
    
    //NSLog(@"route url:%@", directionsUrl.absoluteString);
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                                 ^(NSData *data, NSURLResponse *response, NSError *error)
                                                 {
                                                     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                     if(error)
                                                     {
                                                         if(completionHandler)
                                                             completionHandler(nil);
                                                         return;
                                                     }
                                                     
                                                     NSArray *routesArray;
                                                     
                                                     if(json)
                                                     {
                                                         routesArray = [json objectForKey:@"routes"];
                                                     }
                                                     dispatch_sync(dispatch_get_main_queue(), ^{
                                                         
                                                         GMSPolyline *polyline = nil;
                                                         if (routesArray && [routesArray count] > 0)
                                                         {
                                                             NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                             NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                                             NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                                             GMSPath *path = [GMSPath pathFromEncodedPath:points];
                                                             polyline = [GMSPolyline polylineWithPath:path];
                                                              polyline.strokeColor = UIColorFromRGB(215,192, 170,1);
                                                             
                                                             NSArray *legs = [routeDict objectForKey:@"legs"];
                                                             if ([legs count] > 0)
                                                             {
                                                                 NSDictionary *legDict = [legs objectAtIndex:0];
                                                                 NSDictionary *start_location = [legDict objectForKey:@"start_location"];
                                                                 NSString *s_lat = [start_location objectForKey:@"lat"];
                                                                 NSString *s_lng = [start_location objectForKey:@"lng"];
                                                                 GMSMarker *s_marker = [[GMSMarker alloc] init];
                                                                 CLLocationCoordinate2D s_location;
                                                                 s_location.latitude = [s_lat doubleValue];
                                                                 s_location.longitude = [s_lng doubleValue];
                                                                 
                                                                 s_marker.position = s_location;
                                                                 s_marker.appearAnimation = kGMSMarkerAnimationPop;
                                                                 //marker.icon = [UIImage imageNamed:@"n_radar_icon.png"];
                                                                 //s_marker.map = umapView;
                                                                 
                                                             }
                                                             
                                                         }
                                                         
                                                         // run completionHandler on main thread
                                                         
                                                         if(completionHandler)
                                                             completionHandler(polyline);
                                                     });
                                                 }];
    [fetchDirectionsTask resume];
}

#pragma mark - CLLocationManagerDelegate

/*
 - (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
 {
 NSLog(@"didFailWithError: %@", error);
 }
 
 - (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
 {
 //NSLog(@"didUpdateToLocation: %@", newLocation);
 currentLocation = newLocation;
 
 if (currentLocation != nil) {
 lngUser = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
 latUser = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
 [self drawAnnotation:lat_driver andLng:lng_driver];
 if (draw_user_annotation < 1)
 {
 GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latUser.doubleValue
 longitude:lngUser.doubleValue
 zoom:14];
 [umapView setCamera:camera];
 draw_user_annotation++;
 }
 }
 
 
 }*/

-(void) drawDriverLocation:(double)lat andLng:(double)lng{
    GMSMarker *marker = [[GMSMarker alloc] init];
    CLLocationCoordinate2D location;
    location.latitude = lat;
    location.longitude = lng;
    
    marker.position = location;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    UIImage *markerIcon = [UIImage imageNamed:@"n_car_icon_map.png"];
    markerIcon = [markerIcon imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 0, (markerIcon.size.height/2), 0)];
    marker.icon = markerIcon;
    marker.map = umapView;
}

-(void) drawAnnotation:(double)lat andLng:(double)lng{
    [umapView clear];
    [self drawEndLocation];
    [self drawDriverLocation:lat andLng:lng];
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    
    if (currentLocation != nil && [[[[SharedVar shareManager] mainUser] role] isEqualToString:ROLE_DRIVER]) {
        [self updateChildren:currentLocation.coordinate.latitude andLng:currentLocation.coordinate.longitude];
    }
    
    if (currentLocation != nil) {
        lngUser = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latUser = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        [self drawAnnotation:lat_driver andLng:lng_driver];
        if (draw_user_annotation < 1)
        {
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latUser.doubleValue
                                                                    longitude:lngUser.doubleValue
                                                                         zoom:14];
            [umapView setCamera:camera];
            draw_user_annotation++;
        }
        [self updateCourseInfo];
    }
}

-(void)updateChildren:(double)lat andLng:(double)lng{
    
    FIRDatabaseReference *rootRef= [[FIRDatabase database] referenceWithPath:@"course"];
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSString* userStr = [NSString stringWithFormat:@"/course_%d",appd.currentCourse.idcourse.intValue];
    NSLog(@"firebase course %@", userStr);
    
    NSMutableDictionary* userValue = [NSMutableDictionary new];
    [userValue setValue:[NSNumber numberWithDouble:lat] forKey:@"lat_driver"];
    [userValue setValue:[NSNumber numberWithDouble:lng] forKey:@"lng_driver"];
    NSLog(@"firebase statut %d", appd.current_statut);
    [userValue setValue:[NSNumber numberWithDouble:appd.current_statut] forKey:@"statut"];
    
    NSMutableDictionary* cValue = [NSMutableDictionary new];
    [cValue setValue:userValue forKey:userStr];
    
    [rootRef updateChildValues:cValue];
    
}

-(IBAction)Partager:(id)sender{
    PartageViewController* eViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"PartageViewController"];
    [self.navigationController pushViewController:eViewC animated:YES];
}
- (IBAction)musicAction:(id)sender {
    
    SpotifyLoginViewController* eViewC = [[UIStoryboard storyboardWithName:@"Music" bundle:nil] instantiateViewControllerWithIdentifier:@"SpotifyLoginViewController"];
    [self.navigationController pushViewController:eViewC animated:true];
}

@end

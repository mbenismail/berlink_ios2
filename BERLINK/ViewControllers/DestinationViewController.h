//
//  DestinationViewController.h
//  BERLINK
//
//  Created by BERLINK on 2/10/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <GooglePlaces/GooglePlaces.h>
#import "TRAutocompleteView.h"
#import "Request.h"
#import "Categorie.h"

@import GoogleMaps;

@interface DestinationViewController : UIViewController<CLLocationManagerDelegate, UITextFieldDelegate>
{
    CLLocationManager *locationManager;
    NSString* lngUser;
    NSString* latUser;
    IBOutlet UITextField* userAddressTextField;
    IBOutlet GMSMapView* umapView;
    IBOutlet UITextField* destinationTextField;
    IBOutlet UIButton* lancerButton;
    IBOutlet UIView* messageView;
    IBOutlet UILabel* estimatedDurationLabel;
    IBOutlet UILabel* estimatedPriceLabel;

    TRAutocompleteView *_autocompleteDepartView;
    TRAutocompleteView *_autocompleteDestinationView;
    int draw_user_annotation;
    CLLocation *currentLocation;
    
    NSString* adresseend;
    NSString* addressstart;
    
    NSString* lngstart;
    NSString* latend;
    NSString* latstart;
    NSString* lngend;
    int calculated_distance;
    NSString* calculated_duration;
    int prise_en_charge_distance;
    int estimated_price;
    IBOutlet UIImageView* alertImage;
    IBOutlet UILabel* alertLabel;
    IBOutlet UIImageView* messageViewBackViewSmall;
    IBOutlet UIImageView* messageViewBackViewLarge;
    int messageview_y;
    NSString* strDest;
    
}
-(void)updateReq:(NSString*)str;
@property (weak, nonatomic) IBOutlet UILabel *lblCarName;

@property(nonatomic,retain)NSString* strCategory;
@property (nonatomic, strong) Request* currentRequest;
@property (nonatomic, strong) Categorie* currentCategory;
@property(nonatomic, retain)NSString* strTime;

@end

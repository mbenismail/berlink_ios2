//
//  RequestWaitingViewController.h
//  BERLINK
//
//  Created by BERLINK on 3/21/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Request.h"

@interface RequestWaitingViewController : UIViewController
{
    IBOutlet UITextField* userAddressTextField;
    IBOutlet UITextField* destinationTextField;
    IBOutlet UIActivityIndicatorView* activityIndicator;
    NSTimer *timer;
    IBOutlet UIButton* annulerBtn;

}
@property (nonatomic, strong) Request* request;
@end

//
//  CarDetailsViewController.m
//  BERLINK
//
//  Created by BERLINK on 4/6/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "CarDetailsViewController.h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CarDetailsViewController ()

@end

@implementation CarDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    carImageView.image = [UIImage imageNamed:@"default_car_image"];
    carDescriptionView.text = @"";
    
 NSURL* aURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SERVER_URL,_currentCourse.imageCar]];
    [carImageView sd_setImageWithURL:aURL
                    placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    if(_currentCourse.descriptionCar && _currentCourse.descriptionCar.length > 0)
    {
        carDescriptionView.text = _currentCourse.descriptionCar;
    }
}

-(IBAction)close:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

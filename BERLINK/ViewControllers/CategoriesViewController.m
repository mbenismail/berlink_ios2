//
//  CategoriesViewController.m
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "CategoriesViewController.h"
#import "CategoryTableViewCell.h"
#import "SWRevealViewController.h"
#import "SharedVar.h"
#import "Categorie.h"
#import "Constants.h"
#import "EnchereViewController.h"
#import "Brand.h"
#import "DestinationViewController.h"
#import "WebServiceApi.h"
#import "Util.h"
#import "StringConstants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImage+animatedGIF.h"

@interface CategoriesViewController ()

@end

@implementation CategoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SWRevealViewController *revealController = [self revealViewController];
    
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;

    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    myTableView.scrollEnabled = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = false;
}

#pragma mark - Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[SharedVar shareManager] mainCategories] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CategoryTableViewCell";
    
    CategoryTableViewCell *cell = (CategoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Categorie* c = [[[SharedVar shareManager] mainCategories] objectAtIndex:indexPath.row];
    
    NSString* title = [c.range uppercaseString];
    cell.imageBack.layer.masksToBounds = true;
    cell.imageBack.layer.cornerRadius = 5.0;
    cell.imageBack.layer.borderWidth = 1;
   // cell.imageBack.layer.borderColor = [UIColor colorWithRed:79/255 green:79/255 blue:79/255 alpha:1].CGColor;
    
    cell.imageBack.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    if([title  isEqual: @"PREMIUM"])
    {
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"premium" withExtension:@"gif"];
        cell.imageTitle.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
        //cell.imageTitle.image = [UIImage imageNamed:@"premium"];
    }else
    if([title  isEqual: @"BUSINESS"])
    {
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"business" withExtension:@"gif"];
        cell.imageTitle.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
        //cell.imageTitle.image = [UIImage imageNamed:@"business"];
    }else
    if([title  isEqual: @"PRESTIGE"])
    {
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"prestige" withExtension:@"gif"];
        cell.imageTitle.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
        //cell.imageTitle.image = [UIImage imageNamed:@"prestige"];
    }

    NSMutableString* brands = [NSMutableString new];
    
    for(Brand* brand in c.brands)
    {
        [brands appendFormat:@"%@ - ", brand.title];
    }
    NSString *truncatedString;
    if(brands.length > 3)
        truncatedString = [brands substringToIndex:[brands length]-3];
    else
        truncatedString = brands;
    
    cell.brandsLabel.text = [truncatedString uppercaseString];
    NSString* msg = GATEGORY_DATA;
    msg = [msg stringByReplacingOccurrencesOfString:@"(_P_)" withString:[NSString stringWithFormat:@"%d", c.pricepack.intValue]];
    msg = [msg stringByReplacingOccurrencesOfString:@"(_X_)" withString:[NSString stringWithFormat:@"%d", c.pricesup.intValue]];
    msg = [msg stringByReplacingOccurrencesOfString:@"(_Y_)" withString:[NSString stringWithFormat:@"%d", c.measure.intValue]];
    cell.infoLabel.text = msg;
     NSURL* aURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SERVER_URL,c.photo]];
    [[cell imageBack] sd_setImageWithURL:aURL
                   placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.biddingImage setHidden:true];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return tableView.frame.size.height/3;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Categorie* c = [[[SharedVar shareManager] mainCategories] objectAtIndex:indexPath.row];
    
    [[Util shareManager] showWaitingViewController:self];
    
    [self getAddressFromLocation:currentLocation complationBlock:^(NSString *address){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self getAddressFromAdreesse:address completionHL:^(NSString *city) {
                
                if(city)
                {
                    User* user = [[SharedVar shareManager] mainUser];
                   // city = @"Pimpri Chinchwad";
                    [[WebServiceApi shareManager] invokeDriverDispo_user:user andIdCat:c.idcategory andCity:city completionHandler:^(NSString *errorMsg) {
                        
                        [[Util shareManager] dismissWating];
                        if(errorMsg.length > 0)
                        {
                            [[Util shareManager] showWarning:errorMsg withTitle:APP_NAME andViewController:self];
                        }
                        else{
                            
                            Request* req = [Request new];
                            req.idcustomer = [[SharedVar shareManager] mainUser].idcustomer;
                            req.idcategory = c.idcategory;
                            req.latstart = latUser;
                            req.lngstart = lngUser;
                            req.dateCreated = [[Util shareManager] currentDate];
                            //req.bid = [Bid new];
                            //req.bid.idBid = [NSNumber numberWithInt:0];
                            
                            [[Util shareManager] showWaitingViewController:self];
                            [[WebServiceApi shareManager] invokeCreateRequestWebServiceWithParams_user:[[SharedVar shareManager] mainUser] andRequest:req completionHandler:^(NSString *errorMsg, Request *request) {
                                [[Util shareManager] dismissWating];
                                if(errorMsg && errorMsg.length > 0)
                                {
                                    [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
                                    return;
                                }
                                
                                switch (indexPath.row) {
                                    case 0:
                                            strCat = @"premium";
                                        break;
                                    case 1:
                                        strCat = @"business";
                                        break;
                                    case 2:
                                        strCat = @"prestige";
                                        break;
                                        
                                    default:
                                        break;
                                }
                                
                                //if(c.idcategory.intValue == 1)
                                if(request && request.bid && request.bid.idBid )
                                { [self goToEnchereViewC:request andCategory:c str : strCat];
                               /* { if(c.maxbid.intValue==0){
                                    [self goToEnchereViewC:request andCategory:c];
                               } else
                                {
                                    [self goToDestinationViewC:request andCat:c];
                                }*/
                                }
                                else
                                {
                                    [self goToDestinationViewC:request andCat:c str : strCat];
                                }
                                //if(request && request.idcustomer)
                                //  [self goToDestinationViewC:request andCat:c];
                            }];
                        }
                    }];
                }
                else
                {
                    [[Util shareManager] dismissWating];
                    [[Util shareManager] showError:UNABLE_TO_GET_CITY_MESSAGE withTitle:APP_NAME andViewController:self];
                }
                
            }];
        });
    }];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    
    if (currentLocation != nil) {
        lngUser = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latUser = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
}

- (void) getAddressFromAdreesse:(NSString *)strAdr completionHL:(void (^) (NSString* city))handler
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder geocodeAddressString:strAdr completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if (error){
            NSLog(@"Geocode failed with error: %@", error);
            handler(nil);
        }
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
        NSLog(@"locality %@",placemark.locality);
        NSLog(@"postalCode %@",placemark.postalCode);
        handler(placemark.locality);
    }];
}

typedef void(^addressCompletion)(NSString *);

-(void)getAddressFromLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock
{
  //  CLLocation *LocationAtual = [[CLLocation alloc] initWithLatitude:18.5978796 longitude:73.7556431];
    
   // CLLocation *LocationAtual = [[CLLocation alloc] initWithLatitude:18.5978796 longitude:73.7556431];

    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             address = [NSString stringWithFormat:@"%@, %@ %@", placemark.name, placemark.postalCode, placemark.locality];
             completionBlock(address);
         }
     }];
}

#pragma mark - Navigation
-(void) goToEnchereViewC:(Request*)request andCategory:(Categorie*)c str : (NSString*)strCat{
    EnchereViewController* eViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"EnchereViewController"];
    eViewC.currentRequest = request;
    eViewC.currentCategory = c;
    eViewC.strCategory = strCat;
    [self.navigationController pushViewController:eViewC animated:YES];
}

-(void) goToDestinationViewC:(Request*)req andCat:(Categorie*)c str : (NSString*) strCat{
    DestinationViewController* eViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"DestinationViewController"];
    eViewC.currentRequest = req;
    eViewC.strCategory = strCat;
    eViewC.currentCategory = c;
    [self.navigationController pushViewController:eViewC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

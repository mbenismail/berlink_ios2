//
//  FinCourseViewController.h
//  BERLINK
//
//  Created by BERLINK on 3/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FinCourseViewController : UIViewController
{
    IBOutlet UILabel* nameDriverLabel;
    IBOutlet UIButton* noteBtn1;
    IBOutlet UIButton* noteBtn2;
    IBOutlet UIButton* noteBtn3;
    IBOutlet UIButton* noteBtn4;
    IBOutlet UIButton* noteBtn5;

    int note;
    
    IBOutlet UILabel* pointGainLabelDesc;
    IBOutlet UILabel* pointGainLabel;
    IBOutlet UILabel* durationLabel;

    IBOutlet UIImageView* userImage;

    IBOutlet UILabel* prixLabel;

}
@property (weak, nonatomic) IBOutlet UIView *viewGrad;
@property (weak, nonatomic) IBOutlet UITextField *txtComment;

@end

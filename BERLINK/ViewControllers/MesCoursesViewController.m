//
//  MesCoursesViewController.m
//  BERLINK
//
//  Created by helmi on 4/19/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "MesCoursesViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "SharedVar.h"
#import "StringConstants.h"
#import "WebServiceApi.h"
#import "AppDelegate.h"
#import "Util.h"
#import "StringConstants.h"

@interface MesCoursesViewController ()

@end

@implementation MesCoursesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    SWRevealViewController *revealController = [self revealViewController];
    
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;

    
    _mainUser = [[SharedVar shareManager] mainUser];

   // if([_mainUser.role isEqualToString:ROLE_USER])
   // {
       // _widthViewBtn.constant=0;
       // _passeBtn.hidden=true;
       // _venirBtn.hidden=true;

        
   /* }else{
        
    }*/
    
    [self getListCoursesWS];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    _venirBtn.backgroundColor=[UIColor blackColor];
    _passeBtn.selected=true;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getListCoursesWS {
    
    [[Util shareManager] showWaitingViewController:self];
    [[WebServiceApi shareManager] invokeMyCoureWebServiceWithParams_User:[[SharedVar shareManager] mainUser] completionHandler:^(NSString *errorMsg, Course* course, Request* request,NSArray *courseArray) {
        [[Util shareManager] dismissWating];
        if([errorMsg isEqualToString:@""])
        {
            NSLog(@"%@",courseArray);
            _coursesList=courseArray;
            [self.tableView reloadData];
        }
        else
        {
             [[Util shareManager] showWarning:@"Aucune course enregistrée" withTitle:APP_NAME andViewController:self];
        }
    }];
}

- (IBAction)passeBtnSelected:(id)sender {
    _passeBtn.selected=true;
    _venirBtn.selected=false;
    _venirBtn.backgroundColor=[UIColor blackColor];
    _passeBtn.backgroundColor=[UIColor clearColor];
}

- (IBAction)venirBtnSelected:(id)sender {
    _venirBtn.selected=true;
    _passeBtn.selected=false;
    _passeBtn.backgroundColor=[UIColor blackColor];
    _venirBtn.backgroundColor=[UIColor clearColor];
}
-(NSString *)getDate:(NSString *)dateString{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* date = [dateFormatter dateFromString:dateString];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *mydate = [dateFormatter stringFromDate:date];
    return mydate;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _coursesList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"HistoriqueTableViewCell";
    HistoriqueTableViewCell *cell = (HistoriqueTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HistoriqueTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSString *mydate = _coursesList[indexPath.row][@"dateCreated"];

    cell.dateLabel.text = [self getDate:mydate];
    
    cell.totalLabel.text = [NSString stringWithFormat:@"%@ €",_coursesList[indexPath.row][@"prixTotal"]];
    cell.nameLabel.text = _coursesList[indexPath.row][@"nameDriver"];
    cell.sourceLabel.text = _coursesList[indexPath.row][@"adressStart"];
    cell.destinationLabel.text = _coursesList[indexPath.row][@"adressEnd"];
    int rating = [_coursesList[indexPath.row][@"noteDriver"] intValue];
    if([_mainUser.role isEqualToString:ROLE_DRIVER])
    {
        cell.nameLabel.text = _coursesList[indexPath.row][@"nameCustomer"];
        rating = [_coursesList[indexPath.row][@"noteCustomer"] intValue];
    }
    [cell setRating:rating];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 230;
}
@end

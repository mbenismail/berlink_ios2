//
//  WaitingViewController.m
//  BERLINK
//
//  Created by BERLINK on 3/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "WaitingViewController.h"
#import "SWRevealViewController.h"
#import "Constants.h"
#import "StringConstants.h"
#import "SharedVar.h"
#import "WebServiceApi.h"
#import "Util.h"
#import "AppDelegate.h"
#import "CarDetailsViewController.h"
#import "Gradient.h"
@interface WaitingViewController ()

@end

@implementation WaitingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    SWRevealViewController *revealController = [self revealViewController];
    
     [Gradient setBackgroundGradientForView:driverView color1Red:30.0 color1Green:30.0 color1Blue:30.0 color2Red:0.0 color2Green:0.0 color2Blue:0.0 alpha:1.0];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;

    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    durationLabel.text = @"60 min";
    nameDriverLabel.text = appd.currentCourse.nameDriver;
    
    if([[[[SharedVar shareManager] mainUser] role] isEqualToString:ROLE_USER])
    {
        [tabBarView setHidden:NO];
        [demarrerCourseBtn setHidden:YES];
    }
    else{
        [tabBarView setHidden:YES];
        [demarrerCourseBtn setHidden:NO];
    }

    userAddressTextField.text = appd.currentCourse.adressStart;
    destinationTextField.text = appd.currentCourse.adressEnd;

    draw_user_annotation = 0;

    [self drawTheMap];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    
    FIRDatabase*  database = [FIRDatabase database];
    FIRDatabaseReference* myRef = [database referenceWithPath:@"course"];

    NSString* courseStr = [NSString stringWithFormat:@"course_%d", appd.currentCourse.idcourse.intValue];
    NSLog(@"course %@", courseStr);
    db = [myRef child:courseStr];
    
    [db observeEventType:FIRDataEventTypeValue
               withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                   NSLog(@"event fri 4");
                   NSLog(@"CC %@", [snapshot value]);
                   FIRDataSnapshot* lat_d =[snapshot childSnapshotForPath:@"lat_driver"];
                   if(lat_d && lat_d.value)
                   {
                       lat_driver = [[lat_d value] doubleValue];
                   }
                   FIRDataSnapshot* lng_d =[snapshot childSnapshotForPath:@"lng_driver"];
                   if(lng_d && lng_d.value)
                   {
                       lng_driver = [[lng_d value] doubleValue];
                   }
                   [self drawAnnotation:lat_driver andLng:lng_driver];
               }
         withCancelBlock:^(NSError * _Nonnull error) {
             
         }];
    
    [db observeEventType:FIRDataEventTypeValue
               withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                   NSLog(@"event fri 5");
                   NSLog(@"CC %@", [snapshot value]);
                   FIRDataSnapshot* statut =[snapshot childSnapshotForPath:@"statut"];
                   if([snapshot hasChild:@"statut"] && statut && statut.value && [statut.value intValue] == 1)
                   {
                       AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                       NSLog(@"cc statut %d", [statut.value intValue]);
                       NSLog(@"houni 3");
                       
                       [appd gotoCourseView];
                       [db removeAllObservers];
                       
                   }
                   
               }
         withCancelBlock:^(NSError * _Nonnull error) {
             
         }];
    NSString* modelCar = [[SharedVar shareManager] getCarModelByID:appd.currentCourse.idCar];
    
    if(modelCar && modelCar.length > 0)
        [carBtnLabel setText:[appd.currentCourse.nameDriver uppercaseString]];
    else
        [carBtnLabel setText:appd.currentCourse.name];
    
    if(appd.currentCourse.nameDriver && appd.currentCourse.nameDriver.length > 0)
        nameDriverLabel.text = appd.currentCourse.nameDriver;
    else
        nameDriverLabel.text = @"Chauffeur";
    [self setRating:appd.currentCourse.noteDriver];
}

-(void)setRating:(NSNumber *)rating
{
    for(int i = 1;i<[rating intValue];i++)
    {
        UIImageView *ratingImage =  (UIImageView*)[imageView viewWithTag:i];
        [ratingImage setHighlighted:true];
    }
}

-(IBAction)carDetail:(id)sender  {
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    CarDetailsViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"CarDetailsViewController"];
    catViewC.currentCourse = appd.currentCourse;
    [self presentViewController:catViewC animated:YES completion:nil];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    
    if (currentLocation != nil && [[[[SharedVar shareManager] mainUser] role] isEqualToString:ROLE_DRIVER]) {
        [self updateChildren:currentLocation.coordinate.latitude andLng:currentLocation.coordinate.longitude];
    }
    
    if (currentLocation != nil) {
        lngUser = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latUser = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        [self drawAnnotation:lat_driver andLng:lng_driver];
        if (draw_user_annotation < 1)
        {
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latUser.doubleValue
                                                                    longitude:lngUser.doubleValue
                                                                         zoom:14];
            [umapView setCamera:camera];
            draw_user_annotation++;
        }
    }
}

-(void)updateChildren:(double)lat andLng:(double)lng{
    
    FIRDatabaseReference *rootRef= [[FIRDatabase database] referenceWithPath:@"course"];
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    NSString* userStr = [NSString stringWithFormat:@"/course_%d",appd.currentCourse.idcourse.intValue];
    NSLog(@"firebase course %@", userStr);
    
    NSMutableDictionary* userValue = [NSMutableDictionary new];
    [userValue setValue:[NSNumber numberWithDouble:lat] forKey:@"lat_driver"];
    [userValue setValue:[NSNumber numberWithDouble:lng] forKey:@"lng_driver"];
    NSLog(@"firebase statut %d", appd.current_statut);
    [userValue setValue:[NSNumber numberWithDouble:appd.current_statut] forKey:@"statut"];
    
    NSMutableDictionary* cValue = [NSMutableDictionary new];
    [cValue setValue:userValue forKey:userStr];
    
    [rootRef updateChildValues:cValue];
    
    
}

-(IBAction)demarrerLaCourse:(id)sender{
    
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSNumber* old_statut = appd.currentCourse.statut;
    appd.currentCourse.statut = [NSNumber numberWithInt:1];
    
    [[Util shareManager] showWaitingViewController:self];
    [[WebServiceApi shareManager] invokeUpdateCourseWebServiceWithParams_user:[[SharedVar shareManager] mainUser] andCourse:appd.currentCourse completionHandler:^(NSString *errorMsg, Course *request) {
       [[Util shareManager] dismissWating];
        if([errorMsg isEqualToString:@""])
        {
            FIRDatabaseReference *rootRef= [[FIRDatabase database] referenceWithPath:@"course"];
            
            NSString* userStr = [NSString stringWithFormat:@"/course_%d",appd.currentCourse.idcourse.intValue];
            NSLog(@"firebase course %@", userStr);
            
            NSMutableDictionary* userValue = [NSMutableDictionary new];
            [userValue setValue:[NSNumber numberWithDouble:currentLocation.coordinate.latitude] forKey:@"lat_driver"];
            [userValue setValue:[NSNumber numberWithDouble:currentLocation.coordinate.longitude] forKey:@"lng_driver"];
            appd.current_statut = 1;
            NSLog(@"firebase statut %d", appd.current_statut);
            [userValue setValue:[NSNumber numberWithDouble:1] forKey:@"statut"];
            
            NSMutableDictionary* cValue = [NSMutableDictionary new];
            [cValue setValue:userValue forKey:userStr];
            
            [rootRef updateChildValues:cValue];
        }
        else
        {
            [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
            appd.currentCourse.statut = old_statut;
        }
        
    }];
 
}

-(void)drawTheMap{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
                                                            longitude:151.20
                                                                 zoom:14];
    [umapView setCamera:camera];
    //umapView.myLocationEnabled = YES;
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"mapstyle" withExtension:@"json"];
    NSError *error;
    
    // Set the map style by passing the URL for style.json.
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    
    umapView.mapStyle = style;
}

-(void)drawUserLocation{
    GMSMarker *marker = [[GMSMarker alloc] init];
    CLLocationCoordinate2D location;
    
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    location.latitude = appd.currentCourse.latstart.doubleValue;
    location.longitude = appd.currentCourse.lngstart.doubleValue;
    
    marker.position = location;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    UIImage *markerIcon = [UIImage imageNamed:@"icon_source.png"];
    markerIcon = [markerIcon imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 0, (markerIcon.size.height/2), 0)];
    marker.icon = markerIcon;
    marker.map = umapView;
    
    NSString* depart = [NSString stringWithFormat:@"%8f,%8f", lat_driver, lng_driver];
    NSString* destination = [NSString stringWithFormat:@"%8f,%8f", appd.currentCourse.latstart.doubleValue, appd.currentCourse.lngstart.doubleValue];
    [self fetchPolylineWithOrigin:depart destination:destination completionHandler:^(GMSPolyline *polyline)
     {
         if(polyline)
             polyline.map = umapView;
     }];
}

- (void)fetchPolylineWithOrigin:(NSString* )originStr destination:(NSString* )destinationStr completionHandler:(void (^)(GMSPolyline *))completionHandler
{
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"origin=%@&destination=%@&mode=driving&key=%@", originStr, destinationStr, GOOGLE_MAP_DIRECTION_API_KEY];
    NSString *escapedString = [directionsUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSURL *directionsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", directionsAPI, escapedString]];
    
    //NSLog(@"route url:%@", directionsUrl.absoluteString);
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                                 ^(NSData *data, NSURLResponse *response, NSError *error)
                                                 {
                                                     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                     if(error)
                                                     {
                                                         if(completionHandler)
                                                             completionHandler(nil);
                                                         return;
                                                     }
                                                     
                                                     NSArray *routesArray;
                                                     
                                                     if(json)
                                                     {
                                                         routesArray = [json objectForKey:@"routes"];
                                                     }
                                                     
                                                     dispatch_sync(dispatch_get_main_queue(), ^{
                                                         
                                                         GMSPolyline *polyline = nil;
                                                         if (routesArray && [routesArray count] > 0)
                                                         {
                                                             NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                             NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                                             NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                                             GMSPath *path = [GMSPath pathFromEncodedPath:points];
                                                             polyline = [GMSPolyline polylineWithPath:path];
                                                             
                                                             NSArray *legs = [routeDict objectForKey:@"legs"];
                                                             if ([legs count] > 0)
                                                             {
                                                                 NSDictionary *legDict = [legs objectAtIndex:0];
                                                                 NSDictionary *start_location = [legDict objectForKey:@"start_location"];
                                                                 NSString *s_lat = [start_location objectForKey:@"lat"];
                                                                 NSString *s_lng = [start_location objectForKey:@"lng"];
                                                                 GMSMarker *s_marker = [[GMSMarker alloc] init];
                                                                 CLLocationCoordinate2D s_location;
                                                                 s_location.latitude = [s_lat doubleValue];
                                                                 s_location.longitude = [s_lng doubleValue];
                                                                 
                                                                 s_marker.position = s_location;
                                                                 s_marker.appearAnimation = kGMSMarkerAnimationPop;
                                                                 //marker.icon = [UIImage imageNamed:@"n_radar_icon.png"];
                                                                 //s_marker.map = umapView;
                                                                 
                                                                 
                                                                 
                                                                 NSDictionary *duration = [legDict objectForKey:@"duration"];
                                                                 NSString *duration_text = [duration objectForKey:@"text"];
                                                                 
                                                                 if(duration_text)
                                                                     durationLabel.text = duration_text;
                                                                 else
                                                                     durationLabel.text = @"60 min";
                                                             }
                                                             
                                                         }
                                                         
                                                         // run completionHandler on main thread
                                                      //dispatch_sync(dispatch_get_main_queue(), ^{

                                                         if(completionHandler)
                                                             completionHandler(polyline);
                                                    });
                                                 }];
    [fetchDirectionsTask resume];
}

#pragma mark - CLLocationManagerDelegate
/*
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    
    if (currentLocation != nil) {
        lngUser = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latUser = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        [self drawAnnotation:lat_driver andLng:lng_driver];
        if (draw_user_annotation < 1)
        {
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latUser.doubleValue
                                                                    longitude:lngUser.doubleValue
                                                                         zoom:14];
            [umapView setCamera:camera];
            draw_user_annotation++;
        }
    }
    
    
}*/

-(void) drawDriverLocation:(double)lat andLng:(double)lng{
    GMSMarker *marker = [[GMSMarker alloc] init];
    CLLocationCoordinate2D location;
    location.latitude = lat;
    location.longitude = lng;
    
    marker.position = location;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    UIImage *markerIcon = [UIImage imageNamed:@"n_car_icon_map.png"];
    markerIcon = [markerIcon imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 0, (markerIcon.size.height/2), 0)];
    marker.icon = markerIcon;
    marker.map = umapView;
}

-(void) drawAnnotation:(double)lat andLng:(double)lng{
    [umapView clear];
    [self drawUserLocation];
    [self drawDriverLocation:lat andLng:lng];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
}

-(IBAction)callDriver:(id)sender{
    UIApplication *application = [UIApplication sharedApplication];

    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSString *phNo = [NSString stringWithFormat:@"%@",appd.currentCourse.phoneDriver];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"tel:%@",phNo]];
  
  
    if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [application openURL:phoneUrl options:@{}
           completionHandler:^(BOOL success) {
               //NSLog(@"Open %@: %d",scheme,success);
           }];
    } else {
        [application openURL:phoneUrl];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [locationManager stopUpdatingHeading];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

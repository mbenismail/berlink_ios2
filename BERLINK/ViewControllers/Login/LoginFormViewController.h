//
//  LoginFormViewController.h
//  BERLINK
//
//  Created by BERLINK on 2/7/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginFormViewController : UIViewController
{
    IBOutlet UITextField* emailTextField;
    IBOutlet UITextField* passwordTextField;
    
    
}

@property(nonatomic, retain)NSString* strEmail;

@end

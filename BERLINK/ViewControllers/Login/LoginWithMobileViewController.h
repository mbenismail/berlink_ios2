//
//  LoginWithMobileViewController.h
//  BERLINK
//
//  Created by Pravin Jadhao on 31/01/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoginWithMobileViewController : UIViewController

@property (strong, nonatomic)  NSString *mobileNumber;
@property (strong, nonatomic)  NSString *countryCode;

@end

NS_ASSUME_NONNULL_END

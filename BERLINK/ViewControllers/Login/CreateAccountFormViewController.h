//
//  CreateAccountFormViewController.h
//  app
//
//  Created by BERLINK on 1/31/17.
//  Copyright © 2017 sifo. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Stripe;

@interface CreateAccountFormViewController : UIViewController<STPAddCardViewControllerDelegate>
{
    IBOutlet UIButton* personalButton;
    IBOutlet UIButton* professionalButton;
    
//    IBOutlet UITextField* textField1;
//    IBOutlet UITextField* textField2;
//    IBOutlet UITextField* textField3;
//    IBOutlet UITextField* textField4;
//    IBOutlet UITextField* textField5;
//    IBOutlet UITextField* textField6;
//    IBOutlet UITextField* textField7;
//    IBOutlet UITextField* textField8;
//    IBOutlet UITextField* textField9;
//    IBOutlet UITextField* textField10;
//    IBOutlet UITextField* textField11;
//    IBOutlet UITextField* textField12;
    
    /*rest of views*/
    IBOutlet UIView* formView;
    IBOutlet UIScrollView* formScrollView;
    IBOutlet UILabel* labelPaymentView;
    IBOutlet UIButton* paymentView;
    IBOutlet UIButton* createAccountButton;
    
    int currentViewType;
    
    UIImage* chooseImage;
    
    NSString* fbEmail;
    NSString* fbId;
    NSString* fbFirstName;
    NSString* fbLastName;
    NSString* stripeToken;

    NSNumber* idfacebook;
    NSNumber* countrycode;
    NSString* cardNumber;
    UITextField * activeField ;


}

@property BOOL withFacebook;
@property id result;

@property (weak, nonatomic) IBOutlet UIView *view7;
@property (weak, nonatomic) IBOutlet UIView *view8;
@property (weak, nonatomic) IBOutlet UIView *view9;
@property (weak, nonatomic) IBOutlet UIView *view10;

@property (weak, nonatomic) IBOutlet UIView *view6;

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view11;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view12;

@property (weak, nonatomic) IBOutlet UIView *view3;
@property (weak, nonatomic) IBOutlet UIView *view4;
@property (weak, nonatomic) IBOutlet UIView *view5;



//profession
@property (weak, nonatomic) IBOutlet UITextField *textCompName_nomDeSociete_PROF;
@property (weak, nonatomic) IBOutlet UITextField *textNumSiret_PROF;
@property (weak, nonatomic) IBOutlet UITextField *textNumverTVA_PROF;
@property (weak, nonatomic) IBOutlet UITextField *textCodePostal_PROF;
@property (weak, nonatomic) IBOutlet UITextField *textNOM_PROF;
@property (weak, nonatomic) IBOutlet UITextField *textPrenom_PROF;

@property (weak, nonatomic) IBOutlet UITextField *textEmail_PROF;
@property (weak, nonatomic) IBOutlet UITextField *textTele_PROF;

@property (weak, nonatomic) IBOutlet UITextField *textPsw_motDePasse_PROF;
@property (weak, nonatomic) IBOutlet UITextField *textConfirmation_PROF;
@property (weak, nonatomic) IBOutlet UITextField *textReferalCode_codeParrainage_PROF;
@property (weak, nonatomic) IBOutlet UITextField *textTeleStart_PROF;


@property (weak, nonatomic) IBOutlet UIImageView *imgPersonel;

@property (weak, nonatomic) IBOutlet UIImageView *imgProf;




//personel
@property (weak, nonatomic) IBOutlet UITextField *textNOM;
@property (weak, nonatomic) IBOutlet UITextField *textPrenom;
@property (weak, nonatomic) IBOutlet UITextField *textDOB;

@property (weak, nonatomic) IBOutlet UITextField *textPostalCode;
@property (weak, nonatomic) IBOutlet UITextField *textEmail;


@property (weak, nonatomic) IBOutlet UITextField *textTeleNumber;
@property (weak, nonatomic) IBOutlet UITextField *textPSW_mode_de_passe;
@property (weak, nonatomic) IBOutlet UITextField *textConfirmation;
@property (weak, nonatomic) IBOutlet UITextField *textReferCode_code_parrainage;
@property (weak, nonatomic) IBOutlet UITextField *textTeleStart;



@property (weak, nonatomic) IBOutlet UIView *viewPersonel;
@property (weak, nonatomic) IBOutlet UIView *viewProfessional;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonBottomConstraint;


@end

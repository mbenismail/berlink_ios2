//
//  ViewController.h
//  BERLINK
//
//  Created by Akram on 1/31/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleSignIn;

@interface ViewController : UIViewController<UITextFieldDelegate,GIDSignInUIDelegate,GIDSignInDelegate>
{
    IBOutlet UIButton* loginBtn;
    IBOutlet UIButton* signupButton;
}

@property (weak, nonatomic) IBOutlet UITextField *textEnterEmail;

@property (weak, nonatomic) IBOutlet UILabel *lblEnterMessage;

@property (weak, nonatomic) IBOutlet UIButton *btnEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnMobile;
@property (weak, nonatomic) IBOutlet UIButton *btnLogIN;
@property (weak, nonatomic) IBOutlet UITextField *textEmail;

@property (weak, nonatomic) IBOutlet UIImageView *mobileUndImg;

@property (weak, nonatomic) IBOutlet UIImageView *emailUndImg;

@property (weak, nonatomic) IBOutlet UIView *mobileView;
@property (weak, nonatomic) IBOutlet UITextField *countryCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextFiled;

@end


//
//  ViewController.m
//  BERLINK
//
//  Created by Akram on 1/31/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "ViewController.h"
#import "LoginFormViewController.h"
#import "LoginWithMobileViewController.h"

#import "SharedVar.h"
#import "WebServiceApi.h"
#import "Util.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "CreateAccountFormViewController.h"
#import "Constants.h"
#import "StringConstants.h"
#import "STPhoneFormatter.h"
#import "MessageAlertViewController.h"

@interface ViewController ()<UIPickerViewDelegate,UIPickerViewDataSource,MessageAlertViewControllerDelegate>
{
    BOOL isMobile;
    NSString *countrycode;
    UIPickerView *countryPickerView;
    NSDictionary *userData;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isMobile = true;
    [GIDSignIn sharedInstance].uiDelegate = self;
    [[GIDSignIn sharedInstance] signOut];
   // self.textEmail.hidden = true;
    //self.mobileView.hidden = false;
    countrycode = @"33";
    self.countryCodeTextField.text = [NSString stringWithFormat:@"+%@",countrycode];

    [[SharedVar shareManager] setMainViewController:self];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    
    countryPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 216)];
    [countryPickerView setDataSource: self];
    [countryPickerView setDelegate: self];
    countryPickerView.showsSelectionIndicator = YES;
    self.countryCodeTextField.inputView = countryPickerView;
    /*
    UIColor *color = UIColorFromRGB(239,239,239,0.3);
    UIFont *font = [UIFont fontWithName:@"PlayfairDisplay-Regular" size:13];
    _mobileTextFiled.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile" attributes:@{NSForegroundColorAttributeName: color,NSFontAttributeName:font}];
    _textEmail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: color,NSFontAttributeName:font}];*/
}

-(void)reginFirstResponder
{
    [self.mobileTextFiled resignFirstResponder];
    [self.textEmail resignFirstResponder];
    [self.countryCodeTextField resignFirstResponder];

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

-(void)dismissKeyboard
{
    [self reginFirstResponder];
    [self.view endEditing:YES];
}

-(IBAction)goToCreateAccountViewController:(id)sender{
    
    [self reginFirstResponder];
    CreateAccountFormViewController* createAccountFormViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"CreateAccountFormViewController"];
    createAccountFormViewC.withFacebook = NO;
    [self.navigationController pushViewController:createAccountFormViewC animated:YES];
}

- (IBAction)fbIntegration:(id)sender
{
    [self reginFirstResponder];
    FBSDKLoginManager *loginmanager = [[FBSDKLoginManager alloc] init];
    
    [loginmanager logInWithReadPermissions:@[@"email",@"public_profile"] fromViewController:Nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
     
     {
         if (error)
         {
             // Process error
             NSLog(@"======error%@",error);
             [[Util shareManager] showError:error.description withTitle:APP_NAME andViewController:self];
         }
         else if (result.isCancelled)
         {
             // Handle cancellations
             NSLog(@"canceled");
         }
         else
         {
             if ([result.grantedPermissions containsObject:@"email"])
             {
                 NSLog(@"result is:%@",result);
                 [self fetchUserInfo];
             }
         }
     }];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    [textField resignFirstResponder];
    return  YES;
}

-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, email"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 NSLog(@"%@",connection);
                 [self callLoginSocial:result isFacebook:true];
             }
             else
             {
                 NSLog(@"Error %@",error);
                 [[Util shareManager] showError:error.description withTitle:APP_NAME andViewController:self];
             }
         }];
    }
}

-(void)gotoCreateAccountWithFacebook:(id)result {
    CreateAccountFormViewController* createAccountFormViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"CreateAccountFormViewController"];
    createAccountFormViewC.withFacebook = YES;
    createAccountFormViewC.result = result;
    [self.navigationController pushViewController:createAccountFormViewC animated:YES];
}

-(IBAction)goToLoginFormViewController:(id)sender{
    
    [self reginFirstResponder];

    if(!isMobile)
    {
        LoginFormViewController* loginFormViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"LoginFormViewController"];
        loginFormViewC.strEmail = self.textEmail.text;

        [self.navigationController pushViewController:loginFormViewC animated:YES];
    }
    else{
        [self callLoginWithMobile];
    }
}

- (IBAction)gotoMobileVC:(id)sender
{
    [self reginFirstResponder];

    countrycode = @"33";
    self.countryCodeTextField.text = [NSString stringWithFormat:@"+%@",countrycode];
    self.textEmail.text = @"";
    self.mobileTextFiled.text = @"";
    [self.textEmail resignFirstResponder];
    self.lblEnterMessage.text = @"Mobile";
    self.textEmail.text = @"";
    self.emailUndImg.hidden = true;
    self.mobileUndImg.hidden = false;
    isMobile = true;
    //self.mobileView.hidden = false;
    //self.textEmail.hidden = true;
}

- (IBAction)btnEmail:(id)sender
{
    [self reginFirstResponder];

    [self.textEmail resignFirstResponder];
    self.lblEnterMessage.text = @"Email";
    self.textEmail.text = @"";
    isMobile = false;
    
    self.emailUndImg.hidden = false;
    self.mobileUndImg.hidden = true;
    //self.mobileView.hidden = true;
    //self.textEmail.hidden = false;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[[STPhoneFormatter phoneFormatter] listOfCountrySupported] count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[[[STPhoneFormatter phoneFormatter] listOfCountrySupported] objectAtIndex:row] objectForKey:@"name"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    countrycode = [[[[STPhoneFormatter phoneFormatter] listOfCountrySupported] objectAtIndex:row] objectForKey:@"dial_code"];
    self.countryCodeTextField.text = [NSString stringWithFormat:@"+%@",countrycode ];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark:- Sign in with google

- (IBAction)googlePlusButtonTouchUpInside:(id)sender {
    [self.textEmail resignFirstResponder];
    
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    [[GIDSignIn sharedInstance] signIn];
}

// pressed the Sign In button
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    if(error == nil)
    {
        NSString *userId = user.userID;                  // For client-side use only!
        NSString *idToken = user.authentication.idToken; // Safe to send to the server
        NSString *name = user.profile.name;
        NSString *email = user.profile.email;
        NSLog(@"Customer details: %@ %@ %@ %@", userId, idToken, name, email);
        
        NSDictionary *result = @{
                                 @"email" : email,
                                 @"name"  : name,
                                 @"id"    : userId,
                                 @"token" : idToken
                                 };
        [self callLoginSocial:result isFacebook:false];
    }
}

#pragma mark:- Api Call

-(void)callLoginSocial:(NSDictionary *)data isFacebook:(BOOL)isFacebook
{
    NSString *socialId = [NSString stringWithFormat:@"%@",data[@"id"]];
    
    [[WebServiceApi shareManager] invokeAuthentificationWithFacebookWebServiceWithParams_idfacebook:socialId completionHandler:^(NSString *errorMsg, User *user) {
        if(user && user.idcustomer && [user.role isEqualToString:ROLE_USER])
        {
            NSLog(@"user %@", user.name);
            [[SharedVar shareManager] setMainUser:user];
            [[SharedVar shareManager] goToAccount:self];
        }
        else if(user && user.idUser && [user.role isEqualToString:ROLE_DRIVER])
        {
            NSLog(@"user %@", user.name);
            [[SharedVar shareManager] setMainUser:user];
            [[SharedVar shareManager] goToDriverAccount:self];
        }
        else{
            if([errorMsg isEqualToString:ERROR_FACEBOOK_ACCOUNT_LINK])
            {
                userData = data;
                NSString *message = isFacebook ? ERROR_FACEBOOK_ACCOUNT_LINK : ERROR_GOOGLE_ACCOUNT_LINK;
                 [[Util shareManager] showAlertView:APP_NAME message:message okTitle:@"OK" delegate:self];
            }
            else
                [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
        }
    }];
}

- (void)alertOkButtonClicked
{
    [self gotoCreateAccountWithFacebook:userData];

}
- (void)alertCancelButtonClicked{
    [self gotoCreateAccountWithFacebook:userData];
}

-(void)callLoginWithMobile
{
    NSString *mobileNumber = self.mobileTextFiled.text;
  mobileNumber =  [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];

    if(_textEmail.text.length  == 0)
    {
        [[Util shareManager] showError:ERROR_REQUIRED_INSCRIPTION_FIELDS withTitle:APP_NAME andViewController:self];
        return;
    }else if( ![[Util shareManager] validatePhone:self.textEmail.text])
    {
        [[Util shareManager] showError:ERROR_PHONE_INVALID withTitle:APP_NAME andViewController:self];
        return;
    }
    
    [[Util shareManager] showError:@"coming soon" withTitle:APP_NAME andViewController:self];
    /*
    
    [[WebServiceApi shareManager] invokeAuthentificationWithFMobileWithParams_countryCode:mobileNumber MobileNumber:countrycode completionHandler:^(NSString *errorMsg) {
        if([errorMsg isEqualToString:@""]){
            LoginWithMobileViewController* loginFormViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"LoginWithMobileViewController"];
            loginFormViewC.mobileNumber = mobileNumber;
            loginFormViewC.countryCode  = @"";//countrycode;
            [self.navigationController pushViewController:loginFormViewC animated:YES];
            
        }else{
              [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
        }
    }];*/
}

@end


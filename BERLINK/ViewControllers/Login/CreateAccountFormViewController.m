//
//  CreateAccountFormViewController.m
//  app
//
//  Created by BERLINK on 1/31/17.
//  Copyright © 2017 sifo. All rights reserved.
//

#import "CreateAccountFormViewController.h"
#import "WebServiceApi.h"
#import "Constants.h"
#import "User.h"
#import "SharedVar.h"
#import "Util.h"
#import "StringConstants.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "STPhoneFormatter.h"
#import "CardIO.h"
#import "MusicPreferencesViewController.h"

@interface CreateAccountFormViewController () <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate,CardIOPaymentViewControllerDelegate>
{
    UITextField *phoneTextField;
    UIPickerView *countryPickerView;
    
}

@end

@implementation CreateAccountFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.viewProfessional.hidden = YES;
    [self registerForKeyboardNotifications];
    
    
    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    
    chooseImage = nil;
    
    if(_withFacebook && _result)
    {
        
        idfacebook = [_result valueForKey:@"id"];
        fbEmail = [_result valueForKey:@"email"];
        fbId = [_result valueForKey:@"id"];
        fbFirstName = [_result valueForKey:@"first_name"];
        fbLastName = [_result valueForKey:@"last_name"];
    }
    
    [self switchToPersonalFormView:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    [self setupCountryPicker];
    [self setupNavigationBar];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [CardIOUtilities preload];
}

-(void)setupNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    UIImage *backBtnImage = [UIImage imageNamed:@"n_back_btn.png"]  ;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = [view bounds];
    UIImageView *iView = [[UIImageView alloc] initWithImage:backBtnImage];
    iView.frame =  CGRectMake(0, 5, 20, 20);
    [iView setContentMode:UIViewContentModeScaleAspectFit];
    [view addSubview:iView];
    [view addSubview:backBtn];
    [backBtn addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = backButton;
}

-(void)backButtonAction
{
    [self.navigationController popViewControllerAnimated:true];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

-(void)setupCountryPicker
{
    countryPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 216)];
    [countryPickerView setDataSource: self];
    [countryPickerView setDelegate: self];
    countryPickerView.showsSelectionIndicator = YES;
    self.textTeleStart.inputView = countryPickerView;
    self.textTeleStart_PROF.inputView = countryPickerView;
}

-(void) dateTextField:(id)sender{
    UIDatePicker *picker = (UIDatePicker*)self.textDOB.inputView;
    [picker setMaximumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
   // [dateFormat setDateFormat:@"yyyy-MM-dd"];
    [dateFormat setDateStyle:NSDateFormatterShortStyle];
    NSDate *eventDate = picker.date;
    
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    self.textDOB.text = [NSString stringWithFormat:@"%@",dateString];
}

-(void)clearAllTextField {
    
    self.textCompName_nomDeSociete_PROF.text=@"";
    self.textNumSiret_PROF.text=@"";
    self.textNumverTVA_PROF.text=@"";
    self.textCodePostal_PROF.text=@"";
    self.textNOM_PROF.text=@"";
    self.textPrenom_PROF.text=@"";
    
    self.textEmail_PROF.text=@"";
    self.textTele_PROF.text=@"";
    
    self.textPsw_motDePasse_PROF.text=@"";
    self.textConfirmation_PROF.text=@"";
    self.textReferalCode_codeParrainage_PROF.text=@"";
    
    //personel
    self.textNOM.text=@"";
    self.textPrenom.text=@"";
    self.textDOB.text=@"";
    
    self.textPostalCode.text=@"";
    self.textEmail.text=@"";
    
    self.textTeleStart.text=@"";
    self.textTeleStart_PROF.text=@"";
    
    self.textTeleNumber.text=@"";
    self.textPSW_mode_de_passe.text=@"";
    self.textConfirmation.text=@"";
    self.textReferCode_code_parrainage.text=@"";
}

-(IBAction)switchToPersonalFormView:(id)sender{
    [self resignKeyboard];

    self.buttonBottomConstraint.constant = 0;
    [self.view layoutIfNeeded];
    currentViewType = PERSONAL_VIEW;
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [personalButton setHighlighted:NO];
        [professionalButton setHighlighted:YES];
    }];
    
    self.imgPersonel.hidden = false;
    self.imgProf.hidden = true;
    
    [personalButton setTitleColor:[UIColor colorWithRed:213/255.0 green:186/255.0 blue:168/255.0 alpha:1] forState:UIControlStateNormal];
    [professionalButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self addDatePicker];
    [self clearAllTextField];
    
    countrycode = [[[[STPhoneFormatter phoneFormatter] listOfCountrySupported] objectAtIndex:0] objectForKey:@"dial_code"];
    
    self.textTeleStart.text = [NSString stringWithFormat:@"+%@",countrycode];
    self.textTeleStart_PROF.text = [NSString stringWithFormat:@"+%@",countrycode ];
    
    self.viewProfessional.hidden = YES;
    self.viewPersonel.hidden = NO;
    
    [self.view setNeedsDisplay];
    [self.view layoutIfNeeded];
}

-(IBAction)switchToProfessionalFormView:(id)sender{
    [self resignKeyboard];

    self.buttonBottomConstraint.constant = 149;
    [self.view layoutIfNeeded];
    currentViewType = PRO_VIEW;
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [personalButton setHighlighted:YES];
        [professionalButton setHighlighted:NO];
    }];
    
    self.imgProf.hidden = false;
    self.imgPersonel.hidden = true;
    
    
    [professionalButton setTitleColor:[UIColor colorWithRed:213/255.0 green:186/255.0 blue:168/255.0 alpha:1] forState:UIControlStateNormal];
    [personalButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    
    [self clearAllTextField];
    
    countrycode = [[[[STPhoneFormatter phoneFormatter] listOfCountrySupported] objectAtIndex:0] objectForKey:@"dial_code"];
    
    self.textTeleStart_PROF.text = [NSString stringWithFormat:@"+%@",countrycode];
    
    self.viewPersonel.hidden = YES;
    self.viewProfessional.hidden = NO;
    
    [self.view layoutIfNeeded];
    [self.view setNeedsDisplay];
}

-(IBAction)createAcccount:(id)sender{
    [self resignKeyboard];
    [self.view endEditing:YES];
    NSString* name = @"";
    NSString* ispersonnel = @"";
    NSString* birthday = @"";
    NSString* codepostal = @"";
    NSString* email = @"";
    NSString* password = @"";
    NSString* confirmation = @"";
    NSString* phone = @"";
    NSString* companyname = @"";
    NSString* siret = @"";
    NSString* tva = @"";
    NSString* code_parrainage = @"";
    
    if(currentViewType == PERSONAL_VIEW)
    {
        name = [NSString stringWithFormat:@"%@ %@",self.textNOM.text, self.textPrenom.text];
        ispersonnel = @"true";
        birthday = self.textDOB.text;
        codepostal = self.textPostalCode.text;
        email = self.textEmail.text;
        phone = self.textTeleNumber.text;
        password = self.textPSW_mode_de_passe.text;
        confirmation = self.textConfirmation.text;
        code_parrainage = self.textReferCode_code_parrainage.text;
        
    }
    else{
        ispersonnel = @"false";
        companyname = self.textCompName_nomDeSociete_PROF.text;
        siret = self.textNumSiret_PROF.text;
        tva = self.textNumverTVA_PROF.text;
        codepostal = self.textCodePostal_PROF.text;
        name = [NSString stringWithFormat:@"%@ %@",self.textNOM_PROF.text, self.textPrenom_PROF.text];
        email = self.textEmail_PROF.text;
        phone = self.textTele_PROF.text;
        password = self.textPsw_motDePasse_PROF.text;
        confirmation = self.textConfirmation_PROF.text;
        code_parrainage = self.textReferalCode_codeParrainage_PROF.text;
    }
    
    if(_withFacebook)
    {
        password = @"00000000";
    }
    
    if(currentViewType == PERSONAL_VIEW)
    {
        if(name && name.length > 0
           && birthday && birthday.length > 0
           && codepostal && codepostal.length > 0
           && email && email.length > 0
           && password && password.length > 0
           && phone && phone.length > 0
           )
        {
            
            //textField1 @"NOM PRENOM :"
            if( [[Util shareManager] containSpecailCharacter:self.textNOM.text] ||
               [[Util shareManager] containSpecailCharacter:self.textPrenom.text])
            {
                [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
                
                return;
            }
            if([[Util shareManager] containSpecailCharacterAndNumbers:self.textNOM.text] ||
               [[Util shareManager] containSpecailCharacterAndNumbers:self.textPrenom.text] )
            {
                [[Util shareManager] showError:ERROR_NAME_CARACTERS withTitle:APP_NAME andViewController:self];
                
                return;
            }
            //textField3 @"CODE POSTAL :"
            
            //textField4 @"ADRESSE EMAIL :"
            if( ![[Util shareManager] isValidEmail:self.textEmail.text])
            {
                [[Util shareManager] showError:ERROR_EMAIL_INVALID withTitle:APP_NAME andViewController:self];
                
                return;
            }
            
            //textField5 @"TÉLÉPHONE :"
            if( ![[Util shareManager] validatePhone:self.textTeleNumber.text])
            {
                [[Util shareManager] showError:ERROR_PHONE_INVALID withTitle:APP_NAME andViewController:self];
                return;
            }
            
            //textField6 @"MOT DE PASSE :"
            if( self.textPSW_mode_de_passe.text.length < 6 && !_withFacebook)
            {
                [[Util shareManager] showError:ERROR_PASSWORD_LENGTH withTitle:APP_NAME andViewController:self];
                return;
            }
            if( [[Util shareManager] containSpecailCharacter:self.textNOM.text] && !_withFacebook)
            {
                [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
                return;
            }
            
            //textField7 @"CONFIRMATION :"
            if( ![self.textConfirmation.text  isEqualToString:self.textPSW_mode_de_passe.text])
            {
                [[Util shareManager] showError:ERROR_PASSWORD_CONFIRMATION withTitle:APP_NAME andViewController:self];
                return;
            }
            
            //textField7 @"CODE PARRAINAGE :"
            if( [[Util shareManager] containSpecailCharacter:self.textReferCode_code_parrainage.text])
            {
                [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
                return;
            }
            
            if(stripeToken && stripeToken.length > 0)
            {
                
            }
            else{
                [[Util shareManager] showError:ERROR_PAIEMENT_INFO_REQUIRED withTitle:APP_NAME andViewController:self];
                return;
            }
            
        }
        else
        {
            [[Util shareManager] showError:ERROR_REQUIRED_INSCRIPTION_FIELDS withTitle:APP_NAME andViewController:self];
            return;
        }
        
    }
    else{
        if(name && name.length > 0
           && codepostal && codepostal.length > 0
           && email && email.length > 0
           && password && password.length > 0
           && phone && phone.length > 0
           && siret && siret.length > 0
           && companyname && companyname.length > 0
           )
        {
            //textField1 @"NOM DE LA SOCIÉTÉ :"
            if( [[Util shareManager] containSpecailCharacter:self.textCompName_nomDeSociete_PROF.text])
            {
                [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
                return;
            }
            
            //textField2 @"NUMERO DE SIRET :"
            if([[Util shareManager] containSpecailCharacter:self.textNumSiret_PROF.text])
            {
                [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
                return;
            }
            
            //textField3 @"NUMERO DE TVA :"
            if(self.textNumverTVA_PROF.text.length > 0 && [[Util shareManager] containSpecailCharacter:self.textNumverTVA_PROF.text])
            {
                [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
                return;
            }
            
            //textField4 @"CODE POSTAL :"
            
            //textField5 @"NOM PRENOM :"
            if([[Util shareManager] containSpecailCharacter:self.textNOM_PROF.text] ||
               [[Util shareManager] containSpecailCharacter:self.textPrenom_PROF.text])
            {
                [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
                return;
            }
            if([[Util shareManager] containSpecailCharacterAndNumbers:self.textNOM_PROF.text]||
               [[Util shareManager] containSpecailCharacterAndNumbers:self.textPrenom_PROF.text])
            {
                [[Util shareManager] showError:ERROR_NAME_CARACTERS withTitle:APP_NAME andViewController:self];
                
                return;
            }
            
            //textField6 @"ADRESSE EMAIL :"
            if(![[Util shareManager] isValidEmail:self.textEmail_PROF.text])
            {
                [[Util shareManager] showError:ERROR_EMAIL_INVALID withTitle:APP_NAME andViewController:self];
                return;
            }
            
            //textField7 @"TÉLÉPHONE :"
            if(![[Util shareManager] validatePhone:self.textTele_PROF.text])
            {
                [[Util shareManager] showError:ERROR_PHONE_INVALID withTitle:APP_NAME andViewController:self];
                return;
            }
            
            //textField8 @"MOT DE PASSE :"
            if(self.textPsw_motDePasse_PROF.text.length < 6  && !_withFacebook)
            {
                [[Util shareManager] showError:ERROR_PASSWORD_LENGTH withTitle:APP_NAME andViewController:self];
                return;
            }
            if( [[Util shareManager] containSpecailCharacter:self.textPsw_motDePasse_PROF.text]  && !_withFacebook)
            {
                [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
                return;
            }
            
            //textField9 @"CONFIRMATION :"
            
            if(![self.textPsw_motDePasse_PROF.text  isEqualToString:self.textConfirmation_PROF.text]  && !_withFacebook)
            {
                [[Util shareManager] showError:ERROR_PASSWORD_CONFIRMATION withTitle:APP_NAME andViewController:self];
                return;
            }
            //textField10 @"CODE PARRAINAGE :"
            if([[Util shareManager] containSpecailCharacter:self.textReferalCode_codeParrainage_PROF.text])
            {
                [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
                return;
            }
            
            if(stripeToken && stripeToken.length > 0)
            {
                
            }
            else{
                [[Util shareManager] showError:ERROR_PAIEMENT_INFO_REQUIRED withTitle:APP_NAME andViewController:self];
                return;
            }
            
        }
        else
        {
            [[Util shareManager] showError:ERROR_REQUIRED_INSCRIPTION_FIELDS withTitle:APP_NAME andViewController:self];
            return;
        }
        
    }
    
    
    if(currentViewType == PERSONAL_VIEW) {
        [[Util shareManager] showWaitingViewController:self];
        [[WebServiceApi shareManager] invokePersonalRegistrationWebServiceWithParams_name:name _birth:birthday _codepostal:codepostal _email:email _password:password _countrycode:countrycode _phone:phone _code_parrainage:code_parrainage _idfacebook:idfacebook _stripeToken:stripeToken _cardNumber:cardNumber completionHandler:^(NSString *errorMsg, User *user) {
            [[Util shareManager] dismissWating];
            if(user && user.idcustomer && [errorMsg isEqualToString:@""])
            {
                [[SharedVar shareManager] setMainUser:user];
                [[SharedVar shareManager] goToAccount:self];
            }
            else
                [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
        }];
    }
    else {
        [[Util shareManager] showWaitingViewController:self];
        
        [[WebServiceApi shareManager] invokeProRegistrationWebServiceWithParams_name:name _codepostal:codepostal _email:email _password:password _countrycode:countrycode _phone:phone _company:companyname _siret:siret _tva:tva _code_parrainage:code_parrainage _idfacebook:idfacebook _stripeToken:stripeToken _cardNumber:cardNumber completionHandler:^(NSString *errorMsg, User *user) {
            [[Util shareManager] dismissWating];
            if(user && user.idcustomer && [errorMsg isEqualToString:@""])
            {
                [[SharedVar shareManager] setMainUser:user];
                [[SharedVar shareManager] goToAccount:self];
            }
            else
                [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
        }];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    
    //    [self animateTextField:textField up:YES];
    
}
-(void)addDatePicker
{
    [self.textDOB setKeyboardType:UIKeyboardTypeNumberPad];
    [self.textDOB reloadInputViews];
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    [self.textDOB setInputView:datePicker];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
    
    
    if(currentViewType == PERSONAL_VIEW)
    {
        //textField1 @"NOM PRENOM :"
        if(textField == self.textNOM && [[Util shareManager] containSpecailCharacter:self.textNOM.text])
        {
            [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
        }
        if(textField == self.textNOM && self.textNOM.text.length > 0 && [[Util shareManager] containSpecailCharacterAndNumbers:self.textNOM.text])
        {
            [[Util shareManager] showError:ERROR_NAME_CARACTERS withTitle:APP_NAME andViewController:self];
        }
        //textField3 @"CODE POSTAL :"
        
        //textField4 @"ADRESSE EMAIL :"
        if(textField == self.textEmail && ![[Util shareManager] isValidEmail:self.textEmail.text])
        {
            [[Util shareManager] showError:ERROR_EMAIL_INVALID withTitle:APP_NAME andViewController:self];
        }
        
        //textField5 @"TÉLÉPHONE :"
        if(textField == self.textTeleNumber && ![[Util shareManager] validatePhone:self.textTeleNumber.text])
        {
            [[Util shareManager] showError:ERROR_PHONE_INVALID withTitle:APP_NAME andViewController:self];
        }
        
        //textField6 @"MOT DE PASSE :"
        if(textField == self.textPSW_mode_de_passe && self.textPSW_mode_de_passe.text.length < 6 && !_withFacebook)
        {
            [[Util shareManager] showError:ERROR_PASSWORD_LENGTH withTitle:APP_NAME andViewController:self];
        }
        if(textField == self.textPSW_mode_de_passe && [[Util shareManager] containSpecailCharacter:self.textNOM.text]  && !_withFacebook)
        {
            [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
        }
        
        //textField7 @"CONFIRMATION :"
        if(textField == self.textConfirmation && ![self.textConfirmation.text  isEqualToString:self.textPSW_mode_de_passe.text] && !_withFacebook)
        {
            [[Util shareManager] showError:ERROR_PASSWORD_CONFIRMATION withTitle:APP_NAME andViewController:self];
        }
        
        //textField7 @"CODE PARRAINAGE :"
        if(textField == self.textReferCode_code_parrainage && [[Util shareManager] containSpecailCharacter:self.textReferCode_code_parrainage.text])
        {
            [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
        }
        
    }
    else {
        //textField1 @"NOM DE LA SOCIÉTÉ :"
        if(textField == self.textCompName_nomDeSociete_PROF && [[Util shareManager] containSpecailCharacter:self.textCompName_nomDeSociete_PROF.text])
        {
            [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
        }
        
        //textField2 @"NUMERO DE SIRET :"
        if(textField == self.textNumSiret_PROF && [[Util shareManager] containSpecailCharacter:self.textNumSiret_PROF.text])
        {
            [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
        }
        
        //textField3 @"NUMERO DE TVA :"
        if(textField == self.textNumverTVA_PROF && [[Util shareManager] containSpecailCharacter:self.textNumverTVA_PROF.text])
        {
            [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
        }
        
        //textField4 @"CODE POSTAL :"
        
        //textField5 @"NOM PRENOM :"
        if(textField == self.textNOM_PROF && [[Util shareManager] containSpecailCharacter:self.textNOM_PROF.text])
        {
            [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
        }
        if(textField == self.textNOM_PROF && self.textNOM_PROF.text.length > 0 && [[Util shareManager] containSpecailCharacterAndNumbers:self.textNOM_PROF.text])
        {
            [[Util shareManager] showError:ERROR_NAME_CARACTERS withTitle:APP_NAME andViewController:self];
        }
        if(textField == self.textPrenom_PROF && [[Util shareManager] containSpecailCharacter:self.textPrenom_PROF.text])
        {
            [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
        }
        if(textField == self.textPrenom_PROF && self.textPrenom_PROF.text.length > 0 && [[Util shareManager] containSpecailCharacterAndNumbers:self.textPrenom_PROF.text])
        {
            [[Util shareManager] showError:ERROR_NAME_CARACTERS withTitle:APP_NAME andViewController:self];
        }
        
        
        //textField6 @"ADRESSE EMAIL :"
        if(textField == self.textEmail_PROF && ![[Util shareManager] isValidEmail:self.textEmail_PROF.text])
        {
            [[Util shareManager] showError:ERROR_EMAIL_INVALID withTitle:APP_NAME andViewController:self];
        }
        
        //textField7 @"TÉLÉPHONE :"
        if(textField == self.textTele_PROF && ![[Util shareManager] validatePhone:self.textTele_PROF.text])
        {
            [[Util shareManager] showError:ERROR_PHONE_INVALID withTitle:APP_NAME andViewController:self];
        }
        
        //textField8 @"MOT DE PASSE :"
        if(textField == self.textPsw_motDePasse_PROF && self.textPsw_motDePasse_PROF.text.length < 6  && !_withFacebook)
        {
            [[Util shareManager] showError:ERROR_PASSWORD_LENGTH withTitle:APP_NAME andViewController:self];
        }
        if(textField == self.textPsw_motDePasse_PROF && [[Util shareManager] containSpecailCharacter:self.textPsw_motDePasse_PROF.text]  && !_withFacebook)
        {
            [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
        }
        
        //textField9 @"CONFIRMATION :"
        
        if(textField == self.textConfirmation_PROF && ![self.textConfirmation_PROF.text  isEqualToString:self.textConfirmation_PROF.text]  && !_withFacebook)
        {
            [[Util shareManager] showError:ERROR_PASSWORD_CONFIRMATION withTitle:APP_NAME andViewController:self];
        }
        //textField10 @"CODE PARRAINAGE :"
        if(textField == self.textReferalCode_codeParrainage_PROF && [[Util shareManager] containSpecailCharacter:self.textReferalCode_codeParrainage_PROF.text])
        {
            [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
        }
    }
    
    
    //[self animateTextField:textField up:NO];
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -40; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)payement:(id)sender {
    /*
     STPAddCardViewController *addCardViewController = [[STPAddCardViewController alloc] init];
     addCardViewController.delegate = self;
     // STPAddCardViewController must be shown inside a UINavigationController.
     UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:addCardViewController];
     [self presentViewController:navigationController animated:YES completion:nil];*/
    
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.hideCardIOLogo = YES;
    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:scanViewController animated:YES completion:nil];
}

#pragma mark - CardIOPaymentViewControllerDelegate

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    NSLog(@"Scan succeeded with info: %@", info);
    // Do whatever needs to be done to deliver the purchased items.
    [self dismissViewControllerAnimated:YES completion:nil];
    
    STPCardParams *cardParams = [[STPCardParams alloc] init];
    cardParams.number = info.redactedCardNumber;
    cardParams.expMonth = info.expiryMonth;
    cardParams.expYear = info.expiryYear;
    cardParams.cvc = info.cvv;
    
    cardParams.number = @"4242424242424242";
    cardParams.expMonth = 10;
    cardParams.expYear = 20;
    cardParams.cvc = @"123";
    [[Util shareManager] showWaitingViewController:self];
    
    [[STPAPIClient sharedClient] createTokenWithCard:cardParams completion:^(STPToken *token, NSError *error) {
        [[Util shareManager] dismissWating];

        if(token)
        {
            stripeToken = [token tokenId];
            cardNumber = [[token card] last4];
            labelPaymentView.text = [NSString stringWithFormat:@"XXXX-XXXX-XXXX-%@", cardNumber];
        }
        else
        {
            [[Util shareManager] showError:ERROR_PAIEMENT_INFO withTitle:APP_NAME andViewController:self];
        }
    }];
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    NSLog(@"User cancelled scan");
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark STPAddCardViewControllerDelegate

- (void)addCardViewControllerDidCancel:(STPAddCardViewController *)addCardViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)addCardViewController:(STPAddCardViewController *)addCardViewController
               didCreateToken:(STPToken *)token
                   completion:(STPErrorBlock)completion {
    
    NSLog(@"[token tokenId] : %@", [token tokenId]);
    
    [self dismissViewControllerAnimated:YES completion:^{
        if(token)
        {
            stripeToken = [token tokenId];
            cardNumber = [[token card] last4];
            labelPaymentView.text = [NSString stringWithFormat:@"XXXX-XXXX-XXXX-%@", cardNumber];
        }
        else
        {
            [[Util shareManager] showError:ERROR_PAIEMENT_INFO withTitle:APP_NAME andViewController:self];
        }
    }];
}


#pragma mark -
#pragma mark UIPickerView DataSource & Delegate


-(void)resignKeyboard
{
    [_textCompName_nomDeSociete_PROF resignFirstResponder];
    [_textNumSiret_PROF resignFirstResponder];
    [_textNumverTVA_PROF resignFirstResponder];
    [_textCodePostal_PROF resignFirstResponder];
    [_textNOM_PROF resignFirstResponder];
    
    
    [_textPrenom_PROF resignFirstResponder];
    [_textConfirmation_PROF resignFirstResponder];
    [_textReferalCode_codeParrainage_PROF resignFirstResponder];
    [_textTeleStart_PROF resignFirstResponder];
    [_textCompName_nomDeSociete_PROF resignFirstResponder];
    
    
    [_textEmail_PROF resignFirstResponder];
    [_textTele_PROF resignFirstResponder];
    [_textPsw_motDePasse_PROF resignFirstResponder];
    [ self.textTeleStart resignFirstResponder];
    [self.textTeleStart_PROF resignFirstResponder];
    
    [_textNOM resignFirstResponder];
    [_textPrenom resignFirstResponder];
    [_textDOB resignFirstResponder];
    
    [_textPostalCode resignFirstResponder];
    [_textEmail resignFirstResponder];
    [_textTeleNumber resignFirstResponder];
    
    [_textPSW_mode_de_passe resignFirstResponder];
    [_textConfirmation resignFirstResponder];
    [_textReferCode_code_parrainage resignFirstResponder];
    [_textTeleStart resignFirstResponder];
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[[STPhoneFormatter phoneFormatter] listOfCountrySupported] count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[[[STPhoneFormatter phoneFormatter] listOfCountrySupported] objectAtIndex:row] objectForKey:@"name"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    countrycode = [[[[STPhoneFormatter phoneFormatter] listOfCountrySupported] objectAtIndex:row] objectForKey:@"dial_code"];
    self.textTeleStart.text = [NSString stringWithFormat:@"+%@",countrycode ];
    self.textTeleStart_PROF.text = [NSString stringWithFormat:@"+%@",countrycode ];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height + 30, 0.0);
    formScrollView.contentInset = contentInsets;
    formScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height ;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [formScrollView scrollRectToVisible:activeField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    formScrollView.contentInset = contentInsets;
    formScrollView.scrollIndicatorInsets = contentInsets;
}

-(void)showGenersViewController
{
    MusicPreferencesViewController* eViewC = [[UIStoryboard storyboardWithName:@"Music" bundle:nil] instantiateViewControllerWithIdentifier:@"MusicPreferencesViewController"];
    [self.navigationController pushViewController:eViewC animated:true];
}
@end

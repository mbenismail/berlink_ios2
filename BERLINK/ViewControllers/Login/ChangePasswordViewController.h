//
//  ChangePasswordViewController.h
//  BERLINK
//
//  Created by BERLINK on 3/17/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController
{
    IBOutlet UITextField* codeTextField;
    IBOutlet UITextField* passwordTextField;
    IBOutlet UITextField* confirmationTextField;
}

@property (nonatomic, strong) NSString* email;
@end

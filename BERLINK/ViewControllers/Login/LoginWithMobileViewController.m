//
//  LoginWithMobileViewController.m
//  BERLINK
//
//  Created by Pravin Jadhao on 31/01/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import "LoginWithMobileViewController.h"
#import "SharedVar.h"
#import "WebServiceApi.h"
#import "Util.h"
#import "Constants.h"
#import "StringConstants.h"

@import SVPinView;

@interface LoginWithMobileViewController ()
{
    NSString *otpPin;
}
@property (weak, nonatomic) IBOutlet SVPinView *pinView;
@property (weak, nonatomic) IBOutlet UITextField *txtMobile;

@end

@implementation LoginWithMobileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    [self setupPinView];
    self.txtMobile.text = [NSString stringWithFormat:@"+%@ %@",self.countryCode,self.mobileNumber];
    otpPin = @"";
   [self setupNavigationBar];
}

-(void)setupNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    UIImage *backBtnImage = [UIImage imageNamed:@"n_back_btn.png"]  ;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = [view bounds];
    UIImageView *iView = [[UIImageView alloc] initWithImage:backBtnImage];
    iView.frame =  CGRectMake(0, 5, 20, 20);
    [iView setContentMode:UIViewContentModeScaleAspectFit];
    [view addSubview:iView];
    [view addSubview:backBtn];
    [backBtn addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = backButton;
}

-(void)backButtonAction
{
    [self.navigationController popViewControllerAnimated:true];
}

- (void)setupPinView
{
    _pinView.pinLength = 4;
    _pinView.interSpace = 20;
    _pinView.textColor = [UIColor whiteColor];
    _pinView.borderLineColor = [UIColor colorWithWhite:1 alpha:0.5];
    _pinView.activeBorderLineColor =  [UIColor whiteColor];
    _pinView.borderLineThickness = 1;
    _pinView.shouldSecureText = false;
    _pinView.allowsWhitespaces = false;
    _pinView.fieldBackgroundColor =  [UIColor colorWithWhite:1 alpha:0.5];
    _pinView.activeFieldBackgroundColor =  [UIColor colorWithWhite:1 alpha:0.5];
    _pinView.fieldCornerRadius = 15;
    _pinView.activeFieldCornerRadius = 15;
    _pinView.placeholder = @"****";
    
    UIFont *font = [UIFont fontWithName:@"PlayfairDisplay-Regular" size:30];
    _pinView.font = font;
    
    _pinView.activeBorderLineThickness = 4;
    _pinView.fieldBackgroundColor = [UIColor clearColor];
    _pinView.activeFieldBackgroundColor = [UIColor clearColor];
    _pinView.fieldCornerRadius = 0;
    _pinView.activeFieldCornerRadius = 0;
    _pinView.style = 1;
    _pinView.didChangeCallback = ^(NSString * pin) {
        otpPin = pin;
    };
}


-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)loginButtonAction:(id)sender {

    NSString *pin = otpPin;
    
    NSString *mobileNumber = self.mobileNumber;//self.text;
    
    if (pin.length == 0){
        [[Util shareManager] showError:ERROR_REQUIRED_INSCRIPTION_FIELDS withTitle:APP_NAME andViewController:self];
        return;
    }else if(pin.length != 4)
    {
     [[Util shareManager] showError:ERROR_OTP_LENGTH withTitle:APP_NAME andViewController:self];
        return;
    }/*
    [[WebServiceApi shareManager] invokeAuthentificationWithFMobileWithParams_countryCode:mobileNumber MobileNumber:self.countryCode otp:pin completionHandler:^(NSString *errorMsg,User *user) {
        if([errorMsg isEqualToString:@""]){
            
            if(user && user.idcustomer && [user.role isEqualToString:ROLE_USER])
            {
                [[SharedVar shareManager] setMainUser:user];
                [[SharedVar shareManager] goToAccount:self];
            }
            else if(user && user.idUser && [user.role isEqualToString:ROLE_DRIVER])
            {
                [[SharedVar shareManager] setMainUser:user];
                [[SharedVar shareManager] goToDriverAccount:self];
            }
        }else{
            [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
        }
    }];*/
}

@end

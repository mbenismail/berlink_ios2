//
//  ChangePasswordViewController.m
//  BERLINK
//
//  Created by BERLINK on 3/17/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "Util.h"
#import "WebServiceApi.h"
#import "StringConstants.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"email %@", _email);
    // Do any additional setup after loading the view.
    UIColor *color = [UIColor grayColor];
    codeTextField.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:@"CODE :"
     attributes:@{NSForegroundColorAttributeName:color}];
    [codeTextField setKeyboardType:UIKeyboardTypeDefault];
    codeTextField.secureTextEntry = NO;
    passwordTextField.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:@"MOT DE PASSE :"
     attributes:@{NSForegroundColorAttributeName:color}];
    [passwordTextField setKeyboardType:UIKeyboardTypeAlphabet];
    passwordTextField.secureTextEntry = YES;
    
    confirmationTextField.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:@"CONFIRMATION :"
     attributes:@{NSForegroundColorAttributeName:color}];
    [confirmationTextField setKeyboardType:UIKeyboardTypeAlphabet];
    confirmationTextField.secureTextEntry = YES;

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
}
-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

-(IBAction)valider:(id)sender{
    [self.view endEditing:YES];

    NSString* code = codeTextField.text;
    NSString* password = passwordTextField.text;
    NSString* comfirmation = confirmationTextField.text;
    
    if(code && password && comfirmation && code.length > 0 && password.length > 0  && comfirmation.length > 0)
    {
        if(passwordTextField.text.length < 6)
        {
            [[Util shareManager] showError:ERROR_PASSWORD_LENGTH withTitle:APP_NAME andViewController:self];

            return;
        }
        if([[Util shareManager] containSpecailCharacter:passwordTextField.text])
        {
            [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
        }
        
        // @"CONFIRMATION :"
        if(![passwordTextField.text  isEqualToString:confirmationTextField.text])
        {
            [[Util shareManager] showError:ERROR_PASSWORD_CONFIRMATION withTitle:APP_NAME andViewController:self];

            return;
        }
    
        // @"CODE"
//        if([[Util shareManager] containSpecailCharacter:codeTextField.text])
//        {
//            [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
//            return;
//        }
        [[Util shareManager] showWaitingViewController:self];
        [[WebServiceApi shareManager] invokeValidateEmailWebServiceWithParams_email:_email _password:password _code:code _generated:NO completionHandler:^(NSString *errorMsg, BOOL success) {
            [[Util shareManager] dismissWating];
            
            if(success)
            {
                [[Util shareManager] showSuccess:MESSAGE_PASSWORD_CHANGE withTitle:APP_NAME andViewController:self];
                [self performSelector:@selector(returnBack) withObject:nil afterDelay:1];

                
            }
            else
                [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
            
            
        }];
    }
    else
    [[Util shareManager] showError:ERR0R_LOGIN_REQUIRED_FILED withTitle:APP_NAME andViewController:self];
}

-(void) returnBack {
    [self.navigationController popViewControllerAnimated:YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // @"MOT DE PASSE :"
    if(textField == passwordTextField && passwordTextField.text.length < 6)
    {
        [[Util shareManager] showError:ERROR_PASSWORD_LENGTH withTitle:APP_NAME andViewController:self];
    }
    if(textField == passwordTextField && [[Util shareManager] containSpecailCharacter:passwordTextField.text])
    {
        [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
    }
    
    // @"CONFIRMATION :"
    if(textField == confirmationTextField && ![passwordTextField.text  isEqualToString:confirmationTextField.text])
    {
        [[Util shareManager] showError:ERROR_PASSWORD_CONFIRMATION withTitle:APP_NAME andViewController:self];
    }
    
    // @"CODE"
    if(textField == codeTextField && [[Util shareManager] containSpecailCharacter:codeTextField.text])
    {
        [[Util shareManager] showError:ERROR_SPECIAL_CHARACHTER_ARE_NOT_ALLOWED withTitle:APP_NAME andViewController:self];
    }
    [self animateTextField:textField up:NO];
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -130; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

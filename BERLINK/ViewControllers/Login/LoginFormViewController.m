//
//  LoginFormViewController.m
//  BERLINK
//
//  Created by BERLINK on 2/7/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "LoginFormViewController.h"
#import "SWRevealViewController.h"
#import "WebServiceApi.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "SharedVar.h"
#import "Util.h"
#import "StringConstants.h"
#import "ChangePasswordViewController.h"
#import "SCLAlertView.h"

@interface LoginFormViewController ()

@end

@implementation LoginFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
//    emailTextField.text = @"pravin2@gmail.com";
//    passwordTextField.text = @"aaaaaaaa";
    
    emailTextField.text = self.strEmail;
    passwordTextField.text = @"";

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    [self setupNavigationBar];
}

-(void)setupNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    UIImage *backBtnImage = [UIImage imageNamed:@"n_back_btn.png"]  ;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = [view bounds];
    UIImageView *iView = [[UIImageView alloc] initWithImage:backBtnImage];
    iView.frame =  CGRectMake(0, 5, 20, 20);
    [iView setContentMode:UIViewContentModeScaleAspectFit];
    [view addSubview:iView];
    [view addSubview:backBtn];
    [backBtn addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = backButton;
}

-(void)backButtonAction
{
    [self.navigationController popViewControllerAnimated:true];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

-(void) placeholderDefine {
    
    UIColor *color = [UIColor grayColor];
    emailTextField.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:@"ADRESSE E-MAIL :"
     attributes:@{NSForegroundColorAttributeName:color}];
    [emailTextField setKeyboardType:UIKeyboardTypeEmailAddress];

    passwordTextField.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:@"MOT DE PASSE :"
     attributes:@{NSForegroundColorAttributeName:color}];
    passwordTextField.secureTextEntry = YES;
}

-(IBAction)login:(id)sender{
    [self.view endEditing:YES];

    NSString* email = emailTextField.text;
    NSString* password = passwordTextField.text;
    
    if(email && password && email.length > 0 && password.length > 0)
    {
        [[Util shareManager] showWaitingViewController:self];
        [[WebServiceApi shareManager] invokeAuthentificationWebServiceWithParams_email:email _password:password completionHandler:^(NSString *errorMsg, User *user) {
            [[Util shareManager] dismissWating];
            if(errorMsg && errorMsg.length > 0)
            {
                [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
                return;
            }
            
            if(user && user.idcustomer && [user.role isEqualToString:ROLE_USER])
            {
                [[SharedVar shareManager] setMainUser:user];
                [[SharedVar shareManager] goToAccount:self];
            }
            else if(user && user.idUser && [user.role isEqualToString:ROLE_DRIVER])
            {
                [[SharedVar shareManager] setMainUser:user];
                [[SharedVar shareManager] goToDriverAccount:self];
            }
        }];
    }
    else
    [[Util shareManager] showError:ERR0R_LOGIN_REQUIRED_FILED withTitle:APP_NAME andViewController:self];

}

-(IBAction)forgetPsssword:(id)sender{
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    UITextField *emailfield = [alert addTextField:ENTER_EMAIL];

    //Using Block
    [alert addButton:@"VALIDER" actionBlock:^(void) {
        NSLog(@"Second button tapped");
        NSString* email = emailfield.text;
        if(!email || email.length == 0)
        {
            [[Util shareManager] showError:ERROR_REQUIRED_EMAIL withTitle:APP_NAME andViewController:self];
            return ;
        }
        [[Util shareManager] showWaitingViewController:self];
        [[WebServiceApi shareManager] invokeValidateEmailWebServiceWithParams_email:email _password:nil _code:nil _generated:YES completionHandler:^(NSString *errorMsg, BOOL success) {
            [[Util shareManager] dismissWating];
            if(success)
            {
                [[Util shareManager] showSuccess:FORGET_PASSWORD_MESSAGE withTitle:APP_NAME andViewController:self];
                [self performSelector:@selector(gotoChangeMdp:) withObject:email afterDelay:1];
            }
            else
                [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
            
        }];
    }];    
    [alert showNotice:self title:APP_NAME subTitle:ERR0R_FORGET_PASSWORD_TITLE closeButtonTitle:@"ANNULER" duration:0.0f];
}


-(void)gotoChangeMdp:(NSString*)email{
    ChangePasswordViewController * cview = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
    cview.email = email;
    [self.navigationController pushViewController:cview animated:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -20; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

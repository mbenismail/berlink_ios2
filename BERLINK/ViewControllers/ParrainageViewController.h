//
//  ParrainageViewController.h
//  BERLINK
//
//  Created by BERLINK on 3/31/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ContactsUI/ContactsUI.h>
#import <MessageUI/MessageUI.h>

@interface ParrainageViewController : UIViewController<CNContactPickerDelegate>
{
    IBOutlet UILabel* codeLabel;
}
@end

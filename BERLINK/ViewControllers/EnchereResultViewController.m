//
//  EnchereResultViewController.m
//  BERLINK
//
//  Created by BERLINK on 2/10/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "EnchereResultViewController.h"
#import "DestinationViewController.h"
#import "SharedVar.h"
#import "StringConstants.h"
#import "CategoriesViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Constants.h"
@interface EnchereResultViewController ()

@end

@implementation EnchereResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString* mainUserID = [NSString stringWithFormat:@"user_%d", [[[SharedVar shareManager] mainUser] idcustomer].intValue];
    [aremporteLabel setHidden:NO];

    if([_currentUserID isEqualToString:mainUserID])
    {
        winerNameLabel.text = YOU_WIN;
        aremporteLabel.text = [NSString stringWithFormat: @"%@ réservé notre service de voiture '%@' à %@ €.",_currentUserName,_currentCategory.range,_currentRequest.pricebid];
    }
    else
    {
        winerNameLabel.text = _currentUserName;
    }
    NSURL* aURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SERVER_URL,_currentCategory.photo]];
    
    [catCarImage sd_setImageWithURL:aURL
                 placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = NO;
    [self setupNavigationBar];
}

-(void)setupNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    UIImage *backBtnImage = [UIImage imageNamed:@"n_back_btn.png"]  ;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = [view bounds];
    UIImageView *iView = [[UIImageView alloc] initWithImage:backBtnImage];
    iView.frame =  CGRectMake(0, 5, 20, 20);
    [iView setContentMode:UIViewContentModeScaleAspectFit];
    [view addSubview:iView];
    [view addSubview:backBtn];
    [backBtn addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = backButton;
}

-(void)backButtonAction
{
    [self.navigationController popViewControllerAnimated:true];
}


-(IBAction)goToDestinationViewController:(id)sender{

 
        
     NSString* mainUserID = [NSString stringWithFormat:@"user_%d", [[[SharedVar shareManager] mainUser] idcustomer].intValue];
    if([_currentUserID isEqualToString:mainUserID])
    {    
    DestinationViewController* eViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"DestinationViewController"];
    eViewC.currentRequest = _currentRequest;
    eViewC.currentCategory = _currentCategory;
    [self.navigationController pushViewController:eViewC animated:YES];
    }else{
      CategoriesViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"CategoriesViewController"];
        SWRevealViewController *revealController = self.revealViewController;
        [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
        navigationController.navigationBar.translucent = NO;
        [revealController pushFrontViewController:navigationController animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

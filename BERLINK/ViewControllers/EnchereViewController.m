//
//  EnchereViewController.m
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "EnchereViewController.h"
#import "EnchereResultViewController.h"
#import "Message.h"
#import "SharedVar.h"
#import "Util.h"
#import "Constants.h"
#import "User.h"
#import "Gradient.h"
@interface EnchereViewController ()
{
    NSArray *currentUsersBiding;
}
@end

@implementation EnchereViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
      [Gradient setBackgroundGradientTopToBottomForView:self.view color1Red:0 color1Green:0 color1Blue:0 color2Red:45.0 color2Green:45.0 color2Blue:45.0 alpha:1.0];
    
    // Do any additional setup after loading the view.
    priceLabel.text = [NSString stringWithFormat:@"%d €", [self.currentCategory.pricepack intValue]];
    timerLabel.text = @"30 sec";
    myBid=0;
    [self.lblLastUpdatedBid setHidden:true];
    _lblTotalParticipants.text = [NSString stringWithFormat:@"1"];

    FIRDatabase*  database = [FIRDatabase database];
    FIRDatabaseReference* myRef = [database referenceWithPath:@"bid"];
    NSString* bidStr = [NSString stringWithFormat:@"bid_%d", _currentRequest.bid.idBid.intValue];
    NSLog(@"bidStr %@", bidStr);
    db = [myRef child:bidStr];
    [db observeEventType:FIRDataEventTypeValue
               withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                   NSLog(@"event fri 3");
                   NSLog(@"CC %@", [snapshot value]);
                   FIRDataSnapshot* d =[snapshot childSnapshotForPath:@"countDown"];
                   int cd = 30;
                   if(d && d.value)
                   {
                       cd = [[d value] intValue];
                       timerLabel.text = [NSString stringWithFormat:@"%d sec", cd];
                   }
                   FIRDataSnapshot* val =[snapshot childSnapshotForPath:@"customers"];
                   int MaxVal=0;
                   NSMutableArray *bidingUser = [[NSMutableArray alloc] init];
                   for (FIRDataSnapshot* childSnap in val.children) {
                       FIRDataSnapshot* currentBid= [childSnap childSnapshotForPath:@"biding"];
                     //  NSLog(@"otherDeviceName -> %@", currentBid);
                       if(currentBid && currentBid.value != (NSString *)[NSNull null] &&[currentBid.value intValue] >MaxVal)
                           MaxVal=[currentBid.value intValue];
                       
                       FIRDataSnapshot *currentCustName = [childSnap childSnapshotForPath:@"name"];
                       FIRDataSnapshot *currentCustId = [childSnap childSnapshotForPath:@"id"];
                       FIRDataSnapshot *updatedDate = [childSnap childSnapshotForPath:@"dateUpdateOrCreat"];
                       FIRDataSnapshot *bidPrice = [childSnap childSnapshotForPath:@"biding"];

                       if(currentCustName && currentCustName.value != (NSString *)[NSNull null])
                       {
                           Bid *bidUser = [[Bid alloc] init];
                           
                           bidUser.idBid = [currentCustId value];
                           bidUser.name = [currentCustName value];
                           bidUser.dateCreated = [updatedDate value];
                           bidUser.price = @([[bidPrice value] intValue]);
                           [bidUser dateFromString:bidUser.dateCreated];
                           [bidingUser addObject:bidUser];
                       }
                   }
                   [self checkBiddingStatus:bidingUser];
                   lastEnchere.text=[NSString stringWithFormat:@"%d €",MaxVal];
                   FIRDataSnapshot* winer_id =[snapshot childSnapshotForPath:@"winer_id"];
                   FIRDataSnapshot* winer_name =[snapshot childSnapshotForPath:@"winer_name"];
                   
                  
                   if(winer_name && winer_name.value != (NSString *)[NSNull null] && cd == 0)
                   {
                      // NSLog(@"winer_name %@", [winer_name value]);
                      // NSLog(@"winer_id %@", [winer_id value]);
                       [self gotoEncherResult:[winer_id value] andName:[winer_name value]];
                       
                   }
               }
         withCancelBlock:^(NSError * _Nonnull error) {
             
         }];
    
    if ([self.strCategory  isEqual: @"business"])
    {
        [self.img setImage:[UIImage imageNamed: @"businessImg"]];
    }else
        
        if ([self.strCategory  isEqual: @"premium"])
        {
            [self.img setImage:[UIImage imageNamed: @"premiumImg"]];
            
        }else
            
            if ([self.strCategory  isEqual:  @"prestige"])
            {
                [self.img setImage:[UIImage imageNamed: @"prestigeImg"]];
                
            }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
//    self.navigationController.navigationBar.hidden = NO;
//    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
//
//    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    UIImage *backBtnImage = [UIImage imageNamed:@"crossIcon.png"]  ;
//    [backBtn setImage:backBtnImage forState:UIControlStateNormal];
//    [backBtn addTarget:self action:@selector(goback) forControlEvents:UIControlEventTouchUpInside];
//    backBtn.frame = CGRectMake(0, 0, 30, 30);
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn] ;
//    self.navigationItem.leftBarButtonItem = backButton;
}



-(void)viewDidDisappear:(BOOL)animated{
    myBid=0;
    [super viewWillDisappear:animated];
    [db removeAllObservers];
}

- (IBAction)closeButtonAction:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

- (void)goback
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)checkBiddingStatus:(NSMutableArray*)biddingUsers
{
    
    NSSortDescriptor *dateDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:@"date"
                                        ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
    NSArray *sortedEventArray = [biddingUsers
                                 sortedArrayUsingDescriptors:sortDescriptors];
    _lblTotalParticipants.text = [NSString stringWithFormat:@"1"];
   if(sortedEventArray.count > 0)
   {
       _lblTotalParticipants.text = [NSString stringWithFormat:@"%lu",(unsigned long)sortedEventArray.count];

       Bid *user = sortedEventArray[0];
       NSString *currentUserId = [NSString stringWithFormat:@"%@",[[[SharedVar shareManager] mainUser] idcustomer]] ;
       
       if([[NSString stringWithFormat:@"%@",user.idBid] isEqualToString:currentUserId])
       {
           self.lblMyName.text = [NSString stringWithFormat:@"Gagnant Actuel: %@", user.name];
           [_otherWiningView setHidden:true];
           [_myWiningView setHidden:false];
           
       }else{
           self.lblOtherName.text = [NSString stringWithFormat:@"Gagnant Actuel: %@", user.name];
           [_otherWiningView setHidden:false];
           [_myWiningView setHidden:true];
       }
   }
    [self getCurrentBid:sortedEventArray];
}

-(void)getCurrentBid:(NSArray *)users
{
    if(users.count == 0)
    {
        return;
    }
    [self.lblLastUpdatedBid setHidden:false];
    Bid *bidUser = users[0];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idBid == %@", bidUser.idBid];
    NSArray *filteredArray = [currentUsersBiding filteredArrayUsingPredicate:predicate];
    if(filteredArray.count > 0)
    {
        Bid *oldUser = filteredArray[0];
        if(oldUser.price != bidUser.price)
        {
            int diff = [bidUser.price intValue] - [oldUser.price intValue];
            NSString *updatedBidPrice = [NSString stringWithFormat:@"+%d€ Priceprix de Base",diff];
            self.lblLastUpdatedBid.text = updatedBidPrice;
        }
    }else{
        NSString *updatedBidPrice = [NSString stringWithFormat:@"+%@€ Priceprix de Base",bidUser.price];
        self.lblLastUpdatedBid.text = updatedBidPrice;
    }
    currentUsersBiding = users;
}

-(IBAction)returnToCategoriesViewController:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(void)gotoEncherResult:(NSString*)userId andName:(NSString*)userName{
    EnchereResultViewController* eViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"EnchereResultViewController"];
    eViewC.currentUserID = userId;
    eViewC.currentUserName = userName;
    eViewC.currentCategory = _currentCategory;
    eViewC.currentRequest = _currentRequest;
    [self.navigationController pushViewController:eViewC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(IBAction) bid_5:(id)sender{
    [_currentRequest setPricebid:[NSNumber numberWithInt:(5+myBid)]];
    [self updateChildren:(5+myBid)];
    [self updateButtons:5];
}

-(IBAction) bid_25:(id)sender{
    [_currentRequest setPricebid:[NSNumber numberWithInt:(25+myBid)]];
    [self updateChildren:(25+myBid)];
    [self updateButtons:25];
}

-(IBAction) bid_50:(id)sender{
    [_currentRequest setPricebid:[NSNumber numberWithInt:(50+myBid)]];
    [self updateChildren:(50+myBid)];
    [self updateButtons:50];
}

-(void)updateButtons:(int)senderNumber{
    
    UIImage *selectedImage = [UIImage imageNamed:@"selectedCircle.png"];
    UIImage *deselectedImage = [UIImage imageNamed:@"circle_bid.png"];
    UIColor *selectedColor = UIColorFromRGB(0,0,0,1);
    UIColor *deSelectedColor = UIColorFromRGB(255,255,255,1);
    
    _lbl5Plus.textColor =  senderNumber == 5 ? selectedColor : deSelectedColor;
    _lbl5Value.textColor = senderNumber == 5 ? selectedColor : deSelectedColor;
    _lbl25Plus.textColor = senderNumber == 25 ? selectedColor : deSelectedColor;
    _lbl25Value.textColor = senderNumber == 25 ? selectedColor : deSelectedColor;
    _lbl5P0lus.textColor = senderNumber == 50 ? selectedColor : deSelectedColor;
    _lbl50Value.textColor = senderNumber == 50 ? selectedColor : deSelectedColor;
    
    [btn5  setBackgroundImage: senderNumber == 5 ? selectedImage : deselectedImage forState:UIControlStateNormal];
    [btn25  setBackgroundImage:senderNumber == 25 ? selectedImage : deselectedImage forState:UIControlStateNormal];
    [btn50  setBackgroundImage:senderNumber == 50 ? selectedImage : deselectedImage forState:UIControlStateNormal];
}

-(void)updateChildren:(int)bid{
    
    FIRDatabaseReference *rootRef= [[FIRDatabase database] referenceWithPath:@"bid"];
    Message* post = [Message new];
    myBid=bid;
    [post setBiding:bid];
    [post setName:[[[SharedVar shareManager] mainUser] name]];
    [post setIdm:[[[SharedVar shareManager] mainUser] idcustomer].intValue];
    [post setDateUpdateOrCreat:[[Util shareManager] currentDate]];
    NSString* userStr = [NSString stringWithFormat:@"/bid_%@/customers/user_%d",_currentRequest.bid.idBid, post.idm];
    
    NSMutableDictionary* userValue = [NSMutableDictionary new];
    [userValue setValue:[NSString stringWithFormat:@"%d", post.biding] forKey:@"biding"];
    [userValue setValue:post.dateUpdateOrCreat forKey:@"dateUpdateOrCreat"];
    [userValue setValue:post.name forKey:@"name"];
    [userValue setValue:[NSString stringWithFormat:@"%d", post.idm] forKey:@"id"];
    
    NSMutableDictionary* cValue = [NSMutableDictionary new];
    [cValue setValue:userValue forKey:userStr];
    [rootRef updateChildValues:cValue];
}

@end

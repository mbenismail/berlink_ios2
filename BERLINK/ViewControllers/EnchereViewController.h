//
//  EnchereViewController.h
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "ViewController.h"
#import "Request.h"
#import "Categorie.h"

@import Firebase;
@import FirebaseDatabase;

@interface EnchereViewController : ViewController
{
    IBOutlet UILabel* timerLabel;
    IBOutlet UILabel* priceLabel;
    IBOutlet UILabel* lastEnchere;
  
    int currSeconds;
    int myBid;
    FIRDatabaseReference* db;
    IBOutlet UIButton* btn5;
    IBOutlet UIButton* btn25;
    IBOutlet UIButton* btn50;
}

@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalParticipants;
@property (weak, nonatomic) IBOutlet UILabel *lblLastUpdatedBid;
@property (weak, nonatomic) IBOutlet UIView *myWiningView;
@property (weak, nonatomic) IBOutlet UIView *otherWiningView;

@property (weak, nonatomic) IBOutlet UILabel *lblMyName;

@property (weak, nonatomic) IBOutlet UILabel *lblOtherName;

@property (weak, nonatomic) IBOutlet UILabel *lbl5Value;
@property (weak, nonatomic) IBOutlet UILabel *lbl5Plus;

@property (weak, nonatomic) IBOutlet UILabel *lbl25Value;
@property (weak, nonatomic) IBOutlet UILabel *lbl25Plus;

@property (weak, nonatomic) IBOutlet UILabel *lbl50Value;
@property (weak, nonatomic) IBOutlet UILabel *lbl5P0lus;

@property(nonatomic)NSString* strCategory;
@property (strong, nonatomic) Request* currentRequest;
@property (strong, nonatomic) Categorie* currentCategory;

@end

//
//  FinCourseViewController.m
//  BERLINK
//
//  Created by BERLINK on 3/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "FinCourseViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "SharedVar.h"
#import "StringConstants.h"
#import "WebServiceApi.h"
#import "AppDelegate.h"
#import "Util.h"
#import "Course.h"
#import "Gradient.h"

@interface FinCourseViewController ()

@end

@implementation FinCourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
    SWRevealViewController *revealController = [self revealViewController];
    
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;

    
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    note = 0;
    
    if([[[[SharedVar shareManager] mainUser] role] isEqualToString:ROLE_USER])
    {
        pointGainLabelDesc.hidden = NO;
        pointGainLabel.hidden = NO;
    }
    else
    {
        pointGainLabelDesc.hidden = YES;
        pointGainLabel.hidden = YES;
    }
    NSNumber* pointGain = [[SharedVar shareManager] getCatNoteByID:appd.currentCourse.idCategory];
    
    if(pointGain)
        pointGainLabel.text = [NSString stringWithFormat:@"%d points", pointGain.intValue];
    else
        pointGainLabel.text = @"0 points";

    durationLabel.text = [NSString stringWithFormat:@"Durée de trajet: %@", appd.currentCourse.duration];
    
    if([[[[SharedVar shareManager] mainUser] role] isEqualToString:ROLE_USER])
    {
        if(appd.currentCourse.nameDriver && appd.currentCourse.nameDriver.length > 0)
            nameDriverLabel.text = appd.currentCourse.nameDriver;
        else
            nameDriverLabel.text = @"Chauffeur";
        
        userImage.image = [UIImage imageNamed:@"n_driver_icon.png"];
    }
    else
    {
        if(appd.currentCourse.nameCustomer && appd.currentCourse.nameCustomer.length > 0)
            nameDriverLabel.text = appd.currentCourse.nameCustomer;
        else
            nameDriverLabel.text = @"Client";
        userImage.image = [UIImage imageNamed:@"n_client_icon.png"];

    }
    //calcul final prix
    
    if(appd.currentCourse.prixTotal && appd.currentCourse.prixTotal.intValue > 0)
        [prixLabel setText:[NSString stringWithFormat:@"%d €", appd.currentCourse.prixTotal.intValue]];
    else
    {
        if(appd.currentCourse.prixTotalStr && appd.currentCourse.prixTotalStr.intValue > 0)
            [prixLabel setText:[NSString stringWithFormat:@"%d €", appd.currentCourse.prixTotalStr.intValue]];
        else
            [prixLabel setText:@""];
    }
    
    [noteBtn1 setHighlighted:YES];
    [noteBtn2 setHighlighted:YES];
    [noteBtn3 setHighlighted:YES];
    [noteBtn4 setHighlighted:YES];
    [noteBtn5 setHighlighted:YES];
    
    [Gradient setBackgroundGradientForView:self.viewGrad color1Red:30.0 color1Green:30.0 color1Blue:30.0 color2Red:0.0 color2Green:0.0 color2Blue:0.0 alpha:1.0];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard
{
    [self.txtComment resignFirstResponder];
    [self.view endEditing:YES];
}


-(IBAction)closeInfo:(id)sender{
    [self dismissKeyboard];
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSNumber* old_driverInforme = appd.currentCourse.driverInforme;
    NSNumber* old_userInforme = appd.currentCourse.userInforme;
    appd.currentCourse.userInforme = [NSNumber numberWithInt:1];
    appd.currentCourse.driverInforme = [NSNumber numberWithInt:1];
    [[Util shareManager] showWaitingViewController:self];
    [[WebServiceApi shareManager] invokeUpdateCourseWebServiceWithParams_user:[[SharedVar shareManager] mainUser] andCourse:appd.currentCourse completionHandler:^(NSString *errorMsg, Course *request) {
        [[Util shareManager] dismissWating];
        if([errorMsg isEqualToString:@""])
        {
            AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
            [appd enterApp];
            [appd stopUpdateCourseLocation];
        }
        else
        {
            [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];

            appd.currentCourse.driverInforme = old_driverInforme;
            appd.currentCourse.userInforme = old_userInforme;
        }
        
    }];
}

-(IBAction)soumettre:(id)sender{
    [self dismissKeyboard];

    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    NSNumber* idUserChangNote;
    if([[[[SharedVar shareManager] mainUser] role] isEqualToString:ROLE_USER])
        idUserChangNote = appd.currentCourse.idDriver;
    else
        idUserChangNote = appd.currentCourse.idCustomer;
    NSNumber* note_course = [NSNumber numberWithInt:note];
    [[Util shareManager] showWaitingViewController:self];
    [[WebServiceApi shareManager] invokeNoteCourseWebServiceWithParams_user:[[SharedVar shareManager] mainUser] andCourse:appd.currentCourse andidUserChangNote:idUserChangNote andNote:note_course completionHandler:^(NSString *errorMsg) {
        [[Util shareManager] dismissWating];
        if([errorMsg isEqualToString:@""])
        {
            [self closeInfo:nil];
        }
        else
        {
            [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
            
        }
    }];
}

-(IBAction)note_1:(id)sender{
    [self dismissKeyboard];

    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [noteBtn1 setHighlighted:NO];
        [noteBtn2 setHighlighted:YES];
        [noteBtn3 setHighlighted:YES];
        [noteBtn4 setHighlighted:YES];
        [noteBtn5 setHighlighted:YES];
    }];
    note = 1;
}

-(IBAction)note_2:(id)sender{
    [self dismissKeyboard];

    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [noteBtn1 setHighlighted:NO];
        [noteBtn2 setHighlighted:NO];
        [noteBtn3 setHighlighted:YES];
        [noteBtn4 setHighlighted:YES];
        [noteBtn5 setHighlighted:YES];
    }];
    note = 2;
}

-(IBAction)note_3:(id)sender{
    [self dismissKeyboard];

    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [noteBtn1 setHighlighted:NO];
        [noteBtn2 setHighlighted:NO];
        [noteBtn3 setHighlighted:NO];
        [noteBtn4 setHighlighted:YES];
        [noteBtn5 setHighlighted:YES];
    }];
    note = 3;
}

-(IBAction)note_4:(id)sender{
    [self dismissKeyboard];

    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [noteBtn1 setHighlighted:NO];
        [noteBtn2 setHighlighted:NO];
        [noteBtn3 setHighlighted:NO];
        [noteBtn4 setHighlighted:NO];
        [noteBtn5 setHighlighted:YES];
    }];
    note = 4;
}

-(IBAction)note_5:(id)sender{
    [self dismissKeyboard];

    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [noteBtn1 setHighlighted:NO];
        [noteBtn2 setHighlighted:NO];
        [noteBtn3 setHighlighted:NO];
        [noteBtn4 setHighlighted:NO];
        [noteBtn5 setHighlighted:NO];
    }];
    note = 5;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  CourseRequestViewController.m
//  BERLINK
//
//  Created by BERLINK on 3/21/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "CourseRequestViewController.h"
#import "WebServiceApi.h"
#import "SharedVar.h"
#import "Util.h"
#import "AppDelegate.h"
#import "RequestTableViewCell.h"
#import "Request.h"

@interface CourseRequestViewController ()

@end

@implementation CourseRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    SWRevealViewController *revealController = [self revealViewController];
    
    
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    
    
    if(_list_request_waiting && _list_request_waiting.count > 0)
    {
        myTableView.hidden = NO;
        noRequestLabel.hidden = YES;
        [myTableView reloadData];
    }
    else{
        myTableView.hidden = YES;
        noRequestLabel.hidden = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_list_request_waiting count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"RequestTableViewCell";
    
    RequestTableViewCell *cell = (RequestTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    Request* r = [_list_request_waiting objectAtIndex:indexPath.row];
    
    cell.depart.text = r.addressstart;
    cell.destination.text = r.adresseend;
    cell.viewC = self;
    cell.request = r;
    
    if(r.Products && r.Products.count > 0)
    {
        [cell.productButton setTitle:[NSString stringWithFormat:@"%lu PRODUITS", (unsigned long)r.Products.count] forState:UIControlStateNormal];
    }
    else
        [cell.productButton setTitle:@"AUCUN PRODUIT" forState:UIControlStateNormal];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 147;
}

@end

//
//  ParrainageViewController.m
//  BERLINK
//
//  Created by BERLINK on 3/31/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "ParrainageViewController.h"
#import "SWRevealViewController.h"
#import "SharedVar.h"
#import "Constants.h"
@interface ParrainageViewController ()

@end

@implementation ParrainageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    SWRevealViewController *revealController = [self revealViewController];
    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu.png"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationController.navigationBar.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;

    if([[[SharedVar shareManager] mainUser] hashid])
        codeLabel.text = [NSString stringWithFormat:@"%@", [[[SharedVar shareManager] mainUser] hashid]];
    else
        codeLabel.text = @"";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = NO;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)openPeoplePicker:(id)sender
{
    NSString *message = [NSString stringWithFormat: @"I'm giving you a free ride on the Berlink app (up to $2). To accept, use code '%@' to sign up. Enjoy! Details: %@invite/%@",codeLabel.text,SERVER_URL,codeLabel.text];
    //Add user code to share
    NSString *code = message;//codeLabel.text ;
    NSArray *activityItems = @[code];
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewControntroller.excludedActivityTypes = @[];
    [self presentViewController:activityViewControntroller animated:true completion:nil];
}

@end

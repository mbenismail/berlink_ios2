//
//  AccountLeftViewController.m
//  app
//
//  Created by BERLINK on 1/31/17.
//  Copyright © 2017 sifo. All rights reserved.
//

#import "AccountLeftViewController.h"
#import "SharedVar.h"
#import "CategoriesViewController.h"
#import "AccountCenterViewController.h"
#import "Constants.h"
#import "WaitingViewController.h"
#import "AppDelegate.h"
#import "WebServiceApi.h"
#import "Util.h"
#import "CourseViewController.h"
#import "FinCourseViewController.h"
#import "ParrainageViewController.h"
#import "MesPointsViewController.h"
#import "MesCoursesViewController.h"
#import "CGUViewController.h"
#import "HelpListViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Gradient.h"
@interface AccountLeftViewController ()

@end

@implementation AccountLeftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    currentPhotoPath = @"";
    [self updatePhotoUser];
    
     [Gradient setBackgroundGradientTopTobottomView:self.vieww color1Red:30 color1Green:30 color1Blue:30 color2Red:0.0 color2Green:0.0 color2Blue:0.0 alpha:1.0];
    
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    gradient.frame = self.vieww.bounds;
    
    [self.vieww layoutIfNeeded];
    [self.scroll layoutIfNeeded];
    [self.view layoutIfNeeded];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = YES;
    
    nameLabel.text = [[[SharedVar shareManager] mainUser] name];
    idLabel.text = [NSString stringWithFormat:@"ID : %@", [[[SharedVar shareManager] mainUser] idcustomer]];
    userPhotoImageView.layer.cornerRadius = userPhotoImageView.frame.size.width / 2;
    userPhotoImageView.clipsToBounds = YES;
    userPhotoImageView.layer.borderWidth = 2.0f;
    userPhotoImageView.layer.borderColor = APP_COLOR.CGColor;
    if([currentPhotoPath isEqualToString:[[[SharedVar shareManager ] mainUser] photo]])
    {
        
    }
    else{
        [self updatePhotoUser];
        currentPhotoPath = [[[SharedVar shareManager ] mainUser] photo];
    }
    
}

-(void)updatePhotoUser{
    if([[SharedVar shareManager ] mainUser].photo && [[[[SharedVar shareManager ] mainUser] photo] length] > 0)
    {
        NSURL* aURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SERVER_URL,[[SharedVar shareManager ] mainUser].photo]];
        [userPhotoImageView sd_setImageWithURL:aURL
                              placeholderImage:[UIImage imageNamed:@"default-user.png"]];
    }
    else
    {
        [userPhotoImageView setImage:[UIImage imageNamed:@"default-user.png"]];
    }
}

-(IBAction)goToPersonalInfos:(id)sender{
   
    AccountCenterViewController* viewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"AccountCenterViewController"];
    SWRevealViewController *revealController = self.revealViewController;
    [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewC];
    navigationController.navigationBar.translucent = NO;
    [revealController pushFrontViewController:navigationController animated:YES]; 
}

-(IBAction)goToCategoriesViewController:(id)sender{
    [[Util shareManager] showWaitingViewController:self.revealViewController];
    [[WebServiceApi shareManager] invokeCustomerCourseWebServiceWithParams_User:[[SharedVar shareManager] mainUser] completionHandler:^(NSString *errorMsg, Course *course, Request* request) {
        [[Util shareManager] dismissWating];
        if(course && course.idcourse)
        {
            AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
            appd.currentCourse = course;
            [self goToCourse:appd.currentCourse];
        }
        else if(request && request.idrequest){
            AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
            [appd gotoRequestWaitingView:request];
        }
        else {
            CategoriesViewController* catViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"CategoriesViewController"];
            SWRevealViewController *revealController = self.revealViewController;
            [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
            navigationController.navigationBar.translucent = NO;
            [revealController pushFrontViewController:navigationController animated:YES];
        }
    }];
    
}

-(void)goToCourse:(Course*) course{
    if(course.statut.intValue == 0)
    {
        WaitingViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"WaitingViewController"];
        SWRevealViewController *revealController = self.revealViewController;
        [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
        navigationController.navigationBar.translucent = NO;
        [revealController pushFrontViewController:navigationController animated:YES];
    }
    else if(course.statut.intValue == 1)
    {

        CourseViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"CourseViewController"];
        SWRevealViewController *revealController = self.revealViewController;
        [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
        navigationController.navigationBar.translucent = NO;
        [revealController pushFrontViewController:navigationController animated:YES];
    }
    else if(course.statut.intValue == 2 && course.userInforme.intValue == 0)
    {
        FinCourseViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"FinCourseViewController"];
        SWRevealViewController *revealController = self.revealViewController;
        [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
        navigationController.navigationBar.translucent = NO;
        [revealController pushFrontViewController:navigationController animated:YES];
    }
    else {
        CategoriesViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"CategoriesViewController"];
        SWRevealViewController *revealController = self.revealViewController;
        [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
        navigationController.navigationBar.translucent = NO;
        [revealController pushFrontViewController:navigationController animated:YES];
    }
    
}

-(IBAction)goToWaitingViewController:(id)sender{
    WaitingViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"WaitingViewController"];
    SWRevealViewController *revealController = self.revealViewController;
    [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
    navigationController.navigationBar.translucent = NO;
    [revealController pushFrontViewController:navigationController animated:YES];
}

-(IBAction)goToParrainageViewController:(id)sender{
    ParrainageViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"ParrainageViewController"];
    SWRevealViewController *revealController = self.revealViewController;
    [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
    navigationController.navigationBar.translucent = NO;
    [revealController pushFrontViewController:navigationController animated:YES];
}

-(IBAction)goToMesPointsViewController:(id)sender{
    MesPointsViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"MesPointsViewController"];
    SWRevealViewController *revealController = self.revealViewController;
    [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
    navigationController.navigationBar.translucent = NO;
    [revealController pushFrontViewController:navigationController animated:YES];
}

-(IBAction)goToMesCoursesViewController:(id)sender{
    MesCoursesViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"MesCoursesViewController"];
    SWRevealViewController *revealController = self.revealViewController;
    [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
    navigationController.navigationBar.translucent = NO;
    [revealController pushFrontViewController:navigationController animated:YES];
}
- (IBAction)goToCGU:(id)sender {
    
    CGUViewController* cguViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"CGUViewController"];
    SWRevealViewController *revealController = self.revealViewController;
    [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:cguViewC];
    navigationController.navigationBar.translucent = NO;
    [revealController pushFrontViewController:navigationController animated:YES];
    
}

- (IBAction)goToHelp:(id)sender {
    
    HelpListViewController* helpViewController = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"HelpListViewController"];
    SWRevealViewController *revealController = self.revealViewController;
    [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:helpViewController];
    navigationController.navigationBar.translucent = NO;
    [revealController pushFrontViewController:navigationController animated:YES];
    
}

-(IBAction)logout:(id)sender{
    [[SharedVar shareManager] userLogout];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

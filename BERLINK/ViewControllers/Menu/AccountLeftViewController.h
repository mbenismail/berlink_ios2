//
//  AccountLeftViewController.h
//  app
//
//  Created by BERLINK on 1/31/17.
//  Copyright © 2017 sifo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountLeftViewController : UIViewController
{
    IBOutlet UILabel* nameLabel;
    IBOutlet UILabel* idLabel;
    IBOutlet UIImageView* userPhotoImageView;
    NSString* currentPhotoPath;
}
@property (weak, nonatomic) IBOutlet UIView *vieww;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;


-(IBAction)goToCategoriesViewController:(id)sender;
@end

//
//  AccountDriverLeftViewController.m
//  BERLINK
//
//  Created by BERLINK on 2/7/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "AccountDriverLeftViewController.h"
#import "SharedVar.h"
#import "Constants.h"
#import "UpdateDriveInfoViewController.h"
#import "AppDelegate.h"
#import "Util.h"
#import "WebServiceApi.h"
#import "StringConstants.h"
#import "CourseRequestViewController.h"
#import "MesCoursesViewController.h"
#import "CGUViewController.h"
#import "HelpViewController.h"
#import "Gradient.h"

@interface AccountDriverLeftViewController ()
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end

@implementation AccountDriverLeftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    picker.dataSource = self;
    picker.delegate = self;
    picker.hidden = YES;
    picker.frame = CGRectMake(picker.frame.origin.x,picker.frame.origin.y, self.revealViewController.rearViewController.view.frame.size.width,picker.frame.size.height);
    
    statutDriver = [NSArray arrayWithObjects: HORS_SERVICE, EN_COURSE, ACTIF, nil];
    [self displayStatutDriver];
    [Gradient setBackgroundGradientTopTobottomView:self.contentView color1Red:30 color1Green:30 color1Blue:30 color2Red:0.0 color2Green:0.0 color2Blue:0.0 alpha:1.0];
}

-(void)displayStatutDriver {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [statusLabel setTitle:@"..." forState:UIControlStateNormal];
        [statusLabel setTitleColor:[UIColor yellowColor]  forState:UIControlStateNormal];
        
        if([[[SharedVar shareManager] mainUser] statut].intValue == 0)
        {
            NSLog(@"1 statut %@", [[[SharedVar shareManager] mainUser] statut]);
            [statusLabel setTitle:HORS_SERVICE forState:UIControlStateNormal];
            [statusLabel setTitleColor:[UIColor redColor]  forState:UIControlStateNormal];
        } else if([[[SharedVar shareManager] mainUser] statut].intValue == 1)
        {
            NSLog(@"2 statut %@", [[[SharedVar shareManager] mainUser] statut]);
            [statusLabel setTitle:EN_COURSE forState:UIControlStateNormal];
            [statusLabel setTitleColor:[UIColor orangeColor]  forState:UIControlStateNormal];
        }else if([[[SharedVar shareManager] mainUser] statut].intValue == 2)
        {
            NSLog(@"3 statut %@", [[[SharedVar shareManager] mainUser] statut]);
            [statusLabel setTitle:ACTIF forState:UIControlStateNormal];
            [statusLabel setTitleColor:[UIColor greenColor]  forState:UIControlStateNormal];
        }
        [statusLabel setNeedsLayout];
        [statusLabel layoutIfNeeded];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = YES;
    
    nameLabel.text = [[[SharedVar shareManager] mainUser] name];
    idLabel.text = [NSString stringWithFormat:@"ID : %@", [[[SharedVar shareManager] mainUser] idUser]];
    
    userPhotoImageView.layer.cornerRadius = userPhotoImageView.frame.size.width / 2;
    userPhotoImageView.clipsToBounds = YES;
    userPhotoImageView.layer.borderWidth = 2.0f;
    userPhotoImageView.layer.borderColor = APP_COLOR.CGColor;
}

-(IBAction)goToPersonalInfos:(id)sender{
    UpdateDriveInfoViewController* viewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"UpdateDriveInfoViewController"];
    SWRevealViewController *revealController = self.revealViewController;
    [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewC];
    navigationController.navigationBar.translucent = NO;
    [revealController pushFrontViewController:navigationController animated:YES];
}

-(IBAction)goToDemande:(id)sender
{
    
    [[Util shareManager] showWaitingViewController:self.revealViewController];
    [[WebServiceApi shareManager] invokeDriverCourseWebServiceWithParams_User:[[SharedVar shareManager] mainUser] completionHandler:^(NSString *errorMsg, Course *course, NSMutableArray* list_request_waiting) {
        [[Util shareManager] dismissWating];
        
        if(course && course.idcourse)
        {
            NSLog(@"course id %@", course.idcourse);
            AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
            appd.currentCourse = course;
            NSLog(@"course app id %@", appd.currentCourse.idcourse);
            [self goToCourse:appd.currentCourse];
        }
        else {
            AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
            [appd gotoCourseRequestView:list_request_waiting];
        }
    }];
}

-(void)goToCourse:(Course*) course{
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    [appd startUpdateCourseLocation];
    NSLog(@"course.statut.intValue %d", course.statut.intValue);
    if(course.statut.intValue == 0)
    {
        WaitingViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"WaitingViewController"];
        SWRevealViewController *revealController = self.revealViewController;
        [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
        navigationController.navigationBar.translucent = NO;
        [revealController pushFrontViewController:navigationController animated:YES];
    }
    else if(course.statut.intValue == 1)
    {
        
        CourseViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"CourseViewController"];
        SWRevealViewController *revealController = self.revealViewController;
        [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
        navigationController.navigationBar.translucent = NO;
        [revealController pushFrontViewController:navigationController animated:YES];
    }
    else if(course.statut.intValue == 2 && course.driverInforme.intValue == 0)
    {
        FinCourseViewController* catViewC = [[[SharedVar shareManager] mystoryboard]instantiateViewControllerWithIdentifier:@"FinCourseViewController"];
        SWRevealViewController *revealController = self.revealViewController;
        [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
        navigationController.navigationBar.translucent = NO;
        [revealController pushFrontViewController:navigationController animated:YES];
    }
    else {
        CourseRequestViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"CourseRequestViewController"];
        SWRevealViewController *revealController = self.revealViewController;
        [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
        navigationController.navigationBar.translucent = NO;
        [revealController pushFrontViewController:navigationController animated:YES];
    }
}

-(IBAction)goToMesCoursesViewController:(id)sender{
    MesCoursesViewController* catViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"MesCoursesViewController"];
    SWRevealViewController *revealController = self.revealViewController;
    [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:catViewC];
    navigationController.navigationBar.translucent = NO;
    [revealController pushFrontViewController:navigationController animated:YES];
}

- (IBAction)goToCGU:(id)sender {
    CGUViewController* cguViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"CGUViewController"];
    SWRevealViewController *revealController = self.revealViewController;
    [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:cguViewC];
    navigationController.navigationBar.translucent = NO;
    [revealController pushFrontViewController:navigationController animated:YES];
}

- (IBAction)goToHelp:(id)sender {
    
    HelpViewController* helpViewController = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"HelpViewController"];
    SWRevealViewController *revealController = self.revealViewController;
    [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:helpViewController];
    navigationController.navigationBar.translucent = NO;
    [revealController pushFrontViewController:navigationController animated:YES];
    
}
-(IBAction)logout:(id)sender{
    NSNumber* statut_user = [NSNumber numberWithInt:0];;
    [[[SharedVar shareManager] mainUser] setStatut:statut_user];
    User* user = [[SharedVar shareManager] mainUser];
    user.immatriculation =@"NULL";
    [[Util shareManager] showWaitingViewController:self.revealViewController];
    [[WebServiceApi shareManager] invokeUpdateDriverWebServiceWithParams_user:user completionHandler:^(NSString *errorMsg, User *user) {
        [[Util shareManager] dismissWating];

        if([errorMsg isEqualToString:@""])
        {
            [[SharedVar shareManager] userLogout];
        }else{
            
        }
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)changeStatus:(id)sender{
    if(toolBar && toolBar.superview != nil)
        [toolBar removeFromSuperview];
    picker.hidden = NO;
    toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,(picker.frame.origin.y - 44),picker.frame.size.width,44)];
    [picker selectRow:[[[SharedVar shareManager] mainUser] statut].intValue inComponent:0 animated:YES];
    toolbarLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    toolbarLabel.text = @"Disponibilité";
    [toolbarLabel sizeToFit];
    toolbarLabel.backgroundColor = [UIColor clearColor];
    toolbarLabel.textColor = [UIColor grayColor];
    toolbarLabel.textAlignment = NSTextAlignmentCenter;
    flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Valider"
                                                     style:UIBarButtonItemStylePlain target:self action:@selector(validateStatut:)];
    barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Annuler"
                                                       style:UIBarButtonItemStylePlain target:self action:@selector(cancelStatut:)];
    labelItem = [[UIBarButtonItem alloc] initWithCustomView:toolbarLabel];
    toolBar.items = [NSArray arrayWithObjects:barButtonCancel, flexible, labelItem, flexible, barButtonDone, nil];
    [self.view addSubview:toolBar];
}

-(void)cancelStatut:(id)sender{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if(toolBar && toolBar.superview != nil)
            [toolBar removeFromSuperview];
        picker.hidden = YES;
    }];
}

-(void)validateStatut:(id)sender{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        picker.hidden = YES;
        if(toolBar && toolBar.superview != nil)
            [toolBar removeFromSuperview];
        NSNumber* statut_user = [NSNumber numberWithInteger:statutSelectedIndex];
        [[[SharedVar shareManager] mainUser] setStatut:statut_user];
        NSNumber* old_statut_user =[[[SharedVar shareManager] mainUser] statut];
        [[Util shareManager] showWaitingViewController:self.revealViewController];
        [[WebServiceApi shareManager] invokeUpdateDriverWebServiceWithParams_user:[[SharedVar shareManager] mainUser] completionHandler:^(NSString *errorMsg, User *user) {
            [[Util shareManager] dismissWating];
            if([errorMsg isEqualToString:@""])
            {
                //[[SharedVar shareManager] setMainUser:user];
                if(toolBar && toolBar.superview != nil)
                    [toolBar removeFromSuperview];
                picker.hidden = YES;
                [self displayStatutDriver];
            }
            else
            {
                [[[SharedVar shareManager] mainUser] setStatut:old_statut_user];
                [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:self];
            }
        }];
    }];
    
}

#pragma mark - Picker view Delegate
// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [statutDriver count];
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *s = [statutDriver objectAtIndex:row];
    return [NSString stringWithFormat:@"%@", s];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
    statutSelectedIndex = row;
}

@end

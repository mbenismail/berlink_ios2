//
//  AccountDriverLeftViewController.h
//  BERLINK
//
//  Created by BERLINK on 2/7/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountDriverLeftViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>
{
    IBOutlet UILabel* nameLabel;
    IBOutlet UILabel* idLabel;
    IBOutlet UIButton* statusLabel;
    IBOutlet UIImageView* userPhotoImageView;
    IBOutlet UIPickerView *picker;
    
    NSArray* statutDriver;
    NSInteger statutSelectedIndex;
    UIToolbar *toolBar;
    UIBarButtonItem *labelItem;
    UIBarButtonItem *flexible;
    UIBarButtonItem *barButtonDone;
    UIBarButtonItem *barButtonCancel;
    UILabel *toolbarLabel;
}
-(IBAction)goToDemande:(id)sender;
@end

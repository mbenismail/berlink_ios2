//
//  ConciergerieViewController.h
//  BERLINK
//
//  Created by BERLINK on 3/3/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Request.h"
#import "PayPalMobile.h"
#import "WPoint.h"


#define POINT 0
#define PRODUCT 1
@interface ConciergerieViewController : UIViewController<PayPalPaymentDelegate, PayPalFuturePaymentDelegate, PayPalProfileSharingDelegate, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource>
{
    IBOutlet UIPickerView *picker;
    NSInteger congiergerieSelected;
    NSInteger productSelected;
    NSInteger pointSelected;

    UIToolbar *toolBar;
    UIBarButtonItem *labelItem;
    UIBarButtonItem *flexible;
    UIBarButtonItem *barButtonDone;
    UIBarButtonItem *barButtonCancel;
    int pickerType;
    UILabel *toolbarLabel;
    NSMutableArray* productsList;
    int old_point;
    WPoint* current_point;
    
    IBOutlet UILabel *numberProductLabel;

}


@property(nonatomic, retain)NSString* strCarname;
@property(nonatomic, retain)NSString* strTime;

@property (weak, nonatomic) IBOutlet UIImageView *start1;
@property (weak, nonatomic) IBOutlet UIImageView *start2;

@property (weak, nonatomic) IBOutlet UIImageView *start3;
@property (weak, nonatomic) IBOutlet UIImageView *start4;
@property (weak, nonatomic) IBOutlet UIImageView *start5;




@property (weak, nonatomic) IBOutlet UILabel *lblCarName;
@property (weak, nonatomic) IBOutlet UILabel *lblDriverName;

@property (weak, nonatomic) IBOutlet UILabel *lblTime;


@property (weak, nonatomic) IBOutlet UIView *viewDriverInfo;


@property (weak, nonatomic) IBOutlet UILabel *conciergeriesLabel;

@property (nonatomic, strong) Request* currentRequest;
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, strong, readwrite) NSString *resultText;
@end

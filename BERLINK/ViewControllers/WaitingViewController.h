//
//  WaitingViewController.h
//  BERLINK
//
//  Created by BERLINK on 3/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@import GoogleMaps;
@import Firebase;
@import FirebaseDatabase;

@interface WaitingViewController : UIViewController<CLLocationManagerDelegate, UITextFieldDelegate>
{
    CLLocationManager *locationManager;
    NSString* lngUser;
    NSString* latUser;
    IBOutlet UITextField* userAddressTextField;
    IBOutlet GMSMapView* umapView;
    IBOutlet UITextField* destinationTextField;
     CLLocation *currentLocation;
    int draw_user_annotation;
    FIRDatabaseReference* db;
    double lng_driver;
    double lat_driver;
    IBOutlet UIButton* demarrerCourseBtn;
    IBOutlet UIView* tabBarView;
    IBOutlet UILabel* durationLabel;
    IBOutlet UIButton* carBtn;
    IBOutlet UILabel* carBtnLabel;
    IBOutlet UILabel* nameDriverLabel;
    __weak IBOutlet UIView *driverView;
    
    __weak IBOutlet UIView *imageView;
}
@end

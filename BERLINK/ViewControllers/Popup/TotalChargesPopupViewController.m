//
//  TotalChargesPopupViewController.m
//  BERLINK
//
//  Created by Pravin Jadhao on 11/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import "TotalChargesPopupViewController.h"
#import "PointsValueTableViewCell.h"
#import "Gradient.h"

@interface TotalChargesPopupViewController ()
{
    
    __weak IBOutlet UILabel *lblTotalCharges;
}
@property (weak, nonatomic) IBOutlet UITableView *chargesTableView;
@end

@implementation TotalChargesPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];

       [Gradient setBackgroundGradientTopTobottomView:self.popupView color1Red:30 color1Green:30 color1Blue:30 color2Red:0.0 color2Green:0.0 color2Blue:0.0 alpha:1.0];
    self.popupView.frame = CGRectInset(self.popupView.frame, -1, -1);
    self.popupView.layer.borderColor = [UIColor grayColor].CGColor;
    self.popupView.layer.borderWidth = 1;
}

- (IBAction)crossButtonAction:(id)sender {
}
- (IBAction)accceptAction:(id)sender {
}
- (IBAction)cancelAction:(id)sender {
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"PointsValueTableViewCell";
    PointsValueTableViewCell *cell = (PointsValueTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}


#pragma mark - Show/Hide

- (void)show {
    
    self.popupView.alpha = 0.f;
    self.popupView.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
    
    [UIView animateWithDuration:0.2f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.popupView.alpha = 1.f;
        self.popupView.transform = CGAffineTransformMakeScale(1.05f, 1.05f);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.08f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.popupView.transform = CGAffineTransformIdentity;
        } completion:nil];
    }];
}

- (void)hide {
    
    [UIView animateWithDuration:0.2f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.popupView.alpha = 0.f;
        self.popupView.transform = CGAffineTransformMakeScale(0.05f, 0.05f);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.08f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.popupView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            
           
        }];
    }];
}
@end

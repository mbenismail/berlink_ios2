//
//  MessageAlertView.h
//  BERLINK
//
//  Created by Pravin Jadhao on 11/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MessageAlertViewControllerDelegate <NSObject>

@optional

- (void)alertOkButtonClicked;
- (void)alertCancelButtonClicked;

@end

@interface MessageAlertViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnYes;

@property (weak, nonatomic) IBOutlet UIButton *btnNo;
@property (weak, nonatomic) IBOutlet UIView *yesbtnView;

@property (nonatomic, weak) id delegate;


-(void)showWithTitle:(NSString *)title message:(NSString *)message;
+(id)getInstance;
-(void)showWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okTitle cancelTitle:(NSString *)cancelTitle;
-(void)showWithTitle:(NSString *)title message:(NSString *)message yesButtonTitle:(NSString *)yesTitle noButtonTitle:(NSString *)noTitle;
-(void)hidePopup;

@end


NS_ASSUME_NONNULL_END

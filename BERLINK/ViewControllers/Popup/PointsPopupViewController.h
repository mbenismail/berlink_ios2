//
//  PointsPopupViewController.h
//  BERLINK
//
//  Created by Pravin Jadhao on 11/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WPoint.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PointsPopupViewControllerDelegate <NSObject>

- (void)popupDidClose;
- (void)popupDidCloseOnOk:(WPoint *)point;

@end

@interface PointsPopupViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *pointsTableView;
@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (nonatomic, weak) id delegate;
- (void)show;
- (void)hide;
@end

NS_ASSUME_NONNULL_END

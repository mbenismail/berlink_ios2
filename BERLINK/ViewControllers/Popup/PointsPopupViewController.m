//
//  PointsPopupViewController.m
//  BERLINK
//
//  Created by Pravin Jadhao on 11/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import "PointsPopupViewController.h"
#import "PointsValueTableViewCell.h"
#import "Gradient.h"
#import "SharedVar.h"
#import "WPoint.h"
@interface PointsPopupViewController ()<UITableViewDelegate,UITableViewDragDelegate>
{
    NSInteger selectedIndex;
}
@end

@implementation PointsPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedIndex = 0;
      [Gradient setBackgroundGradientTopTobottomView:self.popupView color1Red:30 color1Green:30 color1Blue:30 color2Red:0.0 color2Green:0.0 color2Blue:0.0 alpha:1.0];
    self.popupView.frame = CGRectInset(self.popupView.frame, -1, -1);
    self.popupView.layer.borderColor = [UIColor grayColor].CGColor;
    self.popupView.layer.borderWidth = 1;
}

- (IBAction)crossButtonActiona:(UIButton *)sender {
    [self hide:false];

}
- (IBAction)acceptAction:(id)sender {
    [self hide:true];
    
}
- (IBAction)cancelAction:(id)sender {
    [self hide:false];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[SharedVar shareManager] mainPoints] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"PointsValueTableViewCell";
    PointsValueTableViewCell *cell = (PointsValueTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    WPoint *p = [[[SharedVar shareManager] mainPoints] objectAtIndex:indexPath.row];
    cell.lblName.text = [NSString stringWithFormat:@"%d %@", p.price.intValue,p.title];
    cell.lblValue.text = [NSString stringWithFormat:@"%d €", p.number.intValue];
    if(indexPath.row == selectedIndex)
    {
        cell.frame = CGRectInset(cell.frame, -1, -1);
        cell.layer.borderColor = [UIColor grayColor].CGColor;
        cell.layer.borderWidth = 1;
    }else{
        cell.layer.borderWidth = 0;
    }
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 56;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndex = indexPath.row;
    [self.pointsTableView reloadData];
}


#pragma mark - Show/Hide

- (void)show {
    
    self.popupView.alpha = 0.f;
    self.popupView.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
    
    [UIView animateWithDuration:0.2f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.popupView.alpha = 1.f;
        self.popupView.transform = CGAffineTransformMakeScale(1.05f, 1.05f);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.08f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.popupView.transform = CGAffineTransformIdentity;
        } completion:nil];
    }];
}

- (void)hide:(BOOL)isOk {
    
    [UIView animateWithDuration:0.2f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.popupView.alpha = 0.f;
        self.popupView.transform = CGAffineTransformMakeScale(0.05f, 0.05f);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.08f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.popupView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self.view removeFromSuperview];
                
                if(isOk && [[[SharedVar shareManager] mainPoints] count] > selectedIndex)
                {
                    WPoint *p = [[[SharedVar shareManager] mainPoints] objectAtIndex:selectedIndex];
                    [self.delegate popupDidCloseOnOk:p];
                }else{
                    
                    [self.delegate popupDidClose];
                }
            }];
        }];
    }];
}
@end

//
//  MessageAlertView.m
//  BERLINK
//
//  Created by Pravin Jadhao on 11/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import "MessageAlertViewController.h"
#import "Gradient.h"
#import "SharedVar.h"

@implementation MessageAlertViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    [Gradient setBackgroundGradientTopTobottomView:self.popupView color1Red:30 color1Green:30 color1Blue:30 color2Red:0.0 color2Green:0.0 color2Blue:0.0 alpha:1.0];
    self.popupView.frame = CGRectInset(self.popupView.frame, -1, -1);
    self.popupView.layer.borderColor = [UIColor grayColor].CGColor;
    self.popupView.layer.borderWidth = 1;
    self.labelTitle.text = @"Berlink";
    self.labelMessage.text = @"Berlink";

}

+(id)getInstance
{
     MessageAlertViewController * eViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"MessageAlertViewController"];
    return eViewC;
}

-(void)showWithTitle:(NSString *)title message:(NSString *)message{
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    
    // self.view.frame = window.bounds;
    [window addSubview:self.view];
    [self show];
    self.labelTitle.text = title;
    self.labelMessage.text = message;
}

-(void)showWithTitle:(NSString *)title message:(NSString *)message okButtonTitle:(NSString *)okTitle cancelTitle:(NSString *)cancelTitle{
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview:self.view];
    self.labelTitle.text = title;
    self.labelMessage.text = message;
    [self.btnOk setTitle:okTitle forState:UIControlStateNormal];
    [self.btnCancel setTitle:cancelTitle forState:UIControlStateNormal];
    [_btnCancel setHidden:false];
    [self show];
}

-(void)showWithTitle:(NSString *)title message:(NSString *)message yesButtonTitle:(NSString *)yesTitle noButtonTitle:(NSString *)noTitle{
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview:self.view];
    self.labelTitle.text = title;
    self.labelMessage.text = message;
    [self.btnYes setTitle:yesTitle forState:UIControlStateNormal];
    [self.btnNo setTitle:noTitle forState:UIControlStateNormal];
    [self.btnOk setHidden:true];
    [self.yesbtnView setHidden:false];
    [self show];
}

- (IBAction)okButtonAction:(id)sender {
    [self hideAertView:true];
}

- (IBAction)crossButtonAction:(id)sender {
    [self hideAertView:false];
    
}
- (IBAction)btnCancelAction:(id)sender {
    [self hideAertView:false];
}
- (IBAction)btnYesAction:(id)sender {
    [self hideAertView:true];
}
- (IBAction)btnNoAction:(id)sender {
    [self hideAertView:false];
}

#pragma mark - Show/Hide

- (void)show {
    
    self.popupView.alpha = 0.f;
    self.popupView.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
    
    [UIView animateWithDuration:0.2f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.popupView.alpha = 1.f;
        self.popupView.transform = CGAffineTransformMakeScale(1.05f, 1.05f);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.08f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.popupView.transform = CGAffineTransformIdentity;
        } completion:nil];
    }];
}

-(void)hidePopup{
    _delegate = nil;
    [self hideAertView:false];
}

- (void)hideAertView:(BOOL)isOk {
    
    [UIView animateWithDuration:0.2f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.popupView.alpha = 0.f;
        self.popupView.transform = CGAffineTransformMakeScale(0.05f, 0.05f);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.08f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.popupView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self.view removeFromSuperview];
                
                if(isOk && [_delegate respondsToSelector:@selector(alertOkButtonClicked)])
                {
                    [_delegate alertOkButtonClicked];
                }if(!isOk && [_delegate respondsToSelector:@selector(alertCancelButtonClicked)])
                {
                    [_delegate alertCancelButtonClicked];
                }
            }];
        }];
    }];
}
@end

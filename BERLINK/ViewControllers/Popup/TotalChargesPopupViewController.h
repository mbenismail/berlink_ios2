//
//  TotalChargesPopupViewController.h
//  BERLINK
//
//  Created by Pravin Jadhao on 11/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TotalChargesPopupViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *popupView;

@end

NS_ASSUME_NONNULL_END

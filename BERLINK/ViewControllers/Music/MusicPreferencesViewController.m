//
//  MusicPreferencesViewController.m
//  BERLINK
//
//  Created by Pravin Jadhao on 30/01/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import "MusicPreferencesViewController.h"
#import "MusicCollectionViewCell.h"
@interface MusicPreferencesViewController ()

@end

@implementation MusicPreferencesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 //   [self.musicPrefCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"MusicCollectionViewCell"];
}

#pragma mark:- Button Action
- (IBAction)musicSelectedAction:(UIButton *)sender {
    
}

#pragma mark - UICollectionView deleagte and datasuource methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    CGFloat size = ([[UIScreen mainScreen] bounds].size.width)/2 - 100;
    CGSize mElementSize = CGSizeMake( size, 140);
    return mElementSize;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return 20;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"MusicCollectionViewCell";
    MusicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell setName:@"Name" isSelected:false];

    if(indexPath.row %2 == 0)
    {
        [cell setName:@"Name" isSelected:true];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

}



@end


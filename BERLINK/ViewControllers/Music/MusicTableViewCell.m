//
//  MusicTableViewCell.m
//  BERLINK
//
//  Created by Pravin Jadhao on 18/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import "MusicTableViewCell.h"

@implementation MusicTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

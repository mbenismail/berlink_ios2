//
//  SpotifyMusicListViewController.h
//  BERLINK
//
//  Created by Pravin Jadhao on 18/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SpotifyMusicListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *btnPlay;

@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIView *infoView;

@end

NS_ASSUME_NONNULL_END

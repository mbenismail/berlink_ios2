//
//  SpotifyMusicViewController.m
//  BERLINK
//
//  Created by Pravin Jadhao on 18/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import "SpotifyMusicViewController.h"
#import "Gradient.h"

@interface SpotifyMusicViewController ()

@end

@implementation SpotifyMusicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [Gradient setBackgroundGradientTopToBottomForView:self.view color1Red:0 color1Green:0 color1Blue:0 color2Red:45.0 color2Green:45.0 color2Blue:45.0 alpha:1.0];
}
- (IBAction)closeButtonActioj:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}
- (IBAction)btnPlayAction:(id)sender {
    
}
- (IBAction)btnPrevAction:(id)sender {
    
}
- (IBAction)btnNextAction:(id)sender {
    
}

@end

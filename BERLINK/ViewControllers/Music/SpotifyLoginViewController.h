//
//  SpotifyLoginViewController.h
//  BERLINK
//
//  Created by Pravin Jadhao on 18/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SpotifyLoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@end

NS_ASSUME_NONNULL_END

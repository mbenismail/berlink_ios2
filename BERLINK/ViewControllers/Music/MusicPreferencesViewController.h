//
//  MusicPreferencesViewController.h
//  BERLINK
//
//  Created by Pravin Jadhao on 30/01/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MusicPreferencesViewController : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *musicPrefCollectionView;

@end

NS_ASSUME_NONNULL_END

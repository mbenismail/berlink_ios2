//
//  MusicCollectionViewCell.h
//  BERLINK
//
//  Created by Pravin Jadhao on 30/01/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MusicCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *musicImage;
@property (weak, nonatomic) IBOutlet UILabel *preferanceNameLbl;
- (void)setName:(NSString *)name isSelected:(BOOL)isSelected;
@end

NS_ASSUME_NONNULL_END

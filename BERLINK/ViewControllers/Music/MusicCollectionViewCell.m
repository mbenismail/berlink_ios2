//
//  MusicCollectionViewCell.m
//  BERLINK
//
//  Created by Pravin Jadhao on 30/01/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import "MusicCollectionViewCell.h"
#import "Constants.h"
@implementation MusicCollectionViewCell

- (void)setName:(NSString *)name isSelected:(BOOL)isSelected
{
    self.preferanceNameLbl.text = name;
    NSString *imageName = isSelected ? @"musicselected.png" : @"music.png";
    _musicImage.image = [UIImage imageNamed:imageName];
    
    UIColor *color = isSelected ? UIColorFromRGB(221,194,164,1) : [UIColor whiteColor];
    
    [self.preferanceNameLbl setTextColor:color];
}
@end

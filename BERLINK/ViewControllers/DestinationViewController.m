//
//  DestinationViewController.m
//  BERLINK
//
//  Created by BERLINK on 2/10/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "DestinationViewController.h"
#import "CategoriesViewController.h"
#import "Constants.h"
#import "TRGoogleMapsAutocompleteItemsSource.h"
#import "TRGoogleMapsAutocompletionCellFactory.h"
#import "TRGoogleMapsSuggestion.h"
#import "WebServiceApi.h"
#import "SharedVar.h"
#import "TotalPriceViewController.h"
#import "ConciergerieViewController.h"
#import "StringConstants.h"
#import "Util.h"
#import "Gradient.h"

#define MESSAGEVIEW_DIFF 0
@interface DestinationViewController ()

@end

@implementation DestinationViewController

- (void) receiveTestNotification:(NSNotification *) notification
{
    
    if ([[notification name] isEqualToString:@"selectDest"])
    {
        [self updateRequest:nil];
    }
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"selectDest"
                                               object:nil];
    
    NSLog(@"000001");
    [self drawTheMap];
    
    [Gradient setBackgroundGradientForView:messageView color1Red:30.0 color1Green:30.0 color1Blue:30.0 color2Red:0.0 color2Green:0.0 color2Blue:0.0 alpha:1.0];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    messageView.hidden = YES;
    lancerButton.hidden = YES;
    messageview_y = messageView.frame.origin.y;
    draw_user_annotation = 0;
    [userAddressTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [destinationTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    _autocompleteDepartView = [TRAutocompleteView autocompleteViewBindedTo:userAddressTextField
                                                               usingSource:[[TRGoogleMapsAutocompleteItemsSource alloc] initWithMinimumCharactersToTrigger:2 apiKey:GOOGLE_PLACE_WS_API_KEY]
                                                               cellFactory:[[TRGoogleMapsAutocompletionCellFactory alloc] initWithCellForegroundColor:[UIColor lightGrayColor] fontSize:14]
                                                              presentingIn:self];
    __weak typeof(self) weakSelf = self;
    _autocompleteDepartView.didAutocompleteWith = ^(id<TRSuggestionItem> item)
    {
        NSLog(@"Autocompleted with: %@", item.completionText);
        addressstart = userAddressTextField.text;
        [weakSelf drawRoute];
    };
    
    _autocompleteDestinationView = [TRAutocompleteView autocompleteViewBindedTo:destinationTextField
                                                                    usingSource:[[TRGoogleMapsAutocompleteItemsSource alloc] initWithMinimumCharactersToTrigger:2 apiKey:GOOGLE_PLACE_WS_API_KEY]
                                                                    cellFactory:[[TRGoogleMapsAutocompletionCellFactory alloc] initWithCellForegroundColor:[UIColor lightGrayColor] fontSize:14]
                                                                   presentingIn:self];
    _autocompleteDestinationView.didAutocompleteWith = ^(id<TRSuggestionItem> item)
    {
        NSLog(@"Autocompleted with: %@", item.completionText);
        adresseend = destinationTextField.text;
        [weakSelf drawRoute];
    };
    
    
//    if ([self.strCategory  isEqual: @"business"])
//    {
//        [self.img setImage:[UIImage imageNamed: @"business"]];
//    }else
//        
//        if ([self.strCategory  isEqual: @"premium"])
//        {
//            [self.img setImage:[UIImage imageNamed: @"premium"]];
//            
//        }else
//            
//            if ([self.strCategory  isEqual:  @"prestige"])
//            {
//                [self.img setImage:[UIImage imageNamed: @"prestige"]];
//                
//            }
    
}


#pragma mark - Google Place autocomplete

-(void)drawTheMap{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
                                                            longitude:151.20
                                                                 zoom:14];
    [umapView setCamera:camera];
    //umapView.myLocationEnabled = YES;
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"mapstyle" withExtension:@"json"];
    NSError *error;
    
    // Set the map style by passing the URL for style.json.
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    
    umapView.mapStyle = style;
}

-(void)drawUserLocation{
    GMSMarker *marker = [[GMSMarker alloc] init];
    CLLocationCoordinate2D location;
    location.latitude = currentLocation.coordinate.latitude;
    location.longitude = currentLocation.coordinate.longitude;
    
    marker.position = location;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    UIImage *markerIcon = [UIImage imageNamed:@"icon_source.png"];
    markerIcon = [markerIcon imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 0, (markerIcon.size.height/2), 0)];
    marker.icon = markerIcon;
    marker.map = umapView;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latUser.doubleValue
                                                            longitude:lngUser.doubleValue
                                                                 zoom:14];
    [umapView setCamera:camera];
}

- (void)drawRoute
{
    NSLog(@"userAddressTextField.text %@", userAddressTextField.text);
    NSLog(@"destinationTextField %@", destinationTextField.text);
    
    [umapView clear];
    [self drawUserLocation];
    
    [self fetchPolylineWithOrigin:userAddressTextField.text destination:destinationTextField.text completionHandler:^(GMSPolyline *polyline)
     {
         if(polyline)
             polyline.map = umapView;
     }];
    
    lancerButton.hidden = NO;
}


-(void)selectedDest : (NSString*)str
{
    strDest = str;
    [self updateRequest:nil];
}

-(IBAction)updateRequest:(id)sender{
    
    [[Util shareManager] showWaitingViewController:self];
    
    [self getAddressFromAdreesse:destinationTextField.text completionHL:^(NSString *city) {
        [[Util shareManager] dismissWating];
        
        if(city)
        {
            _currentRequest.city = city;
            _currentRequest.adresseend = adresseend;
            _currentRequest.addressstart = addressstart;
            
            _currentRequest.lngstart = lngstart;
            _currentRequest.latend = latend;
            _currentRequest.latstart = latstart;
            _currentRequest.lngend = lngend;
            
            _currentRequest.distanceEstimate = [NSNumber numberWithInt:calculated_distance];
            _currentRequest.durationEstimate = calculated_duration;
        
            estimated_price = _currentCategory.pricepack.intValue;
            int diff;
            if(calculated_distance <= prise_en_charge_distance)
            {
                alertImage.hidden = YES;
                alertLabel.hidden = YES;
                messageViewBackViewLarge.hidden = YES;
                messageViewBackViewSmall.hidden = NO;
                diff = messageViewBackViewSmall.frame.origin.y;
            }
            else{
                alertImage.hidden = NO;
                alertLabel.hidden = NO;
                estimated_price = _currentCategory.pricepack.intValue + _currentCategory.pricesup.intValue*(calculated_distance - prise_en_charge_distance);
                messageViewBackViewLarge.hidden = NO;
                messageViewBackViewSmall.hidden = YES;
                diff = 0;
            }

            messageView.frame = CGRectMake(messageView.frame.origin.x, (messageview_y - diff), messageView.frame.size.width, messageView.frame.size.height);
            estimatedPriceLabel.text = [NSString stringWithFormat:@"%d €", estimated_price];
            _currentRequest.estimatePrice = [NSNumber numberWithInt:estimated_price];
        //    estimatedDurationLabel.text = [NSString stringWithFormat:@"Arrivée chauffeur estimée :  %@", @"60 min"];
          
            if (calculated_duration == (id)[NSNull null] || calculated_duration.length == 0 )            {
                estimatedDurationLabel.text = @"60 min";
            }else
            {
            estimatedDurationLabel.text = [NSString stringWithFormat:@"%@", calculated_duration];
            }
            
            if ([_currentCategory.brands objectAtIndex:0] > 0)
            {
                Brand * brand = [_currentCategory.brands objectAtIndex:0];
                self.lblCarName.text = brand.title;
            }
            
            messageView.hidden = NO;
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [lancerButton setHidden:YES];
            }];
        }
        else
        {
            
            [[Util shareManager] showError:UNABLE_TO_GET_CITY_MESSAGE withTitle:APP_NAME andViewController:self];
            
        }
        
    }];
}

-(IBAction)Accepter:(id)sender{
    
    if(_currentRequest && _currentRequest.bid && _currentRequest.bid.idBid)
    {
        TotalPriceViewController* eViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"TotalPriceViewController"];
        eViewC.currentRequest = _currentRequest;
        eViewC.estmatedPrice = estimated_price;
        eViewC.strTime = [calculated_duration length] != 0 ?  calculated_duration: @"60 min";
        [self.navigationController pushViewController:eViewC animated:YES];
    }
    else
    {
        ConciergerieViewController* eViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"ConciergerieViewController"];
        eViewC.currentRequest = _currentRequest;
        eViewC.strTime = [calculated_duration length] != 0 ?  calculated_duration: @"60 min";
        [self.navigationController pushViewController:eViewC animated:YES];
    }
    
}

- (void) getAddressFromAdreesse:(NSString *)strAdr completionHL:(void (^) (NSString* city))handler
{
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder geocodeAddressString:strAdr completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        if (error){
            NSLog(@"Geocode failed with error: %@", error);
            handler(nil);
        }
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
        NSLog(@"locality %@",placemark.locality);
        NSLog(@"postalCode %@",placemark.postalCode);
        handler(placemark.locality);
    }];
    
}


-(IBAction)Refuser:(id)sender{
    messageView.hidden = YES;
}

- (void)fetchPolylineWithOrigin:(NSString* )originStr destination:(NSString* )destinationStr completionHandler:(void (^)(GMSPolyline *))completionHandler
{
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"origin=%@&destination=%@&mode=driving&key=%@", originStr, destinationStr, GOOGLE_MAP_DIRECTION_API_KEY];
    NSString *escapedString = [directionsUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSURL *directionsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", directionsAPI, escapedString]];
    
    //NSLog(@"route url:%@", directionsUrl.absoluteString);
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                                 ^(NSData *data, NSURLResponse *response, NSError *error)
                                                 {
                                                     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                     if(error)
                                                     {
                                                         if(completionHandler)
                                                             completionHandler(nil);
                                                         return;
                                                     }
                                                     
                                                     NSArray *routesArray;
                                                     
                                                     if(json)
                                                     {
                                                         routesArray = [json objectForKey:@"routes"];
                                                     }
                                                     dispatch_sync(dispatch_get_main_queue(), ^{
                                                         
                                                         GMSPolyline *polyline = nil;
                                                         if (routesArray && [routesArray count] > 0)
                                                         {
                                                             NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                             NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                                             NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                                             GMSPath *path = [GMSPath pathFromEncodedPath:points];
                                                             polyline = [GMSPolyline polylineWithPath:path];
                                                             polyline.strokeColor = UIColorFromRGB(215,192, 170,1);

                                                             NSArray *legs = [routeDict objectForKey:@"legs"];
                                                             if ([legs count] > 0)
                                                             {
                                                                 NSDictionary *legDict = [legs objectAtIndex:0];
                                                                 NSDictionary *start_location = [legDict objectForKey:@"start_location"];
                                                                 NSString *s_lat = [start_location objectForKey:@"lat"];
                                                                 NSString *s_lng = [start_location objectForKey:@"lng"];
                                                                 GMSMarker *s_marker = [[GMSMarker alloc] init];
                                                                 CLLocationCoordinate2D s_location;
                                                                 s_location.latitude = [s_lat doubleValue];
                                                                 s_location.longitude = [s_lng doubleValue];
                                                                 
                                                                 s_marker.position = s_location;
                                                                 s_marker.appearAnimation = kGMSMarkerAnimationPop;
                                                                 //marker.icon = [UIImage imageNamed:@"n_radar_icon.png"];
                                                                 //s_marker.map = umapView;
                                                                 
                                                                 NSDictionary *end_location = [legDict objectForKey:@"end_location"];
                                                                 NSString *end_lat = [end_location objectForKey:@"lat"];
                                                                 NSString *end_lng = [end_location objectForKey:@"lng"];
                                                                 GMSMarker *marker = [[GMSMarker alloc] init];
                                                                 CLLocationCoordinate2D location;
                                                                 location.latitude = [end_lat doubleValue];
                                                                 location.longitude = [end_lng doubleValue];
                                                                 
                                                                 marker.position = location;
                                                                 marker.appearAnimation = kGMSMarkerAnimationPop;
                                                                 marker.map = umapView;
                                                                 GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:end_lat.doubleValue
                                                                                                                         longitude:end_lng.doubleValue
                                                                                                                              zoom:14];
                                                                 [umapView setCamera:camera];
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 adresseend = [legDict objectForKey:@"end_address"];
                                                                 addressstart = [legDict objectForKey:@"start_address"];
                                                                 
                                                                 lngstart = s_lng;
                                                                 latend = end_lat;
                                                                 latstart = s_lat;
                                                                 lngend = end_lng;
                                                                 
                                                                 NSDictionary *duration = [legDict objectForKey:@"duration"];
                                                                 calculated_duration = [duration objectForKey:@"text"];
                                                                 
                                                                 NSDictionary *dist_dict = [legDict objectForKey:@"distance"];
                                                                 NSNumber *distance_t = [dist_dict objectForKey:@"value"];
                                                                 
                                                                 NSLog(@"distance_t %@", distance_t);
                                                                 calculated_distance = [distance_t intValue]/1000;
                                                                 NSLog(@"calculated_distance %d", calculated_distance);
                                                                 
                                                                 prise_en_charge_distance = _currentCategory.unitinclu.intValue;
                                                                 
                                                             }
                                                             
                                                         }
                                                         
                                                         // run completionHandler on main thread
                                                         
                                                         if(completionHandler)
                                                             completionHandler(polyline);
                                                     });
                                                 }];
    [fetchDirectionsTask resume];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    
    if (currentLocation != nil && draw_user_annotation < 1) {
        lngUser = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latUser = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
        [self drawUserLocation];
        
        draw_user_annotation++;
        
        //calcul estimation
        [self getAddressFromLocation:currentLocation complationBlock:^(NSString *address){
            dispatch_async(dispatch_get_main_queue(), ^{
                userAddressTextField.text = address;
                addressstart = userAddressTextField.text;
            });
        }];
        
    }
    
    
}

typedef void(^addressCompletion)(NSString *);

-(void)getAddressFromLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock
{
    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             address = [NSString stringWithFormat:@"%@, %@ %@", placemark.name, placemark.postalCode, placemark.locality];
             completionBlock(address);
         }
     }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = true;
    [self setupNavigationBar];
}

-(void)setupNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    UIImage *backBtnImage = [UIImage imageNamed:@"n_back_btn.png"]  ;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = [view bounds];
    UIImageView *iView = [[UIImageView alloc] initWithImage:backBtnImage];
    iView.frame =  CGRectMake(0, 5, 20, 20);
    [iView setContentMode:UIViewContentModeScaleAspectFit];
    [view addSubview:iView];
    [view addSubview:backBtn];
    [backBtn addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = backButton;
}
- (IBAction)backButtonAction:(id)sender {
    [self backButtonAction];
}

-(void)backButtonAction
{
    [self.navigationController popViewControllerAnimated:true];
}

-(void)textFieldDidChange :(UITextField *)theTextField{
    messageView.hidden = YES;
    lancerButton.hidden = YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(IBAction)returnToCategoriesViewController:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end


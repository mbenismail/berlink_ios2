//
//  TotalPriceViewController.m
//  BERLINK
//
//  Created by BERLINK on 3/3/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "TotalPriceViewController.h"
#import "ConciergerieViewController.h"
#import "SharedVar.h"
#import "AppDelegate.h"
#import "Gradient.h"

@interface TotalPriceViewController ()

@end

@implementation TotalPriceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    departText.text = _currentRequest.addressstart;
    destinationText.text = _currentRequest.adresseend;
    departText.userInteractionEnabled = NO;
    destinationText.userInteractionEnabled = NO;

    priseEnChargePrice.text = [NSString stringWithFormat:@"%d €", _estmatedPrice];
    bidPrice.text = [NSString stringWithFormat:@"%d €", _currentRequest.pricebid.intValue];
    totalPrice.text = [NSString stringWithFormat:@"%d €", (_estmatedPrice + _currentRequest.pricebid.intValue)];
    _currentRequest.estimatePrice = [NSNumber numberWithInt:(_estmatedPrice + _currentRequest.pricebid.intValue)];
    
    AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    if(appd.currentCourse.nameDriver && appd.currentCourse.nameDriver.length > 0)
        self.lblDriverName.text = appd.currentCourse.nameDriver;
    else
        self.lblDriverName.text = @"Chauffeur";
    self.lblTime.text = self.strTime;
    
     [Gradient setBackgroundGradientForView:self.driverInfoview color1Red:30.0 color1Green:30.0 color1Blue:30.0 color2Red:0.0 color2Green:0.0 color2Blue:0.0 alpha:1.0];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBar.hidden = NO;
    [self setupNavigationBar];
}

-(void)setupNavigationBar
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    UIImage *backBtnImage = [UIImage imageNamed:@"n_back_btn.png"]  ;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = [view bounds];
    UIImageView *iView = [[UIImageView alloc] initWithImage:backBtnImage];
    iView.frame =  CGRectMake(0, 5, 20, 20);
    [iView setContentMode:UIViewContentModeScaleAspectFit];
    [view addSubview:iView];
    [view addSubview:backBtn];
    [backBtn addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.leftBarButtonItem = backButton;
}

-(void)backButtonAction
{
    [self.navigationController popViewControllerAnimated:true];
}


-(IBAction)Valider:(id)sender{
    ConciergerieViewController* eViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"ConciergerieViewController"];
    eViewC.strTime = self.strTime;
    eViewC.currentRequest = _currentRequest;
    [self.navigationController pushViewController:eViewC animated:YES];
}

-(IBAction)Annuler:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)returnToCategoriesViewController:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

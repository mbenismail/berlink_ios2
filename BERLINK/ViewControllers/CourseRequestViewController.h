//
//  CourseRequestViewController.h
//  BERLINK
//
//  Created by BERLINK on 3/21/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CourseRequestViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView* myTableView;
    IBOutlet UILabel* noRequestLabel;
}
@property (nonatomic, strong) NSMutableArray* list_request_waiting;
@end

//
//  HistoriqueTableViewCell.m
//  BERLINK
//
//  Created by TRIMECH on 07/03/2018.
//  Copyright © 2018 berlink. All rights reserved.
//

#import "HistoriqueTableViewCell.h"
#import "Gradient.h"

@implementation HistoriqueTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setBorderToView:self.baseView];
    [self setBorderToView:self.driverInfoView];
     [Gradient setBackgroundGradientForView:self.driverInfoView color1Red:30.0 color1Green:30.0 color1Blue:30.0 color2Red:0.0 color2Green:0.0 color2Blue:0.0 alpha:1.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setRating:(int)rating
{
    NSMutableArray *ratingImages = [[NSMutableArray alloc] initWithObjects:_starImage1,_starImage2,_starImage3,_starImage4,_starImage5,nil];
    
    for(int i = 0;i<5;i++)
    {
        UIImageView *imageView = ratingImages[i];
        if(i < rating)
        {
            [imageView setHighlighted: false];
        }else{
            [imageView setHighlighted: true];
        }
    }
}

- (void)setBorderToView:(UIView *)view
{
    view.frame = CGRectInset(view.frame, -1, -1);
    view.layer.borderColor = [UIColor grayColor].CGColor;
    view.layer.borderWidth = 1;
}
@end

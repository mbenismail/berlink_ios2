//
//  RequestTableViewCell.h
//  BERLINK
//
//  Created by BERLINK on 4/6/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Request.h"

@interface RequestTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *depart;
@property (nonatomic, weak) IBOutlet UILabel *destination;
@property (nonatomic, weak) IBOutlet UIButton *productButton;

@property (nonatomic, strong) UIViewController* viewC;
@property (nonatomic, strong) Request* request;

@end

//
//  CGUTableViewCell.h
//  BERLINK
//
//  Created by TRIMECH on 09/03/2018.
//  Copyright © 2018 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CGUTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgBG;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

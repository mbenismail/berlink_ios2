//
//  Gradient.h
//  BERLINK
//
//  Created by Sachin Urade on 25/11/18.
//  Copyright © 2018 berlink. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CGUViewController.h"

@interface Gradient : NSObject
+ (void)setBackgroundGradient:(UIImageView*)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha;

+ (void)setBackgroundGradientForView:(UIView *)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha;

+ (void)setBackgroundGradientTopToBottomForView:(UIView *)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha;

+ (void)setBackgroundGradientTopTobottomView:(UIView *)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha;

+ (void)setBackgroundGradientTopToBottomForView:(UIView *)mainView;

@end

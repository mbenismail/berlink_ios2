//
//  PointsTableViewCell.h
//  BERLINK
//
//  Created by Pravin Jadhao on 08/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PointsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblPoints;

@end

NS_ASSUME_NONNULL_END

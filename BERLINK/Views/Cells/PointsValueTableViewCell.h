//
//  PointsValueTableViewCell.h
//  BERLINK
//
//  Created by Pravin Jadhao on 11/02/19.
//  Copyright © 2019 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PointsValueTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;

@end

NS_ASSUME_NONNULL_END

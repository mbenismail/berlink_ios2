//
//  CategoryTableViewCell.h
//  BERLINK
//
//  Created by BERLINK on 2/9/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *brandsLabel;
@property (nonatomic, weak) IBOutlet UILabel *categoryLabel;
@property (nonatomic, weak) IBOutlet UILabel *infoLabel;
@property (nonatomic, weak) IBOutlet UIImageView *imageBack;
@property (nonatomic, weak) IBOutlet UIImageView *imageTitle;
@property (nonatomic, weak) IBOutlet UIImageView *imageCategory;
@property (weak, nonatomic) IBOutlet UIImageView *biddingImage;

@end

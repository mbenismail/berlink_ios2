//
//  Gradient.m
//  BERLINK
//
//  Created by Sachin Urade on 25/11/18.
//  Copyright © 2018 berlink. All rights reserved.
//

#import "Gradient.h"
#import <UIKit/UIKit.h>

@implementation Gradient


+ (void)setBackgroundGradient:(UIImageView *)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha
{
    
   //r [mainView setBackgroundColor:[UIColor clearColor]];
    CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = mainView.bounds;
    
    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:colorR1/255.0 green:colorG1/255.0 blue:colorB1/255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:colorR2/255.0 green:colorG2/255.0 blue:colorB2/255.0 alpha:alpha] CGColor], nil];
    
    grad.startPoint =  CGPointMake(0.0, 1.0);
    grad.endPoint =  CGPointMake(1.0, 0.0);
    
    [mainView.layer insertSublayer:grad atIndex:0];
}

+ (void)setBackgroundGradientForView:(UIView *)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha
{
    //r [mainView setBackgroundColor:[UIColor clearColor]];
    CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = mainView.bounds;
    
    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:colorR1/255.0 green:colorG1/255.0 blue:colorB1/255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:colorR2/255.0 green:colorG2/255.0 blue:colorB2/255.0 alpha:alpha] CGColor], nil];
    
    grad.startPoint =  CGPointMake(0.0, 0.0);
    grad.endPoint =  CGPointMake(1.0, 0.0);
    
    [mainView.layer insertSublayer:grad atIndex:0];
}

+ (void)setBackgroundGradientTopTobottomView:(UIView *)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha
{
    //r [mainView setBackgroundColor:[UIColor clearColor]];
    CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = mainView.bounds;
    
    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:colorR1/255.0 green:colorG1/255.0 blue:colorB1/255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:colorR2/255.0 green:colorG2/255.0 blue:colorB2/255.0 alpha:alpha] CGColor], nil];
    
    [grad setFrame:mainView.bounds];
    [mainView.layer insertSublayer:grad atIndex:0];
}


+ (void)setBackgroundGradientTopToBottomForView:(UIView *)mainView color1Red:(float)colorR1 color1Green:(float)colorG1 color1Blue:(float)colorB1 color2Red:(float)colorR2 color2Green:(float)colorG2 color2Blue:(float)colorB2 alpha:(float)alpha
{
    
    //r [mainView setBackgroundColor:[UIColor clearColor]];
    CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = mainView.bounds;
    
    grad.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:colorR1/255.0 green:colorG1/255.0 blue:colorB1/255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:colorR2/255.0 green:colorG2/255.0 blue:colorB2/255.0 alpha:alpha] CGColor], nil];

    grad.startPoint =  CGPointMake(0.5, 1.0);
    grad.endPoint =  CGPointMake(0.5, 0.0);
   
    [mainView.layer insertSublayer:grad atIndex:0];
}


+ (void)setBackgroundGradientTopToBottomForView:(UIView *)mainView
{
    
    //r [mainView setBackgroundColor:[UIColor clearColor]];
    CAGradientLayer *grad = [CAGradientLayer layer];
    grad.frame = mainView.bounds;
    
    UIColor *blackColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0];
    UIColor *whiteColor = [UIColor colorWithRed:30/255.0 green:30/255.0 blue:30/255.0 alpha:1.0];
    grad.colors = [NSArray arrayWithObjects:(id)[blackColor CGColor], (id)[whiteColor CGColor],(id)[blackColor CGColor], (id)[blackColor CGColor],nil];
    
    grad.startPoint =  CGPointMake(0.5, 1.0);
    grad.endPoint =  CGPointMake(0.5, 0.0);
    
    [mainView.layer insertSublayer:grad atIndex:0];
}
@end

//
//  ConciergerieTableViewCell.m
//  BERLINK
//
//  Created by BERLINK on 3/3/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "ConciergerieTableViewCell.h"
#import "Gradient.h"

@implementation ConciergerieTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self layoutIfNeeded];
    [Gradient setBackgroundGradientForView:self.BaseContentView color1Red:0.0 color1Green:00.0 color1Blue:00.0 color2Red:40.0 color2Green:40.0 color2Blue:40.0 alpha:1.0];
    [self setBorderToView:self.BaseContentView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setBorderToView:(UIView *)view
{
    view.frame = CGRectInset(view.frame, -1, -1);
    view.layer.borderColor = [UIColor grayColor].CGColor;
    view.layer.borderWidth = 1;
}

@end

//
//  ConciergerieTableViewCell.h
//  BERLINK
//
//  Created by BERLINK on 3/3/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConciergerieTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *pointsLabel;
@property (nonatomic, weak) IBOutlet UIImageView *photo;

@property (weak, nonatomic) IBOutlet UIView *BaseContentView;

@end

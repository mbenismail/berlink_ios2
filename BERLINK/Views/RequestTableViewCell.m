//
//  RequestTableViewCell.m
//  BERLINK
//
//  Created by BERLINK on 4/6/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import "RequestTableViewCell.h"
#import "WebServiceApi.h"
#import "SharedVar.h"
#import "Util.h"
#import "AppDelegate.h"
#import "ProductViewController.h"
#import "StringConstants.h"

@implementation RequestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(IBAction)Accepter:(id)sender{
    [[Util shareManager] showWaitingViewController:_viewC];
    [[WebServiceApi shareManager] invokeAccepteCourse_Driver:[[SharedVar shareManager] mainUser] andIdRequest:_request.idrequest andResponse:YES completionHandler:^(NSString *errorMsg, Course *course) {
        [[Util shareManager] dismissWating];
        if(errorMsg.length > 0)
        {
            [[Util shareManager] showError:errorMsg withTitle:APP_NAME andViewController:_viewC];
            
        }
        else{
            
            AppDelegate* appd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
            appd.current_statut = 0;
            appd.currentCourse = course;
            
            [appd startUpdateCourseLocation];
            [appd gotoWaitingView];
            //en course
            [[[SharedVar shareManager] mainUser] setStatut:[NSNumber numberWithInt:1]];
            //[[[SharedVar shareManager] accountDriverLeftVC] goToDemande:nil];
        }
    }];
}

-(IBAction)goToProducts:(id)sender {
    if(_request.Products && _request.Products.count > 0)
    {
        ProductViewController* eViewC = [[[SharedVar shareManager] mystoryboard] instantiateViewControllerWithIdentifier:@"ProductViewController"];
        eViewC.request = _request;
        eViewC.productList = nil;
        [_viewC.navigationController pushViewController:eViewC animated:YES];
        
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

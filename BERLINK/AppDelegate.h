//
//  AppDelegate.h
//  BERLINK
//
//  Created by Akram on 1/31/17.
//  Copyright © 2017 berlink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Course.h"
#import "Request.h"

@import GoogleMaps;
@import GooglePlaces;
@import Firebase;
@import FirebaseDatabase;

@interface AppDelegate : UIResponder <CLLocationManagerDelegate>
{
    CLLocation *currentLocation;
    CLLocationManager *locationManager;
    FIRDatabaseReference* db;
    
}
@property int current_statut;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *strDeviceToken;
@property (strong, nonatomic) Course *currentCourse;

-(void)gotoCourseView;
-(void)gotoFinCourseView;
-(void)enterApp;
-(void)stopUpdateCourseLocation;
-(void)gotoWaitingView;
-(void)startUpdateCourseLocation;
-(void)gotoCourseRequestView:(NSMutableArray*) list_request_waiting;
-(void)gotoRequestWaitingView:(Request*) request;
@end

